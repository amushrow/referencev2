#--- ### Header
bl_info = {
    "name": "Custom Property: Color",
    "category": "Object",
}

import bpy
import bpy.props

from skenginev2 import ui, properties

def register():
    properties.register()
    bpy.utils.register_module(__name__)


def unregister():
    properties.unregister()
    bpy.utils.unregister_module(__name__)

if __name__ == "__main__":
    register()

#yawn
//#include "Tests.h"
//#include <SKInput/DeviceManager.h>
//#include <SKNetwork/NATNegotator.h>
//#include <SKNetwork/Peer.h>
//#include <SKNetwork/Lobby.h>
//#include <SKNetwork/LobbyServer.h>
//#include <SKSystem/Threading/Thread.h>
//
//namespace
//{
//	SK::Network::EndPoint ServerAddress("82.40.19.151", 50001);
//
//	void ConnectionRequest(void* sender, SK::Network::ConnectionRequestArgs& args)
//	{
//		args.Accept();
//	}
//}
//
//
//void Tests::Network::Server()
//{
//	SK::Input::DeviceManager& DevMan = SK::Input::DeviceManager::Instance();
//	DevMan.FindDevices();
//	DevMan.BackgroundInput(true);
//
//	SK::Network::Peer Server(50001);
//	auto LobbyInterface = Server.AddProtocol<SK::Network::LobbyServer>();
//	auto NATNegInterface = Server.AddProtocol<SK::Network::NATNegotiator>();
//
//	SK::EventKey Key = SK::GetEventKey();
//	Server.ConnectionRequest.Register(Key, ConnectionRequest);
//	LobbyInterface->Host(true);
//
//	while (DevMan[SK::Input::SpecialDevice::SystemKeyboard].GetValue(SK::Input::Key::Escape) == 0.0f)
//	{
//		DevMan.Update();
//		Server.Update();
//		SK::Threading::Sleep(SK::Time::Milliseconds(16));
//	}
//
//	Server.ConnectionRequest.Deregister(Key);
//}
//
//void Tests::Network::Client()
//{
//	SK::Input::DeviceManager& DevMan = SK::Input::DeviceManager::Instance();
//	DevMan.FindDevices();
//	DevMan.BackgroundInput(true);
//
//	SK::Network::Peer Peer(50005);
//	auto LobbyInterface = Peer.AddProtocol<SK::Network::LobbyServer>();
//	auto NATNegInterface = Peer.AddProtocol<SK::Network::NATNegotiator>();
//
//	using std::placeholders::_1;
//	using std::placeholders::_2;
//
//	SK::EventKey Key = SK::GetEventKey();
//	Peer.ConnectionRequest.Register(Key, ConnectionRequest);
//
//	LobbyInterface->ConnctedToRemote.Register(Key, [](void* sender, SK::EmptyEventData& args)
//	{
//		// Connected to server, make a lobby
//		auto LobbyInterface = reinterpret_cast<SK::Network::LobbyServer*>(sender);
//
//		SK::Network::Lobby MyLobby("My Lobby", 8);
//		MyLobby.AddFilter("Cake", "Yes");
//		MyLobby.AddFilter("Mode", "Normal");
//
//		LobbyInterface->CreateLobby(MyLobby);
//	});
//
//	LobbyInterface->LobbyCreated.Register(Key, [](void* sender, SK::Network::LobbyCreatedArgs& args)
//	{
//		// My Lobby has been created on the remote server
//		// Let's join it
//		auto LobbyInterface = reinterpret_cast<SK::Network::LobbyServer*>(sender);
//		LobbyInterface->JoinLobby(args.Lobby().LobbyId());
//	});
//
//	LobbyInterface->ConnectToRemoteServer(ServerAddress);
//
//	while (DevMan[SK::Input::SpecialDevice::SystemKeyboard].GetValue(SK::Input::Key::Escape) == 0.0f)
//	{
//		Peer.Update();
//		DevMan.Update();
//		SK::Threading::Sleep(SK::Time::Milliseconds(16));
//	}
//
//	Peer.ConnectionRequest.Deregister(Key);
//	LobbyInterface->ConnctedToRemote.Deregister(Key);
//	LobbyInterface->LobbyCreated.Deregister(Key);
//}
//
//void Tests::Network::ClientTwo()
//{
//	SK::Input::DeviceManager& DevMan = SK::Input::DeviceManager::Instance();
//	DevMan.FindDevices();
//	DevMan.BackgroundInput(true);
//
//	SK::Network::Peer Peer(50006);
//	auto LobbyInterface = Peer.AddProtocol<SK::Network::LobbyServer>();
//	auto NATNegInterface = Peer.AddProtocol<SK::Network::NATNegotiator>();
//
//	using std::placeholders::_1;
//	using std::placeholders::_2;
//
//	SK::EventKey Key = SK::GetEventKey();
//	Peer.ConnectionRequest.Register(Key, ConnectionRequest);
//
//	LobbyInterface->ConnctedToRemote.Register(Key, [](void* sender, SK::EmptyEventData& args)
//	{
//		auto LobbyInterface = reinterpret_cast<SK::Network::LobbyServer*>(sender);
//		SK::Network::LobbyFilters Filters;
//		Filters["Mode"] = "Normal";
//		LobbyInterface->FindLobbies(Filters);
//	});
//
//	LobbyInterface->LobbyFound.Register(Key, [&Peer, Key](void* sender, SK::Network::LobbyFoundArgs& args)
//	{
//		auto LobbyInterface = reinterpret_cast<SK::Network::LobbyServer*>(sender);
//		LobbyInterface->JoinLobby(args.Lobbies()[0].LobbyId());
//	});
//
//	LobbyInterface->JoinLobbySuccess.Register(Key, [](void* sender, SK::Network::LobbyArgs& args)
//	{
//		auto LobbyInterface = reinterpret_cast<SK::Network::LobbyServer*>(sender);
//		LobbyInterface->RequestLobbyPeers(args.LobbyId());
//	});
//
//	LobbyInterface->ReceivedPeers.Register(Key, [NATNegInterface](void* sender, SK::Network::ReceivedPeersArgs& args)
//	{
//		for (auto& Peer : args.Peers())
//		{
//			if (!Peer.LocalPlayer)
//			{
//				NATNegInterface->Connect(ServerAddress, Peer.EndPoint);
//			}
//		}
//	});
//
//	LobbyInterface->ConnectToRemoteServer(ServerAddress);
//
//	while (DevMan[SK::Input::SpecialDevice::SystemKeyboard].GetValue(SK::Input::Key::Escape) == 0.0f)
//	{
//		DevMan.Update();
//		Peer.Update();
//		SK::Threading::Sleep(SK::Time::Milliseconds(16));
//	}
//
//	Peer.ConnectionRequest.Deregister(Key);
//	LobbyInterface->ConnctedToRemote.Deregister(Key);
//	LobbyInterface->LobbyFound.Deregister(Key);
//	LobbyInterface->JoinLobbySuccess.Deregister(Key);
//	LobbyInterface->ReceivedPeers.Deregister(Key);
//}
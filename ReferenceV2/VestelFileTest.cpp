#include "Tests.h"
#include <SKSystem/IO/FileStream.h>
#include <SKSystem/Debug.h>
#include <algorithm>

using namespace SK;
using namespace SK::IO;

namespace
{
	const char* FilePath = "J:\\profile\\mb100_hwprofile.bin";

	struct NameKey
	{
		uint32_t Key;
		SK::String Name;

		bool operator==(const NameKey& other) const
		{
			return Key == other.Key;
		}
	};

	NameKey GetStringKey(const SK::String& str)
	{
		uint32_t num1 = 4057183639;
		uint32_t num2 = 0;
		uint32_t num3 = 0;
		uint32_t num4 = 0;
		uint32_t length = str.Length();

		const uint8_t* strBuffer = str.Buffer();
		uint32_t num5 = num1 ^ length << 6;
		for (uint32_t i = 0; i < length; ++i)
		{
			uint32_t num6 = strBuffer[i];
			uint32_t num7 = (num5 + (~num2 << 1) ^ num3 << 9) + (num6 << 17) ^ num4;
			uint32_t num8 = num7 + (num7 << (num6 % 3));
			num5 = num8 + (num8 >> (num2 % 5));
			num2 = num6;
			num3 += num6;
			num4 = (num4 << 8) + num6;
		}

		return { num5, str };
	}

	struct ProfileEntry
	{
		uint32_t Key;
		uint32_t Unknown;
		uint32_t Count;
		uint32_t Value;
		uint32_t UniqueId;

		uint64_t FileOffset;

		template<SerializeMode Mode>
		bool Serialize(Serializer<Mode>& serializer)
		{
			serializer(Key);
			serializer(Unknown);
			serializer(Count);
			serializer(Value);
			serializer(UniqueId);

			return true;
		}
	};

	
	template<class T>
	void AddUnique(std::vector<T>& list, const T& ass)
	{
		for (auto shit : list)
		{
			if (shit == ass)
				return;
		}

		list.push_back(ass);
	}

	std::vector<NameKey> GetKnownKeys()
	{
		std::vector<NameKey> Result;
		AddUnique(Result, GetStringKey("ApsSorting"));
		AddUnique(Result, GetStringKey("DashboardMenuDemoMode"));
		AddUnique(Result, GetStringKey("DigitalIB"));
		AddUnique(Result, GetStringKey("DigitalIBQMenuControl"));
		AddUnique(Result, GetStringKey("DisabledSkypeForMidConfig"));
		AddUnique(Result, GetStringKey("DolbyDigitalPlusSupport"));
		AddUnique(Result, GetStringKey("EnableIndiaChanges"));
		AddUnique(Result, GetStringKey("EnableTkgstTest"));
		AddUnique(Result, GetStringKey("ExitTimeout"));
		AddUnique(Result, GetStringKey("ISBSupport"));
		AddUnique(Result, GetStringKey("MediasetEnabled"));
		AddUnique(Result, GetStringKey("MenuControl"));
		AddUnique(Result, GetStringKey("MenuEnabled"));
		AddUnique(Result, GetStringKey("Mybutton1SharpEnabled"));
		AddUnique(Result, GetStringKey("SkypeEnabled"));
		AddUnique(Result, GetStringKey("SourceNaming"));
		AddUnique(Result, GetStringKey("TkgsTestMode"));
		AddUnique(Result, GetStringKey("Toshiba"));
		AddUnique(Result, GetStringKey("TouchpadMenuEnabled"));
		AddUnique(Result, GetStringKey("TouchpadMenuType"));
		AddUnique(Result, GetStringKey("ZoomModes"));
		AddUnique(Result, GetStringKey("HeadSetSupport"));
		AddUnique(Result, GetStringKey("HIDSupport"));
		AddUnique(Result, GetStringKey("ShutterGlassesSupport"));
		AddUnique(Result, GetStringKey("UseLanMAC"));
		AddUnique(Result, GetStringKey("AudioPlayEnabled"));
		AddUnique(Result, GetStringKey("DivxEnabled"));
		AddUnique(Result, GetStringKey("DivxPlusHDEnabled"));
		AddUnique(Result, GetStringKey("DRMRegistration"));
		AddUnique(Result, GetStringKey("FlashSWFSupport"));
		AddUnique(Result, GetStringKey("JpegSlideShowFadeInOut"));
		AddUnique(Result, GetStringKey("MediaBrowserEnabled"));
		AddUnique(Result, GetStringKey("Native4KPictureSupported"));
		AddUnique(Result, GetStringKey("UsbAutoPlay"));
		AddUnique(Result, GetStringKey("UsbAutoplayActive"));
		AddUnique(Result, GetStringKey("VideoPlayEnabled"));
		AddUnique(Result, GetStringKey("WmaEnabled"));
		AddUnique(Result, GetStringKey("WmvEnabled"));
		AddUnique(Result, GetStringKey("XVidEnabled"));
		AddUnique(Result, GetStringKey("AACSupport"));
		AddUnique(Result, GetStringKey("DBXAudioHidden"));
		AddUnique(Result, GetStringKey("DBXAudioImprovement"));
		AddUnique(Result, GetStringKey("DolbyMS10Support"));
		AddUnique(Result, GetStringKey("DTSSupport"));
		AddUnique(Result, GetStringKey("DynamicBassVisible"));
		AddUnique(Result, GetStringKey("MS10Support"));
		AddUnique(Result, GetStringKey("SRSSupport"));
		AddUnique(Result, GetStringKey("SurroundDefaultValue"));
		AddUnique(Result, GetStringKey("SurroundModeControl"));
		AddUnique(Result, GetStringKey("UISoundClickEffect"));
		AddUnique(Result, GetStringKey("AAC"));
		AddUnique(Result, GetStringKey("AC3"));
		AddUnique(Result, GetStringKey("AIF"));
		AddUnique(Result, GetStringKey("AU"));
		AddUnique(Result, GetStringKey("DTS"));
		AddUnique(Result, GetStringKey("FLAC"));
		AddUnique(Result, GetStringKey("GSM"));
		AddUnique(Result, GetStringKey("M4A"));
		AddUnique(Result, GetStringKey("MP3"));
		AddUnique(Result, GetStringKey("OOG"));
		AddUnique(Result, GetStringKey("PCM"));
		AddUnique(Result, GetStringKey("RA"));
		AddUnique(Result, GetStringKey("RAW"));
		AddUnique(Result, GetStringKey("VOX"));
		AddUnique(Result, GetStringKey("WAV"));
		AddUnique(Result, GetStringKey("WMA"));
		AddUnique(Result, GetStringKey("3G2"));
		AddUnique(Result, GetStringKey("3GP"));
		AddUnique(Result, GetStringKey("ASF"));
		AddUnique(Result, GetStringKey("AVI"));
		AddUnique(Result, GetStringKey("DAT"));
		AddUnique(Result, GetStringKey("DIVX"));
		AddUnique(Result, GetStringKey("FLA"));
		AddUnique(Result, GetStringKey("FLV"));
		AddUnique(Result, GetStringKey("M2TS"));
		AddUnique(Result, GetStringKey("M4V"));
		AddUnique(Result, GetStringKey("MKV"));
		AddUnique(Result, GetStringKey("MOV"));
		AddUnique(Result, GetStringKey("MP4"));
		AddUnique(Result, GetStringKey("MPE"));
		AddUnique(Result, GetStringKey("MPEG"));
		AddUnique(Result, GetStringKey("MPG"));
		AddUnique(Result, GetStringKey("RM"));
		AddUnique(Result, GetStringKey("RMVB"));
		AddUnique(Result, GetStringKey("SWF"));
		AddUnique(Result, GetStringKey("TP"));
		AddUnique(Result, GetStringKey("TRP"));
		AddUnique(Result, GetStringKey("TS"));
		AddUnique(Result, GetStringKey("VOB"));
		AddUnique(Result, GetStringKey("WMV"));
		AddUnique(Result, GetStringKey("AccuweatherEnabled"));
		AddUnique(Result, GetStringKey("BBCIplayerEnabled"));
		AddUnique(Result, GetStringKey("DailyMotionEnabled"));
		AddUnique(Result, GetStringKey("FacebookEnabled"));
		AddUnique(Result, GetStringKey("FlickrEnabled"));
		AddUnique(Result, GetStringKey("GoogleEnabled"));
		AddUnique(Result, GetStringKey("NetflixEnabled"));
		AddUnique(Result, GetStringKey("NtvHavaEnabled"));
		AddUnique(Result, GetStringKey("OpenBrowserEnabled"));
		AddUnique(Result, GetStringKey("TivibuEnabled"));
		AddUnique(Result, GetStringKey("TRTEnabled"));
		AddUnique(Result, GetStringKey("TTNetEnabled"));
		AddUnique(Result, GetStringKey("TurkcellTVEnabled"));
		AddUnique(Result, GetStringKey("TwitterEnabled"));
		AddUnique(Result, GetStringKey("YoutubeEnabled"));
		AddUnique(Result, GetStringKey("AirMouseEnabled"));
		AddUnique(Result, GetStringKey("AutozapEnabled"));
		AddUnique(Result, GetStringKey("CECAudioControl"));
		AddUnique(Result, GetStringKey("CECEnabled"));
		AddUnique(Result, GetStringKey("CiPlusEnabled"));
		AddUnique(Result, GetStringKey("DualViewSupport"));
		AddUnique(Result, GetStringKey("AutoTVOFFMode"));
		AddUnique(Result, GetStringKey("DVBT2"));
		AddUnique(Result, GetStringKey("Enable3D"));
		AddUnique(Result, GetStringKey("Enable3dDepthForNoMFC"));
		AddUnique(Result, GetStringKey("FilmMode"));
		AddUnique(Result, GetStringKey("FilmModeDefault"));
		AddUnique(Result, GetStringKey("HDMIAutoSwitch"));
		AddUnique(Result, GetStringKey("HDMINoSignalTimeoutEnabled"));
		AddUnique(Result, GetStringKey("HybridUIEnabled"));
		AddUnique(Result, GetStringKey("MovieSense"));
		AddUnique(Result, GetStringKey("MovieSenseDefault"));
		AddUnique(Result, GetStringKey("NewDynamicBacklight"));
		AddUnique(Result, GetStringKey("NoSignalTimeoutEnabled"));
		AddUnique(Result, GetStringKey("OADEnabled"));
		AddUnique(Result, GetStringKey("SportsMode"));
		AddUnique(Result, GetStringKey("StandbyOADEnabled"));
		AddUnique(Result, GetStringKey("USBCloning"));
		AddUnique(Result, GetStringKey("UseDevHdcpKeys"));
		AddUnique(Result, GetStringKey("VideoWall"));
		AddUnique(Result, GetStringKey("CompleteEPG"));
		AddUnique(Result, GetStringKey("CustomChannelList"));
		AddUnique(Result, GetStringKey("CustomChannelListMode"));
		AddUnique(Result, GetStringKey("DeleteAndMoveUpSupport"));
		AddUnique(Result, GetStringKey("DVBSubtitles"));
		AddUnique(Result, GetStringKey("DynamicChannelUpdateMenuOption"));
		AddUnique(Result, GetStringKey("EPGAvailability"));
		AddUnique(Result, GetStringKey("EpgType"));
		AddUnique(Result, GetStringKey("ExtendedEIT"));
		AddUnique(Result, GetStringKey("ExtendedInfo"));
		AddUnique(Result, GetStringKey("FastChannelSwitch"));
		AddUnique(Result, GetStringKey("FixColorPositionDVBSubtitle"));
		AddUnique(Result, GetStringKey("GracenoteSupport"));
		AddUnique(Result, GetStringKey("HDFilter"));
		AddUnique(Result, GetStringKey("HTMLChannelInfo"));
		AddUnique(Result, GetStringKey("HTMLEPG"));
		AddUnique(Result, GetStringKey("OneColourDVBSubtitle"));
		AddUnique(Result, GetStringKey("SatcoDX"));
		AddUnique(Result, GetStringKey("SDTServiceAddition"));
		AddUnique(Result, GetStringKey("ServiceListCloning"));
		AddUnique(Result, GetStringKey("StbySearch"));
		AddUnique(Result, GetStringKey("TerrestrialCableCombinedSearch"));
		AddUnique(Result, GetStringKey("TextTransparency"));
		AddUnique(Result, GetStringKey("TVBannerTransparency"));
		AddUnique(Result, GetStringKey("AstraHDPlusSupport"));
		AddUnique(Result, GetStringKey("CableSupport"));
		AddUnique(Result, GetStringKey("DigiturkSupport"));
		AddUnique(Result, GetStringKey("DsmartSupport"));
		AddUnique(Result, GetStringKey("FastScanSupport"));
		AddUnique(Result, GetStringKey("OrfSupport"));
		AddUnique(Result, GetStringKey("RobotesterSupport"));
		AddUnique(Result, GetStringKey("SaorviewSupport"));
		AddUnique(Result, GetStringKey("SatelliteSupport"));
		AddUnique(Result, GetStringKey("TurksatSupport"));
		AddUnique(Result, GetStringKey("Dlna"));
		AddUnique(Result, GetStringKey("FollowTv"));
		AddUnique(Result, GetStringKey("HbbTv"));
		AddUnique(Result, GetStringKey("InternalWifiEnabled"));
		AddUnique(Result, GetStringKey("IPlayer"));
		AddUnique(Result, GetStringKey("IPlayerEnabled"));
		AddUnique(Result, GetStringKey("IPTVSupport"));
		AddUnique(Result, GetStringKey("Logging"));
		AddUnique(Result, GetStringKey("Mybutton1YoutubeEnabled"));
		AddUnique(Result, GetStringKey("NetworkSettings"));
		AddUnique(Result, GetStringKey("NonSmartTV"));
		AddUnique(Result, GetStringKey("OfflinePortalSupport"));
		AddUnique(Result, GetStringKey("OpenBrowser"));
		AddUnique(Result, GetStringKey("PlayReadyDRM"));
		AddUnique(Result, GetStringKey("Portal"));
		AddUnique(Result, GetStringKey("PortalMakeUp"));
		AddUnique(Result, GetStringKey("PortalMode"));
		AddUnique(Result, GetStringKey("Skype"));
		AddUnique(Result, GetStringKey("SmoothStreaming"));
		AddUnique(Result, GetStringKey("Telnet"));
		AddUnique(Result, GetStringKey("UsbNet"));
		AddUnique(Result, GetStringKey("VirtualRemote"));
		AddUnique(Result, GetStringKey("Wifi"));
		AddUnique(Result, GetStringKey("WifiDirect"));
		AddUnique(Result, GetStringKey("WifiEnabled"));
		AddUnique(Result, GetStringKey("WirelessDisplaySupport"));
		AddUnique(Result, GetStringKey("WPS"));
		AddUnique(Result, GetStringKey("Dmp"));
		AddUnique(Result, GetStringKey("Dmr"));
		AddUnique(Result, GetStringKey("DmrTrickMode"));
		AddUnique(Result, GetStringKey("DmrTrickModeSetSpeed"));
		AddUnique(Result, GetStringKey("FollowTVDmrPlaybackQMenuCont"));
		AddUnique(Result, GetStringKey("FollowTvDmrPlaybackSupport"));
		AddUnique(Result, GetStringKey("FollowTvEnabled"));
		AddUnique(Result, GetStringKey("TrickModesEnabled"));
		AddUnique(Result, GetStringKey("AccurateRecording"));
		AddUnique(Result, GetStringKey("AESEncryptionMethod"));
		AddUnique(Result, GetStringKey("BackgroundPvr"));
		AddUnique(Result, GetStringKey("CopyNever"));
		AddUnique(Result, GetStringKey("ExternalHarddisk"));
		AddUnique(Result, GetStringKey("Pvr"));
		AddUnique(Result, GetStringKey("PVRFeatures"));
		AddUnique(Result, GetStringKey("PVRPartitionFormatTypeCheck"));
		AddUnique(Result, GetStringKey("PvrReady"));
		AddUnique(Result, GetStringKey("TwinTuner"));
		AddUnique(Result, GetStringKey("NumberOfPhysicalDrives"));
		AddUnique(Result, GetStringKey("AutoTimeshift"));
		AddUnique(Result, GetStringKey("AutoTimeshiftDelaySeconds"));
		AddUnique(Result, GetStringKey("RecordingEncryptionMode"));
		AddUnique(Result, GetStringKey("RecordingEncryptionType"));
		AddUnique(Result, GetStringKey("RetentionLimit"));
		AddUnique(Result, GetStringKey("AutoZoomMode"));
		AddUnique(Result, GetStringKey("CustomBootLogoEnabled"));
		AddUnique(Result, GetStringKey("HDMITrueBlack"));
		AddUnique(Result, GetStringKey("IlluminatedLogo"));
		AddUnique(Result, GetStringKey("OverscanSupport"));
		AddUnique(Result, GetStringKey("Panoramic"));
		AddUnique(Result, GetStringKey("Pixellence"));
		AddUnique(Result, GetStringKey("PixellenceControlInvisible"));
		AddUnique(Result, GetStringKey("PixellenceDefault"));
		AddUnique(Result, GetStringKey("PixellenceDemoModeVisible"));
		AddUnique(Result, GetStringKey("PixellenceWorkingMode"));
		AddUnique(Result, GetStringKey("WinkAnimation"));
		AddUnique(Result, GetStringKey("swprofile"));
		AddUnique(Result, GetStringKey("Config"));
		AddUnique(Result, GetStringKey("ActiveAntenna"));
		AddUnique(Result, GetStringKey("AnalogSupport"));
		AddUnique(Result, GetStringKey("AutoSwitch"));
		AddUnique(Result, GetStringKey("AutoTVOFF"));
		AddUnique(Result, GetStringKey("Active"));
		AddUnique(Result, GetStringKey("SmoothStreamingEnabled"));
		AddUnique(Result, GetStringKey("VirtualMouse"));
		AddUnique(Result, GetStringKey("Vod"));
		AddUnique(Result, GetStringKey("Maxdome"));
		AddUnique(Result, GetStringKey("Nowtilus"));
		AddUnique(Result, GetStringKey("Topfun"));
		AddUnique(Result, GetStringKey("Netflix"));
		AddUnique(Result, GetStringKey("Turkcell"));
		AddUnique(Result, GetStringKey("OnlyWifi"));
		AddUnique(Result, GetStringKey("Connectivity"));
		AddUnique(Result, GetStringKey("Country"));
		AddUnique(Result, GetStringKey("CountrySupport"));
		AddUnique(Result, GetStringKey("Australia"));
		AddUnique(Result, GetStringKey("Denmark"));
		AddUnique(Result, GetStringKey("Finland"));
		AddUnique(Result, GetStringKey("France"));
		AddUnique(Result, GetStringKey("Germany"));
		AddUnique(Result, GetStringKey("Italy"));
		AddUnique(Result, GetStringKey("Netherlands"));
		AddUnique(Result, GetStringKey("Norway"));
		AddUnique(Result, GetStringKey("Poland"));
		AddUnique(Result, GetStringKey("Portugal"));
		AddUnique(Result, GetStringKey("Spain"));
		AddUnique(Result, GetStringKey("Sweden"));
		AddUnique(Result, GetStringKey("UnitedKingdom"));
		AddUnique(Result, GetStringKey("Albania"));
		AddUnique(Result, GetStringKey("Austria"));
		AddUnique(Result, GetStringKey("Belgium"));
		AddUnique(Result, GetStringKey("Bulgaria"));
		AddUnique(Result, GetStringKey("China"));
		AddUnique(Result, GetStringKey("CzechRepublic"));
		AddUnique(Result, GetStringKey("Estonia"));
		AddUnique(Result, GetStringKey("Greece"));
		AddUnique(Result, GetStringKey("Hungary"));
		AddUnique(Result, GetStringKey("Israel"));
		AddUnique(Result, GetStringKey("Latvia"));
		AddUnique(Result, GetStringKey("Lithuania"));
		AddUnique(Result, GetStringKey("Macedonia"));
		AddUnique(Result, GetStringKey("Romania"));
		AddUnique(Result, GetStringKey("Russia"));
		AddUnique(Result, GetStringKey("Serbia"));
		AddUnique(Result, GetStringKey("Slovakia"));
		AddUnique(Result, GetStringKey("Slovenia"));
		AddUnique(Result, GetStringKey("Switzerland"));
		AddUnique(Result, GetStringKey("Turkey"));
		AddUnique(Result, GetStringKey("Croatia"));
		AddUnique(Result, GetStringKey("Iceland"));
		AddUnique(Result, GetStringKey("Luxembourg"));
		AddUnique(Result, GetStringKey("Ireland"));
		AddUnique(Result, GetStringKey("SanMarino"));
		AddUnique(Result, GetStringKey("NewZealand"));
		AddUnique(Result, GetStringKey("BacklightTrickMode"));
		AddUnique(Result, GetStringKey("EnterTimeout"));
		AddUnique(Result, GetStringKey("Continuous"));
		AddUnique(Result, GetStringKey("Enabled"));
		AddUnique(Result, GetStringKey("Threshold"));
		AddUnique(Result, GetStringKey("BlueBackground"));
		AddUnique(Result, GetStringKey("FactoryValue"));
		AddUnique(Result, GetStringKey("BootLogo"));
		AddUnique(Result, GetStringKey("BurnInMode"));
		AddUnique(Result, GetStringKey("CEC"));
		AddUnique(Result, GetStringKey("CECString"));
		AddUnique(Result, GetStringKey("ChannelList"));
		AddUnique(Result, GetStringKey("CommonVolControl"));
		AddUnique(Result, GetStringKey("CommVolControl"));
		AddUnique(Result, GetStringKey("Games"));
		AddUnique(Result, GetStringKey("Antix"));
		AddUnique(Result, GetStringKey("InternetLock"));
		AddUnique(Result, GetStringKey("InternetLockEnabled"));
		AddUnique(Result, GetStringKey("Montenegro"));
		AddUnique(Result, GetStringKey("Ukraine"));
		AddUnique(Result, GetStringKey("SaudiArabia"));
		AddUnique(Result, GetStringKey("Persia"));
		AddUnique(Result, GetStringKey("UnitedArabEmirates"));
		AddUnique(Result, GetStringKey("Kuwait"));
		AddUnique(Result, GetStringKey("Oman"));
		AddUnique(Result, GetStringKey("Bahrain"));
		AddUnique(Result, GetStringKey("Qatar"));
		AddUnique(Result, GetStringKey("Iraq"));
		AddUnique(Result, GetStringKey("Jordan"));
		AddUnique(Result, GetStringKey("Lebanon"));
		AddUnique(Result, GetStringKey("Belarussia"));
		AddUnique(Result, GetStringKey("Colombia"));
		AddUnique(Result, GetStringKey("India"));
		AddUnique(Result, GetStringKey("Other"));
		AddUnique(Result, GetStringKey("Customer"));
		AddUnique(Result, GetStringKey("IplayerCertificate"));
		AddUnique(Result, GetStringKey("Hitachi"));
		AddUnique(Result, GetStringKey("Medion"));
		AddUnique(Result, GetStringKey("None"));
		AddUnique(Result, GetStringKey("Oem"));
		AddUnique(Result, GetStringKey("Name"));
		AddUnique(Result, GetStringKey("AEB"));
		AddUnique(Result, GetStringKey("AGENERALE"));
		AddUnique(Result, GetStringKey("AKAI"));
		AddUnique(Result, GetStringKey("AKIOS"));
		AddUnique(Result, GetStringKey("AKIRA"));
		AddUnique(Result, GetStringKey("ALBA"));
		AddUnique(Result, GetStringKey("ALPARI"));
		AddUnique(Result, GetStringKey("ANDERSSON"));
		AddUnique(Result, GetStringKey("ANSONIC"));
		AddUnique(Result, GetStringKey("ANTARION"));
		AddUnique(Result, GetStringKey("ARENA"));
		AddUnique(Result, GetStringKey("AUTOVOX"));
		AddUnique(Result, GetStringKey("AUXSTAR"));
		AddUnique(Result, GetStringKey("AYA"));
		AddUnique(Result, GetStringKey("BELSON"));
		AddUnique(Result, GetStringKey("BENZO"));
		AddUnique(Result, GetStringKey("BERKLAYS"));
		AddUnique(Result, GetStringKey("BIRA"));
		AddUnique(Result, GetStringKey("BLAUPUNKT"));
		AddUnique(Result, GetStringKey("BLUMATIC"));
		AddUnique(Result, GetStringKey("BORA"));
		AddUnique(Result, GetStringKey("BOSE"));
		AddUnique(Result, GetStringKey("BRANDT"));
		AddUnique(Result, GetStringKey("BSB"));
		AddUnique(Result, GetStringKey("BUSH"));
		AddUnique(Result, GetStringKey("CAMPOMATIC"));
		AddUnique(Result, GetStringKey("CELCUS"));
		AddUnique(Result, GetStringKey("CENTRUM"));
		AddUnique(Result, GetStringKey("CGV"));
		AddUnique(Result, GetStringKey("CLAYTON"));
		AddUnique(Result, GetStringKey("CODAMA"));
		AddUnique(Result, GetStringKey("CONCORD"));
		AddUnique(Result, GetStringKey("CONTINENTAL EDI"));
		AddUnique(Result, GetStringKey("CROWN"));
		AddUnique(Result, GetStringKey("DAENYX"));
		AddUnique(Result, GetStringKey("DAEWOO"));
		AddUnique(Result, GetStringKey("DAIKO"));
		AddUnique(Result, GetStringKey("DANTAX"));
		AddUnique(Result, GetStringKey("DIFFERO"));
		AddUnique(Result, GetStringKey("DIGIHOME"));
		AddUnique(Result, GetStringKey("DIMA"));
		AddUnique(Result, GetStringKey("DIORA"));
		AddUnique(Result, GetStringKey("DMTECH"));
		AddUnique(Result, GetStringKey("DUAL"));
		AddUnique(Result, GetStringKey("DURABASE"));
		AddUnique(Result, GetStringKey("D - VISION"));
		AddUnique(Result, GetStringKey("ELBE"));
		AddUnique(Result, GetStringKey("ELECTRIC CO"));
		AddUnique(Result, GetStringKey("ELECTRION"));
		AddUnique(Result, GetStringKey("ELECTRONIA"));
		AddUnique(Result, GetStringKey("ELEKTA"));
		AddUnique(Result, GetStringKey("ESSENTIELB"));
		AddUnique(Result, GetStringKey("EURONICS"));
		AddUnique(Result, GetStringKey("EXTRA"));
		AddUnique(Result, GetStringKey("F&U"));
		AddUnique(Result, GetStringKey("FAIRTEC"));
		AddUnique(Result, GetStringKey("FAVORIT"));
		AddUnique(Result, GetStringKey("FELSON"));
		AddUnique(Result, GetStringKey("FERGUSON"));
		AddUnique(Result, GetStringKey("FINLUX"));
		AddUnique(Result, GetStringKey("FISHER"));
		AddUnique(Result, GetStringKey("FITCO"));
		AddUnique(Result, GetStringKey("FRESTON"));
		AddUnique(Result, GetStringKey("FTV"));
		AddUnique(Result, GetStringKey("FUJICOM"));
		AddUnique(Result, GetStringKey("FUJITECH"));
		AddUnique(Result, GetStringKey("GALA"));
		AddUnique(Result, GetStringKey("GEEPAS"));
		AddUnique(Result, GetStringKey("GENERAL DELUXE"));
		AddUnique(Result, GetStringKey("GERALUX"));
		AddUnique(Result, GetStringKey("G - HANZS"));
		AddUnique(Result, GetStringKey("GODREJ"));
		AddUnique(Result, GetStringKey("GOGEN"));
		AddUnique(Result, GetStringKey("GOODMANS"));
		AddUnique(Result, GetStringKey("GORENJE"));
		AddUnique(Result, GetStringKey("GRAETZ"));
		AddUnique(Result, GetStringKey("GRANDIN"));
		AddUnique(Result, GetStringKey("GRUNDIG"));
		AddUnique(Result, GetStringKey("GRUNES"));
		AddUnique(Result, GetStringKey("GRUNKEL"));
		AddUnique(Result, GetStringKey("GutSCHNEID"));
		AddUnique(Result, GetStringKey("HANDIC"));
		AddUnique(Result, GetStringKey("HARROW"));
		AddUnique(Result, GetStringKey("HB AVUSTURYA"));
		AddUnique(Result, GetStringKey("HITACHI"));
		AddUnique(Result, GetStringKey("HOHER"));
		AddUnique(Result, GetStringKey("HORIZON"));
		AddUnique(Result, GetStringKey("HOTEL"));
		AddUnique(Result, GetStringKey("HYUNDAI"));
		AddUnique(Result, GetStringKey("INNO HIT"));
		AddUnique(Result, GetStringKey("INTER.VIEW"));
		AddUnique(Result, GetStringKey("ISERN"));
		AddUnique(Result, GetStringKey("ISIS"));
		AddUnique(Result, GetStringKey("ITT"));
		AddUnique(Result, GetStringKey("J.LCD"));
		AddUnique(Result, GetStringKey("J.LED"));
		AddUnique(Result, GetStringKey("JMB"));
		AddUnique(Result, GetStringKey("JOHN LEWIS"));
		AddUnique(Result, GetStringKey("JOST UNTERNEHME"));
		AddUnique(Result, GetStringKey("JVC"));
		AddUnique(Result, GetStringKey("KANAL D"));
		AddUnique(Result, GetStringKey("KARCHER"));
		AddUnique(Result, GetStringKey("KENDO"));
		AddUnique(Result, GetStringKey("KRYSTER"));
		AddUnique(Result, GetStringKey("LAURUS"));
		AddUnique(Result, GetStringKey("LAVIS"));
		AddUnique(Result, GetStringKey("LEIKER"));
		AddUnique(Result, GetStringKey("LENCO"));
		AddUnique(Result, GetStringKey("LEXUS"));
		AddUnique(Result, GetStringKey("LEYLAND"));
		AddUnique(Result, GetStringKey("LINETECH"));
		AddUnique(Result, GetStringKey("LINSAR"));
		AddUnique(Result, GetStringKey("LISTO"));
		AddUnique(Result, GetStringKey("LOEWE"));
		AddUnique(Result, GetStringKey("LOWRY"));
		AddUnique(Result, GetStringKey("LUXOR"));
		AddUnique(Result, GetStringKey("LUXTRONIC"));
		AddUnique(Result, GetStringKey("MAG"));
		AddUnique(Result, GetStringKey("MAGIC"));
		AddUnique(Result, GetStringKey("MANHATTAN"));
		AddUnique(Result, GetStringKey("MAXTOR"));
		AddUnique(Result, GetStringKey("MAXWELL"));
		AddUnique(Result, GetStringKey("MEDION"));
		AddUnique(Result, GetStringKey("MEGA STAR"));
		AddUnique(Result, GetStringKey("MEI"));
		AddUnique(Result, GetStringKey("MELLIEHA BAY HO"));
		AddUnique(Result, GetStringKey("MEREDIAN"));
		AddUnique(Result, GetStringKey("MILGO"));
		AddUnique(Result, GetStringKey("MITSAI"));
		AddUnique(Result, GetStringKey("MOBIBOARD MOBIL"));
		AddUnique(Result, GetStringKey("MOBILE TV"));
		AddUnique(Result, GetStringKey("MT LOGIC"));
		AddUnique(Result, GetStringKey("MURPHY"));
		AddUnique(Result, GetStringKey("NABO"));
		AddUnique(Result, GetStringKey("NATIONAL DELUXE"));
		AddUnique(Result, GetStringKey("NATIONAL ELECTR"));
		AddUnique(Result, GetStringKey("NEO"));
		AddUnique(Result, GetStringKey("NEVIR"));
		AddUnique(Result, GetStringKey("NEXT"));
		AddUnique(Result, GetStringKey("NIKAI"));
		AddUnique(Result, GetStringKey("NIKKEI"));
		AddUnique(Result, GetStringKey("NIKURA"));
		AddUnique(Result, GetStringKey("NOBEL"));
		AddUnique(Result, GetStringKey("NORDMENDE"));
		AddUnique(Result, GetStringKey("NORMANDE"));
		AddUnique(Result, GetStringKey("NURVO"));
		AddUnique(Result, GetStringKey("OCEANIC"));
		AddUnique(Result, GetStringKey("OEM"));
		AddUnique(Result, GetStringKey("OK"));
		AddUnique(Result, GetStringKey("OKI"));
		AddUnique(Result, GetStringKey("OLMAYACAK"));
		AddUnique(Result, GetStringKey("ONOMO"));
		AddUnique(Result, GetStringKey("ORANGE"));
		AddUnique(Result, GetStringKey("ORAVA"));
		AddUnique(Result, GetStringKey("ORIMA"));
		AddUnique(Result, GetStringKey("ORION"));
		AddUnique(Result, GetStringKey("OSCAR"));
		AddUnique(Result, GetStringKey("OSIO"));
		AddUnique(Result, GetStringKey("PANASONIC"));
		AddUnique(Result, GetStringKey("PATIENTLINE"));
		AddUnique(Result, GetStringKey("PEAQ"));
		AddUnique(Result, GetStringKey("PHILIPS"));
		AddUnique(Result, GetStringKey("PILOT"));
		AddUnique(Result, GetStringKey("PLATINIUM"));
		AddUnique(Result, GetStringKey("PLATINUM"));
		AddUnique(Result, GetStringKey("POLAROID"));
		AddUnique(Result, GetStringKey("PREMI ? RE"));
		AddUnique(Result, GetStringKey("PRIVILEGE"));
		AddUnique(Result, GetStringKey("PROLINE"));
		AddUnique(Result, GetStringKey("PROSONIC"));
		AddUnique(Result, GetStringKey("PUNKT"));
		AddUnique(Result, GetStringKey("PVISION"));
		AddUnique(Result, GetStringKey("QUADRO"));
		AddUnique(Result, GetStringKey("RAINFORD"));
		AddUnique(Result, GetStringKey("RAYLAN"));
		AddUnique(Result, GetStringKey("REGAL"));
		AddUnique(Result, GetStringKey("REWE"));
		AddUnique(Result, GetStringKey("ROLLS"));
		AddUnique(Result, GetStringKey("ROLS"));
		AddUnique(Result, GetStringKey("SABA"));
		AddUnique(Result, GetStringKey("SALORA"));
		AddUnique(Result, GetStringKey("SAMUS"));
		AddUnique(Result, GetStringKey("SANG"));
		AddUnique(Result, GetStringKey("SANITRON"));
		AddUnique(Result, GetStringKey("SANYO"));
		AddUnique(Result, GetStringKey("SAVARIA"));
		AddUnique(Result, GetStringKey("SB"));
		AddUnique(Result, GetStringKey("SCHAUB LORENZ"));
		AddUnique(Result, GetStringKey("SCHNEIDER"));
		AddUnique(Result, GetStringKey("SCH ? NTECH"));
		AddUnique(Result, GetStringKey("SCOTT"));
		AddUnique(Result, GetStringKey("SCREENLAND"));
		AddUnique(Result, GetStringKey("SEELEX TV"));
		AddUnique(Result, GetStringKey("SEG"));
		AddUnique(Result, GetStringKey("SENON"));
		AddUnique(Result, GetStringKey("SHARP"));
		AddUnique(Result, GetStringKey("SIERA"));
		AddUnique(Result, GetStringKey("SILVA SCHNEIDER"));
		AddUnique(Result, GetStringKey("SILVERCREST"));
		AddUnique(Result, GetStringKey("SINUDYNE"));
		AddUnique(Result, GetStringKey("SKYLINE"));
		AddUnique(Result, GetStringKey("SONAI"));
		AddUnique(Result, GetStringKey("STAR LIGHT"));
		AddUnique(Result, GetStringKey("SUNFEEL"));
		AddUnique(Result, GetStringKey("SUNSTECH"));
		AddUnique(Result, GetStringKey("SUPER GENERAL"));
		AddUnique(Result, GetStringKey("SUPER TECH"));
		AddUnique(Result, GetStringKey("TDSYSTEMS"));
		AddUnique(Result, GetStringKey("TECATELIPTV"));
		AddUnique(Result, GetStringKey("TECHLINE"));
		AddUnique(Result, GetStringKey("TECHNICAL"));
		AddUnique(Result, GetStringKey("TECHNIKA"));
		AddUnique(Result, GetStringKey("TECHNISAT"));
		AddUnique(Result, GetStringKey("TECHWOOD"));
		AddUnique(Result, GetStringKey("TECNISON"));
		AddUnique(Result, GetStringKey("TEK"));
		AddUnique(Result, GetStringKey("TELEFUNKEN"));
		AddUnique(Result, GetStringKey("TELESTAR"));
		AddUnique(Result, GetStringKey("TELETECH"));
		AddUnique(Result, GetStringKey("TESCO"));
		AddUnique(Result, GetStringKey("TEVION"));
		AddUnique(Result, GetStringKey("THOMSON"));
		AddUnique(Result, GetStringKey("TIVUS"));
		AddUnique(Result, GetStringKey("TOSHIBA"));
		AddUnique(Result, GetStringKey("TOUCHAIR"));
		AddUnique(Result, GetStringKey("T - SERIES"));
		AddUnique(Result, GetStringKey("TUCSON"));
		AddUnique(Result, GetStringKey("TUNEX"));
		AddUnique(Result, GetStringKey("TURBO - X"));
		AddUnique(Result, GetStringKey("ULTRAVOX"));
		AddUnique(Result, GetStringKey("UNIDEN"));
		AddUnique(Result, GetStringKey("UNITED"));
		AddUnique(Result, GetStringKey("UNIVERSAL"));
		AddUnique(Result, GetStringKey("UNIVISION"));
		AddUnique(Result, GetStringKey("VANGUARD"));
		AddUnique(Result, GetStringKey("VEGO"));
		AddUnique(Result, GetStringKey("VERYO"));
		AddUnique(Result, GetStringKey("VESPAZ"));
		AddUnique(Result, GetStringKey("VESTEL"));
		AddUnique(Result, GetStringKey("VIDA"));
		AddUnique(Result, GetStringKey("VISITECH"));
		AddUnique(Result, GetStringKey("WALKER"));
		AddUnique(Result, GetStringKey("WALTHAM"));
		AddUnique(Result, GetStringKey("WATSON"));
		AddUnique(Result, GetStringKey("WATSVISION"));
		AddUnique(Result, GetStringKey("WEG"));
		AddUnique(Result, GetStringKey("WELLINGTON"));
		AddUnique(Result, GetStringKey("WESTLINE"));
		AddUnique(Result, GetStringKey("WESTPOINT"));
		AddUnique(Result, GetStringKey("WESTWOOD"));
		AddUnique(Result, GetStringKey("WHITE WESTINGHO"));
		AddUnique(Result, GetStringKey("WHITESTONE"));
		AddUnique(Result, GetStringKey("WILSON"));
		AddUnique(Result, GetStringKey("WINDSOR"));
		AddUnique(Result, GetStringKey("XENIUS"));
		AddUnique(Result, GetStringKey("YASUDA"));
		AddUnique(Result, GetStringKey("ZEPTO"));
		AddUnique(Result, GetStringKey("DefaultStdbySearch"));
		AddUnique(Result, GetStringKey("DefaultZoomMode"));
		AddUnique(Result, GetStringKey("DvdEmcModifications"));
		AddUnique(Result, GetStringKey("FeatureOSD"));
		AddUnique(Result, GetStringKey("FeatureOSDHtml"));
		AddUnique(Result, GetStringKey("FeatureOSDHtmlMode"));
		AddUnique(Result, GetStringKey("FeatureOSDHtmlTitlesOn"));
		AddUnique(Result, GetStringKey("FeatureOSDSettings"));
		AddUnique(Result, GetStringKey("BoxerHDEnabled"));
		AddUnique(Result, GetStringKey("CanalDigitalEnabled"));
		AddUnique(Result, GetStringKey("CanalReadyEnabled"));
		AddUnique(Result, GetStringKey("CableHdReadyEnabled"));
		AddUnique(Result, GetStringKey("ComHemEnabled"));
		AddUnique(Result, GetStringKey("DigitalTickEnabled"));
		AddUnique(Result, GetStringKey("FreeviewEnabled"));
		AddUnique(Result, GetStringKey("FreeviewHDEnabled"));
		AddUnique(Result, GetStringKey("RiksTvEnabled"));
		AddUnique(Result, GetStringKey("TntHdEnabled"));
		AddUnique(Result, GetStringKey("UpcEnabled"));
		AddUnique(Result, GetStringKey("SaorviewEnabled"));
		AddUnique(Result, GetStringKey("YouseeEnabled"));
		AddUnique(Result, GetStringKey("DgtviGoldEnabled"));
		AddUnique(Result, GetStringKey("DgtviSilverEnabled"));
		AddUnique(Result, GetStringKey("StofaEnabled"));
		AddUnique(Result, GetStringKey("ZiggoEnabled"));
		AddUnique(Result, GetStringKey("Dvbt2Enabled"));
		AddUnique(Result, GetStringKey("TntsatEnabled"));
		AddUnique(Result, GetStringKey("OrfEnabled"));
		AddUnique(Result, GetStringKey("AntennaReadyHdEnabled"));
		AddUnique(Result, GetStringKey("CanalDigitaalEnabled"));
		AddUnique(Result, GetStringKey("HdAustriaEnabled"));
		AddUnique(Result, GetStringKey("ReadyForHdEnabled"));
		AddUnique(Result, GetStringKey("SimpliTvEnabled"));
		AddUnique(Result, GetStringKey("SkylinkEnabled"));
		AddUnique(Result, GetStringKey("TelenetEnabled"));
		AddUnique(Result, GetStringKey("TelesatEnabled"));
		AddUnique(Result, GetStringKey("TvVlaanderenEnabled"));
		AddUnique(Result, GetStringKey("UnityMediaEnabled"));
		AddUnique(Result, GetStringKey("SkyEnabled"));
		AddUnique(Result, GetStringKey("MindigEnabled"));
		AddUnique(Result, GetStringKey("PanelLogoType"));
		AddUnique(Result, GetStringKey("FTPUpgrade"));
		AddUnique(Result, GetStringKey("GCCCountrySupport"));
		AddUnique(Result, GetStringKey("HomeStoreMode"));
		AddUnique(Result, GetStringKey("HomeStoreModeEnabled"));
		AddUnique(Result, GetStringKey("HotelTv"));
		AddUnique(Result, GetStringKey("HotelMode"));
		AddUnique(Result, GetStringKey("ILogo"));
		AddUnique(Result, GetStringKey("StandbyLed"));
		AddUnique(Result, GetStringKey("DefaultValueStandbyLed"));
		AddUnique(Result, GetStringKey("DefaultValueILogo"));
		AddUnique(Result, GetStringKey("HotelTVSupport"));
		AddUnique(Result, GetStringKey("Language"));
		AddUnique(Result, GetStringKey("Lcn"));
		AddUnique(Result, GetStringKey("LogoType3D"));
		AddUnique(Result, GetStringKey("MediaBrowser"));
		AddUnique(Result, GetStringKey("displayable_audio_file_extension"));
		AddUnique(Result, GetStringKey("OGG"));
		AddUnique(Result, GetStringKey("displayable_photo_file_extension"));
		AddUnique(Result, GetStringKey("BMP"));
		AddUnique(Result, GetStringKey("JPE"));
		AddUnique(Result, GetStringKey("JPEG"));
		AddUnique(Result, GetStringKey("JPG"));
		AddUnique(Result, GetStringKey("PNG"));
		AddUnique(Result, GetStringKey("GIF"));
		AddUnique(Result, GetStringKey("TIF"));
		AddUnique(Result, GetStringKey("JPS"));
		AddUnique(Result, GetStringKey("displayable_record_file_extension"));
		AddUnique(Result, GetStringKey("displayable_subtitle_file_extension"));
		AddUnique(Result, GetStringKey("SRT"));
		AddUnique(Result, GetStringKey("SUB"));
		AddUnique(Result, GetStringKey("displayable_video_file_extension"));
		AddUnique(Result, GetStringKey("divx_model_id"));
		AddUnique(Result, GetStringKey("ID"));
		AddUnique(Result, GetStringKey("Model"));
		AddUnique(Result, GetStringKey("StageCraftAutoTest"));
		AddUnique(Result, GetStringKey("DisableBannerInLoop"));
		AddUnique(Result, GetStringKey("MenuTimeout"));
		AddUnique(Result, GetStringKey("DefaultMode"));
		AddUnique(Result, GetStringKey("PatientlineSupport"));
		AddUnique(Result, GetStringKey("PcStandby"));
		AddUnique(Result, GetStringKey("PcStanbyOut"));
		AddUnique(Result, GetStringKey("PIN8Standby"));
		AddUnique(Result, GetStringKey("PIN8StandbyOut"));
		AddUnique(Result, GetStringKey("Picture"));
		AddUnique(Result, GetStringKey("Backlight"));
		AddUnique(Result, GetStringKey("BacklightDimmingDefault"));
		AddUnique(Result, GetStringKey("PowerSave"));
		AddUnique(Result, GetStringKey("BacklightDimming"));
		AddUnique(Result, GetStringKey("PowerSaveDefault"));
		AddUnique(Result, GetStringKey("PowerSaveString"));
		AddUnique(Result, GetStringKey("PictureOffMode"));
		AddUnique(Result, GetStringKey("Default3DMode"));
		AddUnique(Result, GetStringKey("DynamicContrast"));
		AddUnique(Result, GetStringKey("ERPAlgo"));
		AddUnique(Result, GetStringKey("DefaultValue"));
		AddUnique(Result, GetStringKey("MovieSenseString"));
		AddUnique(Result, GetStringKey("NoiseReduction"));
		AddUnique(Result, GetStringKey("AnalogRF"));
		AddUnique(Result, GetStringKey("DigitalRFSD"));
		AddUnique(Result, GetStringKey("DigitalRFHD"));
		AddUnique(Result, GetStringKey("ScartCVBS"));
		AddUnique(Result, GetStringKey("ScartRGB"));
		AddUnique(Result, GetStringKey("YPbPr"));
		AddUnique(Result, GetStringKey("SVideo"));
		AddUnique(Result, GetStringKey("FAV"));
		AddUnique(Result, GetStringKey("PC"));
		AddUnique(Result, GetStringKey("HDMI"));
		AddUnique(Result, GetStringKey("USB"));
		AddUnique(Result, GetStringKey("PictureMode"));
		AddUnique(Result, GetStringKey("DTV"));
		AddUnique(Result, GetStringKey("SCARTCVBS"));
		AddUnique(Result, GetStringKey("SCARTRGB"));
		AddUnique(Result, GetStringKey("ATV"));
		AddUnique(Result, GetStringKey("DVD"));
		AddUnique(Result, GetStringKey("PictureModeItems"));
		AddUnique(Result, GetStringKey("Augen"));
		AddUnique(Result, GetStringKey("BrilliantPixel"));
		AddUnique(Result, GetStringKey("FullPIX"));
		AddUnique(Result, GetStringKey("IPIX"));
		AddUnique(Result, GetStringKey("iPe"));
		AddUnique(Result, GetStringKey("Local Dimming"));
		AddUnique(Result, GetStringKey("Pixel Clear"));
		AddUnique(Result, GetStringKey("Pixellence II"));
		AddUnique(Result, GetStringKey("PixLed"));
		AddUnique(Result, GetStringKey("Pixtra"));
		AddUnique(Result, GetStringKey("RealVision"));
		AddUnique(Result, GetStringKey("Vestel Pixellence"));
		AddUnique(Result, GetStringKey("PixellenceString"));
		AddUnique(Result, GetStringKey("SkinTone"));
		AddUnique(Result, GetStringKey("PictureVsSourceVsMode"));
		AddUnique(Result, GetStringKey("Cinema"));
		AddUnique(Result, GetStringKey("Contrast"));
		AddUnique(Result, GetStringKey("Brightness"));
		AddUnique(Result, GetStringKey("Sharpness"));
		AddUnique(Result, GetStringKey("Colour"));
		AddUnique(Result, GetStringKey("Hue"));
		AddUnique(Result, GetStringKey("ColourTemp"));
		AddUnique(Result, GetStringKey("Dynamic"));
		AddUnique(Result, GetStringKey("Game"));
		AddUnique(Result, GetStringKey("Natural"));
		AddUnique(Result, GetStringKey("Sport"));
		AddUnique(Result, GetStringKey("CVBS"));
		AddUnique(Result, GetStringKey("PC - VGA"));
		AddUnique(Result, GetStringKey("Scart - RGB"));
		AddUnique(Result, GetStringKey("S - VHS"));
		AddUnique(Result, GetStringKey("Ypbpr"));
		AddUnique(Result, GetStringKey("PowerUpMode"));
		AddUnique(Result, GetStringKey("QuickStandby"));
		AddUnique(Result, GetStringKey("SaorviewtvLogo"));
		AddUnique(Result, GetStringKey("AstraHDSupport"));
		AddUnique(Result, GetStringKey("FransatSupport"));
		AddUnique(Result, GetStringKey("ScartOutDuringDVD"));
		AddUnique(Result, GetStringKey("Enable"));
		AddUnique(Result, GetStringKey("ShortProtection"));
		AddUnique(Result, GetStringKey("Sound"));
		AddUnique(Result, GetStringKey("AutoLevel"));
		AddUnique(Result, GetStringKey("AutoLevelDefaultValue"));
		AddUnique(Result, GetStringKey("Effect"));
		AddUnique(Result, GetStringKey("EffectDefaultValue"));
		AddUnique(Result, GetStringKey("Surround"));
		AddUnique(Result, GetStringKey("SurroundMode"));
		AddUnique(Result, GetStringKey("Test"));
		AddUnique(Result, GetStringKey("Testtool"));
		AddUnique(Result, GetStringKey("TesttoolSocket"));
		AddUnique(Result, GetStringKey("Uart"));
		AddUnique(Result, GetStringKey("UartDisableOnFTI"));
		AddUnique(Result, GetStringKey("USBStreamPlayerSupport"));
		AddUnique(Result, GetStringKey("UseProductionCredentials"));
		AddUnique(Result, GetStringKey("WelcomeScreen"));
		AddUnique(Result, GetStringKey("ZiggoSupport"));
		AddUnique(Result, GetStringKey("ZiggoSupportDefaultValue"));
		AddUnique(Result, GetStringKey("LanguageSelection"));
		AddUnique(Result, GetStringKey("baq"));
		AddUnique(Result, GetStringKey("bul"));
		AddUnique(Result, GetStringKey("cat"));
		AddUnique(Result, GetStringKey("scc"));
		AddUnique(Result, GetStringKey("cze"));
		AddUnique(Result, GetStringKey("dan"));
		AddUnique(Result, GetStringKey("dut"));
		AddUnique(Result, GetStringKey("eng"));
		AddUnique(Result, GetStringKey("est"));
		AddUnique(Result, GetStringKey("fin"));
		AddUnique(Result, GetStringKey("fre"));
		AddUnique(Result, GetStringKey("gla"));
		AddUnique(Result, GetStringKey("glg"));
		AddUnique(Result, GetStringKey("ger"));
		AddUnique(Result, GetStringKey("gre"));
		AddUnique(Result, GetStringKey("hun"));
		AddUnique(Result, GetStringKey("ice"));
		AddUnique(Result, GetStringKey("ita"));
		AddUnique(Result, GetStringKey("iri"));
		AddUnique(Result, GetStringKey("lav"));
		AddUnique(Result, GetStringKey("lit"));
		AddUnique(Result, GetStringKey("mac"));
		AddUnique(Result, GetStringKey("nor"));
		AddUnique(Result, GetStringKey("pol"));
		AddUnique(Result, GetStringKey("por"));
		AddUnique(Result, GetStringKey("rum"));
		AddUnique(Result, GetStringKey("rus"));
		AddUnique(Result, GetStringKey("slo"));
		AddUnique(Result, GetStringKey("slv"));
		AddUnique(Result, GetStringKey("spa"));
		AddUnique(Result, GetStringKey("swe"));
		AddUnique(Result, GetStringKey("tur"));
		AddUnique(Result, GetStringKey("ukr"));
		AddUnique(Result, GetStringKey("und"));
		AddUnique(Result, GetStringKey("wel"));
		AddUnique(Result, GetStringKey("ara"));
		AddUnique(Result, GetStringKey("per"));
		AddUnique(Result, GetStringKey("heb"));
		AddUnique(Result, GetStringKey("scr"));
		AddUnique(Result, GetStringKey("bel"));
		AddUnique(Result, GetStringKey("alb"));
		AddUnique(Result, GetStringKey("Oad"));
		AddUnique(Result, GetStringKey("DcfName"));
		AddUnique(Result, GetStringKey("ModelId"));
		AddUnique(Result, GetStringKey("Id"));
		AddUnique(Result, GetStringKey("Oui"));
		AddUnique(Result, GetStringKey("WebDownloadDetails"));
		AddUnique(Result, GetStringKey("Address1"));
		AddUnique(Result, GetStringKey("UserName1"));
		AddUnique(Result, GetStringKey("Password1"));
		AddUnique(Result, GetStringKey("ServerType1"));
		AddUnique(Result, GetStringKey("PatentItems"));
		AddUnique(Result, GetStringKey("EPGMenus"));
		AddUnique(Result, GetStringKey("RoviItems"));
		AddUnique(Result, GetStringKey("DisableDeleteSelectServiceSetRecord"));
		AddUnique(Result, GetStringKey("Software"));
		AddUnique(Result, GetStringKey("CustomerPostfix"));
		AddUnique(Result, GetStringKey("SwLabelPostfix"));
		AddUnique(Result, GetStringKey("OverrideVersion"));
		AddUnique(Result, GetStringKey("OverrideMode"));
		AddUnique(Result, GetStringKey("VersionNumber"));
		AddUnique(Result, GetStringKey("Fav"));
		AddUnique(Result, GetStringKey("Ext1S"));
		AddUnique(Result, GetStringKey("Pc"));
		AddUnique(Result, GetStringKey("Ext1Rgb"));
		AddUnique(Result, GetStringKey("Ext2Rgb"));
		AddUnique(Result, GetStringKey("Hdmi1"));
		AddUnique(Result, GetStringKey("Ext1"));
		AddUnique(Result, GetStringKey("Hdmi2"));
		AddUnique(Result, GetStringKey("Hdmi3"));
		AddUnique(Result, GetStringKey("Hdmi4"));
		AddUnique(Result, GetStringKey("Svhs"));
		AddUnique(Result, GetStringKey("Ext2"));
		AddUnique(Result, GetStringKey("Dvd"));
		AddUnique(Result, GetStringKey("Widi"));
		AddUnique(Result, GetStringKey("APM"));
		AddUnique(Result, GetStringKey("Ext2S"));
		AddUnique(Result, GetStringKey("IPCamera"));
		AddUnique(Result, GetStringKey("SwProfileVersion"));
		AddUnique(Result, GetStringKey("Version"));
		AddUnique(Result, GetStringKey("UartBaundrateOption"));
		AddUnique(Result, GetStringKey("UartBaundrate"));
		AddUnique(Result, GetStringKey("ColourShift"));
		AddUnique(Result, GetStringKey("8Watt"));
		AddUnique(Result, GetStringKey("TIGroup3"));
		AddUnique(Result, GetStringKey("default.aqs.bin"));
		AddUnique(Result, GetStringKey("Do_not_change"));
		AddUnique(Result, GetStringKey("MB95"));
		AddUnique(Result, GetStringKey("17MB95_EDID_39W_LCD_HDMI_FullHD_OEM.edid"));
		AddUnique(Result, GetStringKey("edid.edid"));
		AddUnique(Result, GetStringKey("064"));
		AddUnique(Result, GetStringKey("Default"));
		AddUnique(Result, GetStringKey("KeyPad-3Way"));
		AddUnique(Result, GetStringKey("LG32"));
		AddUnique(Result, GetStringKey("03.10.2013"));
		AddUnique(Result, GetStringKey("02:39:27"));
		AddUnique(Result, GetStringKey("HALIMEF"));
		AddUnique(Result, GetStringKey("Rc48XX_2D"));
		AddUnique(Result, GetStringKey("C"));
		AddUnique(Result, GetStringKey("Si2156"));
		AddUnique(Result, GetStringKey("VRc48XX"));
		AddUnique(Result, GetStringKey("hwprofile"));
		AddUnique(Result, GetStringKey("AudioSettings"));
		AddUnique(Result, GetStringKey("AudioDelay"));
		AddUnique(Result, GetStringKey("AudioDelayLow"));
		AddUnique(Result, GetStringKey("AudioDelayHigh"));
		AddUnique(Result, GetStringKey("ClipLevel"));
		AddUnique(Result, GetStringKey("12Watt"));
		AddUnique(Result, GetStringKey("AVC_On"));
		AddUnique(Result, GetStringKey("AVC_Off"));
		AddUnique(Result, GetStringKey("HPLineout_On"));
		AddUnique(Result, GetStringKey("AV"));
		AddUnique(Result, GetStringKey("SCART1"));
		AddUnique(Result, GetStringKey("SCART2"));
		AddUnique(Result, GetStringKey("2.5Watt"));
		AddUnique(Result, GetStringKey("DRC_Threshold"));
		AddUnique(Result, GetStringKey("2.5Watt_TI"));
		AddUnique(Result, GetStringKey("4R12Watt"));
		AddUnique(Result, GetStringKey("6Watt"));
		AddUnique(Result, GetStringKey("6Watt_BMS"));
		AddUnique(Result, GetStringKey("8Watt_12V"));
		AddUnique(Result, GetStringKey("SoundBox"));
		AddUnique(Result, GetStringKey("ClipLevelTI"));
		AddUnique(Result, GetStringKey("DynamicBass"));
		AddUnique(Result, GetStringKey("DynamicbassDefaultValue"));
		AddUnique(Result, GetStringKey("EqualizerSettings"));
		AddUnique(Result, GetStringKey("Classic"));
		AddUnique(Result, GetStringKey("120Hz"));
		AddUnique(Result, GetStringKey("500Hz"));
		AddUnique(Result, GetStringKey("1.5KHz"));
		AddUnique(Result, GetStringKey("5.0KHz"));
		AddUnique(Result, GetStringKey("10.0KHz"));
		AddUnique(Result, GetStringKey("Flat"));
		AddUnique(Result, GetStringKey("Movie"));
		AddUnique(Result, GetStringKey("Music"));
		AddUnique(Result, GetStringKey("Speech"));
		AddUnique(Result, GetStringKey("User"));
		AddUnique(Result, GetStringKey("LineoutHeadphone"));
		AddUnique(Result, GetStringKey("FixedVolumeThroughLineout"));
		AddUnique(Result, GetStringKey("LineoutHeadPhoneEnabled"));
		AddUnique(Result, GetStringKey("PEQ"));
		AddUnique(Result, GetStringKey("Prescales"));
		AddUnique(Result, GetStringKey("Lineout"));
		AddUnique(Result, GetStringKey("Hp"));
		AddUnique(Result, GetStringKey("Main"));
		AddUnique(Result, GetStringKey("Sifout"));
		AddUnique(Result, GetStringKey("Spdif"));
		AddUnique(Result, GetStringKey("LineoutHP"));
		AddUnique(Result, GetStringKey("TIPrescale"));
		AddUnique(Result, GetStringKey("DefaultSpeaker"));
		AddUnique(Result, GetStringKey("Subwoofer"));
		AddUnique(Result, GetStringKey("InternalSubwoofer"));
		AddUnique(Result, GetStringKey("SurroundModeText"));
		AddUnique(Result, GetStringKey("3D"));
		AddUnique(Result, GetStringKey("Panorama"));
		AddUnique(Result, GetStringKey("3DS"));
		AddUnique(Result, GetStringKey("Virtual"));
		AddUnique(Result, GetStringKey("Dolby"));
		AddUnique(Result, GetStringKey("SurroundModeTextSelect"));
		AddUnique(Result, GetStringKey("SurroundType"));
		AddUnique(Result, GetStringKey("SRS"));
		AddUnique(Result, GetStringKey("TIGroup1"));
		AddUnique(Result, GetStringKey("DefinitionControl"));
		AddUnique(Result, GetStringKey("FocusControl"));
		AddUnique(Result, GetStringKey("InputGain"));
		AddUnique(Result, GetStringKey("SpeakerSize"));
		AddUnique(Result, GetStringKey("SurroundLevel"));
		AddUnique(Result, GetStringKey("TruBassControl"));
		AddUnique(Result, GetStringKey("TIGroup10"));
		AddUnique(Result, GetStringKey("TIGroup4"));
		AddUnique(Result, GetStringKey("TIGroup6"));
		AddUnique(Result, GetStringKey("TIGroup8"));
		AddUnique(Result, GetStringKey("SRSGroup"));
		AddUnique(Result, GetStringKey("P.II"));
		AddUnique(Result, GetStringKey("Speaker"));
		AddUnique(Result, GetStringKey("SurroundTypeSelection"));
		AddUnique(Result, GetStringKey("VolumeLevel"));
		AddUnique(Result, GetStringKey("AudioFile"));
		AddUnique(Result, GetStringKey("NewPEQStructure"));
		AddUnique(Result, GetStringKey("BoardConfig"));
		AddUnique(Result, GetStringKey("MB91BoardEnabled"));
		AddUnique(Result, GetStringKey("VoltageDropDetect"));
		AddUnique(Result, GetStringKey("BoardName"));
		AddUnique(Result, GetStringKey("DVB_T2_Enabled"));
		AddUnique(Result, GetStringKey("Edid"));
		AddUnique(Result, GetStringKey("Hdmi1EdidFileName"));
		AddUnique(Result, GetStringKey("Hdmi2EdidFileName"));
		AddUnique(Result, GetStringKey("Hdmi3EdidFileName"));
		AddUnique(Result, GetStringKey("Hdmi4EdidFileName"));
		AddUnique(Result, GetStringKey("PhysicalAddressPosition"));
		AddUnique(Result, GetStringKey("GlassType3D"));
		AddUnique(Result, GetStringKey("NumberOfConnectors"));
		AddUnique(Result, GetStringKey("Swap"));
		AddUnique(Result, GetStringKey("Ports"));
		AddUnique(Result, GetStringKey("HwProfileVersion"));
		AddUnique(Result, GetStringKey("LocalKeyThresholds"));
		AddUnique(Result, GetStringKey("KeyPad"));
		AddUnique(Result, GetStringKey("LocalKey"));
		AddUnique(Result, GetStringKey("Menu"));
		AddUnique(Result, GetStringKey("ADC"));
		AddUnique(Result, GetStringKey("High"));
		AddUnique(Result, GetStringKey("Low"));
		AddUnique(Result, GetStringKey("P-"));
		AddUnique(Result, GetStringKey("P+"));
		AddUnique(Result, GetStringKey("Standby"));
		AddUnique(Result, GetStringKey("TVAV"));
		AddUnique(Result, GetStringKey("V-"));
		AddUnique(Result, GetStringKey("V+"));
		AddUnique(Result, GetStringKey("TK-145-6-8"));
		AddUnique(Result, GetStringKey("TK-147"));
		AddUnique(Result, GetStringKey("FingerThresholdVolMinus"));
		AddUnique(Result, GetStringKey("FingerThresholdVolPlus"));
		AddUnique(Result, GetStringKey("FingerThresholdPrMinus"));
		AddUnique(Result, GetStringKey("FingerThresholdPrPlus"));
		AddUnique(Result, GetStringKey("FingerThresholdTV-AV"));
		AddUnique(Result, GetStringKey("FingerThresholdStandby"));
		AddUnique(Result, GetStringKey("NoiseThreshold"));
		AddUnique(Result, GetStringKey("Hysteresis"));
		AddUnique(Result, GetStringKey("VRef"));
		AddUnique(Result, GetStringKey("Resolution"));
		AddUnique(Result, GetStringKey("ScanSpeed"));
		AddUnique(Result, GetStringKey("SensingParameters"));
		AddUnique(Result, GetStringKey("TouchPad-CY"));
		AddUnique(Result, GetStringKey("TouchPad-F"));
		AddUnique(Result, GetStringKey("46up"));
		AddUnique(Result, GetStringKey("VolumeMinus"));
		AddUnique(Result, GetStringKey("VolumePlus"));
		AddUnique(Result, GetStringKey("ProgramMinus"));
		AddUnique(Result, GetStringKey("ProgramPlus"));
		AddUnique(Result, GetStringKey("TV-AV"));
		AddUnique(Result, GetStringKey("Kabartma"));
		AddUnique(Result, GetStringKey("NormalKabin"));
		AddUnique(Result, GetStringKey("SlimKabin"));
		AddUnique(Result, GetStringKey("VDM"));
		AddUnique(Result, GetStringKey("SensingType"));
		AddUnique(Result, GetStringKey("LocalKeyType"));
		AddUnique(Result, GetStringKey("MemoryMap"));
		AddUnique(Result, GetStringKey("Mpeg4"));
		AddUnique(Result, GetStringKey("PanelConfig"));
		AddUnique(Result, GetStringKey("PanelName"));
		AddUnique(Result, GetStringKey("IsPanel200Hz"));
		AddUnique(Result, GetStringKey("PanelType3D"));
		AddUnique(Result, GetStringKey("ProductId"));
		AddUnique(Result, GetStringKey("ProductIdStr"));
		AddUnique(Result, GetStringKey("RemoteControl"));
		AddUnique(Result, GetStringKey("BBTest"));
		AddUnique(Result, GetStringKey("KeySelect"));
		AddUnique(Result, GetStringKey("KeyOne"));
		AddUnique(Result, GetStringKey("KeyTwo"));
		AddUnique(Result, GetStringKey("KeyThree"));
		AddUnique(Result, GetStringKey("KeyFour"));
		AddUnique(Result, GetStringKey("KeyFive"));
		AddUnique(Result, GetStringKey("KeySix"));
		AddUnique(Result, GetStringKey("KeyEndOfRemoteKeys"));
		AddUnique(Result, GetStringKey("KeySubtitle"));
		AddUnique(Result, GetStringKey("KeyPreset"));
		AddUnique(Result, GetStringKey("KeyNine"));
		AddUnique(Result, GetStringKey("KeyPrevious"));
		AddUnique(Result, GetStringKey("KeyZero"));
		AddUnique(Result, GetStringKey("KeyMute"));
		AddUnique(Result, GetStringKey("KeyEight"));
		AddUnique(Result, GetStringKey("KeyPower"));
		AddUnique(Result, GetStringKey("KeyVolumeDown"));
		AddUnique(Result, GetStringKey("KeyInfo"));
		AddUnique(Result, GetStringKey("KeyDown"));
		AddUnique(Result, GetStringKey("KeyUp"));
		AddUnique(Result, GetStringKey("KeyLeft"));
		AddUnique(Result, GetStringKey("KeyRight"));
		AddUnique(Result, GetStringKey("KeyProgramUp"));
		AddUnique(Result, GetStringKey("KeyTextIndex"));
		AddUnique(Result, GetStringKey("KeyProgramDown"));
		AddUnique(Result, GetStringKey("KeyDual"));
		AddUnique(Result, GetStringKey("KeyTv"));
		AddUnique(Result, GetStringKey("KeyMediaBrowser"));
		AddUnique(Result, GetStringKey("KeyWide"));
		AddUnique(Result, GetStringKey("KeyGreen"));
		AddUnique(Result, GetStringKey("KeyBlue"));
		AddUnique(Result, GetStringKey("KeyRed"));
		AddUnique(Result, GetStringKey("KeyFavourites"));
		AddUnique(Result, GetStringKey("KeyExit"));
		AddUnique(Result, GetStringKey("KeyTextPatMix"));
		AddUnique(Result, GetStringKey("KeyEpg"));
		AddUnique(Result, GetStringKey("KeySleep"));
		AddUnique(Result, GetStringKey("KeyTextReveal"));
		AddUnique(Result, GetStringKey("KeyText"));
		AddUnique(Result, GetStringKey("KeyPipPap"));
		AddUnique(Result, GetStringKey("KeyYellow"));
		AddUnique(Result, GetStringKey("KeyVolumeUp"));
		AddUnique(Result, GetStringKey("KeyLang"));
		AddUnique(Result, GetStringKey("KeyVideo"));
		AddUnique(Result, GetStringKey("KeyPlaylist"));
		AddUnique(Result, GetStringKey("KeyRadio"));
		AddUnique(Result, GetStringKey("KeyServices"));
		AddUnique(Result, GetStringKey("KeyPipProgramDown"));
		AddUnique(Result, GetStringKey("KeyAux"));
		AddUnique(Result, GetStringKey("KeyPageUp"));
		AddUnique(Result, GetStringKey("KeyPageDown"));
		AddUnique(Result, GetStringKey("KeyFforward"));
		AddUnique(Result, GetStringKey("KeyPipProgramUp"));
		AddUnique(Result, GetStringKey("KeyPip"));
		AddUnique(Result, GetStringKey("KeyPap"));
		AddUnique(Result, GetStringKey("KeyMhpApp"));
		AddUnique(Result, GetStringKey("KeyMhpTv"));
		AddUnique(Result, GetStringKey("KeyMhpBack"));
		AddUnique(Result, GetStringKey("KeyRecord"));
		AddUnique(Result, GetStringKey("KeyRewind"));
		AddUnique(Result, GetStringKey("KeyPlay"));
		AddUnique(Result, GetStringKey("KeyTimers"));
		AddUnique(Result, GetStringKey("KeyPause"));
		AddUnique(Result, GetStringKey("KeyStop"));
		AddUnique(Result, GetStringKey("KeyPc"));
		AddUnique(Result, GetStringKey("KeyTextHold"));
		AddUnique(Result, GetStringKey("KeyTextSubpage"));
		AddUnique(Result, GetStringKey("KeyMhegEpg"));
		AddUnique(Result, GetStringKey("KeyTextMix"));
		AddUnique(Result, GetStringKey("KeyAudio"));
		AddUnique(Result, GetStringKey("KeySeven"));
		AddUnique(Result, GetStringKey("KeyTextUpdate"));
		AddUnique(Result, GetStringKey("KeyMenu"));
		AddUnique(Result, GetStringKey("KeyInternetSettings"));
		AddUnique(Result, GetStringKey("KeyEffect"));
		AddUnique(Result, GetStringKey("NEC"));
		AddUnique(Result, GetStringKey("Jvc-C2503"));
		AddUnique(Result, GetStringKey("Panasonic"));
		AddUnique(Result, GetStringKey("IsPvr"));
		AddUnique(Result, GetStringKey("is3D"));
		AddUnique(Result, GetStringKey("KeyFullExit"));
		AddUnique(Result, GetStringKey("KeyBattery"));
		AddUnique(Result, GetStringKey("KeyChannels"));
		AddUnique(Result, GetStringKey("KeyQuick"));
		AddUnique(Result, GetStringKey("KeyMyButton"));
		AddUnique(Result, GetStringKey("KeyLongMyButton"));
		AddUnique(Result, GetStringKey("KeyMyButton2"));
		AddUnique(Result, GetStringKey("KeyLongMyButton2"));
		AddUnique(Result, GetStringKey("KeyFreeze"));
		AddUnique(Result, GetStringKey("Rc1072"));
		AddUnique(Result, GetStringKey("Rc1190"));
		AddUnique(Result, GetStringKey("Rc1208"));
		AddUnique(Result, GetStringKey("Rc1209"));
		AddUnique(Result, GetStringKey("Rc1210"));
		AddUnique(Result, GetStringKey("Rc1230"));
		AddUnique(Result, GetStringKey("Rc1231"));
		AddUnique(Result, GetStringKey("Rc1800"));
		AddUnique(Result, GetStringKey("Rc1825"));
		AddUnique(Result, GetStringKey("Rc1900"));
		AddUnique(Result, GetStringKey("Rc1910"));
		AddUnique(Result, GetStringKey("Rc1912"));
		AddUnique(Result, GetStringKey("KeyEquMode"));
		AddUnique(Result, GetStringKey("Rc3400"));
		AddUnique(Result, GetStringKey("Rc3411"));
		AddUnique(Result, GetStringKey("KeyAlarm"));
		AddUnique(Result, GetStringKey("Rc3900"));
		AddUnique(Result, GetStringKey("Rc4100"));
		AddUnique(Result, GetStringKey("Rc48XX_3D"));
		AddUnique(Result, GetStringKey("Rc4900"));
		AddUnique(Result, GetStringKey("Rc5100"));
		AddUnique(Result, GetStringKey("KeyNextTrack"));
		AddUnique(Result, GetStringKey("KeyPreviousTrack"));
		AddUnique(Result, GetStringKey("Rc5100_RF"));
		AddUnique(Result, GetStringKey("IsRF"));
		AddUnique(Result, GetStringKey("Rc5103"));
		AddUnique(Result, GetStringKey("Rc5110"));
		AddUnique(Result, GetStringKey("Rc5111"));
		AddUnique(Result, GetStringKey("KeyMultimedia"));
		AddUnique(Result, GetStringKey("KeyQuickMenu"));
		AddUnique(Result, GetStringKey("KeyOptions"));
		AddUnique(Result, GetStringKey("KeyYahoo"));
		AddUnique(Result, GetStringKey("KeyDevices"));
		AddUnique(Result, GetStringKey("Rc5112"));
		AddUnique(Result, GetStringKey("Rc5117"));
		AddUnique(Result, GetStringKey("KeyYoutube"));
		AddUnique(Result, GetStringKey("KeyNetflix"));
		AddUnique(Result, GetStringKey("Sharp-RC4847"));
		AddUnique(Result, GetStringKey("Technisat-Rc4850"));
		AddUnique(Result, GetStringKey("Technisat-Rc4852"));
		AddUnique(Result, GetStringKey("RcType"));
		AddUnique(Result, GetStringKey("RF_Remote_LQI"));
		AddUnique(Result, GetStringKey("Discovery_LQI_Threshold"));
		AddUnique(Result, GetStringKey("SourceSelection"));
		AddUnique(Result, GetStringKey("TV"));
		AddUnique(Result, GetStringKey("TunerTypes"));
		AddUnique(Result, GetStringKey("LG_Tdtc"));
		AddUnique(Result, GetStringKey("SAMSUNG"));
		AddUnique(Result, GetStringKey("Type"));
		AddUnique(Result, GetStringKey("SONY"));
		AddUnique(Result, GetStringKey("TunerType"));
		AddUnique(Result, GetStringKey("USBPorts"));
		AddUnique(Result, GetStringKey("USBPowerOff"));
		AddUnique(Result, GetStringKey("VirtualRemoteControl"));
		AddUnique(Result, GetStringKey("VRcType"));
		AddUnique(Result, GetStringKey("YPbPrViaVga"));
		
	
		//AddUnique(Result, { 123688959, "Something" });
		AddUnique(Result, { 1792730140, "EnergySaving" });
		AddUnique(Result, { 123688959, "WhitePoint" });

		//123688959
		// Unknown countries / brands / languages
		AddUnique(Result, { 171759819, "UnkCountry" });
		AddUnique(Result, { 2566668063, "UnkCountry" });
		AddUnique(Result, { 521367954, "UnkCountry" });
		AddUnique(Result, { 2640426593, "UnkCountry" });
		AddUnique(Result, { 3698212987, "UnkBrand" });
		AddUnique(Result, { 3913228441, "UnkBrand" });
		AddUnique(Result, { 1154292241, "UnkBrand" });
		AddUnique(Result, { 151144794, "UnkBrand" });
		AddUnique(Result, { 2996933560, "UnkBrand" });
		AddUnique(Result, { 1089369430, "UnkBrand" });
		AddUnique(Result, { 1030991796, "UnkBrand" });
		AddUnique(Result, { 3623940837, "UnkBrand" });
		AddUnique(Result, { 1421869450, "UnkBrand" });
		AddUnique(Result, { 1453957933, "UnkLang" });
		AddUnique(Result, { 369561468, "UnkLang" });
		AddUnique(Result, { 3352927142, "UnkLang" });
		AddUnique(Result, { 308575932, "UnkLang" });
		AddUnique(Result, { 32660226, "UnkLang" });
		return Result;
	}

	const NameKey* GetKey(const std::vector<NameKey>& keys, uint32_t key)
	{
		for (const NameKey& knownKey : keys)
		{
			if (knownKey.Key == key)
			{
				return &knownKey;
			}
		}

		return nullptr;
	}
}

bool combinationUtil(char arr[], char data[], int start, int end, int index, int r, uint32_t key)
{
	// Current combination is ready 
	// to be printed, print it  
	if (index == r)
	{
		auto arse = GetStringKey(data);
		return arse.Key == key;
	}

	// replace index with all possible  
	// elements. The condition "end-i+1 >= r-index" 
	// makes sure that including one element  
	// at index will make a combination with  
	// remaining elements at remaining positions  
	for (int i = start; i <= end &&
		end - i + 1 >= r - index; i++)
	{
		data[index] = arr[i];
		if (combinationUtil(arr, data, i, end, index + 1, r, key))
			return true;
	}

	return false;
}

bool IncOffset(int offset[], int len, int idx, int maxOffset, char buff[], char arse[])
{
	if (idx == len)
		return false;

	if (++offset[idx] >= maxOffset)
	{
		offset[idx] = 0;
		buff[idx] = arse[offset[idx]];
		return IncOffset(offset, len, idx + 1, maxOffset, buff, arse);
	}
	else
	{
		buff[idx] = arse[offset[idx]];
	}

	return true;
}

SK::String FindText(uint32_t key)
{
	const int MaxLen = 20;
	const int NumChars = 64;
	char AllowedChars[NumChars] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_', ' ' };
	char buff[MaxLen + 1];
	int Offset[MaxLen];

	for (int i = 1; i < MaxLen; ++i)
	{
		for (int k = 0; k < i; ++k)
		{
			Offset[k] = 0;
			buff[k] = 'a';
		}
		buff[i] = 0;

		do
		{
			if (GetStringKey(buff).Key == key)
			{
				SK::Debug::Print(buff);
			}

		} while (IncOffset(Offset, i, 0, NumChars, buff, AllowedChars));
	}
	

	/*for (int i = 1; i < MaxLen; ++i)
	{
		buff[i] = 0;
		if (combinationUtil(AllowedChars, buff, 0, 62, 0, i, key))
		{
			return buff;
		}
	}*/

	return "FuckingShite";
}

void Tests::VestelFile()
{
	FileStream FS(Path(FilePath), FileMode::Read);
	FS.Endianness(Endian::Little);

	auto KnownKeys = GetKnownKeys();
	
	uint8_t HeaderBytes[48];
	FS.ReadData(HeaderBytes, 0, 48);

	ProfileEntry TopLevel;
	FS.Read(TopLevel);

	auto Cake = GetStringKey("Game");
	std::vector<ProfileEntry*> UnknownKeys;
	ProfileEntry* ProfileEntryList = new ProfileEntry[TopLevel.Count];
	std::vector<int> TabCounts;
	for (uint32_t i = 0; i < TopLevel.Count; ++i)
	{
		ProfileEntryList[i].FileOffset = FS.Position();
		FS.Read(ProfileEntryList[i]);

		if (ProfileEntryList[i].Key == Cake.Key)
		{
			//_asm { int 3 }
		}

		SK::String Tabses = "";
		for (int shit : TabCounts)
			Tabses.Concat("  ");

		auto shit = GetKey(KnownKeys, ProfileEntryList[i].Key);
		if (!shit)
		{
			AddUnique(UnknownKeys, &ProfileEntryList[i]);
			auto entry = &ProfileEntryList[i];
			SK::Debug::Print(Tabses, entry->Key, ": ", entry->Value, "[", entry->FileOffset + 12, "]");
		}
		else
		{
			auto entry = &ProfileEntryList[i];
			SK::Debug::Print(Tabses, shit->Name, ": ", entry->Value, "[", entry->FileOffset + 12, "]");
		}

		if (ProfileEntryList[i].Unknown == 1 && ProfileEntryList[i].Count > 0)
			TabCounts.push_back(ProfileEntryList[i].Count + 1);
		for (int cock = TabCounts.size() - 1; cock >= 0; --cock)
		{
			if (--TabCounts[cock] == 0)
			{
				TabCounts.pop_back();
			}
		}
	}

	FS.Close();

	//std::sort(UnknownKeys.begin(), UnknownKeys.end(), [](ProfileEntry* a, ProfileEntry* b)
	//{
	//	return a->FileOffset < b->FileOffset;
	//});

	//for (ProfileEntry* entry : UnknownKeys)
	//{
	//	auto Shit = FindText(entry->Key);
	//	SK::Debug::Print(Shit);
	//	break;
	////	SK::Debug::Print(entry->Key, ": ", entry->Unknown, ": ", entry->Count, ": ", entry->Value, ": ", entry->UniqueId, " [", entry->FileOffset, "]");
	//}

	delete [] ProfileEntryList;
}

#include "Tests.h"
#include <SKSystem/IO/FileStream.h>
#include <SKSystem/IO/MemoryStream.h>
#include <SKSystem/Debug.h>
#include <SKSystem/Time/Clocks.h>
#include <SKSystem/Threading/ThreadPool.h>
#include <algorithm>

using namespace SK;
using namespace SK::IO;

namespace
{
	int AdaptCoeff1[] = { 256, 512, 0, 192, 240, 460, 392 };
	int AdaptCoeff2[] = { 0, -256, 0, 64, 0, -208, -232 };
}

void GetValues(int delta, int opTable, int values[4])
{
	if (opTable == 0)
	{
		int a = delta & 0x07;
		int b = (delta & 0x38) >> 3;

		values[0] = a << 1;
		values[1] = ~(a << 1);
		values[2] = b;
		values[3] = ~(b);
	}
	else if (opTable == 1)
	{
		int a = delta & 0x07;
		int b = (delta & 0x38) >> 3;

		values[0] = a << 2;
		values[1] = ~(a << 2);
		values[2] = b << 1;
		values[3] = ~(b << 1);
	}
	else if (opTable == 2)
	{
		values[0] = delta;
		values[1] = ~delta;
		values[2] = delta / 3;
		values[3] = ~(delta / 3);
	}
	else if (opTable == 3)
	{
		values[0] = delta;
		values[1] = ~delta;
		values[2] = delta >> 2;
		values[3] = ~(delta >> 2);
	}
}

int FindClosest(int target, int value, int opTable, int& op)
{
	int Ix = 0;
	int Best = 255;
	int Values[4];
	GetValues(value, opTable, Values);

	for (int i = 0; i < 4; ++i)
	{
		int Delta = abs(Values[i] - target);
		if (Delta < Best)
		{
			Best = Delta;
			Ix = i;
		}
	}

	op = Ix;
	return Values[Ix];
}

int Clamp255(int a)
{
	a = a > 255 ? 255 : a;
	return a < 0 ? 0 : a;
}

int Clamp127(int a)
{
	a = a > 127 ? 127 : a;
	return a < -128 ? -128 : a;
}

float DRScale = 1.0f / 0.5;

uint8_t ConvertTo8(int16_t sample)
{
	float fSample = roundf(sample / 256.0f) + 128.0f;
	return static_cast<uint8_t>(fSample);
}

uint8_t Filter(uint8_t sample)
{
	//if (sample == 127 || sample == 129)
	//	return 128;

	return sample;
}

int16_t CalcPrediction(int16_t delta1, int16_t delta2, int8_t coeff1, int8_t coeff2, int8_t coeff3)
{
	int16_t div = coeff3 * 2;
	return (((delta1 * coeff1) - (delta2 * coeff2)) + coeff3) / div;
}

void LoadCompressedSnd(SK::IO::Path fileName, int BlockSize, int ChunkSize, SK::IO::Stream& outputBuffer)
{
	//Copy input file to memory buffer
	SK::IO::FileStream InputFile(fileName, SK::IO::FileMode::Read);
	SK::IO::MemoryStream InputBuffer;
	InputFile.CopyTo(InputBuffer);
	InputFile.Close();
	InputBuffer.Position(0);

	uint8_t PrevSample = 0;
	uint16_t Delta1 = 0;
	uint16_t Delta2 = 0;

	int8_t Coeff1, Coeff2, Coeff3;
	int NumBlocks = 0;
	InputBuffer.Read(PrevSample);

	while (InputBuffer.Position() != InputBuffer.Length())
	{
		if (NumBlocks == 0 && ChunkSize > 0)
		{
			uint8_t Coeffs;
			InputBuffer.Read(Coeffs);
			Coeff1 = (Coeffs & 0x07) - 4;
			Coeff2 = ((Coeffs & 0x38) >> 3) - 4;
			Coeff3 = ((Coeffs & 0xC0) >> 6) + 2;
		}
		uint8_t Delta, Compressed;
		InputBuffer.Read(Delta);

		int OpTable = (Delta & 0xC0) >> 6;
		Delta &= 0x3F;

		int Values[4];
		GetValues(Delta, OpTable, Values);

		for (int h = 0; h < BlockSize; ++h)
		{
			InputBuffer.Read(Compressed);

			int PredictedDelta = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
			PredictedDelta += Values[Compressed & 0x03];
			Delta2 = Delta1;
			Delta1 = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PredictedDelta);
			outputBuffer.Write(Filter(PrevSample));

			PredictedDelta = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
			Delta2 = Delta1;
			Delta1 = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PredictedDelta);
			outputBuffer.Write(Filter(PrevSample));

			PredictedDelta = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
			PredictedDelta += Values[(Compressed & 0x0C) >> 2];
			Delta2 = Delta1;
			Delta1 = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PredictedDelta);
			outputBuffer.Write(Filter(PrevSample));

			PredictedDelta = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
			Delta2 = Delta1;
			Delta1 = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PredictedDelta);
			outputBuffer.Write(Filter(PrevSample));

			PredictedDelta = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
			PredictedDelta += Values[(Compressed & 0x30) >> 4];
			Delta2 = Delta1;
			Delta1 = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PredictedDelta);
			outputBuffer.Write(Filter(PrevSample));

			PredictedDelta = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
			Delta2 = Delta1;
			Delta1 = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PredictedDelta);
			outputBuffer.Write(Filter(PrevSample));

			PredictedDelta = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
			PredictedDelta += Values[(Compressed & 0xC0) >> 6];
			Delta2 = Delta1;
			Delta1 = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PredictedDelta);
			outputBuffer.Write(Filter(PrevSample));

			PredictedDelta = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
			Delta2 = Delta1;
			Delta1 = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PredictedDelta);
			outputBuffer.Write(Filter(PrevSample));
		}

		if (ChunkSize > 0 && ++NumBlocks == ChunkSize)
			NumBlocks = 0;
	}

	InputBuffer.Close();
}

bool FindBestDelta(int OpTable, int16_t PrevDelta[2], uint8_t PrevSample, uint8_t* ChunkSamples, int8_t coeff1, int8_t coeff2, int8_t coeff3, int BlockSize, float& bestError, int& delta)
{
	bool Better = false;
	for (int k = 0; k < 64; ++k)
	{
		__declspec (align(16)) int OpTableValues[4];
		GetValues(k, OpTable, OpTableValues);
		
		__m128i Values = _mm_load_si128((__m128i*)OpTableValues);

		int S = PrevSample;
		int D = PrevDelta[0];
		int D2 = PrevDelta[1];
		int Op = 0;

		float Error = 0;
		for (int h = 0; h < BlockSize; ++h)
		{
			uint8_t BestOpDelta = 255;
			int DeltaDelta = 0;
			int ActualDelta = ChunkSamples[h] - S;
			int PredictedDelta = CalcPrediction(D, D2, coeff1, coeff2, coeff3);

			__m128i Target = _mm_set1_epi32(ActualDelta - PredictedDelta);
			__m128i OpDelta = _mm_abs_epi32(_mm_sub_epi32(Target, Values));
			OpDelta = _mm_or_si128(OpDelta, _mm_set_epi16(0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00));
			int PacketBest = _mm_cvtsi128_si32(_mm_minpos_epu16(OpDelta));
			int Ix = (PacketBest & 0xF0000) >> 17;
			DeltaDelta = OpTableValues[Ix];

			D2 = D;
			D = PredictedDelta + DeltaDelta;
			S = Clamp255(S + D);
			Error += (S - ChunkSamples[h]) * (S - ChunkSamples[h]);
			if (Error > bestError)
				break;
		}

		if (Error < bestError)
		{
			bestError = Error;
			delta = k;
			Better = true;
		}
	}

	return Better;
}

void FindCoeffs(SK::IO::Stream& stream, uint8_t prevSample, int16_t prevDelta[2], uint32_t numSamples, int8_t& out_Coeff1, int8_t& out_Coeff2, int8_t& out_Coeff3)
{
	uint64_t StartPos = stream.Position();
	int16_t* Samples16 = new int16_t[numSamples];
	stream.ReadData(reinterpret_cast<uint8_t*>(Samples16), 0, numSamples * sizeof(int16_t));
	stream.Position(StartPos);

	uint8_t* Samples = new uint8_t[numSamples];
	for (int i = 0; i < numSamples; ++i)
		Samples[i] = ConvertTo8(Samples16[i]);
	delete[] Samples16;

	float CoeffError = FLT_MAX;
	for (int Coeff1 = -4; Coeff1 < 4; ++Coeff1)
	{
		for (int Coeff2 = -4; Coeff2 < 4; ++Coeff2)
		{
			for (int Coeff3 = 2; Coeff3 < 6; ++Coeff3)
			{
				float Error = 0.0f;

				int16_t Delta1 = prevDelta[0];
				int16_t Delta2 = prevDelta[1];
				uint8_t PrevSample = Samples[0];

				for (int i = 0; i < numSamples; i += 2)
				{
					int16_t DeltaPrediction = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
					Delta2 = Delta1;
					Delta1 = Samples[i] - PrevSample;
					PrevSample = Samples[i];

					float DeltaDelta = DeltaPrediction - Delta1;
					Error += DeltaDelta * DeltaDelta;
					if (Error > CoeffError)
						break;

					DeltaPrediction = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
					Delta2 = Delta1;
					Delta1 = DeltaPrediction;
					PrevSample = PrevSample + DeltaPrediction;
				}

				if (Error < CoeffError)
				{
					CoeffError = Error;
					out_Coeff1 = Coeff1;
					out_Coeff2 = Coeff2;
					out_Coeff3 = Coeff3;
				}
			}
		}
	}

	delete[] Samples;
}

void Tests::PCMTest()
{
	const int BlockBytes = 3;
	const int BlockSampleSize = 8 * BlockBytes;
	const int ChunkSize = 8;

	SK::IO::FileStream FS(SK::String("D:/PCMTest/Original/Lucifer_16.22.raw"), SK::IO::FileMode::Read);
	SK::IO::MemoryStream OUT;

	SK::IO::MemoryStream IN;
	FS.CopyTo(IN);
	IN.Position(0);
	FS.Close();

	static double RMSError = 0;
	int NumErrs = 0;

	uint32_t Length = static_cast<uint32_t>(IN.Length());
	int16_t* ChunkSamples16 = new int16_t[BlockSampleSize];
	uint8_t* ChunkSamples = new uint8_t[BlockSampleSize];

	int8_t Coeff1, Coeff2, Coeff3;
	int NumBlocksInChunk = 0;
	uint8_t PrevSample;
	int16_t PrevDelta[2] = { 0 };
	int OpTableCounts[4] = { 0,0,0,0 };
	int OpTableThingCounts[4][4] = { 0 };

	int16_t FirstSample = 0;
	IN.Read(FirstSample);
	IN.Position(0);
	PrevSample = ConvertTo8(FirstSample);

	OUT.Write(PrevSample);

	while(IN.Position() < IN.Length())
	{
		if (NumBlocksInChunk == 0 && ChunkSize > 0)
		{
			FindCoeffs(IN, PrevSample, PrevDelta, BlockSampleSize * ChunkSize, Coeff1, Coeff2, Coeff3);

			IN.ReadData(reinterpret_cast<uint8_t*>(ChunkSamples16), 0, BlockSampleSize * sizeof(int16_t));
			for (int s = 0; s < BlockSampleSize; ++s)
				ChunkSamples[s] = ConvertTo8(ChunkSamples16[s]);

			uint8_t Coeffs = (Coeff1 + 4) | ((Coeff2 + 4) << 3) | ((Coeff3 - 2) << 6);
			OUT.Write(Coeffs);
		}
		else
		{
			IN.ReadData(reinterpret_cast<uint8_t*>(ChunkSamples16), 0, BlockSampleSize * sizeof(int16_t));
			for (int s = 0; s < BlockSampleSize; ++s)
				ChunkSamples[s] = ConvertTo8(ChunkSamples16[s]);
		}

		bool Predict = true;

		int OpTable = 0;

		int BestDelta = 0;
		float BestError = FLT_MAX;

		if (FindBestDelta(0, PrevDelta, PrevSample, ChunkSamples, Coeff1, Coeff2, Coeff3, BlockSampleSize, BestError, BestDelta))
			OpTable = 0;

		if (FindBestDelta(1, PrevDelta, PrevSample, ChunkSamples, Coeff1, Coeff2, Coeff3, BlockSampleSize, BestError, BestDelta))
			OpTable = 1;

		if (FindBestDelta(2, PrevDelta, PrevSample, ChunkSamples, Coeff1, Coeff2, Coeff3, BlockSampleSize, BestError, BestDelta))
			OpTable = 2;

		if (FindBestDelta(3, PrevDelta, PrevSample, ChunkSamples, Coeff1, Coeff2, Coeff3, BlockSampleSize, BestError, BestDelta))
			OpTable = 3;

		RMSError += BestError;
		NumErrs += BlockSampleSize;

		++OpTableCounts[OpTable];

		uint8_t Flags = OpTable << 6;
		OUT.Write(static_cast<uint8_t>(BestDelta | Flags));

		for (int h = 0; h < BlockBytes; ++h)
		{
			uint8_t Compressed = 0;
			int Op = 0;
			int Ix = h * 8;
			int PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Coeff1, Coeff2, Coeff3);
			int ActualDelta = ChunkSamples[Ix + 0] - PrevSample;
			int Delta = FindClosest(ActualDelta - PredictedDelta, BestDelta, OpTable, Op);
			PrevDelta[1] = PrevDelta[0];
			PrevDelta[0] = PredictedDelta + Delta;
			PrevSample = Clamp255(PrevSample + PrevDelta[0]);
			Compressed |= Op;


			++OpTableThingCounts[OpTable][Op];

			PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Coeff1, Coeff2, Coeff3);
			PrevDelta[1] = PrevDelta[0];
			PrevDelta[0] = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PrevDelta[0]);

			PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Coeff1, Coeff2, Coeff3);
			ActualDelta = ChunkSamples[Ix + 2] - PrevSample;
			Delta = FindClosest(ActualDelta - PredictedDelta, BestDelta, OpTable, Op);
			PrevDelta[1] = PrevDelta[0];
			PrevDelta[0] = PredictedDelta + Delta;
			PrevSample = Clamp255(PrevSample + PrevDelta[0]);
			Compressed |= Op << 2;

			++OpTableThingCounts[OpTable][Op];

			PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Coeff1, Coeff2, Coeff3);
			PrevDelta[1] = PrevDelta[0];
			PrevDelta[0] = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PrevDelta[0]);

			PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Coeff1, Coeff2, Coeff3);
			ActualDelta = ChunkSamples[Ix + 4] - PrevSample;
			Delta = FindClosest(ActualDelta - PredictedDelta, BestDelta, OpTable, Op);
			PrevDelta[1] = PrevDelta[0];
			PrevDelta[0] = PredictedDelta + Delta;
			PrevSample = Clamp255(PrevSample + PrevDelta[0]);
			Compressed |= Op << 4;

			++OpTableThingCounts[OpTable][Op];

			PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Coeff1, Coeff2, Coeff3);
			PrevDelta[1] = PrevDelta[0];
			PrevDelta[0] = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PrevDelta[0]);

			PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Coeff1, Coeff2, Coeff3);
			ActualDelta = ChunkSamples[Ix + 6] - PrevSample;
			Delta = FindClosest(ActualDelta - PredictedDelta, BestDelta, OpTable, Op);
			PrevDelta[1] = PrevDelta[0];
			PrevDelta[0] = PredictedDelta + Delta;
			PrevSample = Clamp255(PrevSample + PrevDelta[0]);
			Compressed |= Op << 6;

			++OpTableThingCounts[OpTable][Op];

			PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Coeff1, Coeff2, Coeff3);
			PrevDelta[1] = PrevDelta[0];
			PrevDelta[0] = PredictedDelta;
			PrevSample = Clamp255(PrevSample + PrevDelta[0]);

			OUT.Write(Compressed);
		}

		if (ChunkSize > 0 && ++NumBlocksInChunk == ChunkSize)
			NumBlocksInChunk = 0;
	}

	float TotalUsages = OpTableCounts[0] + OpTableCounts[1] + OpTableCounts[2] + OpTableCounts[3];

	static float Usages[4] =
	{
		OpTableCounts[0] / TotalUsages,
		OpTableCounts[1] / TotalUsages,
		OpTableCounts[2] / TotalUsages,
		OpTableCounts[3] / TotalUsages,
	};

	static float OpUsages[4][4];
	for (int i = 0; i < 4; ++i)
	{
		float TotalUsages = OpTableThingCounts[i][0] + OpTableThingCounts[i][1] + OpTableThingCounts[i][2] + OpTableThingCounts[i][3];
		OpUsages[i][0] = OpTableThingCounts[i][0] / TotalUsages;
		OpUsages[i][1] = OpTableThingCounts[i][1] / TotalUsages;
		OpUsages[i][2] = OpTableThingCounts[i][2] / TotalUsages;
		OpUsages[i][3] = OpTableThingCounts[i][3] / TotalUsages;
	}

	RMSError = sqrt(RMSError / NumErrs);
	
	SK::IO::FileStream CompressedFile(SK::String("D:/PCMTest/Compressed/Lucifer_L_SKPCM.raw"), SK::IO::FileMode::Truncate);
	OUT.CopyTo(CompressedFile);
	CompressedFile.Close();
	OUT.Close();

	SK::IO::MemoryStream PCM;
	LoadCompressedSnd("D:/PCMTest/Compressed/Lucifer_L_SKPCM.raw", BlockBytes, ChunkSize, PCM);
	SK::IO::FileStream OutputFile(SK::String("D:/PCMTest/Lucifer_L_Test.raw"), SK::IO::FileMode::Truncate);
	PCM.CopyTo(OutputFile);
	OutputFile.Close();
	PCM.Close();

	return;
}
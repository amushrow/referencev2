#include <SKSystem/Threading/ThreadPool.h>
#include <SKSystem/Threading/Thread.h>
#include <SKSystem/Debug.h>
#include "Tests.h"

using namespace SK;
using namespace SK::Threading;

namespace
{
	int DoSomethingElse()
	{
		std::thread::id hmm = std::this_thread::get_id();
		Debug::Print("Doing Something Else on thread: ", hmm);

		return 72;
	}

	void DoSomething(ThreadId thread)
	{
		std::thread::id hmm = std::this_thread::get_id();
		Debug::Print("Doing Something on thread: ", hmm);
		Sleep(Time::Milliseconds(250));
		int Val = InvokeEx(thread, Time::Milliseconds(500), DoSomethingElse);
		Debug::Print("Value is: ", Val);
	}
}

void Tests::Thread()
{
	std::thread::id hmm = std::this_thread::get_id();
	Debug::Print("Main Thread Id: ", hmm);

	SK::Threading::Thread ThreadFunc(DoSomething, GetThreadId());
	
	Time::TimePoint Start = Time::Now();
	while ((Time::Now() - Start) < Time::Milliseconds(5000))
	{
		Dispatch();
		Yield();
	}

	Debug::Print("Main Thread Finished");
	ThreadFunc.Join();
}

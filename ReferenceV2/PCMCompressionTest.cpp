﻿#include "Tests.h"
#include <SKSystem/IO/FileStream.h>
#include <SKSystem/IO/MemoryStream.h>
#include <SKSystem/Debug.h>
#include <SKSystem/Time/Clocks.h>
#include <SKSystem/Threading/ThreadPool.h>
#include <algorithm>

#include <SKSystem/Memory.h>
#include <SKSystem/Text/Encoding.h>

extern void FuckThis();
extern uint32_t FuckThat(int optable, int blocks, int threads, int coeff1, int coeff2, int delta, int delta2, uint8_t* samples, int prevSample, int prevDelta, int prevDelta2, float bestError, float& out_error, int& out_delta, int& out_optable);
extern bool FuckAllOfIt();

using namespace SK;
using namespace SK::IO;

#define DEBUG_USAGE

//SKTD: Still have 2 bits spare per chunk
#define FindBestDelta FindBestDelta_SIMD
#define FindCoeffs FindCoeffs_C

template<int base, int length>
bool ThingyNext(int value[length])
{
	int cur = 0;
	while (++value[cur] >= base)
	{
		value[cur++] = 0;
		if (cur >= length)
			return false;
	}

	return true;
}

namespace
{
	//Stupid Low: 32, 4,  1
	//Very Low:   3,  8,  1
	//Low:        24, 12, 2
	//High:       3,  64, 2
	const int BytesPerBlock = 3;
	const int BlocksPerChunk = 8;
	const int BitsPerSample = 1;

	const int SamplesPerBlock = (8 / BitsPerSample) * BytesPerBlock;
	const int SamplesPerChunk = SamplesPerBlock * BlocksPerChunk;

	SK::Threading::ThreadPool _ThreadPool(15);
	std::mutex _BestErrorMutex;
}

void GetValues(int delta, int opTable, int values[4])
{
	int Table[8] = { 0,2,3,4,5,7,8,11 };

	if (BitsPerSample == 2)
	{
		if (opTable == 0)
		{
			int a = delta & 0x07;
			int b = (delta & 0x38) >> 3;

			values[0] = a << 1;
			values[1] = ~(a << 1);
			values[2] = b;
			values[3] = ~(b);
		}
		else if (opTable == 1)
		{
			int a = delta & 0x07;
			int b = (delta & 0x38) >> 3;

			values[0] = a << 2;
			values[1] = ~(a << 2);
			values[2] = b << 1;
			values[3] = ~(b << 1);
		}
		else if (opTable == 2)
		{
			values[0] = delta;
			values[1] = ~delta;
			values[2] = delta / 3;
			values[3] = ~(delta / 3);
		}
		else if (opTable == 3)
		{
			values[0] = delta;
			values[1] = ~delta;
			values[2] = delta >> 2;
			values[3] = ~(delta >> 2);
		}
	}
	else if (BitsPerSample == 1)
	{
		int a = Table[delta & 0x07];
		int b = Table[(delta & 0x38) >> 3];

		// Set 1
		if (opTable == 0)
		{
			values[0] = a;
			values[1] = ~b;
		}
		else if (opTable == 1)
		{
			values[0] = (a << 1);
			values[1] = ~(b << 1);

		}
		else if (opTable == 2)
		{
			values[0] = (a << 1);
			values[1] = ~(b * 3);
		}
		else if (opTable == 3)
		{
			values[0] = (a * 3);//sktd
			values[1] = ~(b << 1);
		}

		values[2] = values[0];
		values[3] = values[1];
	}
}

int FindClosest(int target, int value, int opTable, int& op)
{
	int Ix = 0;
	uint32_t Best = -1;
	int Values[4];
	GetValues(value, opTable, Values);

	const int MaxIndex = BitsPerSample == 1 ? 2 : 4;
	for (int i = 0; i < MaxIndex; ++i)
	{
		uint32_t Delta = abs(Values[i] - target);
		if (Delta <= Best)
		{
			Best = Delta;
			Ix = i;
		}
	}

	op = Ix;
	return Values[Ix];
}

int Clamp255(int a)
{
	if (a > 255)
		return 255;
	else if (a < 0)
		return 0;
	return a;
}

float Clamp255(float a)
{
	if (a > 255)
		return 255.0f;
	else if (a < 0)
		return 0.0f;
	return a;
}

uint8_t ConvertTo8(int16_t sample)
{
	float fSample = roundf(sample / 256.0f) + 128.0f;
	return static_cast<uint8_t>(fSample);
}

int CalcPrediction(int delta1, int delta2, int coeff1, int coeff2)
{
	return (((delta1 * coeff1) + (delta2 * coeff2)) >> 3);
}

void LoadCompressedSnd(SK::IO::Path fileName, SK::IO::Stream& outputBuffer)
{
	//Copy input file to memory buffer
	SK::IO::FileStream InputFile(fileName, SK::IO::FileMode::Read);
	SK::IO::MemoryStream InputBuffer;
	InputFile.CopyTo(InputBuffer);
	InputFile.Close();
	InputBuffer.Position(0);

	int PrevSample = 0;
	int Delta1 = 0;
	int Delta2 = 0;

	int Coeff1 = 0;
	int Coeff2 = 0;
	int Coeff3 = 0;

	int NumBlocks = 0;
	int T = 0;
	uint8_t PrevSample8 = 0;
	InputBuffer.Read(PrevSample8);
	PrevSample = PrevSample8;

	while (InputBuffer.Position() != InputBuffer.Length())
	{
		if (NumBlocks == 0 && BlocksPerChunk > 0)
		{
			uint8_t Coeffs;
			InputBuffer.Read(Coeffs);
			Coeff1 = (Coeffs & 0x0F) - 8;
			Coeff2 = ((Coeffs & 0xF0) >> 4) - 8;
			Coeff3 = 0;
		}
		uint8_t Delta, Compressed;
		InputBuffer.Read(Delta);

		int OpTable = (Delta & 0xC0) >> 6;
		Delta &= 0x3F;

		int Values[4];
		GetValues(Delta, OpTable, Values);

		for (int h = 0; h < BytesPerBlock; ++h)
		{
			InputBuffer.Read(Compressed);

			if (BitsPerSample == 2)
			{
				for (int i = 0; i < 4; ++i)
				{
					int PredictedDelta = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2);
					PredictedDelta += Values[(Compressed >> (2*i)) & 0x03];
					Delta2 = Delta1;
					Delta1 = PredictedDelta;
					PrevSample = Clamp255(PrevSample + PredictedDelta);
					outputBuffer.Write(static_cast<uint8_t>(PrevSample));
				}
			}
			else if (BitsPerSample == 1)
			{
				for (int i = 0; i < 8; ++i)
				{
					int PredictedDelta = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2);
					PredictedDelta += Values[(Compressed >> i) & 0x01];
					Delta2 = Delta1;
					Delta1 = PredictedDelta;
					PrevSample = Clamp255(PrevSample + PredictedDelta);
					outputBuffer.Write(static_cast<uint8_t>(PrevSample));
				}
			}
		}

		if (BlocksPerChunk > 0 && ++NumBlocks == BlocksPerChunk)
			NumBlocks = 0;
	}

	InputBuffer.Close();
}

template<int NUM_OPTIONS, int NUM_SAMPLES>
class SampleThingy
{
public:
	SampleThingy(uint8_t* samples, int coeff1, int coeff2, int coeff3, int deltaValues[NUM_OPTIONS])
		: m_samples(samples), m_coeff1(coeff1), m_coeff2(coeff2), m_coeff3(coeff3)
		, m_bestError(FLT_MAX)
		, m_deltaValues(deltaValues)
	{
		for (int i = 0; i < NUM_SAMPLES; ++i)
			m_bestValues[i] = m_workingValues[i] = 0;
	}

	bool Start(int prevSample, int delta, int delta2, float error)
	{
		m_bestError = error;
		ThingAlt(0, prevSample, delta, delta2);
		return m_bestError < error;
	}

	int* GetBestValues()
	{
		return m_bestValues;
	}

	float GetBestError()
	{
		return m_bestError;
	}

private:
	struct Node
	{
		uint8_t op;
		int delta2;
		int delta;
		uint8_t sample;
		float error;
	};

	void ThingAlt(int shit, int prevSample, int delta, int delta2)
	{
		const uint32_t MaxOps = 1 << NUM_SAMPLES;
		const int StackSize = std::min(NUM_SAMPLES - 1, 32);
		_declspec(align(32)) Node Stack[std::max(StackSize, 1)];

		uint8_t s = prevSample;
		int d = delta;
		int d2 = delta2;
		float error = 0;

		int sid = 0;
		uint32_t ops = 0;

		const int Coeff1 = m_coeff1;
		const int Coeff2 = m_coeff2;

		while (true)
		{
			//Stack
			for (; sid < (NUM_SAMPLES - 1); ++sid)
			{
				int op = (ops >> ((NUM_SAMPLES - 1) - sid)) & 1;
				int Pred = CalcPrediction(d, d2, Coeff1, Coeff2);
				d2 = d;
				d = Pred + m_deltaValues[op];
				s = Clamp255(s + d);
				
				error += (m_samples[sid] - s) * (m_samples[sid] - s);

				if (error >= m_bestError)
					break;

				int stackId = sid - (NUM_SAMPLES - StackSize - 1);
				if (stackId >= 0)
				{
					Stack[stackId].delta = d;
					Stack[stackId].delta2 = d2;
					Stack[stackId].sample = s;
					Stack[stackId].error = error;
				}
			}

			//Last
			if (error < m_bestError)
			{
				int op = (ops >> ((NUM_SAMPLES - 1) - sid)) & 1;
				int Pred = CalcPrediction(d, d2, Coeff1, Coeff2);
				d2 = d;
				d = Pred + m_deltaValues[op];
				s = Clamp255(s + d);
				
				error += (m_samples[sid] - s) * (m_samples[sid] - s);
			}

			if (error >= m_bestError)
			{
				uint32_t shift = (NUM_SAMPLES - 1) - sid;
				uint32_t mask = ~((1 << shift) - 1);
				ops = ((ops & mask) + (1 << shift)) - 1;
			}
			else
			{
				m_bestError = error;
				for (int h = 0; h < SamplesPerBlock; ++h)
					m_bestValues[h] = (ops >> ((NUM_SAMPLES - 1) - h)) & 1;
			}

			sid = NUM_SAMPLES - 1 - _tzcnt_u32(~ops);
			++ops;

			if (ops >= MaxOps)
				break;

			int stackId = sid - (NUM_SAMPLES - StackSize);
			if (stackId >= 0)
			{
				d = Stack[stackId].delta;
				d2 = Stack[stackId].delta2;
				s = Stack[stackId].sample;
				error = Stack[stackId].error;
			}
			else
			{
				sid = 0;
				s = prevSample;
				d = delta;
				d2 = delta2;
				error = 0;
			}
		}
	}

	void Thing(int shit, int prevSample, int delta, int delta2)
	{
#if 1
		_declspec(align(32)) Node Stack[NUM_SAMPLES];

		int predictedDelta = CalcPrediction(delta, delta2, m_coeff1, m_coeff2);

		int16_t d2 = delta;
		int16_t d = predictedDelta + m_deltaValues[0];
		uint8_t s = Clamp255(prevSample + d);
		int er = m_samples[0] - s;
		int e = (er*er);

		Stack[0].op = 0;
		Stack[0].delta2 = delta;
		Stack[0].delta = predictedDelta + m_deltaValues[1];
		Stack[0].sample = Clamp255(prevSample + Stack[0].delta);
		er = m_samples[0] - Stack[0].sample;
		Stack[0].error = static_cast<uint32_t>(er*er);

		int hopBack[NUM_SAMPLES+1];
		int numHops = 0;
		hopBack[numHops++] = 0;

		int Level = 0;
		while (true)
		{
			for (int jizz = Level; jizz < (NUM_SAMPLES - 1); ++jizz)
			{
				predictedDelta = CalcPrediction(d, d2, m_coeff1, m_coeff2);
				Stack[jizz + 1].op = 0;
				Stack[jizz + 1].delta2 = d;
				Stack[jizz + 1].delta = predictedDelta + m_deltaValues[1];
				Stack[jizz + 1].sample = Clamp255(s + Stack[jizz + 1].delta);
				er = m_samples[jizz + 1] - Stack[jizz + 1].sample;
				Stack[jizz + 1].error = e + (er*er);

				d2 = d;
				d = predictedDelta + m_deltaValues[0];
				s = Clamp255(s + d);
				er = m_samples[jizz + 1] - s;
				e += (er*er);
				hopBack[numHops++] = jizz + 1;

				if (e >= m_bestError)
					break;
			}
			
			if (e < m_bestError)
			{
				m_bestError = e;
				for (int i = 0; i < NUM_SAMPLES; ++i)
					m_bestValues[i] = Stack[i].op;
			}

			if (numHops != 0)
			{
				Level = hopBack[--numHops];
				while (Stack[Level].error >= m_bestError && numHops > 0)
					Level = hopBack[--numHops];
				
				Stack[Level].op = 1;
				d2 = Stack[Level].delta2;
				d = Stack[Level].delta;
				s = Stack[Level].sample;
				e = Stack[Level].error;
			}
			else
			{
				return;
			}
		}
#else
		if (shit == NUM_SAMPLES)
		{
			if (error < m_bestError)
			{
				for (int i = 0; i < NUM_SAMPLES; ++i)
					m_bestValues[i] = m_workingValues[i];

				m_bestError = error;
			}
			return;
		}
		else
		{
			int PredictedDelta = CalcPrediction(delta, delta2, m_coeff1, m_coeff2, m_coeff3);

			//for (int i = 0; i < NUM_OPTIONS; ++i)
			{
				int NewDelta2 = delta;
				int NewDelta[2] = { PredictedDelta + m_deltaValues[0], PredictedDelta + m_deltaValues[1] };
				int NewSample[2] = { Clamp255(prevSample + NewDelta[0]), Clamp255(prevSample + NewDelta[1]) };
				int SampleError[2] = { m_samples[shit] - NewSample[0], m_samples[shit] - NewSample[1] };

				uint32_t serror = static_cast<uint32_t>(SampleError[0] * SampleError[0]) + error;
				if (serror <= m_bestError)
				{
					m_workingValues[shit] = 0;
					Thing(shit + 1, NewSample[0], NewDelta[0], NewDelta2, serror);
				}
				
				serror = static_cast<uint32_t>(SampleError[1] * SampleError[1]) + error;
				if (serror <= m_bestError)
				{
					m_workingValues[shit] = 1;
					Thing(shit + 1, NewSample[1], NewDelta[1], NewDelta2, serror);
				}
			}
		}
#endif
	}

	void ThingBalls(int shit, int prevSample, int delta, int delta2, uint32_t error, uint64_t start, uint64_t end)
	{
		if (shit == NUM_SAMPLES)
		{
			if (error < m_bestError)
			{
				for (int i = 0; i < NUM_SAMPLES; ++i)
					m_bestValues[i] = m_workingValues[i];

				m_bestError = error;
			}
			return;
		}
		else
		{
			int PredictedDelta = CalcPrediction(delta, delta2, m_coeff1, m_coeff2, m_coeff3);

			//for (int i = 0; i < NUM_OPTIONS; ++i)
			{
				int NewDelta2 = delta;
				int NewDelta[2] = { PredictedDelta + m_deltaValues[0], PredictedDelta + m_deltaValues[1] };
				int NewSample[2] = { Clamp255(prevSample + NewDelta[0]), Clamp255(prevSample + NewDelta[1]) };
				int SampleError[2] = { m_samples[shit] - NewSample[0], m_samples[shit] - NewSample[1] };

				uint32_t serror = static_cast<uint32_t>(SampleError[0] * SampleError[0]) + error;
				if (serror <= m_bestError)
				{
					m_workingValues[shit] = 0;
					Thing(shit + 1, NewSample[0], NewDelta[0], NewDelta2, serror);
				}

				serror = static_cast<uint32_t>(SampleError[1] * SampleError[1]) + error;
				if (serror <= m_bestError)
				{
					m_workingValues[shit] = 1;
					Thing(shit + 1, NewSample[1], NewDelta[1], NewDelta2, serror);
				}
			}
		}
	}

	uint8_t* m_samples;
	int m_bestValues[NUM_SAMPLES];
	int m_workingValues[NUM_SAMPLES];

	float m_bestError;
	const int* const m_deltaValues;

	const int m_coeff1;
	const int m_coeff2;
	const int m_coeff3;
};

template<int NumVals>
class BestVals
{
public:
	BestVals()
	{
		for (int i = 0; i < NumVals; ++i)
			m_error[i] = -1;
		
		m_worstIndex = 0;
	}

	void PushVals(uint32_t error, int delta)
	{
		if (error < m_error[m_worstIndex])
		{
			m_error[m_worstIndex] = error;
			m_delta[m_worstIndex] = delta;

			uint32_t WorstError = error;
			for (int i = NumVals-1; i >= 0; --i)
			{
				if (m_error[i] > WorstError)
				{
					WorstError = m_error[i];
					m_worstIndex = i;
				}
			}
		}
	}

	const int* GetBestDeltas() const { return m_delta; }
	int        GetNumDeltas() const { return NumVals; }

private:
	uint32_t m_worstVal;
	int m_worstIndex;
	uint32_t m_error[NumVals];
	int m_delta[NumVals];
};

bool FindBestDelta_C(int OpTable, int PrevDelta[2], int PrevSample, uint8_t* ChunkSamples, int coeff1, int coeff2, int coeff3, float& bestError, int& delta, int out_OpValues[SamplesPerBlock], bool fuckMe = false)
{
	bool Better = false;

	if (!fuckMe || bestError == FLT_MAX)
	{
		for (int k = 0; k < 64; ++k)
		{
			int OpTableValues[4];
			GetValues(k, OpTable, OpTableValues);

			int S = PrevSample;
			int D = PrevDelta[0];
			int D2 = PrevDelta[1];


			int Ops[SamplesPerBlock] = { 0 };
			float Error = 0;
			for (int h = 0; h < SamplesPerBlock; ++h)
			{
				int DeltaDelta = 0;
				int ActualDelta = ChunkSamples[h] - S;
				int PredictedDelta = CalcPrediction(D, D2, coeff1, coeff2);

				int Target = ActualDelta - PredictedDelta;
				int OpDelta = abs(Target - OpTableValues[0]);
				DeltaDelta = OpTableValues[0];
				Ops[h] = 0;

				int OpDelta1 = abs(Target - OpTableValues[1]);
				if (OpDelta1 < OpDelta)
				{
					OpDelta = OpDelta1;
					DeltaDelta = OpTableValues[1];
					Ops[h] = 1;
				}

				int OpDelta2 = abs(Target - OpTableValues[2]);
				if (OpDelta2 < OpDelta)
				{
					OpDelta = OpDelta2;
					DeltaDelta = OpTableValues[2];
					Ops[h] = 2;
				}

				int OpDelta3 = abs(Target - OpTableValues[3]);
				if (OpDelta3 < OpDelta)
				{
					DeltaDelta = OpTableValues[3];
					Ops[h] = 3;
				}

				D2 = D;
				D = PredictedDelta + DeltaDelta;
				S = Clamp255(S + D);
				Error += static_cast<float>(S - ChunkSamples[h]) * static_cast<float>(S - ChunkSamples[h]);
				if (Error > bestError)
					break;
			}

			if (Error < bestError)
			{
				Better = true;
				bestError = Error;
				delta = k;

				for (int i = 0; i < SamplesPerBlock; ++i)
					out_OpValues[i] = Ops[i];
			}
		}
	}

	if (fuckMe)
	{
		for (int k = 0; k < 64; ++k)
		{
			int OpTableValues[4];
			GetValues(k, OpTable, OpTableValues);
			SampleThingy<2, SamplesPerBlock> Yearg(ChunkSamples, coeff1, coeff2, coeff3, OpTableValues);
			if (Yearg.Start(PrevSample, PrevDelta[0], PrevDelta[1], bestError))
			{
				Better = true;
				delta = k;
				bestError = Yearg.GetBestError();

				for (int i = 0; i < SamplesPerBlock; ++i)
					out_OpValues[i] = Yearg.GetBestValues()[i];
			}
		}
	}

	return Better;
}

bool FindBestDelta_SIMD(int OpTable, int PrevDelta[2], int PrevSample, uint8_t* ChunkSamples, int coeff1, int coeff2, int coeff3, float& bestError, int& delta, int out_OpValues[SamplesPerBlock], bool fuckMe = true)
{
	const float OriginalError = bestError;
	bool Force = false;
	if (bestError == FLT_MAX)
	{
		Force = true;
		const __m128i SIMDCoeff2 = _mm_set1_epi32(coeff2);
		const __m128i SIMDCoeff1 = _mm_set1_epi32(coeff1);

		for (int k = 0; k < 64; k += 4)
		{
			int OpTableValues[4][4];
			GetValues(k, OpTable, OpTableValues[0]);
			GetValues(k + 1, OpTable, OpTableValues[1]);
			GetValues(k + 2, OpTable, OpTableValues[2]);
			GetValues(k + 3, OpTable, OpTableValues[3]);

			const __m128 BestError = _mm_set1_ps(static_cast<float>(bestError));
			const __m128i Value0 = _mm_set_epi32(OpTableValues[0][0], OpTableValues[1][0], OpTableValues[2][0], OpTableValues[3][0]);
			const __m128i Value1 = _mm_set_epi32(OpTableValues[0][1], OpTableValues[1][1], OpTableValues[2][1], OpTableValues[3][1]);
			const __m128i Value2 = _mm_set_epi32(OpTableValues[0][2], OpTableValues[1][2], OpTableValues[2][2], OpTableValues[3][2]);
			const __m128i Value3 = _mm_set_epi32(OpTableValues[0][3], OpTableValues[1][3], OpTableValues[2][3], OpTableValues[3][3]);

			__m128i S = _mm_set1_epi32(PrevSample);
			__m128i D = _mm_set1_epi32(PrevDelta[0]);
			__m128i D2 = _mm_set1_epi32(PrevDelta[1]);
			__m128 Error = _mm_set1_ps(0);

			for (int h = 0; h < SamplesPerBlock; ++h)
			{
				int DeltaDelta = 0;
				__m128i CurrentSample = _mm_set1_epi32(ChunkSamples[h]);
				__m128i ActualDelta = _mm_sub_epi32(CurrentSample, S);

				__m128i PredictedDelta = _mm_add_epi32(_mm_mullo_epi32(D, SIMDCoeff1), _mm_mullo_epi32(D2, SIMDCoeff2));
				PredictedDelta = _mm_srai_epi32(PredictedDelta, 3);

				__m128i Target = _mm_sub_epi32(ActualDelta, PredictedDelta);
				__m128i OpDelta = _mm_abs_epi32(_mm_sub_epi32(Target, Value0));
				__m128i BestestValue = Value0;

				__m128i OpDelta2 = _mm_abs_epi32(_mm_sub_epi32(Target, Value1));
				__m128i Cmp = _mm_cmplt_epi32(OpDelta2, OpDelta);
				BestestValue = _mm_andnot_si128(Cmp, BestestValue);
				BestestValue = _mm_or_si128(BestestValue, _mm_and_si128(Value1, Cmp));
				OpDelta = _mm_min_epi32(OpDelta, OpDelta2);

				__m128i OpDelta3 = _mm_abs_epi32(_mm_sub_epi32(Target, Value2));
				Cmp = _mm_cmplt_epi32(OpDelta3, OpDelta);
				BestestValue = _mm_andnot_si128(Cmp, BestestValue);
				BestestValue = _mm_or_si128(BestestValue, _mm_and_si128(Value2, Cmp));
				OpDelta = _mm_min_epi32(OpDelta, OpDelta3);

				__m128i OpDelta4 = _mm_abs_epi32(_mm_sub_epi32(Target, Value3));
				Cmp = _mm_cmplt_epi32(OpDelta4, OpDelta);
				BestestValue = _mm_andnot_si128(Cmp, BestestValue);
				BestestValue = _mm_or_si128(BestestValue, _mm_and_si128(Value3, Cmp));

				D2 = D;
				D = _mm_add_epi32(PredictedDelta, BestestValue);
				S = _mm_add_epi32(S, D);

				__m128 E = _mm_cvtepi32_ps(_mm_sub_epi32(S, CurrentSample));
				Error = _mm_add_ps(Error, _mm_mul_ps(E, E));

				if (_mm_testc_ps(_mm_cmpgt_ps(Error, BestError), _mm_cvtepi32_ps(_mm_set1_epi32(~0))))
					break;
			}

			__declspec(align(16)) float Errors[4];
			_mm_store_ps(Errors, Error);

			if (Errors[3] < bestError && Errors[3] <= Errors[0] && Errors[3] <= Errors[1] && Errors[3] <= Errors[2])
			{
				if (Errors[3] < bestError)
				{
					bestError = Errors[3];
					delta = k;
				}
			}
			else if (Errors[2] < bestError && Errors[2] <= Errors[0] && Errors[2] <= Errors[1] && Errors[2] <= Errors[3])
			{
				if (Errors[2] < bestError)
				{
					bestError = Errors[2];
					delta = k + 1;
				}
			}
			else if (Errors[1] < bestError && Errors[1] <= Errors[0] && Errors[1] <= Errors[2] && Errors[1] <= Errors[3])
			{
				if (Errors[1] < bestError)
				{
					bestError = Errors[1];
					delta = k + 2;
				}
			}
			else if (Errors[0] < bestError && Errors[0] <= Errors[1] && Errors[0] <= Errors[2] && Errors[0] <= Errors[3])
			{
				if (Errors[0] < bestError)
				{
					bestError = Errors[0];
					delta = k + 3;
				}
			}
		}
	}

	//SKTD: Ugh
	if (Force)
		bestError = (bestError + 1) * 1.05f;

	for (int k = 0; k < 64; ++k)
	{
		int OpTableValues[4];
		GetValues(k, OpTable, OpTableValues);
		SampleThingy<2, SamplesPerBlock> Yearg(ChunkSamples, coeff1, coeff2, coeff3, OpTableValues);
		if (Yearg.Start(PrevSample, PrevDelta[0], PrevDelta[1], bestError))
		{
			delta = k;
			bestError = Yearg.GetBestError();
			for (int i = 0; i < SamplesPerBlock; ++i)
				out_OpValues[i] = Yearg.GetBestValues()[i];
		}
	}

	return bestError < OriginalError;
}


float CalcBlockError(uint8_t* chunkSamples, int coeff1, int coeff2, int coeff3, int prevDelta[2], int& prevSample, float maxError, bool elFantastico = false)
{
	float Error = 0;

	int OpTable = -1;
	int BestDelta = 0;
	int OpValues[SamplesPerBlock];
	float BestError = maxError;

	if (elFantastico)
	{
		if (FindBestDelta_SIMD(0, prevDelta, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta, OpValues))
			OpTable = 0;

		if (FindBestDelta_SIMD(1, prevDelta, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta, OpValues))
			OpTable = 1;

		if (FindBestDelta_SIMD(2, prevDelta, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta, OpValues))
			OpTable = 2;

		if (FindBestDelta_SIMD(3, prevDelta, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta, OpValues))
			OpTable = 3;
	}
	else
	{
		if (FindBestDelta_C(0, prevDelta, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta, OpValues, false))
			OpTable = 0;

		if (FindBestDelta_C(1, prevDelta, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta, OpValues, false))
			OpTable = 1;

		if (FindBestDelta_C(2, prevDelta, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta, OpValues, false))
			OpTable = 2;

		if (FindBestDelta_C(3, prevDelta, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta, OpValues, false))
			OpTable = 3;
	}
	
	if (OpTable == -1)
		return FLT_MAX;

	int Values[4];
	GetValues(BestDelta, OpTable, Values);

	for (int h = 0; h < BytesPerBlock; ++h)
	{
		uint8_t Compressed = 0;
		int Op = 0;

		if (BitsPerSample == 2)
		{
			int Ix = h * 4;

			for (int i = 0; i < 4; ++i)
			{
				int PredictedDelta = CalcPrediction(prevDelta[0], prevDelta[1], coeff1, coeff2);
				int Delta = Values[OpValues[Ix++]];
				prevDelta[1] = prevDelta[0];
				prevDelta[0] = PredictedDelta + Delta;
				prevSample = Clamp255(prevSample + prevDelta[0]);
			}
		}
		else if (BitsPerSample == 1)
		{
			int Ix = h * 8;

			for (int i = 0; i < 8; ++i)
			{
				int PredictedDelta = CalcPrediction(prevDelta[0], prevDelta[1], coeff1, coeff2);
				int Delta = Values[OpValues[Ix++]];
				prevDelta[1] = prevDelta[0];
				prevDelta[0] = PredictedDelta + Delta;
				prevSample = Clamp255(prevSample + prevDelta[0]);
			}
		}
	}

	return BestError;
}
/*
void FindCoeffs_SIMD(SK::IO::Stream& stream, int prevSample, int prevDelta[2], int& out_Coeff1, int& out_Coeff2, int& out_Coeff3)
{
	uint64_t StartPos = stream.Position();
	int16_t* Samples16 = new int16_t[SamplesPerChunk];
	stream.ReadData(reinterpret_cast<uint8_t*>(Samples16), 0, SamplesPerChunk * sizeof(int16_t));
	stream.Position(StartPos);

	int* Samples = new int[SamplesPerChunk];
	for (int i = 0; i < SamplesPerChunk; ++i)
		Samples[i] = ConvertTo8(Samples16[i]);
	delete[] Samples16;

	int CoeffCalcOrder[8] = { 0, -1, 1, -2, 2, -3, 3, -4 };
	double CoeffError = DBL_MAX;
	for (int i = 0; i < 8; ++i)
	{
		int Coeff1 = CoeffCalcOrder[i];
		for (int j = 0; j < 8; ++j)
		{
			int Coeff2 = CoeffCalcOrder[j];

			for (int Coeff3 = 0; Coeff3 < 4; ++Coeff3)
			{
				__m128i SIMDCoeff2 = _mm_set1_epi32(Coeff2);
				__m128i SIMDCoeff1 = _mm_set1_epi32(Coeff1);
				double Error = 0;

				//First four samples, need to be separate from the main loop because the first 2 deltas are passed
				// in as arguemnts
				{
					int Delta1 = prevDelta[0];
					int Delta2 = prevDelta[1];
					int PrevSample = prevSample;

					int DeltaPrediction = CalcPrediction(Delta1, Delta2, T++, Coeff1, Coeff2, Coeff3);
					Delta2 = Samples[0] - PrevSample;
					PrevSample = Samples[0];
					Error += static_cast<double>(DeltaPrediction - Delta2) * static_cast<double>(DeltaPrediction - Delta2);

					DeltaPrediction = CalcPrediction(Delta2, Delta1, T++, Coeff1, Coeff2, Coeff3);
					Delta1 = Samples[1] - PrevSample;
					PrevSample = Samples[1];
					Error += static_cast<double>(DeltaPrediction - Delta1) * static_cast<double>(DeltaPrediction - Delta1);

					DeltaPrediction = CalcPrediction(Delta1, Delta2, T++, Coeff1, Coeff2, Coeff3);
					Delta2 = Samples[2] - PrevSample;
					PrevSample = Samples[2];
					Error += static_cast<double>(DeltaPrediction - Delta2) * static_cast<double>(DeltaPrediction - Delta2);

					DeltaPrediction = CalcPrediction(Delta2, Delta1, T++, Coeff1, Coeff2, Coeff3);
					Delta1 = Samples[3] - PrevSample;
					PrevSample = Samples[3];
					Error += static_cast<double>(DeltaPrediction - Delta1) * static_cast<double>(DeltaPrediction - Delta1);
				}

				for (int k = 4; k < SamplesPerChunk; k += 4)
				{
					//Loading up these samples and getting the deltas could be quicker, but ¯\_(ツ)_/¯
					__m128i Sample = _mm_loadu_si128(reinterpret_cast<__m128i*>(&Samples[k - 0]));
					__m128i PrevSample1 = _mm_loadu_si128(reinterpret_cast<__m128i*>(&Samples[k - 1]));
					__m128i PrevSample2 = _mm_loadu_si128(reinterpret_cast<__m128i*>(&Samples[k - 2]));
					__m128i PrevSample3 = _mm_loadu_si128(reinterpret_cast<__m128i*>(&Samples[k - 3]));

					__m128i ActualDelta = _mm_sub_epi32(Sample, PrevSample1);
					__m128i Delta1 = _mm_sub_epi32(PrevSample1, PrevSample2);
					__m128i Delta2 = _mm_sub_epi32(PrevSample2, PrevSample3);

					//SKTD: CalcSample if BlocksPerChunk > 0
					__m128i Prediction = _mm_add_epi32(_mm_mullo_epi32(Delta1, SIMDCoeff1), _mm_mullo_epi32(Delta2, SIMDCoeff2));
					Prediction = _mm_srai_epi32(Prediction, 2);
					__m128 Errors = _mm_cvtepi32_ps(_mm_sub_epi32(Prediction, ActualDelta));
					Errors = _mm_mul_ps(Errors, Errors);
					Errors = _mm_hadd_ps(Errors, Errors);
					Errors = _mm_hadd_ps(Errors, Errors);

					Error += _mm_cvtss_f32(Errors);

					if (Error > CoeffError)
						break;
				}

				if (Error < CoeffError)
				{
					CoeffError = Error;
					out_Coeff1 = Coeff1;
					out_Coeff2 = Coeff2;
					out_Coeff3 = Coeff3;
				}
			}
		}
	}

	delete[] Samples;
}
*/
struct shitballs
{
	volatile float& CoeffError;
	float* BlockErrors;
	int& out_Coeff1;
	int& out_Coeff2;
	int& out_Coeff3;
	int* prevDelta;
	int prevSample;
	uint8_t* Samples;
};
int CoeffCalcOrder[16] = { 0, -1, 1, -2, 2, -3, 3, -4, 4, -5, 5, -6, 6, -7, 7, -8 };

float lerp(float a, float b, float t)
{
	return ((b - a) * t) + a;
}

//SKTD: Only need this shit when going nuts
void jizz(shitballs* b, int Coeff1, int Coeff2, bool elFantastico)
{
	{
		{
			//for (int Coeff3 = 0; Coeff3 < 4; ++Coeff3)
			int Coeff3 = 0;
			{
				float BlockErrors[BlocksPerChunk];
				float BlockErrorsCopy[BlocksPerChunk];
				float Error = 0;
				float CumulativeBlockTarget = std::ceilf(b->CoeffError * (0.75f / BlocksPerChunk));

				for (int i = 0; i < BlocksPerChunk; ++i)
				{
					BlockErrorsCopy[i] = b->BlockErrors[i];
				}
#if 1
				int D[2] = { b->prevDelta[0], b->prevDelta[1] };
				int S = b->prevSample;
				for (int B = 0; B < BlocksPerChunk; ++B)
				{
					CumulativeBlockTarget += BlockErrorsCopy[B];
					float TargetError = b->CoeffError - Error;
					if (elFantastico)
						TargetError = std::min(TargetError, CumulativeBlockTarget - Error);

					BlockErrors[B] = CalcBlockError(b->Samples + (B * SamplesPerBlock), Coeff1, Coeff2, Coeff3, D, S, TargetError, elFantastico);
					Error += BlockErrors[B];

					if (Error >= b->CoeffError)
						break;
				}
#else
				int Delta1 = b->prevDelta[0];
				int Delta2 = b->prevDelta[1];
				int PrevSample = b->prevSample;

				for (int k = 0; k < SamplesPerChunk; k += 4)
				{
					uint32_t Sample = *reinterpret_cast<uint32_t*>(&b->Samples[k]);
					int DeltaPrediction = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2);
					Delta2 = (Sample & 0xFF) - PrevSample;
					PrevSample = Sample & 0xFF;
					Error += (DeltaPrediction - Delta2) * (DeltaPrediction - Delta2);

					DeltaPrediction = CalcPrediction(Delta2, Delta1, Coeff1, Coeff2);
					Delta1 = (Sample >> 8 & 0xFF) - PrevSample;
					PrevSample = Sample >> 8 & 0xFF;
					Error += (DeltaPrediction - Delta1) * (DeltaPrediction - Delta1);

					DeltaPrediction = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2);
					Delta2 = (Sample >> 16 & 0xFF) - PrevSample;
					PrevSample = Sample >> 16 & 0xFF;
					Error += (DeltaPrediction - Delta2) * (DeltaPrediction - Delta2);

					DeltaPrediction = CalcPrediction(Delta2, Delta1, Coeff1, Coeff2);
					Delta1 = (Sample >> 24 & 0xFF) - PrevSample;
					PrevSample = Sample >> 24 & 0xFF;
					Error += (DeltaPrediction - Delta1) * (DeltaPrediction - Delta1);

					if (Error > b->CoeffError)
						break;
				}
#endif

				if (Error < b->CoeffError)
				{
					_BestErrorMutex.lock();
					if (Error < b->CoeffError)
					{
						b->CoeffError = Error;
						b->out_Coeff1 = Coeff1;
						b->out_Coeff2 = Coeff2;
						b->out_Coeff3 = Coeff3;

						for (int i = 0; i < BlocksPerChunk; ++i)
						{
							b->BlockErrors[i] = BlockErrors[i];
						}
					}
					_BestErrorMutex.unlock();
				}
			}
		}
	}
};

void FindCoeffs_C(SK::IO::Stream& stream, int prevSample, int prevDelta[2], int& out_Coeff1, int& out_Coeff2, int& out_Coeff3)
{
	uint64_t StartPos = stream.Position();
	int16_t* Samples16 = new int16_t[SamplesPerChunk];
	stream.ReadData(reinterpret_cast<uint8_t*>(Samples16), 0, SamplesPerChunk * sizeof(int16_t));
	stream.Position(StartPos);

	uint8_t* Samples = new uint8_t[SamplesPerChunk];
	for (int i = 0; i < SamplesPerChunk; ++i)
		Samples[i] = ConvertTo8(Samples16[i]);
	delete[] Samples16;

	SK::Threading::FutureGroup<void, 256> Tasks;

	const bool Threads = true;

	volatile float CoeffError = FLT_MAX;
	float BlockErrors[BlocksPerChunk];
	for (int i = 0; i < BlocksPerChunk; ++i)
		BlockErrors[i] = FLT_MAX;

	shitballs balls = { CoeffError, BlockErrors, out_Coeff1, out_Coeff2, out_Coeff3, prevDelta, prevSample, Samples };

	auto Start = SK::Time::Now();

	if (Threads)
		_ThreadPool.BeginAddTasks();

	for (int cock = 0; cock < 16; ++cock)
	{
		int Coeff1 = CoeffCalcOrder[cock];
		for (int arse = 0; arse < 16; ++arse)
		{
			int Coeff2 = CoeffCalcOrder[arse];

			if (Threads)
				Tasks.Add(_ThreadPool.AddTask_I(jizz, &balls, Coeff1, Coeff2, false));
			else
				jizz(&balls, Coeff1, Coeff2, false);
		}
	}
	
	if (Threads)
	{
		_ThreadPool.EndAddTasks();
		_ThreadPool.RunTasksInCurrentContext();
		Tasks.Wait();
		Tasks.Clear();
	}

#if 1
	if (Threads)
		_ThreadPool.BeginAddTasks();

	for (int cock = 0; cock < 16; ++cock)
	{
		int Coeff1 = CoeffCalcOrder[cock];
		for (int arse = 0; arse < 16; ++arse)
		{
			int Coeff2 = CoeffCalcOrder[arse];

			if (Threads)
				Tasks.Add(_ThreadPool.AddTask_I(jizz, &balls, Coeff1, Coeff2, true));
			else
				jizz(&balls, Coeff1, Coeff2, true);
		}
	}

	if (Threads)
	{
		_ThreadPool.EndAddTasks();
		_ThreadPool.RunTasksInCurrentContext();
		Tasks.Wait();
	}
#endif
	//SK::Debug::Print("Fuck: ", (SK::Time::Now() - Start).Milliseconds());

	delete[] Samples;
}

void Tests::PCMTest()
{
	FuckThis();

	SK::IO::FileStream FS(SK::String("D:/PCMTest/Original/Lucifer_16.22.raw"), SK::IO::FileMode::Read);
	SK::IO::MemoryStream OUT;
	SK::IO::MemoryStream TEST_OUT;

	SK::IO::MemoryStream IN;
	FS.CopyTo(IN);
	IN.Position(0);
	FS.Close();

#ifdef DEBUG_USAGE
	static double RMSError = 0;
	int NumErrs = 0;
	int OpTableCounts[4] = { 0,0,0,0 };
	int OpTableThingCounts[4][4] = { 0 };
	static int Coeff3Usage[4] = { 0 };
#endif

	uint32_t Length = static_cast<uint32_t>(IN.Length());
	int16_t* ChunkSamples16 = new int16_t[SamplesPerBlock];
	uint8_t* ChunkSamples = new uint8_t[SamplesPerBlock];

	int Coeff1 = 0;
	int Coeff2 = 0;
	int Coeff3 = 0;

	int NumBlocksInChunk = 0;
	int PrevSample;
	int PrevDelta[2] = { 0 };

	auto Start = SK::Time::Now();

	//while (true)
	{
		//for (int tableIdx = 1; tableIdx < 8; ++tableIdx)
		{
			//int bestTableDelta = 0;
			//double bestTableError = DBL_MAX;
			//for (int tableDelta = 1; tableDelta < 16; ++tableDelta)
			{
				//Table[tableIdx] = tableDelta;
				//RMSError = 0;
				//OUT.Position(0);

				uint64_t DebugStartPos = 0;// (IN.Length() / 100) * 80;
				uint64_t DebugEndPos = IN.Length();// (IN.Length() / 100) * 81;

				int16_t FirstSample = 0;
				IN.Read(FirstSample);
				IN.Position(DebugStartPos);
				PrevSample = ConvertTo8(FirstSample);
				OUT.Write(static_cast<uint8_t>(PrevSample));

				auto LastPrintTime = Start;

				while (IN.Position() < DebugEndPos)
				{
					if (NumBlocksInChunk == 0 && BlocksPerChunk > 0)
					{
						FindCoeffs(IN, PrevSample, PrevDelta, Coeff1, Coeff2, Coeff3);
						++Coeff3Usage[Coeff3];

						IN.ReadData(reinterpret_cast<uint8_t*>(ChunkSamples16), 0, SamplesPerBlock * sizeof(int16_t));
						for (int s = 0; s < SamplesPerBlock; ++s)
							ChunkSamples[s] = ConvertTo8(ChunkSamples16[s]);

						uint8_t Coeffs = (Coeff1 + 8) | ((Coeff2 + 8) << 4);// | ((Coeff3 - 0) << 6);
						OUT.Write(Coeffs);

						if ((SK::Time::Now() - LastPrintTime).Milliseconds() > 500)
						{
							LastPrintTime = SK::Time::Now();
							uint64_t Pos = IN.Position() - DebugStartPos;
							uint64_t Len = DebugEndPos - DebugStartPos;
							double Complete = static_cast<double>(Pos) / static_cast<double>(Len);
							double CurrentTime = (LastPrintTime - Start).Milliseconds<double>();
							double TotalTime = (1 / Complete) * CurrentTime;
							auto Remain = SK::Time::Milliseconds(uint64_t(TotalTime - CurrentTime));

							uint64_t Days = Remain.Days();
							uint64_t Hours = Remain.Hours();
							uint64_t Minutes = Remain.Minutes();
							uint64_t Seconds = Remain.Seconds();
							Seconds -= Minutes * 60;
							Minutes -= Hours * 60;
							Hours -= Days * 24;
							SK::Debug::Print(SK::String::Format("Complete: %.2f%%, ETA: %llud %lluh %llum %llus, T=%llu", Complete * 100, Days, Hours, Minutes, Seconds, static_cast<uint64_t>(CurrentTime)));
						}
					}
					else
					{
						IN.ReadData(reinterpret_cast<uint8_t*>(ChunkSamples16), 0, SamplesPerBlock * sizeof(int16_t));
						for (int s = 0; s < SamplesPerBlock; ++s)
							ChunkSamples[s] = ConvertTo8(ChunkSamples16[s]);
					}

					int OpTable = 0;
					int BestDelta = 0;
					int OpValues[SamplesPerBlock];
					float BestError = FLT_MAX;

					if (FindBestDelta_SIMD(0, PrevDelta, PrevSample, ChunkSamples, Coeff1, Coeff2, Coeff3, BestError, BestDelta, OpValues))
						OpTable = 0;

					if (FindBestDelta_SIMD(1, PrevDelta, PrevSample, ChunkSamples, Coeff1, Coeff2, Coeff3, BestError, BestDelta, OpValues))
						OpTable = 1;

					if (FindBestDelta_SIMD(2, PrevDelta, PrevSample, ChunkSamples, Coeff1, Coeff2, Coeff3, BestError, BestDelta, OpValues))
						OpTable = 2;

					if (FindBestDelta_SIMD(3, PrevDelta, PrevSample, ChunkSamples, Coeff1, Coeff2, Coeff3, BestError, BestDelta, OpValues))
						OpTable = 3;

					int Values[4];
					GetValues(BestDelta, OpTable, Values);

#ifdef DEBUG_USAGE
					RMSError += BestError;
					NumErrs += SamplesPerBlock;
					++OpTableCounts[OpTable];
#endif

					int Flags = OpTable << 6;
					OUT.Write(static_cast<uint8_t>(BestDelta | Flags));

					for (int h = 0; h < BytesPerBlock; ++h)
					{
						uint8_t Compressed = 0;

						if (BitsPerSample == 2)
						{
							int Ix = h * 4;

							for (int i = 0; i < 4; ++i)
							{
								int Op = OpValues[Ix++];
								int PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Coeff1, Coeff2);
								int Delta = Values[Op];
								PrevDelta[1] = PrevDelta[0];
								PrevDelta[0] = PredictedDelta + Delta;
								PrevSample = Clamp255(PrevSample + PrevDelta[0]);
								Compressed |= Op << (2 * i);

								//TEST_OUT.Write(int8_t(ChunkSamples[Ix-1]- PrevSample));
							}

#ifdef DEBUG_USAGE
							++OpTableThingCounts[OpTable][Compressed & 0x03];
							++OpTableThingCounts[OpTable][(Compressed & 0x0C) >> 2];
							++OpTableThingCounts[OpTable][(Compressed & 0x30) >> 4];
							++OpTableThingCounts[OpTable][(Compressed & 0xC0) >> 6];
#endif
						}
						else if (BitsPerSample == 1)
						{
							int Ix = h * 8;

							for (int i = 0; i < 8; ++i)
							{
								int Op = OpValues[Ix++];
								int PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Coeff1, Coeff2);
								int Delta = Values[Op];
								PrevDelta[1] = PrevDelta[0];
								PrevDelta[0] = PredictedDelta + Delta;
								PrevSample = Clamp255(PrevSample + PrevDelta[0]);
								Compressed |= Op << i;

								//TEST_OUT.Write(int8_t(ActualDelta - PredictedDelta));
							}

#ifdef DEBUG_USAGE
							++OpTableThingCounts[OpTable][Compressed & 0x01];
							++OpTableThingCounts[OpTable][(Compressed & 0x02) >> 1];
							++OpTableThingCounts[OpTable][(Compressed & 0x04) >> 2];
							++OpTableThingCounts[OpTable][(Compressed & 0x08) >> 3];
							++OpTableThingCounts[OpTable][(Compressed & 0x10) >> 4];
							++OpTableThingCounts[OpTable][(Compressed & 0x20) >> 5];
							++OpTableThingCounts[OpTable][(Compressed & 0x40) >> 6];
							++OpTableThingCounts[OpTable][(Compressed & 0x80) >> 7];
#endif
						}

						OUT.Write(Compressed);
					}

					if (BlocksPerChunk > 0 && ++NumBlocksInChunk == BlocksPerChunk)
						NumBlocksInChunk = 0;
				}

#ifdef DEBUG_USAGE
				double TotalTableUsages = OpTableCounts[0] + OpTableCounts[1] + OpTableCounts[2] + OpTableCounts[3];
				static double Usages[4] =
				{
					OpTableCounts[0] / TotalTableUsages,
					OpTableCounts[1] / TotalTableUsages,
					OpTableCounts[2] / TotalTableUsages,
					OpTableCounts[3] / TotalTableUsages,
				};

				static double OpUsages[4][4];
				for (int i = 0; i < 4; ++i)
				{
					double TotalOpUsages = OpTableThingCounts[i][0] + OpTableThingCounts[i][1] + OpTableThingCounts[i][2] + OpTableThingCounts[i][3];
					if (TotalOpUsages > 0)
					{
						OpUsages[i][0] = OpTableThingCounts[i][0] / TotalOpUsages;
						OpUsages[i][1] = OpTableThingCounts[i][1] / TotalOpUsages;
						OpUsages[i][2] = OpTableThingCounts[i][2] / TotalOpUsages;
						OpUsages[i][3] = OpTableThingCounts[i][3] / TotalOpUsages;
					}
				}

				//RMSError = sqrt(RMSError / NumErrs);
#endif
				/*if (RMSError < bestTableError)
				{
					bestTableError = RMSError;
					bestTableDelta = tableDelta;
				}*/
			}
			//Table[tableIdx] = bestTableDelta;
		}
		
		//SK::Debug::Print(SK::String::Format("Table = [%i,%i,%i,%i,%i,%i,%i,%i]", Table[0], Table[1], Table[2], Table[3], Table[4], Table[5], Table[6], Table[7]));
	}
	
	auto Duration = SK::Time::Now() - Start;
	SK::Debug::Print("Finished in ", Duration.Seconds(), "seconds");
	SK::Debug::Print("Error: ", static_cast<uint64_t>(RMSError));
	
	SK::IO::FileStream CompressedFile(SK::String("D:/PCMTest/Compressed/Lucifer_L_SKPCM.raw"), SK::IO::FileMode::Truncate);
	OUT.CopyTo(CompressedFile);
	CompressedFile.Close();
	OUT.Close();

	SK::IO::MemoryStream PCM;
	LoadCompressedSnd("D:/PCMTest/Compressed/Lucifer_L_SKPCM.raw", PCM);
	SK::IO::FileStream OutputFile(SK::String("D:/PCMTest/Lucifer_L_Test.raw"), SK::IO::FileMode::Truncate);
	PCM.CopyTo(OutputFile);
	OutputFile.Close();
	PCM.Close();

	_ThreadPool.Stop();

	FuckAllOfIt();

	return;
}

void WriteLine(SK::IO::Stream& stream, const char* str)
{
	stream.WriteData(reinterpret_cast<const uint8_t*>(str), 0, strlen(str));
}

void Tests::CharSetTest()
{
	SK::IO::FileStream FS("C:/makestuff/roms/Doom.32x", SK::IO::FileMode::Read);

	SK::IO::MemoryStream Nads;
	FS.CopyTo(Nads);
	Nads.Position(0);
	Nads.Endianness(Endian::Big);

	uint16_t Set[] = { 0xd8, 0x00, 0xE0, 0x00, 0x40, 0x00, 0x20 };

	while (Nads.Position() < Nads.Length())
	{
		bool sweet = true;
		for (int i = 0; i < sizeof(Set) / sizeof(uint16_t); ++i)
		{
			uint16_t Word;
			Nads.Read(Word);
			if ((Word & 0x00FF) != Set[i])
			{
				sweet = false;
				break;
			}
		}

		if (sweet)
		{
			SK::Debug::Halt();
		}
	}
	

	//Load the character set
	//SK::IO::FileStream FS(SK::IO::Path(SpecialFolder::Install, "FontCharSet.txt"), SK::IO::FileMode::Read);
	//if (!FS.IsOpen())
	//	return;

	//size_t Length = FS.Length();
	//uint8_t* DataBuff = new uint8_t[Length];
	//FS.ReadData(DataBuff, 0, Length);
	//FS.Close();

	////Decode from UTF-8
	//std::vector<CodePoint> CodePoints;
	//size_t Pos = 0;
	//CodePoint CP = SK::Encoding::UTF8::GetNextCodePoint(DataBuff, Pos, Length);
	//while (CP != 0)
	//{
	//	if (CP != '\r' && CP != '\n' && CP != 0xFEFF)
	//		CodePoints.push_back(CP);
	//	CP = SK::Encoding::UTF8::GetNextCodePoint(DataBuff, Pos, Length);
	//}

	//if (std::find(CodePoints.begin(), CodePoints.end(), 0xFFFD) == CodePoints.end())
	//	CodePoints.push_back(0xFFFD);

	////Sort chars in order
	//std::sort(CodePoints.begin(), CodePoints.end());

	////Save list
	//SK::IO::FileStream CharMap(SK::IO::Path(SpecialFolder::Install, "CharMap.s"), SK::IO::FileMode::Truncate);
	//WriteLine(CharMap, ".section .rodata\r\n");
	//WriteLine(CharMap, ".align 2\r\n");
	//WriteLine(CharMap, ".global char_map\r\n");
	//WriteLine(CharMap, "char_map:\r\n");
	//WriteLine(CharMap, SK::String::Format("    dc.w    0x%04x", CodePoints.size()));

	//const int maxColumns = 6;
	//int column = 0;
	//for (CodePoint cp : CodePoints)
	//{
	//	if (column == 0)
	//		WriteLine(CharMap, SK::String::Format("\r\n    dc.w    0x%04x", cp));
	//	else
	//		WriteLine(CharMap, SK::String::Format(", 0x%04x", cp));

	//	if (++column == maxColumns)
	//		column = 0;
	//}
	//WriteLine(CharMap, "\r\n");
	//CharMap.Close();

	////Convert back to a UTF8 string and save that too
	//uint8_t* UTF32Data = reinterpret_cast<uint8_t*>(&CodePoints[0]);
	//SK::String SortedCharSet = SK::Encoding::UTF32::FromBytes(UTF32Data, CodePoints.size() * sizeof(CodePoint));
	//SK::IO::FileStream CharMapString(SK::IO::Path(SpecialFolder::Install, "CharMap.txt"), SK::IO::FileMode::Truncate);
	//CharMapString.WriteData(SortedCharSet.Buffer(), 0, SortedCharSet.Size());
	//CharMapString.Close();

	return;
}
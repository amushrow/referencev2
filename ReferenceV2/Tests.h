#pragma once
#include <SKSystem/Text/String.h>

namespace Tests
{
	void Thread();
	void Window();
	void Input();
	void VestelFile();
	void PCMTest();
	void CharSetTest();
	void NormalMapTest(int numArgs, SK::String args[]);

	namespace Network
	{
		void Server();
		void Client();
		void ClientTwo();
	}
}

extern int main_vq32pcm(int argc, char **argv);
extern int main_pcm2vq3(int argc, char **argv);

extern int main_vq42pcm(int argc, char **argv);
extern int main_pcm2vq4(int argc, char **argv);
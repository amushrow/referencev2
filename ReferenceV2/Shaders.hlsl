//-- From Stub.h
struct Vertex2D
{
	float2 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
	float4 Colour   : TEXCOORD1;
};

struct Vertex3D
{
	float3 Position : POSITION;
	float3 Normal   : NORMAL;
	float2 TexCoord : TEXCOORD0;
};

struct Vertex3D_TC
{
	float3 Position : POSITION;
	float3 Normal   : NORMAL;
	float2 TexCoord : TEXCOORD0;
	float3 Tangent  : TANGENT;
	float4 Colour   : TEXCOORD1;
	float  Reserved : TEXCOORD2;
};

struct Vertex_InstMatCol
{
	float3  Position : POSITION;
	float   Reserved : TEXCOORD0;
	float4x4 Transform : INSTANCE_TRANSFORM;
	float4   Colour : TEXCOORD1;
};

cbuffer Matrices
{
	float4x4 World;
	float4x4 View;
	float4x4 Projection;
	float4x4 ViewProj;
	float4x4 WorldViewProj;
};
//--

float3 LightDir = float3(-0.707106769, -0.707106769, 0);
float4 LightCol = float4(0.5, 0.5, 0.5, 1.0);
float4 AmbientLight = float4(0.5, 0.5, 0.5, 1.0);

Texture2D Diffuse  : register(t0);
Texture2D Specular : register(t1);
Texture2D Normal   : register(t2);

SamplerState LinearSampler : register(s0);
SamplerState PointSampler  : register(s1);

struct VertexOut
{
	float4 Position : SV_POSITION;
	float3 Normal   : NORMAL;
	float2 TexCoord : TEXCOORD0;
};

struct UnlitVertexOut
{
	float4 Position : SV_POSITION;
	float4 Colour   : TEXCOORD0;
};

VertexOut Standard3D(const Vertex3D IN)
{
	VertexOut OUT;
	OUT.Position = mul(WorldViewProj, float4(IN.Position, 1.0));
	OUT.TexCoord = IN.TexCoord;
	OUT.Normal = mul(World, float4(IN.Normal, 0.0)).xyz;
	return OUT;
}

float4 StandardLit(const VertexOut IN) : SV_Target
{
	float3 Normal = normalize(IN.Normal);
	float LightCoverage = dot(Normal, -LightDir);
	float3 Reflection = mul(View, float4(reflect(LightDir, Normal), 0.0)).xyz;
	float4 LightDiffuse = (LightCol * LightCoverage) + AmbientLight;
	float4 DiffuseColour = Diffuse.Sample(LinearSampler, IN.TexCoord);
	return float4((LightDiffuse * DiffuseColour).xyz, DiffuseColour.a);
}

UnlitVertexOut DebugShapes(const Vertex_InstMatCol IN)
{
	VertexOut OUT;
	float4x4 WVP = mul(ViewProj, IN.Transform);
	OUT.Position = mul(WVP, float4(IN.Position, 1.0));
	//OUT.Colour = IN.Colour;
	return OUT;
}

float4 Unlit(const UnlitVertexOut IN) : SV_Target
{
	return IN.Colour;
}
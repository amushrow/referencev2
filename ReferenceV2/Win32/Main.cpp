// ReferenceV2.cpp : Defines the entry point for the application.
//
#include <SKSystem/Win32/Win32.h>
#include <SKSystem/Text/Encoding.h>
#include "../ReferenceV2.h"
#include "../Tests.h"
#include <SKSystem\Math\Vector.h>

int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	//RefV2::TestFunction();
	//Tests::PCMTest();
	//Tests::CharSetTest();
	//Tests::NormalMapTest();
	//Tests::VestelFile();

	return 0;
}

int _tmain(int argc, _TCHAR* argv [])
{
	//RefV2::TestFunction();
	//Tests::PCMTest();
	//main_vq42pcm(argc, (char**)argv);
	
	SK::String* Argses = new SK::String[argc];
	for (int i = 1; i < argc; ++i)
		Argses[i-1] = SK::Encoding::UTF16::FromBytes(reinterpret_cast<uint8_t*>(argv[i]));

	Tests::NormalMapTest(argc - 1, Argses);

	delete[] Argses;

	return 0;
}
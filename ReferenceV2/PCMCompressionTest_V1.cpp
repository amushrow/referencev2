#include "Tests.h"
#include <SKSystem/IO/FileStream.h>
#include <SKSystem/IO/MemoryStream.h>
#include <SKSystem/Debug.h>
#include <SKSystem/Time/Clocks.h>
#include <algorithm>

using namespace SK;
using namespace SK::IO;

namespace
{
	int AdaptCoeff1[] = { 256, 512, 0, 192, 240, 460, 392 };
	int AdaptCoeff2[] = { 0, -256, 0, 64, 0, -208, -232 };
}

void GetValues(int delta, int opTable, int values[4])
{
	if (opTable == 0)
	{
		int a = delta & 0x07;
		int b = (delta & 0x38) >> 3;

		values[0] = a << 1;
		values[1] = ~(a << 1);
		values[2] = b;
		values[3] = ~(b);
	}
	else if (opTable == 1)
	{
		int a = delta & 0x07;
		int b = (delta & 0x38) >> 3;

		values[0] = a << 2;
		values[1] = ~(a << 2);
		values[2] = b << 1;
		values[3] = -(b << 1);
	}
	else if (opTable == 2)
	{
		values[0] = delta;
		values[1] = ~delta;
		values[2] = delta / 3;
		values[3] = ~(delta / 3);
	}
	else if (opTable == 3)
	{
		values[0] = delta;
		values[1] = ~delta;
		values[2] = delta >> 2;
		values[3] = ~(delta >> 2);
	}
}

int FindClosest(int target, int value, int opTable, int& op)
{
	int Ix = 0;
	int Best = 255;
	int Values[4];
	GetValues(value, opTable, Values);

	for (int i = 0; i < 4; ++i)
	{
		int Delta = abs(Values[i] - target);
		if (Delta < Best)
		{
			Best = Delta;
			Ix = i;
		}
	}

	op = Ix;
	return Values[Ix];
}

int Clamp255(int a)
{
	a = a > 255 ? 255 : a;
	return a < 0 ? 0 : a;
}

float DRScale = 1.0f / 0.5;

uint8_t ConvertTo8(int16_t sample)
{
	return (sample / 256) + 0x80;
}

uint8_t Filter(uint8_t sample)
{
	//if (sample == 127 || sample == 129)
	//	return 128;

	return sample;
}

uint8_t CalcPrediction(uint8_t sample1, uint8_t sample2, int8_t coeff1, int8_t coeff2)
{
	return Clamp255(((sample1 * coeff1) + (sample2 * coeff2)) / 4);
}

void LoadCompressedSnd(SK::IO::Path fileName, int BlockSize, int ChunkSize, SK::IO::Stream& outputBuffer)
{
	//Copy input file to memory buffer
	SK::IO::FileStream InputFile(fileName, SK::IO::FileMode::Read);
	SK::IO::MemoryStream InputBuffer;
	InputFile.CopyTo(InputBuffer);
	InputFile.Close();
	InputBuffer.Position(0);

	uint8_t Sample = 0;
	uint8_t Sample2 = 0;
	int8_t Coeff1, Coeff2;
	int NumBlocks = 0;
	InputBuffer.Read(Sample);
	Sample2 = Sample;

	while (InputBuffer.Position() != InputBuffer.Length())
	{
		if (NumBlocks == 0)
		{
			uint8_t Coeffs;
			InputBuffer.Read(Coeffs);
			Coeff1 = ((Coeffs & 0xF0) >> 4) - 8;
			Coeff2 = (Coeffs & 0x0F) - 8;
		}
		uint8_t Delta, Compressed;
		InputBuffer.Read(Delta);

		int OpTable = (Delta & 0xC0) >> 6;
		Delta &= 0x3F;

		int Values[4];
		GetValues(Delta, OpTable, Values);

		for (int h = 0; h < BlockSize; ++h)
		{
			InputBuffer.Read(Compressed);

			int Prediction = CalcPrediction(Sample, Sample2, Coeff1, Coeff2);
			Sample2 = Sample;
			Sample = Clamp255(Prediction + Values[Compressed & 0x03]);
			outputBuffer.Write(Filter(Sample));

			Prediction = CalcPrediction(Sample, Sample2, Coeff1, Coeff2);
			Sample2 = Sample;
			Sample = Clamp255(Prediction + Values[(Compressed & 0x0C) >> 2]);
			outputBuffer.Write(Filter(Sample));

			Prediction = CalcPrediction(Sample, Sample2, Coeff1, Coeff2);
			Sample2 = Sample;
			Sample = Clamp255(Prediction + Values[(Compressed & 0x30) >> 4]);
			outputBuffer.Write(Filter(Sample));

			Prediction = CalcPrediction(Sample, Sample2, Coeff1, Coeff2);
			Sample2 = Sample;
			Sample = Clamp255(Prediction + Values[(Compressed & 0xC0) >> 6]);
			outputBuffer.Write(Filter(Sample));
		}

		if (++NumBlocks == ChunkSize)
			NumBlocks = 0;
	}

	InputBuffer.Close();
}

int FindBestDelta(int OpTable, uint8_t PrevSamples[2], uint8_t* ChunkSamples, int8_t coeff1, int8_t coeff2, int BlockSize, float& bestError)
{
	int BestDelta = 0;
	for (int k = 0; k < 64; ++k)
	{
		int Values[4];
		GetValues(k, OpTable, Values);

		int S = PrevSamples[0];
		int S2 = PrevSamples[1];
		int Op = 0;

		float Error = 0;
		for (int h = 0; h < BlockSize; ++h)
		{
			uint8_t BestOpDelta = 255;
			int Delta = 0;
			int Prediction = CalcPrediction(S, S2, coeff1, coeff2);
			int Target = ChunkSamples[h] - Prediction;

			int OpDelta = abs(Values[0] - Target);
			if (OpDelta < BestOpDelta)
			{
				BestOpDelta = OpDelta;
				Delta = Values[0];
			}

			OpDelta = abs(Values[1] - Target);
			if (OpDelta < BestOpDelta)
			{
				BestOpDelta = OpDelta;
				Delta = Values[1];
			}

			OpDelta = abs(Values[2] - Target);
			if (OpDelta < BestOpDelta)
			{
				BestOpDelta = OpDelta;
				Delta = Values[2];
			}

			OpDelta = abs(Values[3] - Target);
			if (OpDelta < BestOpDelta)
			{
				BestOpDelta = OpDelta;
				Delta = Values[3];
			}

			S2 = S;
			S = Clamp255(Prediction + Delta);
			Error += (S - ChunkSamples[h]) * (S - ChunkSamples[h]);
		}

		if (Error < bestError)
		{
			bestError = Error;
			BestDelta = k;
		}
	}

	return BestDelta;
}

void FindCoeffs(SK::IO::Stream& stream, uint8_t prevSample[2], uint32_t numSamples, int8_t& out_Coeff1, int8_t& out_Coeff2)
{
	uint64_t StartPos = stream.Position();

	double CoeffError = DBL_MAX;
	for (int Coeff1 = -8; Coeff1 < 8; ++Coeff1)
	{
		for (int Coeff2 = -8; Coeff2 < 8; ++Coeff2)
		{
			int16_t Sample1;
			double Error = 0.0f;
			stream.Position(StartPos);

			uint8_t Sample8_1 = prevSample[0];
			uint8_t Sample8_2 = prevSample[1];

			while (stream.Position() < StartPos + (numSamples * sizeof(int16_t)) && stream.Position() < stream.Length())
			{
				uint8_t Prediction = CalcPrediction(Sample8_1, Sample8_2, Coeff1, Coeff2);

				Sample8_2 = Sample8_1;
				stream.Read(Sample1);
				Sample8_1 = ConvertTo8(Sample1);

				double Delta = Sample8_1 - Prediction;
				Error += (Delta * Delta);
			}

			if (Error < CoeffError)
			{
				CoeffError = Error;
				out_Coeff1 = Coeff1;
				out_Coeff2 = Coeff2;
			}
		}
	}

	stream.Position(StartPos);
}

void Tests::PCMTest()
{
	const int BlockBytes = 12;
	const int BlockSampleSize = 4 * BlockBytes;
	const int ChunkSize = 16;

	SK::IO::FileStream FS(SK::String("D:/PCMTest/Lucifer_16.raw"), SK::IO::FileMode::Read);
	SK::IO::MemoryStream OUT;
	SK::IO::MemoryStream DELTA_OUT;

	SK::IO::MemoryStream IN;
	FS.CopyTo(IN);
	IN.Position(0);
	FS.Close();

	static double RMSError = 0;
	int NumErrs = 0;

	uint32_t Length = static_cast<uint32_t>(IN.Length());
	int16_t* ChunkSamples16 = new int16_t[BlockSampleSize];
	uint8_t* ChunkSamples = new uint8_t[BlockSampleSize];

	int8_t Coeff1, Coeff2;
	int NumBlocksInChunk = 0;
	uint8_t PrevSample[2];
	int OpTableCounts[4] = { 0,0,0,0 };
	int OpTableThingCounts[4][4] = { 0 };

	int16_t FirstSample = 0;
	IN.Read(FirstSample);
	IN.Position(0);

	PrevSample[0] = ConvertTo8(FirstSample);
	PrevSample[1] = PrevSample[0];
	OUT.Write(PrevSample[0]);

	while(IN.Position() < IN.Length())
	{
		if (NumBlocksInChunk == 0)
		{
			FindCoeffs(IN, PrevSample, BlockSampleSize * ChunkSize, Coeff1, Coeff2);

			IN.ReadData(reinterpret_cast<uint8_t*>(ChunkSamples16), 0, BlockSampleSize * sizeof(int16_t));
			for (int s = 0; s < BlockSampleSize; ++s)
				ChunkSamples[s] = ConvertTo8(ChunkSamples16[s]);

			uint8_t Coeffs = ((Coeff1 + 8) << 4) | (Coeff2 + 8);
			OUT.Write(Coeffs);
		}
		else
		{
			IN.ReadData(reinterpret_cast<uint8_t*>(ChunkSamples16), 0, BlockSampleSize * sizeof(int16_t));
			for (int s = 0; s < BlockSampleSize; ++s)
				ChunkSamples[s] = ConvertTo8(ChunkSamples16[s]);
		}

		bool Predict = true;

		int OpTable = 0;
		int BestDelta = 0;
		float BestError = FLT_MAX;

		float Error = FLT_MAX;
		int Delta = FindBestDelta(0, PrevSample, ChunkSamples, Coeff1, Coeff2, BlockSampleSize, Error);
		if (Error < BestError)
		{
			BestDelta = Delta;
			BestError = Error;
			OpTable = 0;
		}

		Error = FLT_MAX;
		Delta = FindBestDelta(1, PrevSample, ChunkSamples, Coeff1, Coeff2, BlockSampleSize, Error);
		if (Error < BestError)
		{
			BestDelta = Delta;
			BestError = Error;
			OpTable = 1;
		}

		Error = FLT_MAX;
		Delta = FindBestDelta(2, PrevSample, ChunkSamples, Coeff1, Coeff2, BlockSampleSize, Error);
		if (Error < BestError)
		{
			BestDelta = Delta;
			BestError = Error;
			OpTable = 2;
		}

		Error = FLT_MAX;
		Delta = FindBestDelta(3, PrevSample, ChunkSamples, Coeff1, Coeff2, BlockSampleSize, Error);
		if (Error < BestError)
		{
			BestDelta = Delta;
			BestError = Error;
			OpTable = 3;
		}

		RMSError += BestError;
		NumErrs += BlockSampleSize;

		++OpTableCounts[OpTable];

		uint8_t Flags = OpTable << 6;
		OUT.Write(static_cast<uint8_t>(BestDelta | Flags));

		for (int h = 0; h < BlockBytes; ++h)
		{
			uint8_t Compressed = 0;
			int Op = 0;
			int Ix = h * 4;
			int Prediction = CalcPrediction(PrevSample[0], PrevSample[1], Coeff1, Coeff2);
			int Delta = FindClosest(ChunkSamples[Ix + 0] - Prediction, BestDelta, OpTable, Op);
			PrevSample[1] = PrevSample[0];
			PrevSample[0] = Clamp255(Prediction + Delta);
			Compressed |= Op;

			DELTA_OUT.Write(static_cast<uint8_t>(Clamp255(Delta + 0x80)));

			++OpTableThingCounts[OpTable][Op];

			Prediction = CalcPrediction(PrevSample[0], PrevSample[1], Coeff1, Coeff2);
			Delta = FindClosest(ChunkSamples[Ix + 1] - Prediction, BestDelta, OpTable, Op);
			PrevSample[1] = PrevSample[0];
			PrevSample[0] = Clamp255(Prediction + Delta);
			Compressed |= Op << 2;

			DELTA_OUT.Write(static_cast<uint8_t>(Clamp255(Delta + 0x80)));

			++OpTableThingCounts[OpTable][Op];

			Prediction = CalcPrediction(PrevSample[0], PrevSample[1], Coeff1, Coeff2);
			Delta = FindClosest(ChunkSamples[Ix + 2] - Prediction, BestDelta, OpTable, Op);
			PrevSample[1] = PrevSample[0];
			PrevSample[0] = Clamp255(Prediction + Delta);
			Compressed |= Op << 4;

			DELTA_OUT.Write(static_cast<uint8_t>(Clamp255(Delta + 0x80)));

			++OpTableThingCounts[OpTable][Op];

			Prediction = CalcPrediction(PrevSample[0], PrevSample[1], Coeff1, Coeff2);
			Delta = FindClosest(ChunkSamples[Ix + 3] - Prediction, BestDelta, OpTable, Op);
			PrevSample[1] = PrevSample[0];
			PrevSample[0] = Clamp255(Prediction + Delta);
			Compressed |= Op << 6;

			DELTA_OUT.Write(static_cast<uint8_t>(Clamp255(Delta + 0x80)));

			++OpTableThingCounts[OpTable][Op];

			OUT.Write(Compressed);
		}

		if (++NumBlocksInChunk == ChunkSize)
			NumBlocksInChunk = 0;
	}

	float TotalUsages = OpTableCounts[0] + OpTableCounts[1] + OpTableCounts[2] + OpTableCounts[3];

	static float Usages[4] =
	{
		OpTableCounts[0] / TotalUsages,
		OpTableCounts[1] / TotalUsages,
		OpTableCounts[2] / TotalUsages,
		OpTableCounts[3] / TotalUsages,
	};

	static float OpUsages[4][4];
	for (int i = 0; i < 4; ++i)
	{
		float TotalUsages = OpTableThingCounts[i][0] + OpTableThingCounts[i][1] + OpTableThingCounts[i][2] + OpTableThingCounts[i][3];
		OpUsages[i][0] = OpTableThingCounts[i][0] / TotalUsages;
		OpUsages[i][1] = OpTableThingCounts[i][1] / TotalUsages;
		OpUsages[i][2] = OpTableThingCounts[i][2] / TotalUsages;
		OpUsages[i][3] = OpTableThingCounts[i][3] / TotalUsages;
	}

	RMSError = sqrt(RMSError / NumErrs);
	
	SK::IO::FileStream CompressedFile(SK::String("D:/PCMTest/Lucifer_C.raw"), SK::IO::FileMode::Truncate);
	OUT.CopyTo(CompressedFile);
	CompressedFile.Close();
	OUT.Close();

	SK::IO::FileStream DeltaFile(SK::String("D:/PCMTest/Lucifer_Delta.raw"), SK::IO::FileMode::Truncate);
	DELTA_OUT.CopyTo(DeltaFile);
	DeltaFile.Close();
	DELTA_OUT.Close();

	SK::IO::MemoryStream PCM;
	LoadCompressedSnd("D:/PCMTest/Lucifer_C.raw", BlockBytes, ChunkSize, PCM);
	SK::IO::FileStream OutputFile(SK::String("D:/PCMTest/Lucifer_Test.raw"), SK::IO::FileMode::Truncate);
	PCM.CopyTo(OutputFile);
	OutputFile.Close();
	PCM.Close();

	return;
}
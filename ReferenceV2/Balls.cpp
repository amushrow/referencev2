#include "Tests.h"
#include <SKSystem/IO/FileStream.h>
#include <SKSystem/IO/MemoryStream.h>
#include <SKSystem/Debug.h>
#include <SKSystem/Time/Clocks.h>
#include <SKSystem/Threading/ThreadPool.h>
#include <algorithm>

using namespace SK;
using namespace SK::IO;

#define DEBUG_USAGE
//#define PREDICT

//SKTD: Still have 2 bits spare per chunk
#ifdef PREDICT
	//Only the standard version supports this mode
	#define FindBestDelta FindBestDelta_C
	#define FindCoeffs FindCoeffs_C
#else
	#define FindBestDelta FindBestDelta_C
	#define FindCoeffs FindCoeffs_C
#endif

namespace
{
	//Stupid Low: 32, 4,  1
	//Very Low:   3,  8,  1
	//Low:        24, 12, 2
	//High:       3,  64, 2
	const int BytesPerBlock = 3;
	const int BlocksPerChunk = 8;
	const int BitsPerSample = 1;

#ifdef PREDICT
	const int SamplesPerBlock = (16 / BitsPerSample) * BytesPerBlock;
#else
	const int SamplesPerBlock = (8 / BitsPerSample) * BytesPerBlock;
#endif
	
	const int SamplesPerChunk = SamplesPerBlock * BlocksPerChunk;

	SK::Threading::ThreadPool _ThreadPool(7);
	std::mutex _BestErrorMutex;
}

void GetValues(int delta, int opTable, int coeff3, int values[4])
{
	if (BitsPerSample == 2)
	{
		if (opTable == 0)
		{
			int a = delta & 0x07;
			int b = (delta & 0x38) >> 3;

			values[0] = a << 1;
			values[1] = ~(a << 1);
			values[2] = b;
			values[3] = ~(b);
		}
		else if (opTable == 1)
		{
			int a = delta & 0x07;
			int b = (delta & 0x38) >> 3;

			values[0] = a << 2;
			values[1] = ~(a << 2);
			values[2] = b << 1;
			values[3] = ~(b << 1);
		}
		else if (opTable == 2)
		{
			values[0] = delta;
			values[1] = ~delta;
			values[2] = delta / 3;
			values[3] = ~(delta / 3);
		}
		else if (opTable == 3)
		{
			values[0] = delta;
			values[1] = ~delta;
			values[2] = delta >> 2;
			values[3] = ~(delta >> 2);
		}
	}
	else if (BitsPerSample == 1)
	{
		// Set 1
		if (opTable == 0)
		{
			int a = delta & 0x07;
			int b = (delta & 0x38) >> 3;
			values[0] = a;
			values[1] = ~b;
		}
		else if (opTable == 1)
		{
			int a = delta & 0x07;
			int b = (delta & 0x38) >> 3;
			values[0] = (a * 3);
			values[1] = ~(b << 1);
		}
		else if (opTable == 2)
		{
			int a = delta & 0x07;
			int b = (delta & 0x38) >> 3;
			values[0] = (a << 1);
			values[1] = ~(b * 3);
		}
		else if (opTable == 3)
		{
			int a = delta & 0x07;
			int b = (delta & 0x38) >> 3;
			values[0] = (a * 3);//sktd
			values[1] = ~(b * 3);
		}

		values[2] = values[0];
		values[3] = values[1];
	}
}

int FindClosest(int target, int value, int opTable, int coeff3, int& op)
{
	int Ix = 0;
	uint32_t Best = -1;
	int Values[4];
	GetValues(value, opTable, coeff3, Values);

	const int MaxIndex = BitsPerSample == 1 ? 2 : 4;
	for (int i = 0; i < MaxIndex; ++i)
	{
		uint32_t Delta = abs(Values[i] - target);
		if (Delta < Best)
		{
			Best = Delta;
			Ix = i;
		}
	}

	op = Ix;
	return Values[Ix];
}

int Clamp255(int a)
{
	a = a > 255 ? 255 : a;
	return a < 0 ? 0 : a;
}

uint8_t ConvertTo8(int16_t sample)
{
	float fSample = roundf(sample / 256.0f) + 128.0f;
	return static_cast<uint8_t>(fSample);
}

int CalcPrediction(int delta1, int delta2, int avg, int coeff1, int coeff2, int coeff3)
{
	avg = avg >> 3;
	coeff3 = 0;
	return ((delta1 * coeff1) + (delta2 * coeff2)) >> 2;
}

int AddAvg(int avg, int newVal)
{
	avg -= avg >> 3;
	avg += newVal;

	return 0;
}

void LoadCompressedSnd(SK::IO::Path fileName, SK::IO::Stream& outputBuffer)
{
	//Copy input file to memory buffer
	SK::IO::FileStream InputFile(fileName, SK::IO::FileMode::Read);
	SK::IO::MemoryStream InputBuffer;
	InputFile.CopyTo(InputBuffer);
	InputFile.Close();
	InputBuffer.Position(0);

	uint8_t PrevSample = 0;
	int Delta1 = 0;
	int Delta2 = 0;

	int Coeff1 = 0;
	int Coeff2 = 0;
	int Coeff3 = 0;

	int TotAvg = 0;

	int NumBlocks = 0;
	InputBuffer.Read(PrevSample);

	while (InputBuffer.Position() != InputBuffer.Length())
	{
		if (NumBlocks == 0 && BlocksPerChunk > 0)
		{
			//uint8_t Coeffs;
			//InputBuffer.Read(Coeffs);
			//Coeff1 = (Coeffs & 0x07) - 4;
			//Coeff2 = ((Coeffs & 0x38) >> 3) - 4;
			//Coeff3 = ((Coeffs & 0xC0) >> 6) + 2;

			int8_t Coeffs;
			InputBuffer.Read(Coeffs);
			Coeff1 = Coeffs;
			InputBuffer.Read(Coeffs);
			Coeff2 = Coeffs;
			InputBuffer.Read(Coeffs);
			Coeff3 = Coeffs;
		}
		uint8_t Delta, Compressed;
		InputBuffer.Read(Delta);

		int OpTable = (Delta & 0xC0) >> 6;
		Delta &= 0x3F;

		int Values[4];
		GetValues(Delta, OpTable, Coeff3, Values);

		//SKTD: avg bollocks
		for (int h = 0; h < BytesPerBlock; ++h)
		{
			InputBuffer.Read(Compressed);

			if (BitsPerSample == 2)
			{
				for (int i = 0; i < 4; ++i)
				{
					int PredictedDelta = CalcPrediction(Delta1, Delta2, 0, Coeff1, Coeff2, Coeff3);
					PredictedDelta += Values[(Compressed >> (2*i)) & 0x03];
					Delta2 = Delta1;
					Delta1 = PredictedDelta;
					PrevSample = Clamp255(PrevSample + PredictedDelta);
					outputBuffer.Write(PrevSample);

#ifdef PREDICT
					PredictedDelta = CalcPrediction(Delta1, Delta2, 0, Coeff1, Coeff2, Coeff3);
					Delta2 = Delta1;
					Delta1 = PredictedDelta;
					PrevSample = Clamp255(PrevSample + PredictedDelta);
					outputBuffer.Write(PrevSample);
#endif
				}
			}
			else if (BitsPerSample == 1)
			{
				for (int i = 0; i < 8; ++i)
				{
					int PredictedDelta = CalcPrediction(Delta1, Delta2, 0, Coeff1, Coeff2, Coeff3);
					PredictedDelta += Values[(Compressed >> i) & 0x01];
					Delta2 = Delta1;
					Delta1 = PredictedDelta;
					PrevSample = Clamp255(PrevSample + PredictedDelta);
					outputBuffer.Write(PrevSample);
#ifdef PREDICT
					PredictedDelta = CalcPrediction(Delta1, Delta2, 0, Coeff1, Coeff2, Coeff3);
					Delta2 = Delta1;
					Delta1 = PredictedDelta;
					PrevSample = Clamp255(PrevSample + PredictedDelta);
					outputBuffer.Write(PrevSample);
#endif
				}
			}
		}

		if (BlocksPerChunk > 0 && ++NumBlocks == BlocksPerChunk)
			NumBlocks = 0;
	}

	InputBuffer.Close();
}

bool FindBestDelta_C(int OpTable, int PrevDelta[2], int AverageD, int PrevSample, uint8_t* ChunkSamples, int coeff1, int coeff2, int coeff3, volatile double& bestError, int& delta)
{
	bool Better = false;
	for (int k = 0; k < 64; ++k)
	{
		__declspec (align(16)) int OpTableValues[4];
		GetValues(k, OpTable, coeff3, OpTableValues);

		__m128i Values = _mm_load_si128((__m128i*)OpTableValues);

		int S = PrevSample;
		int D = PrevDelta[0];
		int D2 = PrevDelta[1];
		int Avg = AverageD;
		int Op = 0;

		double Error = 0;
		for (int h = 0; h < SamplesPerBlock; ++h)
		{
			int DeltaDelta = 0;
			int ActualDelta = ChunkSamples[h] - S;
			int PredictedDelta = CalcPrediction(D, D2, Avg, coeff1, coeff2, coeff3);

			__m128i Target = _mm_set1_epi32(ActualDelta - PredictedDelta);
			__m128i OpDelta = _mm_abs_epi32(_mm_sub_epi32(Target, Values));
			OpDelta = _mm_or_si128(OpDelta, _mm_set_epi16(0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00));
			int PacketBest = _mm_cvtsi128_si32(_mm_minpos_epu16(OpDelta));
			int Ix = (PacketBest & 0xF0000) >> 17;
			DeltaDelta = OpTableValues[Ix];

			D2 = D;
			D = PredictedDelta + DeltaDelta;
			Avg = AddAvg(Avg, D);
			S = Clamp255(S + D);
			Error += static_cast<double>(S - ChunkSamples[h]) * static_cast<double>(S - ChunkSamples[h]);

#ifdef PREDICT
			++h;
			if (h < SamplesPerBlock)
			{
				PredictedDelta = CalcPrediction(D, D2, coeff1, coeff2, coeff3);
				D2 = D;
				D = PredictedDelta;
				Avg = AddAvg(Avg, D);
				S = Clamp255(S + D);
				Error += static_cast<double>(S - ChunkSamples[h]) * static_cast<double>(S - ChunkSamples[h]);
			}
#endif

			if (Error > bestError)
				break;
		}

		if (Error < bestError)
		{
			bestError = Error;
			delta = k;
			Better = true;
		}
	}

	return Better;
}

#if 0
bool FindBestDelta_SIMD(int OpTable, int PrevDelta[2], int PrevSample, uint8_t* ChunkSamples, int coeff1, int coeff2, int coeff3, volatile double& bestError, int& delta)
{
	if (bestError == 0.0)
		return false;

	const __m128i SIMDCoeff2 = _mm_set1_epi32(coeff2);
	const __m128i SIMDCoeff1 = _mm_set1_epi32(coeff1);
	const double OriginalError = bestError;

	for (int k = 0; k < 64; k += 4)
	{
		int OpTableValues[4][4];
		GetValues(k, OpTable, coeff3, OpTableValues[0]);
		GetValues(k + 1, OpTable, coeff3, OpTableValues[1]);
		GetValues(k + 2, OpTable, coeff3, OpTableValues[2]);
		GetValues(k + 3, OpTable, coeff3, OpTableValues[3]);

		const __m128 BestError = _mm_set1_ps(static_cast<float>(bestError));
		const __m128i Value0 = _mm_set_epi32(OpTableValues[0][0], OpTableValues[1][0], OpTableValues[2][0], OpTableValues[3][0]);
		const __m128i Value1 = _mm_set_epi32(OpTableValues[0][1], OpTableValues[1][1], OpTableValues[2][1], OpTableValues[3][1]);
		const __m128i Value2 = _mm_set_epi32(OpTableValues[0][2], OpTableValues[1][2], OpTableValues[2][2], OpTableValues[3][2]);
		const __m128i Value3 = _mm_set_epi32(OpTableValues[0][3], OpTableValues[1][3], OpTableValues[2][3], OpTableValues[3][3]);

		__m128i S = _mm_set1_epi32(PrevSample);
		__m128i D = _mm_set1_epi32(PrevDelta[0]);
		__m128i D2 = _mm_set1_epi32(PrevDelta[1]);
		__m128 Error = _mm_set1_ps(0);

		for (int h = 0; h < SamplesPerBlock; ++h)
		{
			int DeltaDelta = 0;
			__m128i CurrentSample = _mm_set1_epi32(ChunkSamples[h]);
			__m128i ActualDelta = _mm_sub_epi32(CurrentSample, S);

			__m128i PredictedDelta = _mm_add_epi32(_mm_mullo_epi32(D, SIMDCoeff1), _mm_mullo_epi32(D2, SIMDCoeff2));
			PredictedDelta = _mm_srai_epi32(PredictedDelta, 2);

			__m128i Target = _mm_sub_epi32(ActualDelta, PredictedDelta);
			__m128i OpDelta = _mm_abs_epi32(_mm_sub_epi32(Target, Value0));
			__m128i BestestValue = Value0;

			__m128i OpDelta2 = _mm_abs_epi32(_mm_sub_epi32(Target, Value1));
			__m128i Cmp = _mm_cmplt_epi32(OpDelta2, OpDelta);
			BestestValue = _mm_andnot_si128(Cmp, BestestValue);
			BestestValue = _mm_or_si128(BestestValue, _mm_and_si128(Value1, Cmp));
			OpDelta = _mm_min_epi32(OpDelta, OpDelta2);

			__m128i OpDelta3 = _mm_abs_epi32(_mm_sub_epi32(Target, Value2));
			Cmp = _mm_cmplt_epi32(OpDelta3, OpDelta);
			BestestValue = _mm_andnot_si128(Cmp, BestestValue);
			BestestValue = _mm_or_si128(BestestValue, _mm_and_si128(Value2, Cmp));
			OpDelta = _mm_min_epi32(OpDelta, OpDelta3);

			__m128i OpDelta4 = _mm_abs_epi32(_mm_sub_epi32(Target, Value3));
			Cmp = _mm_cmplt_epi32(OpDelta4, OpDelta);
			BestestValue = _mm_andnot_si128(Cmp, BestestValue);
			BestestValue = _mm_or_si128(BestestValue, _mm_and_si128(Value3, Cmp));

			D2 = D;
			D = _mm_add_epi32(PredictedDelta, BestestValue);
			S = _mm_add_epi32(S, D);

			__m128 E = _mm_cvtepi32_ps(_mm_sub_epi32(S, CurrentSample));
			Error = _mm_add_ps(Error, _mm_mul_ps(E, E));

			if (_mm_testc_ps(_mm_cmpgt_ps(Error, BestError), _mm_cvtepi32_ps(_mm_set1_epi32(~0))))
				break;
		}

		__declspec(align(16)) float Errors[4];
		_mm_store_ps(Errors, Error);

		if (Errors[3] < bestError && Errors[3] <= Errors[0] && Errors[3] <= Errors[1] && Errors[3] <= Errors[2])
		{
			if (Errors[3] < bestError)
			{
				bestError = Errors[3];
				delta = k;
			}
		}
		else if (Errors[2] < bestError && Errors[2] <= Errors[0] && Errors[2] <= Errors[1] && Errors[2] <= Errors[3])
		{
			if (Errors[2] < bestError)
			{
				bestError = Errors[2];
				delta = k + 1;
			}
		}
		else if (Errors[1] < bestError && Errors[1] <= Errors[0] && Errors[1] <= Errors[2] && Errors[1] <= Errors[3])
		{
			if (Errors[1] < bestError)
			{
				bestError = Errors[1];
				delta = k + 2;
			}
		}
		else if (Errors[0] < bestError && Errors[0] <= Errors[1] && Errors[0] <= Errors[2] && Errors[0] <= Errors[3])
		{
			if (Errors[0] < bestError)
			{
				bestError = Errors[0];
				delta = k + 3;
			}
		}
	}

	return bestError < OriginalError;
}
#endif

double CalcBlockError(uint8_t* chunkSamples, int coeff1, int coeff2, int coeff3, int prevDelta[2], int avgD, int& prevSample, double maxError)
{
	double Error = 0;

	int OpTable = 0;
	int BestDelta = 0;
	double BestError = DBL_MAX;

	if (FindBestDelta(0, prevDelta, avgD, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta))
		OpTable = 0;

	if (FindBestDelta(1, prevDelta, avgD, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta))
		OpTable = 1;

	if (FindBestDelta(2, prevDelta, avgD, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta))
		OpTable = 2;

	if (FindBestDelta(3, prevDelta, avgD, prevSample, chunkSamples, coeff1, coeff2, coeff3, BestError, BestDelta))
		OpTable = 3;

	Error += BestError;

	if (Error > maxError)
		return Error;

	for (int h = 0; h < BytesPerBlock; ++h)
	{
		uint8_t Compressed = 0;
		int Op = 0;

		if (BitsPerSample == 2)
		{
			int Ix = h * 4;
#ifdef PREDICT
			Ix *= 2;
#endif

			for (int i = 0; i < 4; ++i)
			{
				int PredictedDelta = CalcPrediction(prevDelta[0], prevDelta[1], avgD, coeff1, coeff2, coeff3);
				int ActualDelta = chunkSamples[Ix++] - prevSample;
				int Delta = FindClosest(ActualDelta - PredictedDelta, BestDelta, OpTable, coeff3, Op);
				prevDelta[1] = prevDelta[0];
				prevDelta[0] = PredictedDelta + Delta;
				avgD = AddAvg(avgD, prevDelta[0]);
				prevSample = Clamp255(prevSample + prevDelta[0]);

#ifdef PREDICT
				Ix++;
				PredictedDelta = CalcPrediction(prevDelta[0], prevDelta[1], avgD, coeff1, coeff2, coeff3);
				prevDelta[1] = prevDelta[0];
				prevDelta[0] = PredictedDelta;
				avgD = AddAvg(avgD, prevDelta[0]);
				prevSample = Clamp255(prevSample + prevDelta[0]);
#endif
			}
		}
		else if (BitsPerSample == 1)
		{
			int Ix = h * 8;
#ifdef PREDICT
			Ix *= 2;
#endif

			for (int i = 0; i < 8; ++i)
			{
				int PredictedDelta = CalcPrediction(prevDelta[0], prevDelta[1], avgD, coeff1, coeff2, coeff3);
				int ActualDelta = chunkSamples[Ix++] - prevSample;
				int Delta = FindClosest(ActualDelta - PredictedDelta, BestDelta, OpTable, coeff3, Op);
				prevDelta[1] = prevDelta[0];
				prevDelta[0] = PredictedDelta + Delta;
				avgD = AddAvg(avgD, prevDelta[0]);
				prevSample = Clamp255(prevSample + prevDelta[0]);

#ifdef PREDICT
				Ix++;
				PredictedDelta = CalcPrediction(prevDelta[0], prevDelta[1], avgD, coeff1, coeff2, coeff3);
				prevDelta[1] = prevDelta[0];
				prevDelta[0] = PredictedDelta;
				prevSample = Clamp255(prevSample + prevDelta[0]);
#endif
			}
		}
	}

	return Error;
}

#if 0
void FindCoeffs_SIMD(SK::IO::Stream& stream, int prevSample, int prevDelta[2], int& out_Coeff1, int& out_Coeff2, int& out_Coeff3)
{
	uint64_t StartPos = stream.Position();
	int16_t* Samples16 = new int16_t[SamplesPerChunk];
	stream.ReadData(reinterpret_cast<uint8_t*>(Samples16), 0, SamplesPerChunk * sizeof(int16_t));
	stream.Position(StartPos);

	int* Samples = new int[SamplesPerChunk];
	for (int i = 0; i < SamplesPerChunk; ++i)
		Samples[i] = ConvertTo8(Samples16[i]);
	delete[] Samples16;

	int CoeffCalcOrder[8] = { 0, -1, 1, -2, 2, -3, 3, -4 };
	double CoeffError = DBL_MAX;
	for (int i = 0; i < 8; ++i)
	{
		int Coeff1 = CoeffCalcOrder[i];
		for (int j = 0; j < 8; ++j)
		{
			int Coeff2 = CoeffCalcOrder[j];

			for (int Coeff3 = -4; Coeff3 < 4; ++Coeff3)
			{
				__m128i SIMDCoeff2 = _mm_set1_epi32(Coeff2);
				__m128i SIMDCoeff1 = _mm_set1_epi32(Coeff1);
				double Error = 0;

				//First four samples, need to be separate from the main loop because the first 2 deltas are passed
				// in as arguemnts
				{
					int Delta1 = prevDelta[0];
					int Delta2 = prevDelta[1];
					int PrevSample = prevSample;

					int DeltaPrediction = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
					Delta2 = Samples[0] - PrevSample;
					PrevSample = Samples[0];
					Error += static_cast<double>(DeltaPrediction - Delta2) * static_cast<double>(DeltaPrediction - Delta2);

					DeltaPrediction = CalcPrediction(Delta2, Delta1, Coeff1, Coeff2, Coeff3);
					Delta1 = Samples[1] - PrevSample;
					PrevSample = Samples[1];
					Error += static_cast<double>(DeltaPrediction - Delta1) * static_cast<double>(DeltaPrediction - Delta1);

					DeltaPrediction = CalcPrediction(Delta1, Delta2, Coeff1, Coeff2, Coeff3);
					Delta2 = Samples[2] - PrevSample;
					PrevSample = Samples[2];
					Error += static_cast<double>(DeltaPrediction - Delta2) * static_cast<double>(DeltaPrediction - Delta2);

					DeltaPrediction = CalcPrediction(Delta2, Delta1, Coeff1, Coeff2, Coeff3);
					Delta1 = Samples[3] - PrevSample;
					PrevSample = Samples[3];
					Error += static_cast<double>(DeltaPrediction - Delta1) * static_cast<double>(DeltaPrediction - Delta1);
				}

				for (int k = 4; k < SamplesPerChunk; k += 4)
				{
					//Loading up these samples and getting the deltas could be quicker, but �\_(?)_/�
					__m128i Sample = _mm_loadu_si128(reinterpret_cast<__m128i*>(&Samples[k - 0]));
					__m128i PrevSample1 = _mm_loadu_si128(reinterpret_cast<__m128i*>(&Samples[k - 1]));
					__m128i PrevSample2 = _mm_loadu_si128(reinterpret_cast<__m128i*>(&Samples[k - 2]));
					__m128i PrevSample3 = _mm_loadu_si128(reinterpret_cast<__m128i*>(&Samples[k - 3]));

					__m128i ActualDelta = _mm_sub_epi32(Sample, PrevSample1);
					__m128i Delta1 = _mm_sub_epi32(PrevSample1, PrevSample2);
					__m128i Delta2 = _mm_sub_epi32(PrevSample2, PrevSample3);

					//SKTD: CalcSample if BlocksPerChunk > 0
					__m128i Prediction = _mm_add_epi32(_mm_mullo_epi32(Delta1, SIMDCoeff1), _mm_mullo_epi32(Delta2, SIMDCoeff2));
					Prediction = _mm_srai_epi32(Prediction, 2);
					__m128 Errors = _mm_cvtepi32_ps(_mm_sub_epi32(Prediction, ActualDelta));
					Errors = _mm_mul_ps(Errors, Errors);
					Errors = _mm_hadd_ps(Errors, Errors);
					Errors = _mm_hadd_ps(Errors, Errors);

					Error += _mm_cvtss_f32(Errors);

					if (Error > CoeffError)
						break;
				}

				if (Error < CoeffError)
				{
					CoeffError = Error;
					out_Coeff1 = Coeff1;
					out_Coeff2 = Coeff2;
					out_Coeff3 = Coeff3;
				}
			}
		}
	}

	delete[] Samples;
}
#endif

struct shitballs
{
	volatile double& CoeffError;
	int& out_Coeff1;
	int& out_Coeff2;
	int& out_Coeff3;
	int* prevDelta;
	int prevSample;
	int avg;
	uint8_t* Samples;
};

//SKTD: Only need this shit when going nuts
void jizz(shitballs* b, int Coeff1)
{
	int CoeffCalcOrder[8] = { 0, -1, 1, -2, 2, -3, 3, -4 };
	//for (int i = 0; i < 8; ++i)
	{
		//int Coeff1 = CoeffCalcOrder[i];
		for (int j = 0; j < 8; ++j)
		{
			int Coeff2 = CoeffCalcOrder[j];
			//for (int Coeff3 = -4; Coeff3 < 4; ++Coeff3)
			{

				int Coeff3 = 0;
				double Error = 0;
#if 0
				double localCoeffError = b->CoeffError;
				int D[2] = { b->prevDelta[0], b->prevDelta[1] };
				int Avg = b->avg;
				int S = b->prevSample;
				for (int B = 0; B < BlocksPerChunk; ++B)
				{
					Error += CalcBlockError(b->Samples + (B * SamplesPerBlock), Coeff1, Coeff2, Coeff3, D, Avg, S, localCoeffError);
					if (Error >= localCoeffError)
						break;
				}
#else
				int Delta1 = b->prevDelta[0];
				int Delta2 = b->prevDelta[1];
				int PrevSample = b->prevSample;
				int Avg = b->avg;

				for (int k = 0; k < SamplesPerChunk; k += 4)
				{
					uint32_t Sample = *reinterpret_cast<uint32_t*>(&b->Samples[k]);
					int DeltaPrediction = CalcPrediction(Delta1, Delta2, Avg, Coeff1, Coeff2, Coeff3);
					Delta2 = (Sample & 0xFF) - PrevSample;
					Avg = AddAvg(Avg, Delta2);
					PrevSample = (Sample & 0xFF);
					Error += (DeltaPrediction - Delta2) * (DeltaPrediction - Delta2);

					DeltaPrediction = CalcPrediction(Delta2, Delta1, Avg, Coeff1, Coeff2, Coeff3);
					Delta1 = (Sample >> 8 & 0xFF) - PrevSample;
					Avg = AddAvg(Avg, Delta1);
					PrevSample = (Sample >> 8 & 0xFF);
					Error += (DeltaPrediction - Delta1) * (DeltaPrediction - Delta1);

					DeltaPrediction = CalcPrediction(Delta1, Delta2, Avg, Coeff1, Coeff2, Coeff3);
					Delta2 = (Sample >> 16 & 0xFF) - PrevSample;
					Avg = AddAvg(Avg, Delta2);
					PrevSample = (Sample >> 16 & 0xFF);
					Error += (DeltaPrediction - Delta2) * (DeltaPrediction - Delta2);

					DeltaPrediction = CalcPrediction(Delta2, Delta1, Avg, Coeff1, Coeff2, Coeff3);
					Delta1 = (Sample >> 24 & 0xFF) - PrevSample;
					Avg = AddAvg(Avg, Delta1);
					PrevSample = (Sample >> 24 & 0xFF);
					Error += (DeltaPrediction - Delta1) * (DeltaPrediction - Delta1);

					if (Error > b->CoeffError)
						break;
				}
#endif

				if (Error < b->CoeffError)
				{
					_BestErrorMutex.lock();
					if (Error < b->CoeffError)
					{
						b->CoeffError = Error;
						b->out_Coeff1 = Coeff1;
						b->out_Coeff2 = Coeff2;
						b->out_Coeff3 = Coeff3;
					}
					_BestErrorMutex.unlock();
				}
			}
		}
	}
};

void FindCoeffs_C(SK::IO::Stream& stream, int prevSample, int prevDelta[2], int avg, int& out_Coeff1, int& out_Coeff2, int& out_Coeff3)
{
	uint64_t StartPos = stream.Position();
	int16_t* Samples16 = new int16_t[SamplesPerChunk];
	stream.ReadData(reinterpret_cast<uint8_t*>(Samples16), 0, SamplesPerChunk * sizeof(int16_t));
	stream.Position(StartPos);

	uint8_t* Samples = new uint8_t[SamplesPerChunk];
	for (int i = 0; i < SamplesPerChunk; ++i)
		Samples[i] = ConvertTo8(Samples16[i]);
	delete[] Samples16;

	SK::Threading::FutureGroup<void, 256> Tasks;

	_ThreadPool.BeginAddTasks();

	int CoeffCalcOrder[8] = { 0, -1, 1, -2, 2, -3, 3, -4 };
	volatile double CoeffError = DBL_MAX;
	shitballs balls = { CoeffError, out_Coeff1, out_Coeff2, out_Coeff3, prevDelta, prevSample, avg, Samples };

	for (int i = 0; i < 8; ++i)
	{
		int Coeff1 = CoeffCalcOrder[i];
		//for (int j = 0; j < 8; ++j)
		{
			//int Coeff2 = CoeffCalcOrder[j];
			//for (int Coeff3 = -4; Coeff3 < 4; ++Coeff3)
			{
				Tasks.Add(_ThreadPool.AddTask_I(jizz, &balls, Coeff1));
			}
		}
	}
	
	_ThreadPool.EndAddTasks();
	_ThreadPool.RunTasksInCurrentContext();
	Tasks.Wait();

	delete[] Samples;
}

void Tests::PCMTest()
{
	SK::IO::FileStream FS(SK::String("D:/PCMTest/Original/Lucifer_16.22.raw"), SK::IO::FileMode::Read);
	SK::IO::MemoryStream OUT;
	SK::IO::MemoryStream TEST_OUT;

	SK::IO::MemoryStream IN;
	FS.CopyTo(IN);
	IN.Position(0);
	FS.Close();

#ifdef DEBUG_USAGE
	static double RMSError = 0;
	int NumErrs = 0;
	int OpTableCounts[4] = { 0,0,0,0 };
	int OpTableThingCounts[4][4] = { 0 };
	static int Coeff3Usage[4] = { 0 };
#endif

	uint32_t Length = static_cast<uint32_t>(IN.Length());
	int16_t* ChunkSamples16 = new int16_t[SamplesPerBlock];
	uint8_t* ChunkSamples = new uint8_t[SamplesPerBlock];

	int Coeff1 = 0;
	int Coeff2 = 0;
	int Coeff3 = 3;

	int NumBlocksInChunk = 0;
	int PrevSample;
	int PrevDelta[2] = { 0 };
	int Avg = 0;

	int16_t FirstSample = 0;
	IN.Read(FirstSample);
	IN.Position(0);
	PrevSample = ConvertTo8(FirstSample);

	OUT.Write(static_cast<uint8_t>(PrevSample));

	while(IN.Position() < IN.Length())
	{
		if (NumBlocksInChunk == 0 && BlocksPerChunk > 0)
		{
			FindCoeffs(IN, PrevSample, PrevDelta, Avg, Coeff1, Coeff2, Coeff3);

			//++Coeff3Usage[Coeff3-2];
			IN.ReadData(reinterpret_cast<uint8_t*>(ChunkSamples16), 0, SamplesPerBlock * sizeof(int16_t));
			for (int s = 0; s < SamplesPerBlock; ++s)
				ChunkSamples[s] = ConvertTo8(ChunkSamples16[s]);

			//uint8_t Coeffs = (Coeff1 + 4) | ((Coeff2 + 4) << 3) | ((Coeff3-2) << 6);
			//OUT.Write(Coeffs);
			OUT.Write(static_cast<int8_t>(Coeff1));
			OUT.Write(static_cast<int8_t>(Coeff2));
			OUT.Write(static_cast<int8_t>(Coeff3));
		}
		else
		{
			IN.ReadData(reinterpret_cast<uint8_t*>(ChunkSamples16), 0, SamplesPerBlock * sizeof(int16_t));
			for (int s = 0; s < SamplesPerBlock; ++s)
				ChunkSamples[s] = ConvertTo8(ChunkSamples16[s]);
		}

		int OpTable = 0;
		int BestDelta = 0;
		volatile double BestError = DBL_MAX;

		if (FindBestDelta(0, PrevDelta, PrevSample, Avg, ChunkSamples, Coeff1, Coeff2, Coeff3, BestError, BestDelta))
			OpTable = 0;

		if (FindBestDelta(1, PrevDelta, PrevSample, Avg, ChunkSamples, Coeff1, Coeff2, Coeff3, BestError, BestDelta))
			OpTable = 1;

		if (FindBestDelta(2, PrevDelta, PrevSample, Avg, ChunkSamples, Coeff1, Coeff2, Coeff3, BestError, BestDelta))
			OpTable = 2;

		if (FindBestDelta(3, PrevDelta, PrevSample, Avg, ChunkSamples, Coeff1, Coeff2, Coeff3, BestError, BestDelta))
			OpTable = 3;

#ifdef DEBUG_USAGE
		RMSError += BestError;
		NumErrs += SamplesPerBlock;
		++OpTableCounts[OpTable];
#endif

		int Flags = OpTable << 6;
		OUT.Write(static_cast<uint8_t>(BestDelta | Flags));

		for (int h = 0; h < BytesPerBlock; ++h)
		{
			uint8_t Compressed = 0;
			int Op = 0;

			if (BitsPerSample == 2)
			{
				int Ix = h * 4;
#ifdef PREDICT
				Ix *= 2;
#endif

				for (int i = 0; i < 4; ++i)
				{
					int PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Avg, Coeff1, Coeff2, Coeff3);
					int ActualDelta = ChunkSamples[Ix++] - PrevSample;
					int Delta = FindClosest(ActualDelta - PredictedDelta, BestDelta, OpTable, Coeff3, Op);
					PrevDelta[1] = PrevDelta[0];
					PrevDelta[0] = PredictedDelta + Delta;
					Avg = AddAvg(Avg, PrevDelta[0]);
					PrevSample = Clamp255(PrevSample + PrevDelta[0]);
					Compressed |= Op << (2*i);

					TEST_OUT.Write(static_cast<int8_t>(PrevDelta[0] - PrevDelta[1]));
#ifdef PREDICT
					Ix++;
					PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Avg, Coeff1, Coeff2, Coeff3);
					PrevDelta[1] = PrevDelta[0];
					PrevDelta[0] = PredictedDelta;
					Avg = AddAvg(Avg, PrevDelta[0]);
					PrevSample = Clamp255(PrevSample + PrevDelta[0]);
#endif
				}

#ifdef DEBUG_USAGE
				++OpTableThingCounts[OpTable][Compressed & 0x03];
				++OpTableThingCounts[OpTable][(Compressed & 0x0C) >> 2];
				++OpTableThingCounts[OpTable][(Compressed & 0x30) >> 4];
				++OpTableThingCounts[OpTable][(Compressed & 0xC0) >> 6];
#endif
			}
			else if (BitsPerSample == 1)
			{
				int Ix = h * 8;
#ifdef PREDICT
				Ix *= 2;
#endif

				for (int i = 0; i < 8; ++i)
				{
					int PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Avg, Coeff1, Coeff2, Coeff3);
					int ActualDelta = ChunkSamples[Ix++] - PrevSample;
					int Delta = FindClosest(ActualDelta - PredictedDelta, BestDelta, OpTable, Coeff3, Op);
					PrevDelta[1] = PrevDelta[0];
					PrevDelta[0] = PredictedDelta + Delta;
					Avg = AddAvg(Avg, PrevDelta[0]);
					PrevSample = Clamp255(PrevSample + PrevDelta[0]);
					Compressed |= Op << i;

#ifdef PREDICT
					Ix++;
					PredictedDelta = CalcPrediction(PrevDelta[0], PrevDelta[1], Avg, Coeff1, Coeff2, Coeff3);
					PrevDelta[1] = PrevDelta[0];
					PrevDelta[0] = PredictedDelta;
					Avg = AddAvg(Avg, PrevDelta[0]);
					PrevSample = Clamp255(PrevSample + PrevDelta[0]);
#endif
				}

#ifdef DEBUG_USAGE
				++OpTableThingCounts[OpTable][Compressed & 0x01];
				++OpTableThingCounts[OpTable][(Compressed & 0x02) >> 1];
				++OpTableThingCounts[OpTable][(Compressed & 0x04) >> 2];
				++OpTableThingCounts[OpTable][(Compressed & 0x08) >> 3];
				++OpTableThingCounts[OpTable][(Compressed & 0x10) >> 4];
				++OpTableThingCounts[OpTable][(Compressed & 0x20) >> 5];
				++OpTableThingCounts[OpTable][(Compressed & 0x40) >> 6];
				++OpTableThingCounts[OpTable][(Compressed & 0x80) >> 7];
#endif
			}

			OUT.Write(Compressed);
		}

		if (BlocksPerChunk > 0 && ++NumBlocksInChunk == BlocksPerChunk)
			NumBlocksInChunk = 0;
	}

#ifdef DEBUG_USAGE
	double TotalTableUsages = OpTableCounts[0] + OpTableCounts[1] + OpTableCounts[2] + OpTableCounts[3];
	static double Usages[4] =
	{
		OpTableCounts[0] / TotalTableUsages,
		OpTableCounts[1] / TotalTableUsages,
		OpTableCounts[2] / TotalTableUsages,
		OpTableCounts[3] / TotalTableUsages,
	};

	static double OpUsages[4][4];
	for (int i = 0; i < 4; ++i)
	{
		double TotalOpUsages = OpTableThingCounts[i][0] + OpTableThingCounts[i][1] + OpTableThingCounts[i][2] + OpTableThingCounts[i][3];
		if (TotalOpUsages > 0)
		{
			OpUsages[i][0] = OpTableThingCounts[i][0] / TotalOpUsages;
			OpUsages[i][1] = OpTableThingCounts[i][1] / TotalOpUsages;
			OpUsages[i][2] = OpTableThingCounts[i][2] / TotalOpUsages;
			OpUsages[i][3] = OpTableThingCounts[i][3] / TotalOpUsages;
		}
	}

	RMSError = sqrt(RMSError / NumErrs);
#endif
	
	SK::IO::FileStream CompressedFile(SK::String("D:/PCMTest/Compressed/Lucifer_L_SKPCM.raw"), SK::IO::FileMode::Truncate);
	OUT.CopyTo(CompressedFile);
	CompressedFile.Close();
	OUT.Close();

	SK::IO::FileStream ShitFile(SK::String("D:/PCMTest/ActualDeltas.raw"), SK::IO::FileMode::Truncate);
	TEST_OUT.CopyTo(ShitFile);
	ShitFile.Close();
	TEST_OUT.Close();

	SK::IO::MemoryStream PCM;
	LoadCompressedSnd("D:/PCMTest/Compressed/Lucifer_L_SKPCM.raw", PCM);
	SK::IO::FileStream OutputFile(SK::String("D:/PCMTest/Lucifer_L_Test.raw"), SK::IO::FileMode::Truncate);
	PCM.CopyTo(OutputFile);
	OutputFile.Close();
	PCM.Close();

	_ThreadPool.Stop();

	return;
}
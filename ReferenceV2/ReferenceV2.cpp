#include <SKSystem/Time/Clocks.h>
#include <SKSystem/Debug.h>
#include "Tests.h"
#include "ReferenceV2.h"

#include <SKSystem/GUI/Window.h>
#include <SKSystem/Threading/Thread.h>
#include <SKGraphics/Renderer.h>
#include <SKSystem/IO/FileStream.h>
#include <SKGraphics/MeshLoader.h>
#include <SKGraphics/RenderTree.h>
#include <SKGraphics/RenderPass.h>
#include <SKGraphics/ResourceCache.h>
#include <SKInput/DeviceManager.h>
#include <SKInput/GameKeys.h>
#include "TestShader.h"

#include <SKGraphics/Debug/DebugShapes.h>

#include <SKSystem/Win32/Win32.h>
#include <directxmath.h>
#include <inttypes.h>


namespace
{
	void Profile(SK::Graphics::RenderPass& pass, SK::Graphics::Camera& camera)
	{
		SK::Time::HighPerformanceClock Clock;
		long long Count = 0;
		auto Start = Clock.Time();

		uint64_t Pos = 0;
		do
		{
			// Insert stuffs here
			camera.Position(-camera.Position());
			pass.Prepare();
			++Count;

		} while ((Clock.Time() - Start) < SK::Time::Seconds(1));

		SK::Debug::Print("Number of executions: ", Count);
	}

	SK::Math::Matrix LookAt(const SK::Math::Vector& vcPos, const SK::Math::Vector& vcLookAt, const SK::Math::Vector& vcWorldUp)
	{
		SK::Math::Vector vcDir = vcLookAt - vcPos;
		vcDir.Normalise();

		float fAngle = 0.0f;
		fAngle = SK::Math::Vector::Dot(vcWorldUp, vcDir);

		SK::Math::Vector vcUp(vcWorldUp);
		vcUp -= (vcDir * fAngle);
		vcUp.Normalise();

		SK::Math::Vector vcRight(vcUp);
		vcRight.Cross(vcDir);

		SK::Math::Matrix ViewMat = SK::Math::Matrix::Identity();
		ViewMat.Row0 = vcRight.Data;
		ViewMat.Row1 = vcUp.Data;
		ViewMat.Row2 = vcDir.Data;
		ViewMat.Row3 = (-vcPos).Data;

		ViewMat.Row0.m128_f32[3] = 0.0f;
		ViewMat.Row1.m128_f32[3] = 0.0f;
		ViewMat.Row2.m128_f32[3] = 0.0f;
		ViewMat.Row3.m128_f32[3] = 1.0f;

		return ViewMat;
	}

	void LoadMeshTextures(SK::Graphics::MeshPtr mesh, SK::Graphics::Renderer& renderer)
	{
		std::hash<SK::String> NameHash;
		auto& TextureCache = SK::Graphics::ResourceCache<SK::Graphics::Texture>::Instance();

		for (auto& Material : mesh->Materials())
		{
			for (int i = 0; i < SK::Graphics::MAX_TEXTURE_UNITS; ++i)
			{
				auto& TextureName = Material.TextureNames[i];
				if (TextureName.Length() > 0 && !TextureCache.HasResource(NameHash(TextureName)))
				{
					SK::IO::FileStream TextureFile(SK::IO::Path(SK::IO::SpecialFolder::Install, TextureName), SK::IO::FileMode::Read);
					if (TextureFile.IsOpen())
					{
						uint32_t TextureDataLength = static_cast<uint32_t>(TextureFile.Length());
						uint8_t* TextureData = new uint8_t[TextureDataLength];
						TextureFile.ReadData(TextureData, 0, TextureDataLength);
						auto Texture = renderer.CreateTextureFromDDS(TextureData, TextureDataLength);
						delete [] TextureData;
						TextureFile.Close();

						TextureCache.AddResource(NameHash(TextureName), Texture);
					}
				}
			}
		}

		for (auto Child : mesh->Children())
		{
			LoadMeshTextures(Child, renderer);
		}
	}

	enum class GameKeys : int
	{
		Quit,
		CamLeft,
		CamRight,
		CamForward,
		CamBackward,
		CamUp,
		CamDown,

		CamRotUp,
		CamRotDown,
		CamRotLeft,
		CamRotRight,
	};
}

void RenderExtents(SK::Graphics::Renderer& renderer, SK::Graphics::DebugShapes& shapes, SK::Graphics::Renderable* mesh, bool recursive = true)
{
	if (mesh->Visible())
	{
		auto& Box = mesh->AABB();
		shapes.AddBox(SK::Math::Matrix::Translation(Box.Centre.Get()), Box.Size.Get().xyz());
	}

	if (recursive)
	{
		for (auto child = mesh->Child(); child; child = child->Next())
			RenderExtents(renderer, shapes, child, true);
	}
}

static float RenderTime = 0;
static float PrepareTime = 0;
static float GFXTime = 0;

void RefV2::TestFunction()
{
	static bool _ShowOcclusionBuffer = false;
	static bool _RenderExtents = false;

	SK::Input::DeviceManager& DevMan = SK::Input::DeviceManager::Instance();
	DevMan.FindDevices();
	DevMan.DisableSystemShortcuts(true);
	DevMan.BackgroundInput(false);

	SK::Input::KeyMap<GameKeys> KeyMap;
	KeyMap.Add(GameKeys::Quit, SK::Input::KeyBinding(SK::Input::SpecialDevice::SystemKeyboard, SK::Input::Key::Escape));
	KeyMap.Add(GameKeys::CamLeft, SK::Input::KeyBinding(SK::Input::SpecialDevice::SystemKeyboard, SK::Input::Key::A));
	KeyMap.Add(GameKeys::CamRight, SK::Input::KeyBinding(SK::Input::SpecialDevice::SystemKeyboard, SK::Input::Key::D));
	KeyMap.Add(GameKeys::CamForward, SK::Input::KeyBinding(SK::Input::SpecialDevice::SystemKeyboard, SK::Input::Key::W));
	KeyMap.Add(GameKeys::CamBackward, SK::Input::KeyBinding(SK::Input::SpecialDevice::SystemKeyboard, SK::Input::Key::S));
	KeyMap.Add(GameKeys::CamRotLeft, SK::Input::KeyBinding(SK::Input::SpecialDevice::SystemKeyboard, SK::Input::Key::Left));
	KeyMap.Add(GameKeys::CamRotRight, SK::Input::KeyBinding(SK::Input::SpecialDevice::SystemKeyboard, SK::Input::Key::Right));
	KeyMap.Add(GameKeys::CamRotUp, SK::Input::KeyBinding(SK::Input::SpecialDevice::SystemKeyboard, SK::Input::Key::Up));
	KeyMap.Add(GameKeys::CamRotDown, SK::Input::KeyBinding(SK::Input::SpecialDevice::SystemKeyboard, SK::Input::Key::Down));

	if (DevMan.Joypads().NumDevices() > 0)
	{
		auto& Pad = DevMan.Joypads()[0];
		KeyMap.Add(GameKeys::Quit, SK::Input::KeyBinding(Pad.Id(), SK::Input::Key::Button2));
	}

	SK::GUI::Window MyWindow("Graphics Test", SK::Math::int2(1280, 720), SK::GUI::WindowStyle::FixedSize);

	// Load application icon
	SK::IO::FileStream FS(SK::IO::Path(SK::IO::SpecialFolder::Install, "Icon.png"), SK::IO::FileMode::Read);
	SK::Graphics::Image IconPng = SK::Graphics::Image::Load(FS, FS.Length());
	FS.Close();

	// Apply icon
	SK::Graphics::MipmapImage Icon(IconPng, 6);
	MyWindow.SetIcon(Icon);

	// Show the window and get some sweet GFX going
	MyWindow.Show();
	static SK::Graphics::RenderSettings Settings;
	Settings.Width = 0;
	Settings.Height = 0;
	Settings.Multisample = SK::Graphics::MultisampleType::MSAAx2;
	Settings.PresentInterval = 0;
	Settings.Windowed = true;
	Settings.RationalRefreshRate = SK::Math::int2(0, 0);

	SK::Graphics::Renderer MyRenderer;

	SK::Graphics::RenderCaps Caps;
	MyRenderer.GetCaps(Caps);

	MyRenderer.Initialise(MyWindow, Settings);

	SK::IO::FileStream MyMesh(SK::IO::Path(SK::IO::SpecialFolder::Install, "library1.skmesh"), SK::IO::FileMode::Read);
	auto MySourceMesh = SK::Graphics::MeshLoader::LoadMesh(MyMesh, MyRenderer);
	MyMesh.Close();
	LoadMeshTextures(MySourceMesh, MyRenderer);

	//-- Shaders
	SK::String Errors;
	SK::IO::FileStream ShaderFile = SK::IO::FileStream(SK::IO::Path(SK::IO::SpecialFolder::Install, "Shaders.hlsl"), SK::IO::FileMode::Read);
	uint32_t ShaderLen = static_cast<uint32_t>(ShaderFile.Length());
	uint8_t* ShaderBytes = new uint8_t[ShaderLen + 1];
	ShaderFile.ReadData(ShaderBytes, 0, ShaderLen);
	ShaderFile.Close();

	auto VSCode = MyRenderer.CompileShader(ShaderBytes, ShaderLen, "Standard3D", SK::Graphics::FeatureLevel::vs_2_0, &Errors);
	auto PSCode = MyRenderer.CompileShader(ShaderBytes, ShaderLen, "StandardLit", SK::Graphics::FeatureLevel::ps_2_0, &Errors);

	auto VShader = MyRenderer.CreateVertexShader("TestVertexShader", VSCode, static_cast<uint32_t>(VSCode.Length()), SK::Graphics::InputLayout::Vertex3D);
	auto PShader = MyRenderer.CreatePixelShader("TestPixelShader", PSCode, static_cast<uint32_t>(PSCode.Length()));
	MyRenderer.SetVertexShader(VShader);
	MyRenderer.SetPixelShader(PShader);

	VSCode = MyRenderer.CompileShader(ShaderBytes, ShaderLen, "Standard2D", SK::Graphics::FeatureLevel::vs_2_0, &Errors);
	PSCode = MyRenderer.CompileShader(ShaderBytes, ShaderLen, "ShowTexture", SK::Graphics::FeatureLevel::ps_2_0, &Errors);
	auto VShader2D = MyRenderer.CreateVertexShader("2D", VSCode, static_cast<uint32_t>(VSCode.Length()), SK::Graphics::InputLayout::Vertex2D);
	auto PShader2D = MyRenderer.CreatePixelShader("Tex", PSCode, static_cast<uint32_t>(PSCode.Length()));

	VSCode = MyRenderer.CompileShader(ShaderBytes, ShaderLen, "DebugShapes", SK::Graphics::FeatureLevel::vs_2_0, &Errors);
	PSCode = MyRenderer.CompileShader(ShaderBytes, ShaderLen, "Unlit", SK::Graphics::FeatureLevel::ps_2_0, &Errors);
	auto DebugVShader = MyRenderer.CreateVertexShader("DebugVertexShader", VSCode, static_cast<uint32_t>(VSCode.Length()), SK::Graphics::InputLayout::Vertex3D_InstanceMatCol);
	auto DebugPShader = MyRenderer.CreatePixelShader("DebugPixelShader", PSCode, static_cast<uint32_t>(PSCode.Length()));

	VSCode = MyRenderer.CompileShader(ShaderBytes, ShaderLen, "Standard3D_Instanced", SK::Graphics::FeatureLevel::vs_2_0, &Errors);
	delete[] ShaderBytes;

	auto InstancedVShader = MyRenderer.CreateVertexShader("Standard3D_Instanced", VSCode, static_cast<uint32_t>(VSCode.Length()), SK::Graphics::InputLayout::Vertex3D_InstanceMat);
	//

	auto WVPId = MyRenderer.GetShaderParamId("WorldViewProj");
	auto WId = MyRenderer.GetShaderParamId("World");
	//--

	MyRenderer.SetCullingMode(SK::Graphics::CullMode::CounterClockwise);
	MyRenderer.SetBlendingMode(SK::Graphics::BlendMode::Normal);

	SK::Math::float3 CamPos(-14.1594f, 7.29f, -29.51073f);
	
	SK::Graphics::Camera MyCamera;
	MyCamera.Aspect(1280.0f / 720.0f);
	MyCamera.FoV(0.5f);
	MyCamera.ClippingPlanes(1.0f, 2000.0f);
	MyCamera.Position(SK::Math::Vector(SK::Math::float4(CamPos, 1.0f)));
	MyCamera.LookAt(SK::Math::Vector(-5.8f, 7.0f, 0, 1));

	SK::Math::Vector LightDir(-1, -2, 1, 0);
	LightDir.Normalise();
	SK::Math::float3 Daters = LightDir.Get().xyz();
	std::hash<SK::String> Hash;
	MyRenderer.SetShaderParam(Hash("LightDir"), &Daters, 12);

#ifdef _DEBUG
	SK::Math::int2 GridSize(4, 8);
#else
	SK::Math::int2 GridSize(64, 128);
#endif
	SK::Math::float2 MeshBlockSize(38, 23);
	SK::Graphics::RenderTree RTree(256000);

	SK::Graphics::MeshInstanceManager InstanceMan;
	InstanceMan.SortOrder(SK::Graphics::RenderSortOrder::Last);
	InstanceMan.Cullable(false);

	float StartX = (GridSize.x / 2) * -MeshBlockSize.x;
	float StartZ = (GridSize.y / 2) * -MeshBlockSize.y;
	for (int i = 0; i < GridSize.x; ++i)
	{
		for (int k = 0; k < GridSize.y; ++k)
		{
			auto TestMesh = MySourceMesh->CreateInstance(InstanceMan, MyRenderer);
			TestMesh->LocalTransform(SK::Math::Matrix::Translation(StartX + i*MeshBlockSize.x, 0, StartZ + k*MeshBlockSize.y));
			RTree.Add(TestMesh);
		}
	}

	auto CPUGPUSoftwareDepthBufferThing = MyRenderer.CreateTexture(SK::Math::int3(1280, 720, 0), SK::Graphics::TextureFormat::Red_8, SK::Graphics::TextureFlag::Lockable, nullptr);
	RTree.Add(&InstanceMan);

	SK::Graphics::RenderPass MainPass(2);
	MainPass.SetCamera(&MyCamera);
	MainPass.SetRenderTree(&RTree);
	MainPass.SetRenderTarget(CPUGPUSoftwareDepthBufferThing);
	MainPass.EnableOcclusionCulling(SK::Math::int2(1280, 720), 8192);

	SK::Graphics::DebugShapes DebugRender(0, GridSize.x * GridSize.y * 32);
	DebugRender.Initialise(MyRenderer);

	SK::Time::HighPerformanceClock Clock;
	auto StartTime = Clock.Time();
	float CamSpeed = 0.5f;
	int UpdateRate = 60;
	auto FrameDuration = SK::Time::Seconds(1) / 60;
	SK::Graphics::ColourFloat ClearCol(1.0f, 0.0f, 0.0f, 0.0f);
	uint64_t FPS = 0;
	auto FPSCounterTime = Clock.Time();
	uint64_t LastFPS = 0;

	SK::Graphics::Vertex2D PP[6] =
	{
		{ SK::Math::float2(1, 1), SK::Math::float2(1, 0), SK::Graphics::ColourFloat(1, 1, 0, 0) },
		{ SK::Math::float2(1, -1), SK::Math::float2(1, 1), SK::Graphics::ColourFloat(1, 0, 1, 0) },
		{ SK::Math::float2(-1, 1), SK::Math::float2(0, 0), SK::Graphics::ColourFloat(1, 0, 0, 1) },
		{ SK::Math::float2(-1, -1), SK::Math::float2(0, 1), SK::Graphics::ColourFloat(1, 1, 1, 1) },
	};
	auto BumHole = MyRenderer.CreateVertexBuffer(SK::Graphics::VertexType::Vertex2D, 4, false, &PP, SK::Graphics::VertexSize(SK::Graphics::VertexType::Vertex2D) * 4);

	while (MyWindow.IsOpen())
	{
		auto CurrentTime = Clock.Time();
		auto Delta = CurrentTime - StartTime;
		
		if (CurrentTime - FPSCounterTime >= SK::Time::Seconds(1))
		{
			FPSCounterTime = CurrentTime - ((CurrentTime - FPSCounterTime) - SK::Time::Seconds(1));
			SK::Debug::Print("FPS: ", FPS);
			LastFPS = FPS;

			float Prep = PrepareTime / FPS;
			float Render = RenderTime / FPS;
			float GFX = GFXTime / FPS;
			MyWindow.SetTitle(SK::String::Format("Graphics Test - FPS: %llu Update: %.2fms Render: %.2fms GFX: %.2fms Total: %.2fms", FPS, Prep, Render, GFX, Prep + Render + GFX));
			FPS = 0;
			RenderTime = 0;
			PrepareTime = 0;
			GFXTime = 0;
		}

		FPS++;

		{
			MyWindow.Update();
			DevMan.Update();
			
			KeyMap.Update();

			if (Delta >= FrameDuration)
			{

				if (Delta > FrameDuration * 3)
					Delta = FrameDuration;

				DebugRender.Clear();

				StartTime = CurrentTime - (Delta - FrameDuration);
				auto DeltaF = FrameDuration.Seconds<float>();

				SK::Math::float3 MoveDirection;
				MoveDirection.x = KeyMap.Value(GameKeys::CamRight) - KeyMap.Value(GameKeys::CamLeft);
				MoveDirection.z = KeyMap.Value(GameKeys::CamForward) - KeyMap.Value(GameKeys::CamBackward);

				SK::Math::float3 RotDirection;
				RotDirection.y = KeyMap.Value(GameKeys::CamRotLeft) - KeyMap.Value(GameKeys::CamRotRight);
				RotDirection.x = KeyMap.Value(GameKeys::CamRotUp) - KeyMap.Value(GameKeys::CamRotDown);

				SK::Math::Quaternion CamOrientation = MyCamera.Orientation();
				SK::Math::Quaternion OriDelta(SK::Math::float4(RotDirection * DeltaF * 0.5f, 1.0f));
				CamOrientation = OriDelta * CamOrientation;
				CamOrientation.Normalise();
				MyCamera.Orientation(CamOrientation);

				SK::Math::Vector Offset(MoveDirection);
				float UserSpeed = Offset.Normalise();
				UserSpeed = std::min(20.0f, UserSpeed * 20.0f);
				if (UserSpeed > 0)
				{
					Offset = SK::Math::Quaternion::Conjugate(MyCamera.Orientation()) * Offset;
					Offset *= DeltaF * UserSpeed * CamSpeed;
					MyCamera.Position(MyCamera.Position() + Offset);
				}

				if (_RenderExtents)
				{
					for (auto& thing : RTree)
						RenderExtents(MyRenderer, DebugRender, thing, true);
				}
			}
		}

		MyRenderer.SetInputLayout(SK::Graphics::InputLayout::Vertex3D_InstanceMat);
		MyRenderer.SetVertexShader(InstancedVShader);
		MyRenderer.SetPixelShader(PShader);
		MyRenderer.SetDepthStencilMode(SK::Graphics::DepthStencilMode::Enabled);
		MyRenderer.SetCullingMode(SK::Graphics::CullMode::None);
		MyRenderer.Clear(ClearCol);

		auto ProfStart = Clock.Time();
		MainPass.Prepare();
		PrepareTime += (Clock.Time() - ProfStart).Milliseconds<float>();

		ProfStart = Clock.Time();
		MainPass.Render(MyRenderer);
		
		if (_RenderExtents)
		{
			MyRenderer.SetDepthStencilMode(SK::Graphics::DepthStencilMode::ReadOnly);
			MyRenderer.SetCullingMode(SK::Graphics::CullMode::Clockwise);
			MyRenderer.SetInputLayout(SK::Graphics::InputLayout::Vertex3D_InstanceMatCol);
			MyRenderer.SetVertexShader(DebugVShader);
			MyRenderer.SetPixelShader(DebugPShader);

			SK::Math::Matrix ViewProj = MyCamera.ViewMatrix() * MyCamera.ProjectionMatrix();
			SK::Math::Vector CamPos = MyCamera.Position();
			auto ViewProjId = MyRenderer.GetShaderParamId("ViewProj");
			auto CamPosId = MyRenderer.GetShaderParamId("CameraPosition");

			MyRenderer.SetShaderParam(ViewProjId, ViewProj);
			MyRenderer.SetShaderParam(CamPosId, CamPos);
			MyRenderer.CommitShaderParams();

			DebugRender.Render(MyRenderer);
		}
		
		RenderTime += (Clock.Time() - ProfStart).Milliseconds<float>();

#if SKGRAPHICS_SHOWOCCLUSIONBUFFER
		if (_ShowOcclusionBuffer)
		{
			MainPass.CopyOcclusionBuffer(MyRenderer, CPUGPUSoftwareDepthBufferThing);

			MyRenderer.SetInputLayout(SK::Graphics::InputLayout::Vertex2D);
			MyRenderer.SetVertexShader(VShader2D);
			MyRenderer.SetPixelShader(PShader2D);
			MyRenderer.SetShaderSampler(0, SK::Graphics::SamplerState::Point);
			MyRenderer.SetShaderTexture(0, CPUGPUSoftwareDepthBufferThing);
			MyRenderer.SetShaderParam(WId, SK::Math::Matrix::Identity());
			MyRenderer.CommitShaderParams();
			MyRenderer.DrawBuffer(SK::Graphics::PrimType::TriangleStrip, BumHole, 4, 0);
		}
#endif

		ProfStart = Clock.Time();
		MyRenderer.Present();
		GFXTime += (Clock.Time() - ProfStart).Milliseconds<float>();
	}
}
#include "Tests.h"
#include <SKInput/DeviceManager.h>
#include <SKInput/GameKeys.h>
#include <SKSystem/Threading/Thread.h>

using namespace SK;
using namespace SK::Input;

namespace
{
	enum class GameKeys : int
	{
		Quit
	};

	void AxisChangedHandler(void* sender, SK::Input::KeyEventArgs& e)
	{
		if (e.Key() == Key::MouseXAxis ||
			e.Key() == Key::MouseYAxis ||
			e.Key() == Key::MouseXPos ||
			e.Key() == Key::MouseYPos)
		{
			return;
		}

		Device& device = *reinterpret_cast<Device*>(sender);
		if (std::abs(e.Value()) > 0.15f)
		{
			auto Message = String::Format("%s: %s (%s)",
				device.Name().Buffer(),
				device.GetKeyName(e.Key()).Buffer(),
				SK::Input::GetKeyName(e.Key()).Buffer());

			SK::Debug::Print(Message);
		}
	}

	void ButtonDownHandler(void* sender, SK::Input::KeyEventArgs& e)
	{
		Device& device = *reinterpret_cast<Device*>(sender);
		auto Message = String::Format("%s: %s (%s)",
			device.Name().Buffer(),
			device.GetKeyName(e.Key()).Buffer(),
			SK::Input::GetKeyName(e.Key()).Buffer());

		SK::Debug::Print(Message);
	}
}

void Tests::Input()
{
	SK::Input::DeviceManager& DevMan = SK::Input::DeviceManager::Instance();
	DevMan.FindDevices();
	DevMan.DisableSystemShortcuts(true);
	DevMan.BackgroundInput(false);

	SK::Input::KeyMap<GameKeys> KeyMap;
	KeyMap.Add(GameKeys::Quit, SK::Input::KeyBinding(SpecialDevice::SystemKeyboard, SK::Input::Key::Escape));

	auto EventKey = SK::GetEventKey();
	unsigned int JoyId = -1;

	for (auto& Device : DevMan.Mice())
	{
		SK::Debug::Print("Found Mouse:    ", Device.Name());
	}
	for (auto& Device : DevMan.Keyboards())
	{
		SK::Debug::Print("Found Keyboard: ", Device.Name());
	}
	for (auto& Device : DevMan.Joypads())
	{
		if (JoyId == -1)
			JoyId = Device.Id();

		SK::Debug::Print("Found Joypad:   ", Device.Name());
	}
	for (auto& Device : DevMan.Joysticks())
	{
		SK::Debug::Print("Found Joystick: ", Device.Name());
	}
	for (auto& Device : DevMan.Wheels())
	{
		if (JoyId == -1)
			JoyId = Device.Id();

		SK::Debug::Print("Found Wheel:    ", Device.Name());
	}

	if (JoyId != -1)
	{
		KeyMap.Add(GameKeys::Quit, SK::Input::KeyBinding(JoyId, SK::Input::Key::Button2));
	}

	DevMan.AxisChanged.Register(EventKey, AxisChangedHandler);
	DevMan.ButtonDown.Register(EventKey, ButtonDownHandler);

	while (true)
	{
		DevMan.Update();
		KeyMap.Update();

		if (KeyMap.JustReleased(GameKeys::Quit))
		{
			break;
		}

		if (JoyId != -1)
		{
			if (DevMan[JoyId].AvailableFFAxis().HasFlags(ForceFeedbackAxis::X, ForceFeedbackAxis::Y))
			{
				DevMan[JoyId].SetForceFeedback({ AxisMagnitude(ForceFeedbackAxis::X, DevMan[JoyId].GetValue(Key::Axis_Ry)),
					AxisMagnitude(ForceFeedbackAxis::Y, DevMan[JoyId].GetValue(Key::Slider2)) });
			}
			else
			{
				DevMan[JoyId].SetForceFeedback(ForceFeedbackAxis::X, DevMan[JoyId].GetValue(Key::Axis_X));
			}
		}

		SK::Threading::Sleep(SK::Time::Milliseconds(5));
	}

	SK::Input::DeviceManager::Release();
}
#include "Tests.h"
#include <SKSystem/IO/FileStream.h>
#include <SKGraphics/Image/Image.h>
#include <SKGraphics/Image/MipmapImage.h>
#include <SKSystem/GUI/Window.h>
#include <SKSystem/Threading/Thread.h>

using namespace SK;
using namespace SK::IO;
using namespace SK::GUI;
using namespace SK::Graphics;

namespace
{
	void WindowMovedHandler(void* sender, const SK::GUI::WindowMoveEventData& e)
	{
		SK::Debug::Print(String::Format("Window Moved: %i,%i", e.Position.x, e.Position.y));
	}
}

void Tests::Window()
{
	FileStream FS(Path(SpecialFolder::Install, "Icon.png"), FileMode::Read);
	Image TestPng = Image::Load(FS, FS.Length());
	FS.Close();

	SK::Graphics::MipmapImage Icon(TestPng, 6);

	SK::GUI::Window MainWindow("Reference V2", { 640, 480 }, WindowStyle::Normal);
	MainWindow.Show();
	MainWindow.SetIcon(Icon);

	auto EventKey = SK::GetEventKey();
	MainWindow.Moved.Register(EventKey, WindowMovedHandler);
	
	while (MainWindow.IsOpen())
	{
		auto Events = MainWindow.Update();
		
		if (Events.HasFlags(WindowEvent::Maximise))
		{
			SK::Debug::Print("Bigger!");
		}

		SK::Threading::Sleep(SK::Time::Milliseconds(5));
	}
}
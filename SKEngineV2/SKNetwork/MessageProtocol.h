#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Text/String.h>
#include <SKNetwork/EndPoint.h>
#include <SKNetwork/Events.h>
#include <SKNetwork/MessageTypes.h>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		class Peer;
		class RemotePeer;

		class MessageProtocol
		{
		public:

			//---- Types ----//
			//===============//

			friend class Peer;

			//--- Methods ---//
			//===============//

			MessageProtocol(Peer& peer);
			virtual ~MessageProtocol() {}

			uint32_t       ProtocolId() const { return m_ProtocolId; }
			void           ProtocolSend(const uint8_t* packet, uint32_t packetLength, const EndPoint& ep);
			void           ProtocolSend(const uint8_t* packet, uint32_t packetLength, RemotePeer& peer, MessageType type);

			virtual String ProtocolName() = 0;

		protected:

			//--- Methods ---//
			//===============//

			Peer&           Peer() { return m_Peer; }

		private:

			//--- Methods ---//
			//===============//
			
			void            Init();
			virtual void    HandleMessage(MessageReceivedArgs& args) = 0;

			//-- Variables --//
			//===============//

			SK::Network::Peer& m_Peer;
			uint32_t           m_ProtocolId;
		};
	}
}
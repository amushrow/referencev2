#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Text/String.h>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		class EndPoint
		{
		public:

			//---- Types ----//
			//===============//

			struct PlatformData;

			//--- Methods ---//
			//===============//

			EndPoint();
			EndPoint(const SK::String& address, int port);
			EndPoint(int port);
			~EndPoint();

			EndPoint(const EndPoint& other);
			EndPoint& operator=(const EndPoint& other);

			bool operator==(const EndPoint& rhs) const;
			bool operator!=(const EndPoint& rhs) const
			{
				return !(*this == rhs);
			}

			bool operator<(const EndPoint& rhs) const;

			const PlatformData* Data() const { return m_Platform; }
			PlatformData*       Data()       { return m_Platform; }

		private:

			//-- Variables --//
			//===============//

			PlatformData* m_Platform;
		};
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Text/String.h>
#include <SKNetwork/EndPoint.h>
#include <SKNetwork/Events.h>
#include <SKSystem/Events.h>
#include <SKNetwork/MessageTypes.h>
#include <SKNetwork/MessageProtocol.h>
#include <unordered_map>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//
		class RemotePeer;

		enum class PeerUpdateType : uint8_t
		{
			Joined,
			Left
		};

		class Peer
		{
		public:

			//---- Types ----//
			//===============//

			typedef std::function<void(const Peer& peer, MessageReceivedArgs& args)> MessageHandler;
			friend class MessageProtocol;
			struct Implementation;

			//--- Methods ---//
			//===============//

			Peer(uint16_t port);
			~Peer();

			Peer(const Peer& other) = delete;
			Peer& operator=(const Peer& other) = delete;

			void                    Update();
			void                    Connect(const EndPoint& ep);
			void                    AllowUnsolicited(bool val) { m_AllowUnsolicited = val; }
			uint16_t                Port() const { return m_Port; }
			void                    Send(const uint8_t* packet, uint32_t packetLength, const EndPoint& ep);
			void                    Send(const uint8_t* packet, uint32_t packetLength, RemotePeer& peer, MessageType type);
			template<typename T> T* AddProtocol();
			template<typename T> T* GetProtocol();		

			static uint32_t         MaxPacketSize();

			//-- Variables --//
			//===============//

			EventDelegate<MessageReceivedArgs>   MessageReceived;
			EventDelegate<ConnectionRequestArgs> ConnectionRequest;
			EventDelegate<ConnectionSuccessArgs> ConnectionSuccess;
			EventDelegate<ConnectionFailedArgs>  ConnectionFailed;

		private:

			//--- Methods ---//
			//===============//

			void Init();
			void Shutdown();

			void OnMessageReceived(MessageReceivedArgs& args);
			void Send(const uint8_t* packet, uint32_t packetLength, const EndPoint& ep, uint32_t protocolId);
			void Send(const uint8_t* packet, uint32_t packetLength, RemotePeer& peer, MessageType type, uint32_t protocolId);

			//-- Variables --//
			//===============//

			uint16_t                                     m_Port;
			std::vector<RemotePeer*>                     m_ConnectedPeers;
			std::vector<MessageProtocol*>                m_Protocols;
			bool                                         m_AllowUnsolicited;
			Implementation*                              m_Implementation;
			Event<MessageReceivedArgs>                   m_MessageReceived;
			Event<ConnectionRequestArgs>                 m_ConnectionRequest;
			Event<ConnectionSuccessArgs>                 m_ConnectionSuccess;
			Event<ConnectionFailedArgs>                  m_ConnectionFailed;
		};
	}
}

//--- Methods ---//
//===============//

template<typename T>
T* SK::Network::Peer::AddProtocol()
{
	static_assert(std::is_base_of<MessageProtocol, T>::value, "Peer::AddProtocol - Type does not inherit MessageProtocol");

	for (MessageProtocol* Protocol : m_Protocols)
	{
		if (typeid(*Protocol) == typeid(T))
		{
			SK::Debug::Assert(false, "Peer::AddProtocol - This protocol has already been added");
			return dynamic_cast<T*>(Protocol);
		}
	}

	T* NewProtocol = new T(*this);
	NewProtocol->Init();
	for (MessageProtocol* Protocol : m_Protocols)
	{
		if (NewProtocol->ProtocolId() == Protocol->ProtocolId())
		{
			SK::Debug::Assert(false, "Peer::AddProtocol - ProtocolId is not unique");

			delete NewProtocol;
			return nullptr;
		}
	}

	m_Protocols.push_back(NewProtocol);
	return NewProtocol;
}

template<typename T>
T* SK::Network::Peer::GetProtocol()
{
	static_assert(std::is_base_of<MessageProtocol, T>::value, "Peer::GetProtocol - Type does not inherit MessageProtocol");

	for (MessageProtocol* Protocol : m_Protocols)
	{
		if (typeid(*Protocol) == typeid(T))
		{
			return dynamic_cast<T*>(Protocol);
		}
	}

	return nullptr;
}
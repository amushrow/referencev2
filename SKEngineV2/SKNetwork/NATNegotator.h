#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKNetwork/MessageProtocol.h>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		class NATNegotiator :  public MessageProtocol
		{
		public:

			//--- Methods ---//
			//===============//

			NATNegotiator(SK::Network::Peer& peer) : MessageProtocol(peer) {}

			SK::String ProtocolName() override { return "SK::Network::NATNegotiator"; }

			void Connect(const EndPoint& negotiator, const EndPoint& remote);

		private:

			//--- Methods ---//
			//===============//

			void HandleMessage(MessageReceivedArgs& args) override;

		};

	}
}
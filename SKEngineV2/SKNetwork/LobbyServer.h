#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/IO/MemoryStream.h>
#include <SKNetwork/Lobby.h>
#include <SKNetwork/MessageProtocol.h>
#include <SKNetwork/Events.h>
#include <SKSystem/Events.h>
#include <vector>
#include <cstdint>
#include <map>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		class LobbyServer : public MessageProtocol
		{
		public:

			//---- Types ----//
			//===============//

			enum class Message : uint8_t
			{
				FindLobby,
				CreateLobby,
				DestroyLobby,
				JoinLobby,
				LeaveLobby,
				GetPeers,

				LobbyFound,
				LobbyCreated,
				LobbyDestroyed,
				JoinLobbySuccess,
				JoinLobbyFailed,
				PeerUpdated,
				PeerList
			};

			//--- Methods ---//
			//===============//

			LobbyServer(SK::Network::Peer& peer);

			SK::String                ProtocolName() override { return "SK::Network::LobbyServer"; }
			
			void                      Host(bool val);
			void                      ConnectToRemoteServer(const EndPoint& remoteServer);
			void                      DisconnectRemoteServer();

			void                      CreateLobby(const Lobby& lobby);
			void                      CreateLocalLobby(Lobby& lobby, RemotePeer* owner = nullptr);
			void                      FindLobbies(const LobbyFilters& filters);
			std::vector<const Lobby*> FindLocalLobbies(const LobbyFilters& filters);
			void                      JoinLobby(uint64_t lobbyId);
			void                      LeaveLobby(uint64_t lobbyId);
			void                      RequestLobbyPeers(uint64_t lobbyId);

			//-- Variables --//
			//===============//

			EventDelegate<EmptyEventData>    ConnctedToRemote;
			EventDelegate<LobbyCreatedArgs>  LobbyCreated;
			EventDelegate<LobbyFoundArgs>    LobbyFound;
			EventDelegate<LobbyArgs>         LobbyDestroyed;
			EventDelegate<ReceivedPeersArgs> ReceivedPeers;
			EventDelegate<LobbyArgs>         JoinLobbySuccess;
			EventDelegate<LobbyArgs>         JoinLobbyFailed;
			EventDelegate<PeerUpdateArgs>    PeerUpdated;

		private:

			//---- Types ----//
			//===============//

			struct LobbyPeers
			{
				RemotePeer*              Owner;
				std::vector<RemotePeer*> Peers;
				Lobby                    Lobby;
			};

			//--- Methods ---//
			//===============//

			void   HandleMessage(MessageReceivedArgs& args) override;
			void   SendPeers(const LobbyPeers& lobby, RemotePeer& remote);
			void   ReceivePeers(SK::IO::MemoryStream& messageStream);
			void   SendLobbies(const std::vector<const Lobby*>& lobbies, RemotePeer& remote);
			void   ReceiveLobbies(SK::IO::MemoryStream& messageStream);

			void   ConnectionSuccess(void* sender, ConnectionSuccessArgs& args);
			void   ConnectionFailed(void* sender, ConnectionFailedArgs& args);

			void   OnJoinLobby(const LobbyPeers& lobby, RemotePeer& peer);
			void   OnLeaveLobby(const LobbyPeers& lobby, RemotePeer& peer);
			void   OnLobbyDestroyed(const LobbyPeers& lobby);
			void   OnCreateLobby(const Lobby& lobby, RemotePeer& creator);
			
			//-- Variables --//
			//===============//
		
			EndPoint                       m_RemoteLobbyServerEP;
			RemotePeer*                    m_RemoteLobbyServer;
			bool                           m_Hosting;

			SK::EventKey                   m_EventKey;
			uint64_t                       m_NextLobbyId;
			std::map<uint64_t, LobbyPeers> m_Lobbies;

			Event<EmptyEventData>          m_ConnctedToRemote;
			Event<LobbyCreatedArgs>        m_LobbyCreated;
			Event<LobbyFoundArgs>          m_LobbyFound;
			Event<LobbyArgs>               m_LobbyDestroyed;
			Event<ReceivedPeersArgs>       m_ReceivedPeers;
			Event<LobbyArgs>               m_JoinLobbySuccess;
			Event<LobbyArgs>               m_JoinLobbyFailed;
			Event<PeerUpdateArgs>          m_PeerUpdated;
		};

		//--- Methods ---//
		//===============//
	}
}
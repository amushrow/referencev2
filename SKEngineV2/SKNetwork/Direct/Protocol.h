#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstdint>
#include <map>
#include <SKSystem/Queues.h>
#include <SKNetwork/Peer.h>
#include <SKNetwork/RemotePeer.h>
#include <SKNetwork/EndPoint.h>
#include <SKSystem/Time/Time.h>
#include <SKSystem/Math/Average.h>
#include <SKSystem/Array.h>
#include <SKSystem/Threading/Thread.h>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		static const uint32_t MAX_PACKET_SIZE = 508;
		static const uint32_t MAX_WINDOW_SIZE = 64 * 1024;

		enum class InternalMessage : uint8_t
		{
			UserMessage,
			ConnectionRequest,
			ConnectionDenied,
			ConnectionAccepted,
			Ack,
			Ping,
			Pong,
			Multipart
		};

		struct MultipartBuffer
		{
			uint8_t* Buffer;
			uint32_t DataLength;
			uint32_t DataReceived;
		};

		struct Packet
		{
			Packet() : Message(InternalMessage::UserMessage), Ack(0), OrderedId(0), GlobalId(0), Protocol(0) {}
			Packet(InternalMessage message) : Message(message), Ack(0), OrderedId(0), GlobalId(0), Protocol(0) {}

			InternalMessage Message;
			uint16_t        Ack;         // Ack id of ack message to be returned (optional)
			uint16_t        OrderedId;   // Used for ordered packets (optional)
			uint16_t        GlobalId;    // Used to detect duplicate messages
			uint32_t        Protocol;    // CRC32 hash of Protocol Name
		};

		struct MultipartPacket
		{
			Packet   Packet;
			uint16_t SharedId;         // Id of the entire packet, must be the same for all MultipartPackets in a group
			uint32_t DataOffset;
			uint32_t TotalDataLength;
		};

		struct QueuedPacket
		{
			QueuedPacket() : Packet(nullptr), Length(0), SendTime(0), OnWire(false)
			{

			}

			uint8_t*            Packet;
			uint32_t            Length;
			EndPoint            EndPoint;
			SK::Time::TimePoint SendTime;
			bool                OnWire;
		};

		struct RoutedMessage
		{
			RoutedMessage(uint32_t handler, SK::Array<uint8_t>& message) : Protocol(handler), Message(message) {}
			RoutedMessage() : Protocol(0) {}

			uint32_t           Protocol;
			SK::Array<uint8_t> Message;
		};
	}
}
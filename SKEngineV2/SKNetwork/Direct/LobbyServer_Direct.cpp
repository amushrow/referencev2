//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/LobbyServer.h>
#include <SKNetwork/Lobby.h>
#include <SKNetwork/Direct/Protocol.h>
#include <SKSystem/IO/MemoryStream.h>
using namespace SK::Network;

//-- Declarations --//
//==================//

namespace
{
	const uint16_t LobbyServerPort = 33000;
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

LobbyServer::LobbyServer(SK::Network::Peer& peer)
: MessageProtocol(peer)
, m_RemoteLobbyServer(nullptr)
, m_Hosting(false)
, m_EventKey(SK::GetEventKey())
, m_NextLobbyId(0)
, ConnctedToRemote(m_ConnctedToRemote)
, LobbyCreated(m_LobbyCreated)
, LobbyFound(m_LobbyFound)
, LobbyDestroyed(m_LobbyDestroyed)
, ReceivedPeers(m_ReceivedPeers)
, JoinLobbySuccess(m_JoinLobbySuccess)
, JoinLobbyFailed(m_JoinLobbyFailed)
, PeerUpdated(m_PeerUpdated)
{
	using std::placeholders::_1;
	using std::placeholders::_2;

	Peer().ConnectionSuccess.Register(m_EventKey, std::bind(&LobbyServer::ConnectionSuccess, this, _1, _2));
	Peer().ConnectionFailed.Register(m_EventKey, std::bind(&LobbyServer::ConnectionFailed, this, _1, _2));
}

void LobbyServer::Host(bool val)
{
	m_Hosting = val;

	if (!m_Hosting)
	{
		// Remove any remote lobbies
		auto It = m_Lobbies.begin();
		while (It != m_Lobbies.end())
		{
			if (It->second.Owner != nullptr)
			{
				OnLobbyDestroyed(It->second);
				It = m_Lobbies.erase(It);
			}
		}
	}
}

void LobbyServer::ConnectToRemoteServer(const EndPoint& remoteServer)
{
	SK::Debug::Assert(!m_Hosting, "LobbyServer::ConnectToRemoteServer - This server is already a host");
	SK::Debug::Assert(m_RemoteLobbyServer == nullptr && m_RemoteLobbyServerEP == EndPoint(), "LobbyServer::ConnectToRemoteServer - Already connnected to a remote server");
	m_RemoteLobbyServerEP = remoteServer;
	Peer().Connect(remoteServer);
}

void LobbyServer::CreateLobby(const Lobby& lobby)
{
	SK::Debug::Assert(m_RemoteLobbyServer != nullptr, "LobbyServer::CreateLobby - Not connected to a remote server");
	uint8_t MessageData[MAX_PACKET_SIZE];
	SK::IO::MemoryStream Message(MessageData, MAX_PACKET_SIZE, false);
	Message.Write(Message::CreateLobby);
	Message.Write(lobby);

	ProtocolSend(MessageData, static_cast<uint32_t>(Message.Position()), *m_RemoteLobbyServer, MessageType::Reliable);
}

void LobbyServer::CreateLocalLobby(Lobby& lobby, RemotePeer* owner)
{
	lobby.LobbyId(m_NextLobbyId++);
	LobbyPeers NewLobby;
	NewLobby.Owner = owner;
	NewLobby.Lobby = lobby;

	m_Lobbies[lobby.LobbyId()] = NewLobby;
}

void LobbyServer::FindLobbies(const LobbyFilters& filters)
{
	uint8_t MessageData[MAX_PACKET_SIZE];
	SK::IO::MemoryStream Message(MessageData, MAX_PACKET_SIZE, false);
	Message.Write(Message::FindLobby);
	Message.Write(static_cast<uint32_t>(filters.size()));
	for (auto& key : filters)
	{
		Message.Write(key.first, key.second);
	}

	if (m_RemoteLobbyServer)
	{
		ProtocolSend(MessageData, static_cast<uint32_t>(Message.Position()), *m_RemoteLobbyServer, MessageType::Reliable);
	}
	else
	{
		//SKTD: Search on LAN
	}
}

std::vector<const Lobby*> LobbyServer::FindLocalLobbies(const LobbyFilters& filters)
{
	std::vector<const Lobby*> FoundLobbies;
	
	for (auto& Lobby : m_Lobbies)
	{
		bool Match = true;
		LobbyFilters CurrentFilters = Lobby.second.Lobby.Filters();
		for (auto filter : filters)
		{
			auto It = CurrentFilters.find(filter.first);
			if (It == CurrentFilters.end() || !It->second.Equals(filter.second))
			{
				Match = false;
				break;
			}
		}

		if (Match)
		{
			FoundLobbies.push_back(&Lobby.second.Lobby);
		}
	}

	return FoundLobbies;
}

void LobbyServer::JoinLobby(uint64_t lobbyId)
{
	uint8_t MessageData[MAX_PACKET_SIZE];
	SK::IO::MemoryStream Message(MessageData, MAX_PACKET_SIZE, false);
	Message.Write(Message::JoinLobby);
	Message.Write(lobbyId);
	ProtocolSend(MessageData, static_cast<uint32_t>(Message.Position()), *m_RemoteLobbyServer, MessageType::Reliable);
}

void LobbyServer::LeaveLobby(uint64_t lobbyId)
{
	uint8_t MessageData[MAX_PACKET_SIZE];
	SK::IO::MemoryStream Message(MessageData, MAX_PACKET_SIZE, false);
	Message.Write(Message::LeaveLobby);
	Message.Write(lobbyId);
	ProtocolSend(MessageData, static_cast<uint32_t>(Message.Position()), *m_RemoteLobbyServer, MessageType::Reliable);
}

void LobbyServer::RequestLobbyPeers(uint64_t lobbyId)
{
	uint8_t MessageData[MAX_PACKET_SIZE];
	SK::IO::MemoryStream Message(MessageData, MAX_PACKET_SIZE, false);
	Message.Write(Message::GetPeers);
	Message.Write(lobbyId);
	ProtocolSend(MessageData, static_cast<uint32_t>(Message.Position()), *m_RemoteLobbyServer, MessageType::Reliable);
}

//-- Private Methods --//
//=====================//

void LobbyServer::ConnectionSuccess(void* sender, ConnectionSuccessArgs& args)
{
	if (args.RemotePeer()->EndPoint() == m_RemoteLobbyServerEP)
	{
		m_RemoteLobbyServer = args.RemotePeer();

		EmptyEventData Args;
		m_ConnctedToRemote.OnEvent(this, Args);
	}
}

void LobbyServer::ConnectionFailed(void* sender, ConnectionFailedArgs& args)
{
	if (args.EndPoint() == m_RemoteLobbyServerEP)
	{
		m_RemoteLobbyServerEP = EndPoint();
		m_RemoteLobbyServer = nullptr;
	}
}

void LobbyServer::HandleMessage(MessageReceivedArgs& args)
{
	SK::IO::MemoryStream MessageStream(args.Data().Get(), args.Data().Length());
	Message Id = MessageStream.Read<Message>();

	if (args.RemotePeer())
	{
		switch (Id)
		{
		case Message::CreateLobby:
			{
				Lobby NewLobby = MessageStream.Read<Lobby>();
				CreateLocalLobby(NewLobby, args.RemotePeer());
				OnCreateLobby(NewLobby, *args.RemotePeer());
			}
			break;

		case Message::DestroyLobby:
			{
				uint64_t LobbyId = MessageStream.Read<uint64_t>();
				auto It = m_Lobbies.find(LobbyId);
				if (It != m_Lobbies.end() && It->second.Owner == args.RemotePeer())
				{
					OnLobbyDestroyed(It->second);
					m_Lobbies.erase(It);
				}
			}
			break;

		case Message::JoinLobby:
			{
				uint64_t LobbyId = MessageStream.Read<uint64_t>();
				auto It = m_Lobbies.find(LobbyId);
				bool Joined = false;
				if (It != m_Lobbies.end())
				{
					Lobby& Lobby = It->second.Lobby;
					std::vector<RemotePeer*>& Peers = It->second.Peers;

					if (Peers.size() < Lobby.MaxNbPlayers())
					{
						Joined = true;
						Peers.push_back(args.RemotePeer());
						Lobby.NbPlayers(static_cast<uint16_t>(Peers.size()));
						OnJoinLobby(It->second, *args.RemotePeer());
					}
				}

				if (!Joined)
				{
					uint8_t Message[sizeof(Message) + sizeof(uint64_t)];
					SK::IO::MemoryStream ReturnMessage(Message, sizeof(Message), false);
					MessageStream.Write(Message::JoinLobbyFailed);
					MessageStream.Write(LobbyId);
					ProtocolSend(Message, sizeof(Message), *args.RemotePeer(), MessageType::Reliable);
				}
			}
			break;

		case Message::LeaveLobby:
			{
				uint64_t LobbyId = MessageStream.Read<uint64_t>();
				auto LobbyIter = m_Lobbies.find(LobbyId);
				if (LobbyIter != m_Lobbies.end())
				{
					std::vector<RemotePeer*>& Peers = LobbyIter->second.Peers;
					for (auto PeersIter = Peers.begin(); PeersIter != Peers.end(); ++PeersIter)
					{
						if (*PeersIter == args.RemotePeer())
						{
							Peers.erase(PeersIter);
							LobbyIter->second.Lobby.NbPlayers(static_cast<uint16_t>(Peers.size()));
							OnLeaveLobby(LobbyIter->second, *args.RemotePeer());
							break;
						}
					}
				}
			}
			break;

		case Message::GetPeers:
			{
				uint64_t LobbyId = MessageStream.Read<uint64_t>();
				auto It = m_Lobbies.find(LobbyId);
				if (It != m_Lobbies.end())
				{
					SendPeers(It->second, *args.RemotePeer());
				}
			}
			break;

		case Message::PeerList:
			{
				ReceivePeers(MessageStream);
			}
			break;

		case Message::FindLobby:
			{
				LobbyFilters Filters;
				uint32_t NbFilters = MessageStream.Read<uint32_t>();
				for (uint32_t i = 0; i < NbFilters; ++i)
				{
					SK::String Key, Value;
					MessageStream.Read(Key, Value);

					Filters[Key] = Value;
				}

				SendLobbies(FindLocalLobbies(Filters), *args.RemotePeer());
			}
			break;

		case Message::LobbyFound:
			{
				ReceiveLobbies(MessageStream);
			}
			break;

		case Message::LobbyCreated:
			{
				Lobby NewLobby;
				MessageStream.Read(NewLobby);

				LobbyCreatedArgs Args(NewLobby);
				m_LobbyCreated.OnEvent(this, Args);
			}
			break;

		case Message::LobbyDestroyed:
			{
				uint64_t LobbyId = MessageStream.Read<uint64_t>();
				LobbyArgs Args(LobbyId);
				m_LobbyDestroyed.OnEvent(this, Args);
			}
			break;

		case Message::JoinLobbySuccess:
			{
				uint64_t LobbyId = MessageStream.Read<uint64_t>();
				LobbyArgs Args(LobbyId);
				m_JoinLobbySuccess.OnEvent(this, Args);
			}
			break;

		case Message::JoinLobbyFailed:
			{
				uint64_t LobbyId = MessageStream.Read<uint64_t>();
				LobbyArgs Args(LobbyId);
				m_JoinLobbyFailed.OnEvent(this, Args);
			}
			break;

		case Message::PeerUpdated:
			{
				uint64_t LobbyId = MessageStream.Read<uint64_t>();
				PeerUpdateType UpdateType = MessageStream.Read<PeerUpdateType>();
				EndPoint EndPoint = MessageStream.Read<SK::Network::EndPoint>();

				PeerUpdateArgs Args(LobbyId, EndPoint, UpdateType);
				m_PeerUpdated.OnEvent(this, Args);
			}
			break;
		}
	}
}

void LobbyServer::SendPeers(const LobbyPeers& lobby, RemotePeer& remote)
{
	SK::IO::MemoryStream MessageStream;
	MessageStream.Write(Message::PeerList);
	MessageStream.Write(lobby.Lobby.LobbyId());
	MessageStream.Write(static_cast<uint32_t>(lobby.Peers.size()));
	for (auto Peer : lobby.Peers)
	{
		MessageStream.Write(Peer->EndPoint() == remote.EndPoint());
		MessageStream.Write(Peer->Name());
		MessageStream.Write(Peer->EndPoint());
	}

	ProtocolSend(MessageStream.Data(), static_cast<uint32_t>(MessageStream.Length()), remote, MessageType::Reliable);
}

void LobbyServer::ReceivePeers(SK::IO::MemoryStream& messageStream)
{
	uint64_t LobbyId = messageStream.Read<uint64_t>();
	uint32_t NbPeers = messageStream.Read<uint32_t>();

	std::vector<PeerEP> Peers;
	for (uint32_t i = 0; i < NbPeers; ++i)
	{
		PeerEP Peer;
		messageStream.Read(Peer.LocalPlayer, Peer.Name, Peer.EndPoint);
		Peers.push_back(Peer);
	}

	ReceivedPeersArgs Args(LobbyId, Peers);
	m_ReceivedPeers.OnEvent(this, Args);
}

void LobbyServer::SendLobbies(const std::vector<const Lobby*>& lobbies, RemotePeer& remote)
{
	SK::IO::MemoryStream MessageStream;
	MessageStream.Write(Message::LobbyFound);
	MessageStream.Write(static_cast<uint32_t>(lobbies.size()));
	for (auto Lobby : lobbies)
	{
		MessageStream.Write(*Lobby);
	}

	ProtocolSend(MessageStream.Data(), static_cast<uint32_t>(MessageStream.Length()), remote, MessageType::Reliable);
}

void LobbyServer::ReceiveLobbies(SK::IO::MemoryStream& messageStream)
{
	uint32_t NbLobbies = messageStream.Read<uint32_t>();

	std::vector<Lobby> Lobbies;
	for (uint32_t i = 0; i < NbLobbies; ++i)
	{
		Lobby NewLobby = messageStream.Read<Lobby>();
		Lobbies.push_back(NewLobby);
	}

	LobbyFoundArgs Args(Lobbies);
	m_LobbyFound.OnEvent(this, Args);
}

void LobbyServer::OnCreateLobby(const Lobby& lobby, RemotePeer& creator)
{
	uint8_t Message[MAX_PACKET_SIZE];
	SK::IO::MemoryStream MessageStream(Message, MAX_PACKET_SIZE, false);
	MessageStream.Write(Message::LobbyCreated);
	MessageStream.Write(lobby);
	ProtocolSend(Message, static_cast<uint32_t>(MessageStream.Position()), creator, MessageType::Reliable);
}

void LobbyServer::OnJoinLobby(const LobbyPeers& lobby, RemotePeer& peer)
{
	uint8_t Message[MAX_PACKET_SIZE];
	SK::IO::MemoryStream MessageStream(Message, MAX_PACKET_SIZE, false);

	MessageStream.Write(Message::JoinLobbySuccess);
	MessageStream.Write(lobby.Lobby.LobbyId());
	ProtocolSend(Message, static_cast<uint32_t>(MessageStream.Position()), peer, MessageType::Reliable);

	for (auto LobbyPeer : lobby.Peers)
	{
		if (LobbyPeer->EndPoint() != peer.EndPoint())
		{
			MessageStream.Position(0);
			MessageStream.Write(Message::PeerUpdated);
			MessageStream.Write(lobby.Lobby.LobbyId());
			MessageStream.Write(PeerUpdateType::Joined);
			MessageStream.Write(LobbyPeer->EndPoint());
			ProtocolSend(Message, static_cast<uint32_t>(MessageStream.Position()), *LobbyPeer, MessageType::Reliable);
		}
	}
}

void LobbyServer::OnLeaveLobby(const LobbyPeers& lobby, RemotePeer& peer)
{
	uint8_t Message[MAX_PACKET_SIZE];
	SK::IO::MemoryStream MessageStream(Message, MAX_PACKET_SIZE, false);
	MessageStream.Write(Message::PeerUpdated);
	MessageStream.Write(lobby.Lobby.LobbyId());
	MessageStream.Write(PeerUpdateType::Left);
	MessageStream.Write(peer.EndPoint());

	ProtocolSend(Message, static_cast<uint32_t>(MessageStream.Position()), peer, MessageType::Reliable);
	for (auto LobbyPeer : lobby.Peers)
	{
		ProtocolSend(Message, static_cast<uint32_t>(MessageStream.Position()), *LobbyPeer, MessageType::Reliable);
	}
}

void LobbyServer::OnLobbyDestroyed(const LobbyPeers& lobby)
{
	uint8_t Message[MAX_PACKET_SIZE];
	SK::IO::MemoryStream MessageStream(Message, MAX_PACKET_SIZE, false);
	MessageStream.Write(Message::LobbyDestroyed);
	MessageStream.Write(lobby.Lobby.LobbyId());

	for (auto LobbyPeer : lobby.Peers)
	{
		ProtocolSend(Message, static_cast<uint32_t>(MessageStream.Position()), *LobbyPeer, MessageType::Reliable);
	}
}
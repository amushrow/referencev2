//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/Direct/Win32/Network.h>
using namespace SK::Network;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

EndPoint::EndPoint()
: m_Platform(new PlatformData())
{
	
}

EndPoint::EndPoint(int Port)
: m_Platform(new PlatformData())
{
	m_Platform->Address.sin_family = AF_INET;
	m_Platform->Address.sin_port = htons(Port);
	m_Platform->Address.sin_addr.S_un.S_addr = INADDR_BROADCAST;
	memset(m_Platform->Address.sin_zero, 0, 8);
}

EndPoint::EndPoint(const SK::String& address, int port)
: m_Platform(new PlatformData())
{
	memset(m_Platform->Address.sin_zero, 0, 8);

	addrinfo Hints = { 0 };
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_DGRAM;
	Hints.ai_protocol = IPPROTO_UDP;

	addrinfo* FoundAddresses = nullptr;
	while (WSATRY_AGAIN == getaddrinfo(address, SK::String::Format("%i", port), &Hints, &FoundAddresses)) {}
	if (FoundAddresses)
	{
		for (addrinfo* Address = FoundAddresses; Address != nullptr; Address = Address->ai_next)
		{
			if (Address->ai_family == AF_INET)
			{
				memcpy(&m_Platform->Address, Address->ai_addr, Address->ai_addrlen);
				break;
			}
		}

		freeaddrinfo(FoundAddresses);
		FoundAddresses = nullptr;
	}
}

EndPoint::EndPoint(const EndPoint& other)
{
	m_Platform = new PlatformData();
	*m_Platform = *other.m_Platform;
}

EndPoint::~EndPoint()
{
	delete m_Platform;
}

EndPoint& EndPoint::operator = (const EndPoint& other)
{
	m_Platform = new PlatformData();
	*m_Platform = *other.m_Platform;
	
	return *this;
}

bool EndPoint::operator==(const EndPoint& rhs) const
{
	return (m_Platform->Address.sin_addr.S_un.S_addr == rhs.m_Platform->Address.sin_addr.S_un.S_addr)
		&& (m_Platform->Address.sin_port == rhs.m_Platform->Address.sin_port);
}

bool EndPoint::operator<(const EndPoint& rhs) const
{
	if (m_Platform->Address.sin_addr.S_un.S_addr == rhs.m_Platform->Address.sin_addr.S_un.S_addr)
	{
		return m_Platform->Address.sin_port < rhs.m_Platform->Address.sin_port;
	}
	return m_Platform->Address.sin_addr.S_un.S_addr < rhs.m_Platform->Address.sin_addr.S_un.S_addr;
}

//-- Private Methods --//
//=====================//

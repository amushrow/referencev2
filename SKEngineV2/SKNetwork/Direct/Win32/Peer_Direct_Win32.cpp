//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/Direct/Win32/Network.h>
#include <SKSystem/IO/MemoryStream.h>
using namespace SK::Network;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

void Peer::Init()
{
	m_Implementation->Data = new Implementation::PlatformData;

	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	m_Implementation->Data->Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	unsigned long OptionValue = 1;
	setsockopt(m_Implementation->Data->Socket, SOL_SOCKET, SO_BROADCAST, reinterpret_cast<char*>(&OptionValue), sizeof(BOOL));
	ioctlsocket(m_Implementation->Data->Socket, FIONBIO, &OptionValue);

	OptionValue = 0;
	setsockopt(m_Implementation->Data->Socket, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<char*>(&OptionValue), sizeof(long));

	sockaddr_in Address = { 0 };
	Address.sin_family = AF_INET;
	Address.sin_port = htons(m_Port);
	Address.sin_addr.s_addr = INADDR_ANY;
	bind(m_Implementation->Data->Socket, reinterpret_cast<sockaddr*>(&Address), sizeof(Address));
}

void Peer::Shutdown()
{
	if (m_Implementation->Data->Socket != INVALID_SOCKET)
	{
		closesocket(m_Implementation->Data->Socket);
	}
	delete m_Implementation->Data;
	WSACleanup();
}

void Peer::Implementation::Send(const Packet& packet, const EndPoint& to)
{
	uint8_t PacketData[sizeof(Packet)];
	SK::IO::MemoryStream Stream(PacketData, sizeof(Packet), false);
	Stream.Endianness(SK::IO::Endian::Big);
	Stream.Write(packet);

	Send(Stream.Data(), sizeof(Packet), to);
}

void Peer::Implementation::Send(const uint8_t* buffer, int length, const EndPoint& to)
{
	int Error = ERROR_SUCCESS;
	do
	{
		int BytesSent = sendto(Data->Socket, reinterpret_cast<const char*>(buffer), length, 0, reinterpret_cast<const sockaddr*>(&to.Data()->Address), sizeof(to.Data()->Address));
		if (BytesSent == SOCKET_ERROR)
		{
			Error = WSAGetLastError();
		}
	} while (Error == WSAEWOULDBLOCK);
}

int Peer::Implementation::Receive(uint8_t* buffer, uint32_t bufferLength, EndPoint& from)
{
	int AddressSize = sizeof(from.Data()->Address);
	return recvfrom(Data->Socket, reinterpret_cast<char*>(buffer), bufferLength, 0, reinterpret_cast<sockaddr*>(&from.Data()->Address), &AddressSize);
}

//-- Private Methods --//
//=====================//

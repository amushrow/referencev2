//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/Direct/Implementations.h>
#include <SKSystem/IO/MemoryStream.h>
using namespace SK::Network;

//-- Declarations --//
//==================//

namespace
{
	void PeerUpdateLoop(Peer* peer, Peer::Implementation* impl);
	void ProcessPacket(Packet& packet, SK::IO::MemoryStream& messageStream, EndPoint& from, Peer& peer, Peer::Implementation* impl);
	void CheckOrderedPackets(RemotePeer::Implementation& impl);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

uint32_t Peer::MaxPacketSize()
{
	return MAX_PACKET_SIZE - sizeof(Packet);
}

Peer::Peer(uint16_t port)
: m_Port(port)
, m_AllowUnsolicited(false)
, m_Implementation(new Implementation(m_ConnectedPeers))
, MessageReceived(m_MessageReceived)
, ConnectionRequest(m_ConnectionRequest)
, ConnectionSuccess(m_ConnectionSuccess)
, ConnectionFailed(m_ConnectionFailed)
{
	Init();
	m_Implementation->UpdateThread = SK::Threading::Thread(PeerUpdateLoop, this, m_Implementation);
}

Peer::~Peer()
{
	m_Implementation->Update = false;
	m_Implementation->UpdateThread.Join();

	Shutdown();

	delete m_Implementation;
}

void Peer::Update()
{
	Implementation::NetworkEvent Event;
	while (m_Implementation->Events.Dequeue(Event))
	{
		// Handle event
		switch (Event.Message)
		{
		case InternalMessage::ConnectionRequest:
			{
				ConnectionRequestArgs Args(Event.From);
				m_ConnectionRequest.OnEvent(this, Args, [](ConnectionRequestArgs& args){ return args.Accepted(); });

				if (Args.Accepted())
				{
					// User has accepted this connection
					QueuedPacket ConnectionSuccessPacket;
					ConnectionSuccessPacket.Length = sizeof(Packet);
					ConnectionSuccessPacket.Packet = new uint8_t[ConnectionSuccessPacket.Length];
					ConnectionSuccessPacket.EndPoint = Event.From;

					SK::IO::MemoryStream MessageStream(ConnectionSuccessPacket.Packet, ConnectionSuccessPacket.Length, false);
					MessageStream.Endianness(SK::IO::Endian::Big);
					MessageStream.Write(Packet(InternalMessage::ConnectionAccepted));
					m_Implementation->UnreliablePacketQueue.Enqueue(ConnectionSuccessPacket);

					// Add new remote peer to our list
					m_Implementation->RemotePeersLock.lock();
					RemotePeer* NewRemotePeer = new RemotePeer(Event.From);
					m_ConnectedPeers.push_back(NewRemotePeer);
					m_Implementation->RemotePeersLock.unlock();

					ConnectionSuccessArgs SuccessArgs(NewRemotePeer);
					m_ConnectionSuccess.OnEvent(this, SuccessArgs);
				}
			}
			break;

		case InternalMessage::ConnectionAccepted:
			{
				// Add new remote peer to our list
				m_Implementation->RemotePeersLock.lock();
				RemotePeer* NewRemotePeer = new RemotePeer(Event.From);
				m_ConnectedPeers.push_back(NewRemotePeer);
				m_Implementation->RemotePeersLock.unlock();

				ConnectionSuccessArgs SuccessArgs(NewRemotePeer);
				m_ConnectionSuccess.OnEvent(this, SuccessArgs);
			}
			break;
		}
	}

	// Pass on messages
	for (auto ConnectedPeer : m_ConnectedPeers)
	{
		auto RemotPeerData = ConnectedPeer->Data();
		RoutedMessage UserMessage;
		while (RemotPeerData->IncomingPacketQueue.Dequeue(UserMessage))
		{
			MessageReceivedArgs Args(ConnectedPeer, UserMessage.Message, UserMessage.Protocol);
			OnMessageReceived(Args);
		}
	}

	Implementation::UnsolicitedMessage UserMessage;
	while (m_Implementation->UnsolicitedPacketQueue.Dequeue(UserMessage))
	{
		MessageReceivedArgs Args(UserMessage.first, UserMessage.second.Message, UserMessage.second.Protocol);
		OnMessageReceived(Args);
	}
}

void Peer::Connect(const EndPoint& ep)
{
	m_Implementation->Send(Packet(InternalMessage::ConnectionRequest), ep);
}

//-- Private Methods --//
//=====================//

void Peer::Send(const uint8_t* packet, uint32_t packetLength, const EndPoint& ep, uint32_t protocolId)
{
	SK::Debug::Assert(packetLength < MaxPacketSize());
	if (packetLength < MaxPacketSize())
	{
		QueuedPacket UserPacket;
		UserPacket.Length = packetLength + sizeof(Packet);
		UserPacket.Packet = new uint8_t[UserPacket.Length];
		UserPacket.EndPoint = ep;

		Packet PacketData(InternalMessage::UserMessage);
		PacketData.Protocol = protocolId;
		
		SK::IO::MemoryStream MessageStream(UserPacket.Packet, UserPacket.Length, false);
		MessageStream.Endianness(SK::IO::Endian::Big);
		MessageStream.Write(PacketData);
		MessageStream.WriteData(packet, 0, packetLength);
		
		m_Implementation->UnreliablePacketQueue.Enqueue(UserPacket);
	}
}

void Peer::Send(const uint8_t* packet, uint32_t packetLength, RemotePeer& peer, MessageType type, uint32_t protocolId)
{
	if (packetLength > MaxPacketSize())
	{
		SK::Debug::Assert(type != MessageType::Unreliable, "Message too large, forcing reliable packet");

		uint32_t PayloadSize = MAX_PACKET_SIZE - sizeof(MultipartPacket);
		int NbPackets = (packetLength + (PayloadSize - 1)) / PayloadSize;

		int MultipartSharedId = peer.Data()->GlobalPacketId;

		for (int i = 0; i < NbPackets; ++i)
		{
			int Offset = i * PayloadSize;
			int Length = PayloadSize;
			Length = std::min(PayloadSize, packetLength - Offset);

			QueuedPacket UserPacket;
			UserPacket.Length = Length + sizeof(MultipartPacket);
			UserPacket.Packet = new uint8_t[UserPacket.Length];
			UserPacket.EndPoint = peer.EndPoint();

			SK::IO::MemoryStream MessageStream(UserPacket.Packet, UserPacket.Length, false);
			MessageStream.Endianness(SK::IO::Endian::Big);

			MultipartPacket PacketData;
			PacketData.Packet.Message = InternalMessage::Multipart;
			PacketData.Packet.Ack = peer.Data()->GlobalPacketId;
			PacketData.Packet.GlobalId = peer.Data()->GlobalPacketId;
			PacketData.Packet.Protocol = protocolId;
			PacketData.SharedId = MultipartSharedId;
			PacketData.TotalDataLength = packetLength;
			PacketData.DataOffset = MaxPacketSize() * i;

			if (type == MessageType::ReliebleOrdered)
			{
				PacketData.Packet.OrderedId = peer.Data()->OrderedPacketId;
			}
			else
			{
				PacketData.Packet.OrderedId = 0;
			}

			MessageStream.Write(PacketData);
			MessageStream.WriteData(packet, Offset, Length);


			++peer.Data()->GlobalPacketId;
			peer.Data()->ReliablePacketQueue.Enqueue(UserPacket);
		}

		if (type == MessageType::ReliebleOrdered)
		{
			++peer.Data()->OrderedPacketId;
		}
	}
	else
	{
		QueuedPacket UserPacket;
		UserPacket.Length = packetLength + sizeof(Packet);
		UserPacket.Packet = new uint8_t[UserPacket.Length];
		UserPacket.EndPoint = peer.EndPoint();

		Packet PacketData(InternalMessage::UserMessage);
		PacketData.Protocol = protocolId;
		PacketData.GlobalId = peer.Data()->GlobalPacketId;

		SK::IO::MemoryStream MessageStream(UserPacket.Packet, UserPacket.Length, false);
		MessageStream.Endianness(SK::IO::Endian::Big);

		if (type == MessageType::Unreliable)
		{
			MessageStream.Write(PacketData);
			MessageStream.WriteData(packet, 0, packetLength);

			++peer.Data()->GlobalPacketId;
			m_Implementation->UnreliablePacketQueue.Enqueue(UserPacket);
		}
		else
		{
			PacketData.Ack = peer.Data()->GlobalPacketId;

			if (type == MessageType::ReliebleOrdered)
			{
				PacketData.OrderedId = peer.Data()->OrderedPacketId++;
			}

			MessageStream.Write(PacketData);
			MessageStream.WriteData(packet, 0, packetLength);

			++peer.Data()->GlobalPacketId;
			peer.Data()->ReliablePacketQueue.Enqueue(UserPacket);
		}
	}
}


namespace
{

	void PeerUpdateLoop(Peer* peer, Peer::Implementation* impl)
	{
		uint8_t Buffer[1024];
		EndPoint From;

		while (impl->Update)
		{
			// Send unreliable packets
			QueuedPacket UnreliablePacket;
			while (impl->UnreliablePacketQueue.Dequeue(UnreliablePacket))
			{
				impl->Send(UnreliablePacket.Packet, UnreliablePacket.Length, UnreliablePacket.EndPoint);
			}

			// Send reliable packets
			impl->RemotePeersLock.lock();
			for (auto Remote : impl->ConnectedPeersRef)
			{
				auto& ThreadData = Remote->Data()->Thread;

				// Update ping times
				if (ThreadData.RecalcAveragePing)
				{
					Remote->Data()->AveragePingTime = ThreadData.PingTimes.Mean();
				}

				// Move reliable packets to a vector that this thread manages
				QueuedPacket ReliablePacket;
				while (Remote->Data()->ReliablePacketQueue.Dequeue(ReliablePacket))
				{
					SK::IO::MemoryStream PacketStream(ReliablePacket.Packet, ReliablePacket.Length);
					PacketStream.Endianness(SK::IO::Endian::Big);
					Packet packet = PacketStream.Read<Packet>();
					ThreadData.ReliablePacketsCache[packet.Ack] = ReliablePacket;
				}

				// Send reliable packets
				bool LostPacket = false;
				SK::Time::Duration RetryTime = Remote->PingTime() * 5;
				for (auto& ReliablePacket : Remote->Data()->Thread.ReliablePacketsCache)
				{
					if (!ReliablePacket.second.OnWire || (SK::Time::Now() - ReliablePacket.second.SendTime) > RetryTime)
					{
						if (ReliablePacket.second.OnWire)
						{
							// The packet is (assumed to be) lost
							ReliablePacket.second.OnWire = false;
							ThreadData.BytesOnWire -= ReliablePacket.second.Length;

							// But only reduce the window once per frame
							if (!LostPacket)
							{
								ThreadData.WindowSize = std::max(ThreadData.WindowSize / 2, MAX_PACKET_SIZE);
								LostPacket = true;
							}

							// Slow down window increase rate
							ThreadData.CongestionStartUp = false;
						}

						if (ThreadData.BytesOnWire + ReliablePacket.second.Length <= ThreadData.WindowSize)
						{
							ThreadData.BytesOnWire += ReliablePacket.second.Length;
							ReliablePacket.second.SendTime = SK::Time::Now();
							ReliablePacket.second.OnWire = true;

							impl->Send(ReliablePacket.second.Packet, ReliablePacket.second.Length, ReliablePacket.second.EndPoint);
						}
					}
				}
			}
			impl->RemotePeersLock.unlock();

			// Receive packets
			int BytesReceived = 0;
			do
			{
				BytesReceived = impl->Receive(Buffer, sizeof(Buffer), From);
				if (BytesReceived >= int(sizeof(Packet)))
				{
					SK::IO::MemoryStream MessageStream(Buffer, BytesReceived);
					MessageStream.Endianness(SK::IO::Endian::Big);
					Packet ReceivedPacket = MessageStream.Read<Packet>();

					if (ReceivedPacket.GlobalId)
					{
						// Check packet is not a duplicate
						Peer::Implementation::PacketKey Key(ReceivedPacket.GlobalId, From);
						auto It = impl->ReceivedPackets.find(Key);
						if (It != impl->ReceivedPackets.end())
						{
							// Handle duplicate
							if (ReceivedPacket.Ack)
							{
								Packet AckPacket(InternalMessage::Ack);
								AckPacket.Ack = ReceivedPacket.Ack;
								impl->Send(AckPacket, From);
							}
							It->second = SK::Time::Now();

							// Don't do anything further
							continue;
						}
						else
						{
							// Store packet id
							impl->ReceivedPackets[Key] = SK::Time::Now();
						}
					}

					ProcessPacket(ReceivedPacket, MessageStream, From, *peer, impl);
				}

			} while (BytesReceived > 0);

			SK::Threading::Sleep(SK::Time::Milliseconds(10));
		}
	}

	void ProcessPacket(Packet& packet, SK::IO::MemoryStream& messageStream, EndPoint& from, Peer& peer, Peer::Implementation* impl)
	{
		RemotePeer* RemotePeer = nullptr;
		impl->RemotePeersLock.lock();
		for (auto Remote : impl->ConnectedPeersRef)
		{
			if (Remote->EndPoint() == from)
			{
				RemotePeer = Remote;
				break;
			}
		}
		impl->RemotePeersLock.unlock();

		if (RemotePeer)
		{
			auto& ThreadData = RemotePeer->Data()->Thread;

			switch (packet.Message)
			{
			case InternalMessage::ConnectionRequest:
				{
					SK::Debug::Assert(false, "Remote Peer is already connected");
					impl->Send(Packet(InternalMessage::ConnectionAccepted), from);
				}
				break;

			case InternalMessage::Ack:
				{
					auto It = ThreadData.ReliablePacketsCache.find(packet.Ack);
					if (It != ThreadData.ReliablePacketsCache.end())
					{
						// Successful transmission, increase window size
						uint32_t WindowIncrement = ThreadData.CongestionStartUp ? It->second.Length * 2 : (It->second.Length * MAX_PACKET_SIZE) / ThreadData.WindowSize;
						ThreadData.WindowSize = std::min(ThreadData.WindowSize + WindowIncrement, MAX_WINDOW_SIZE);

						// If OnWire is false here, it just means we thought that the packet had been lost
						if (It->second.OnWire)
						{
							ThreadData.BytesOnWire -= It->second.Length;
						}

						ThreadData.PingTimes.Push(SK::Time::Now() - It->second.SendTime);
						ThreadData.RecalcAveragePing = true;

						delete [] It->second.Packet;
						ThreadData.ReliablePacketsCache.erase(It);
					}
				}
				break;

			case InternalMessage::Ping:
				{
					impl->Send(Packet(InternalMessage::Pong), from);
				}
				break;

			case InternalMessage::Multipart:
				{
					if (packet.Ack > 0)
					{
						Packet AckPacket(InternalMessage::Ack);
						AckPacket.Ack = packet.Ack;
						impl->Send(AckPacket, from);
					}

					//Reset stream and read a multipart packet
					messageStream.Position(0);
					MultipartPacket MultiPacket = messageStream.Read<MultipartPacket>();
					uint32_t PayloadLength = static_cast<uint32_t>(messageStream.Length() - messageStream.Position());
					if (ThreadData.MultipartPackets.count(MultiPacket.SharedId) > 0)
					{
						auto& ReconstructedPacket = ThreadData.MultipartPackets[MultiPacket.SharedId];
						messageStream.ReadData(ReconstructedPacket.Buffer, MultiPacket.DataOffset, PayloadLength);
						ReconstructedPacket.DataReceived += PayloadLength;
					}
					else
					{
						// First packet in a group
						MultipartBuffer NewPart;
						NewPart.DataLength = MultiPacket.TotalDataLength;
						NewPart.DataReceived = PayloadLength;
						NewPart.Buffer = new uint8_t[MultiPacket.TotalDataLength];
						messageStream.ReadData(NewPart.Buffer, MultiPacket.DataOffset, PayloadLength);

						ThreadData.MultipartPackets[MultiPacket.SharedId] = NewPart;
					}

					MultipartBuffer& MultiBuffer = ThreadData.MultipartPackets[MultiPacket.SharedId];
					if (MultiBuffer.DataReceived >= MultiBuffer.DataLength)
					{
						// Packet is ready, move the data
						// SK::Array will make sure the data is deleted when we are finished with it
						SK::Array<uint8_t> Buffer(MultiBuffer.Buffer, MultiBuffer.DataLength);
						RoutedMessage Message(packet.Protocol, Buffer);
						if (packet.OrderedId == 0)
						{
							RemotePeer->Data()->IncomingPacketQueue.Enqueue(Message);
						}
						else
						{
							ThreadData.UserPacketsCache.push_back(std::make_pair(packet.OrderedId, Message));
							if (packet.OrderedId == RemotePeer->Data()->ExpectedPacketId)
							{
								// Add ordered packets to the queue (in order, of course)
								CheckOrderedPackets(*RemotePeer->Data());
							}
						}
						ThreadData.MultipartPackets.erase(MultiPacket.SharedId);
					}
				}
				break;

			case InternalMessage::UserMessage:
				{
					if (packet.Ack > 0)
					{
						Packet AckPacket(InternalMessage::Ack);
						AckPacket.Ack = packet.Ack;
						impl->Send(AckPacket, from);
					}

					int PayloadLength = static_cast<uint32_t>(messageStream.Length() - messageStream.Position());
					uint8_t* UserBuffer = new uint8_t[PayloadLength];
					messageStream.ReadData(UserBuffer, 0, PayloadLength);

					SK::Array<uint8_t> Buffer(UserBuffer, PayloadLength);
					RoutedMessage Message(packet.Protocol, Buffer);
					if (packet.OrderedId == 0)
					{
						RemotePeer->Data()->IncomingPacketQueue.Enqueue(Message);
					}
					else
					{
						ThreadData.UserPacketsCache.push_back(std::make_pair(packet.OrderedId, Message));
						if (packet.OrderedId == RemotePeer->Data()->ExpectedPacketId)
						{
							// Add ordered packets to the queue (in order, of course)
							CheckOrderedPackets(*RemotePeer->Data());
						}
					}
				}
				break;
			}
		}
		else
		{
			// This message is from an unknown source
			switch (packet.Message)
			{
			case InternalMessage::ConnectionAccepted:
			case InternalMessage::ConnectionRequest:
			case InternalMessage::ConnectionDenied:
				{
					Peer::Implementation::NetworkEvent Event;
					Event.Message = packet.Message;
					Event.From = from;
					impl->Events.Enqueue(Event);
				}
				break;

			case InternalMessage::UserMessage:
				int PayloadLength = static_cast<uint32_t>(messageStream.Length() - messageStream.Position());
				uint8_t* UserBuffer = new uint8_t[PayloadLength];
				messageStream.ReadData(UserBuffer, 0, PayloadLength);
					
				SK::Array<uint8_t> Buffer(UserBuffer, PayloadLength);
				RoutedMessage Message(packet.Protocol, Buffer);
				impl->UnsolicitedPacketQueue.Enqueue(std::make_pair(from, Message));
				break;
			}
		}
	}

	void CheckOrderedPackets(RemotePeer::Implementation& impl)
	{
		bool LoopAgain = false;
		do
		{
			LoopAgain = false;
			for (auto& CachedPacket : impl.Thread.UserPacketsCache)
			{
				if (CachedPacket.first == impl.ExpectedPacketId)
				{
					impl.IncomingPacketQueue.Enqueue(CachedPacket.second);
					++impl.ExpectedPacketId;
					LoopAgain = true;
					break;
				}
			}
		} while (LoopAgain);
	}
}
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKNetwork/Peer.h>
#include <SKNetwork/RemotePeer.h>
#include <SKNetwork/Direct/Implementations.h>
using namespace SK::Network;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

RemotePeer::RemotePeer(const SK::Network::EndPoint& ep)
: m_Implementation(new Implementation())
, m_EndPoint(ep)
{
	
}

SK::Time::Duration RemotePeer::PingTime() const
{
	if (m_Implementation->AveragePingTime == SK::Time::Zero())
	{
		return SK::Time::Milliseconds(50);
	}
	return m_Implementation->AveragePingTime;
}

const SK::String& RemotePeer::Name() const
{
	//SKTD: Get an actual name from somewhere
	static SK::String Unknown("Unknown");
	return Unknown;
}

//-- Private Methods --//
//=====================//

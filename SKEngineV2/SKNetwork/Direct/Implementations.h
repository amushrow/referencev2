#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKNetwork/Direct/Protocol.h>

namespace SK
{
	namespace Network
	{
		//---- Types ----//
		//===============//

		struct RemotePeer::Implementation
		{
			Implementation() : ExpectedPacketId(1), OrderedPacketId(1), GlobalPacketId(1)
			{

			}

			uint16_t                 GlobalPacketId;      // Id of next packet to be sent (to detect duplicate packets)
			uint16_t                 ExpectedPacketId;    // Id of next ordered packet we expect to receive
			uint16_t                 OrderedPacketId;     // Id of next ordered packet to be sent

			SK::Queue<QueuedPacket>  ReliablePacketQueue; // Outgoing packets
			SK::Queue<RoutedMessage> IncomingPacketQueue; // Incoming packets

			SK::Time::Duration       AveragePingTime;

			//-- Data managed by the network thread
			struct ThreadData
			{
				ThreadData() : WindowSize(MAX_PACKET_SIZE), BytesOnWire(0), CongestionStartUp(true), RecalcAveragePing(false) {}

				std::map<uint16_t, QueuedPacket>                ReliablePacketsCache; // Reliable packets waiting for Ack
				std::vector<std::pair<uint16_t, RoutedMessage>> UserPacketsCache;     // Complete packets ready for the user
				std::map<uint16_t, MultipartBuffer>             MultipartPackets;     // Partial packets

				uint32_t                                        WindowSize;           // Max allowed bytes awaiting ack
				uint32_t                                        BytesOnWire;          //
				bool                                            CongestionStartUp;

				SK::Math::Average<SK::Time::Duration, 10>       PingTimes;
				bool                                            RecalcAveragePing;
			};
			ThreadData Thread;
			//-- 
		};

		struct Peer::Implementation
		{
			//---- Types ----//
			//===============//

			struct PlatformData;
			typedef std::pair<EndPoint, RoutedMessage> UnsolicitedMessage;

			struct PacketKey
			{
				PacketKey(uint16_t id, EndPoint from) : Id(id), From(from) {}

				bool operator<(const PacketKey& rhs) const
				{
					if (Id == rhs.Id) return From < rhs.From;
					else              return Id < rhs.Id;
				}

				uint16_t Id;
				EndPoint From;
			};

			struct NetworkEvent
			{
				InternalMessage Message;
				EndPoint        From;
				uint16_t        Id;
			};

			//--- Methods ---//
			//===============//

			Implementation(std::vector<RemotePeer*>& peers) : ConnectedPeersRef(peers), Update(true) {}

			void Send(const uint8_t* buffer, int length, const EndPoint& to);
			void Send(const Packet& packet, const EndPoint& to);
			int  Receive(uint8_t* buffer, uint32_t bufferLength, EndPoint& from);

			//-- Variables --//
			//===============//

			PlatformData*                            Data;
			std::mutex                               RemotePeersLock;
			std::vector<RemotePeer*>&                ConnectedPeersRef;
			SK::Queue<QueuedPacket>                  UnreliablePacketQueue;
			SK::Threading::Thread                    UpdateThread;
			bool                                     Update;
			std::map<PacketKey, SK::Time::TimePoint> ReceivedPackets;
			SK::Queue<NetworkEvent>                  Events;

			SK::Queue<UnsolicitedMessage>            UnsolicitedPacketQueue; // Incoming packets
		};

		struct Lobby::PlatformData
		{
			PlatformData(const SK::String& name, uint16_t maxPlayers) : Name(name), NbPlayers(0), MaxNbPlayers(maxPlayers), Id(-1) {}
			LobbyFilters Filters;
			SK::String   Name;
			uint16_t     NbPlayers;
			uint16_t     MaxNbPlayers;
			uint64_t     Id;
		};
	}
}
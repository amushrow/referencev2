#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <type_traits>
#include <initializer_list>

namespace SK
{
	//---- Types ----//
	//===============//

	template<typename Enum>
	class Flags
	{
	public:
		//---- Types ----//
		//===============//

		typedef int Repr;

		//--- Methods ---//
		//===============//

		Flags() : m_Flags(0) {}
		Flags(Enum value) : m_Flags(Repr(value)) {}

		Flags(const std::initializer_list<Enum>& flags) : m_Flags(0)
		{
			for (const Enum& flag : flags)
			{
				m_Flags |= Repr(flag);
			}
		}

		Flags&       operator=(Enum value)         { m_Flags = value; return *this; }
		bool         operator==(Flags other) const { return m_Flags == other.m_Flags; }
		bool         operator!=(Flags other) const { return m_Flags != other.m_Flags; }
		Flags        operator~() const             { return Flags(~m_Flags); }
		Flags        operator&(Flags other) const  { return Flags(m_Flags & other.m_Flags); }
		const Flags& operator&=(Flags other)       { m_Flags &= other.m_Flags; return *this; }
		Flags        operator|(Flags other) const  { return Flags(m_Flags | other.m_Flags); }
		const Flags& operator|=(Flags other)       { m_Flags |= other.m_Flags; return *this; }
		Flags        operator|(Enum other) const   { return Flags(m_Flags | Repr(other)); }

		bool HasFlags(Flags flags)
		{
			return (m_Flags & flags.m_Flags) == flags.m_Flags;
		}

		bool HasFlags(Enum flag)
		{
			return (m_Flags & Repr(flag)) == Repr(flag);
		}

		template<typename... Arguments>
		bool HasFlags(Enum flag, Arguments... arguments)
		{
			int OtherFlags = Repr(flag) | Or(arguments...);
			return (m_Flags & OtherFlags) == OtherFlags;
		}

	private:
		//--- Methods ---//
		//===============//
		
		template<class = typename std::enable_if<!std::is_same<Enum, Repr>::value>::type>
		Flags(Repr value) : m_Flags(value) {}

		Repr Or() const { return 0; }

		template<class Enum, typename... Arguments>
		Repr Or(Enum flag, Arguments... arguments) const
		{
			return Repr(flag) | Or(arguments...);
		}

		//-- Variables --//
		//===============//

		Repr m_Flags;
	};

	class GenericFlags
	{
	public:
		//--- Methods ---//
		//===============//

		GenericFlags() : m_Flags(0) {}
		GenericFlags(int value) : m_Flags(static_cast<int>(value)) {}

		GenericFlags(const std::initializer_list<int>& flags) : m_Flags(0)
		{
			for (const int& flag : flags)
			{
				m_Flags |= static_cast<int>(flag);
			}
		}

		template<class T, class = typename std::enable_if<std::is_enum<T>::value>::type>
		GenericFlags(const std::initializer_list<T>& flags)
		{
			for (const T& flag : flags)
			{
				m_Flags |= static_cast<int>(flag);
			}
		}

		template<class T, class = typename std::enable_if<std::is_enum<T>::value>::type>
		GenericFlags(T flag) : m_Flags(static_cast<int>(flag)) {}

		GenericFlags&       operator=(int value)                 { m_Flags = value; return *this; }
		bool                operator==(GenericFlags other) const { return m_Flags == other.m_Flags; }
		bool                operator!=(GenericFlags other) const { return m_Flags != other.m_Flags; }
		GenericFlags        operator~() const                    { return GenericFlags(~m_Flags); }
		GenericFlags        operator&(GenericFlags other) const  { return GenericFlags(m_Flags & other.m_Flags); }
		const GenericFlags& operator&=(GenericFlags other)       { m_Flags &= other.m_Flags; return *this; }
		GenericFlags        operator|(GenericFlags other) const  { return GenericFlags(m_Flags | other.m_Flags); }
		const GenericFlags& operator|=(GenericFlags other)       { m_Flags |= other.m_Flags; return *this; }
		GenericFlags        operator|(int other) const           { return GenericFlags(m_Flags | static_cast<int>(other)); }

		bool HasFlags(GenericFlags flags)
		{
			return (m_Flags & flags.m_Flags) == flags.m_Flags;
		}

		bool HasFlags(int flags)
		{
			return (m_Flags & flags) == flags;
		}

		template<class T, class = typename std::enable_if<std::is_enum<T>::value>::type>
		bool HasFlags(T flag)
		{
			return (m_Flags & static_cast<int>(flag)) == static_cast<int>(flag);
		}

		template<typename T, typename... Arguments>
		bool HasFlags(T flag, Arguments... arguments)
		{
			int OtherFlags = GenericFlags::Or(flag, arguments...);
			return (m_Flags & OtherFlags) == OtherFlags;
		}

		template<class T>
		static int Or(T flag) { return static_cast<int>(flag);  }

		template<class T, typename... Arguments>
		static int Or(T flag, Arguments... arguments)
		{
			return static_cast<int>(flag) | GenericFlags::Or(arguments...);
		}

	private:
		//-- Variables --//
		//===============//

		int m_Flags;
	};
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Time/Time.h>
#include <SKSystem/Time/Clocks.h>
#include <SKSystem/Threading/Thread.h>

namespace SK
{
	namespace Time
	{
		//---- Types ----//
		//===============//

		class Timer
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Timer(Duration interval, TimeEvent callback);
			Timer(Timer&& other);
			Timer& operator=(Timer&& other);

			void Update(const TimeEventArgs& e);

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//
			
			Timer(const Timer& other) = delete;
			Timer& operator=(const Timer& other) = delete;

			//-- Variables --//
			//===============//

			Duration  m_Interval;
			Duration  m_Elapsed;
			TimeEvent m_IntermittentFunction;
		};

		class AutoUpdateTimer
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			AutoUpdateTimer(Duration interval, TimeEvent callback, ClockPtr clock = nullptr, bool start = false);
			AutoUpdateTimer(AutoUpdateTimer&& other);
			AutoUpdateTimer& operator=(AutoUpdateTimer&& other);
			~AutoUpdateTimer();

			void Stop();
			void Start();

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			AutoUpdateTimer(const AutoUpdateTimer& other) = delete;
			AutoUpdateTimer& operator=(const AutoUpdateTimer& other) = delete;

			void UpdateTime();

			//-- Variables --//
			//===============//

			Duration              m_Interval;
			TimePoint             m_UpdateTime;
			bool                  m_Stopped;
			ClockPtr              m_Clock;
			TimeEvent             m_IntermittentFunction;
			SK::Threading::Thread m_UpdateThread;
		};

		//--- Methods ---//
		//===============//
	}
}
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Win32/Win32.h>
#include <SKSystem/Time/Clocks.h>

using namespace SK;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

namespace
{
	int64_t GetClockFrequency()
	{
		LARGE_INTEGER Frequency;
		QueryPerformanceFrequency(&Frequency);
		return Frequency.QuadPart;
	}

	int64_t _ClockFrequency = 0;
}

//-- Public Methods --//
//====================//

Time::TimePoint Time::HighPerformanceClock::Time()
{
	if (_ClockFrequency == 0)
		_ClockFrequency = GetClockFrequency();

	LARGE_INTEGER currentTime;
	QueryPerformanceCounter(&currentTime);
	return TimePoint(static_cast<int64_t>((1000000 * currentTime.QuadPart) / _ClockFrequency));
}

//-- Private Methods --//
//=====================//

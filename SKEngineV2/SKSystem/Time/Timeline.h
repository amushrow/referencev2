#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <memory>
#include <unordered_map>
#include "Clocks.h"

namespace SK
{
	namespace Time
	{
		//---- Types ----//
		//===============//

		class Timeline
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Timeline(ClockPtr clock, Duration fixedUpdateInterval = Duration(0));

			void  Update();
			void  SetScale(float scale) { m_Scale = scale; }
			float GetScale()            { return m_Scale; }
			bool  IsPaused() const      { return m_Scale == 0.0f; }

			template<class T>
			void  Add(T* object)        { m_Objects.insert({ object, [object](const TimeEventArgs& e) { object->Update(e); } }); }
			void  Remove(void* object)  { m_Objects.erase(object); }

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			void OnUpdate(Duration delta, TimePoint now);

			//-- Variables --//
			//===============//

			ClockPtr                             m_Clock;
			Duration                             m_UpdateInterval;
			Duration                             m_Elapsed;
			TimePoint                            m_LastUpdate;
			TimePoint                            m_Now;
			float                                m_Scale;
			std::unordered_map<void*, TimeEvent> m_Objects;
		};

		//--- Methods ---//
		//===============//
	}
}
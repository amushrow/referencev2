#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstdint>
#include <SKSystem/IO/Stream.h>

namespace SK
{
	namespace Crypto
	{
		//---- Types ----//
		//===============//

		class CRC32
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			CRC32() : m_crc(0xFFFFFFFF) {}
			void     Update(const uint8_t* buffer, uint32_t length);
			void     Update(IO::Stream& stream, uint32_t length);
			uint32_t Hash();

			static uint32_t Compute(const uint8_t* buffer, uint32_t length);
			static uint32_t Compute(IO::Stream& stream, uint32_t length);

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			uint32_t m_crc;
		};

		//--- Methods ---//
		//===============//
	}
}
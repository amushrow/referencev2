#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

// Windows specific includes used in SKEngine
// Includes helper function to get wchar_t* null-terminated string from SK::String for API calls

//--- Include ---//
//===============//

#define _WIN32_WINNT_NT4                    0x0400
#define _WIN32_WINNT_WIN2K                  0x0500
#define _WIN32_WINNT_WINXP                  0x0501
#define _WIN32_WINNT_WS03                   0x0502
#define _WIN32_WINNT_WIN6                   0x0600
#define _WIN32_WINNT_VISTA                  0x0600
#define _WIN32_WINNT_WS08                   0x0600
#define _WIN32_WINNT_LONGHORN               0x0600
#define _WIN32_WINNT_WIN7                   0x0601
#define _WIN32_WINNT_WIN8                   0x0602
#define _WIN32_WINNT_WINBLUE                0x0603

#define WIN32_LEAN_AND_MEAN 
#define NOMINMAX
//#define _WIN32_WINNT _WIN32_WINNT_VISTA
#include <Windows.h>
#include <tchar.h>
#include <codecvt>
#include <SKSystem/Array.h>
#include <SKSystem/Text/String.h>

#undef Yield

namespace Win32
{
	//---- Types ----//
	//===============//

	class WindowsGayAssWideString
	{
	public:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		WindowsGayAssWideString(SK::Array<uint8_t> data) : m_Data(data) {}

		operator const wchar_t*()
		{
			return reinterpret_cast<wchar_t*>(m_Data.Get());
		}

		//-- Variables --//
		//===============//

	private:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		//-- Variables --//
		//===============//

		SK::Array<uint8_t> m_Data;
	};
	
	//--- Methods ---//
	//===============//

	WindowsGayAssWideString GetWideString(const SK::String& string);
}
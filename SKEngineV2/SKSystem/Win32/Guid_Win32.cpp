//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <Rpc.h>
#include <SKSystem/Guid.h>

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

SK::Guid SK::Guid::Generate()
{
	Guid guid;
	UuidCreate(reinterpret_cast<UUID*>(&guid.Data));
	return guid;
}

//-- Private Methods --//
//=====================//
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Debug.h>
#include <functional>
#include <vector>
#include <utility>

namespace SK
{
	//---- Types ----//
	//===============//

	struct EmptyEventData
	{

	};

	class EventKey
	{
	public:
		//---- Types ----//
		//===============//

		friend EventKey GetEventKey(void);
	
		//--- Methods ---//
		//===============//

		EventKey(const EventKey& rhs) : m_Id(rhs.m_Id) {}

		EventKey& operator=(const EventKey& rhs)
		{
			m_Id = rhs.m_Id;
			return *this;
		}

		bool operator==(const EventKey& rhs)
		{
			return m_Id == rhs.m_Id;
		}

		bool operator!=(const EventKey& rhs)
		{
			return m_Id != rhs.m_Id;
		}
		
		//-- Variables --//
		//===============//

	private:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		EventKey(unsigned int id) : m_Id(id) {}
		
		//-- Variables --//
		//===============//

		unsigned int m_Id;
	};

	template<typename EventData>
	class Event
	{
	public:
		//---- Types ----//
		//===============//
		
		//friend class EventDelegate<EventData>;
		typedef std::function<void(void*, EventData&)> EventHandler;
		typedef std::vector<std::pair<EventKey, EventHandler>> HandlerList;

		//--- Methods ---//
		//===============//
		
		Event() {}

		void Register(EventKey key, EventHandler handler)
		{
			Deregister(key);
			m_Handlers.push_back(std::make_pair(key, handler));
		}

		void Deregister(EventKey key)
		{
			for (auto It = m_Handlers.begin(); It != m_Handlers.end(); ++It)
			{
				if (It->first == key)
				{
					m_Handlers.erase(It);
					break;
				}
			}
		}

		void OnEvent(void* sender, EventData& data, std::function<bool (EventData& data)> continueFunc)
		{
			for (auto& Handler : m_Handlers)
			{
				Handler.second(sender, data);
				if (!continueFunc(data))
					break;
			}
		}

		void OnEvent(void* sender, EventData& data)
		{
			for (auto& Handler : m_Handlers)
			{
				Handler.second(sender, data);
			}
		}

		void OnEvent(EventData& data)
		{
			OnEvent(nullptr, data);
		}

		bool Empty()
		{
			return m_Handlers.size() == 0;
		}
		
		//-- Variables --//
		//===============//

	private:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		//-- Variables --//
		//===============//
		HandlerList m_Handlers;
	};

	// EventDelegate only exposes Register and Deregister and should be used as the public interface
	// to an event to prevent unwanted calls to OnEvent
	template<typename EventData>
	class EventDelegate
	{
	public:
		//---- Types ----//
		//===============//

		typedef std::function<void(void*, EventData&)>         EventHandler;
		typedef std::vector<std::pair<EventKey, EventHandler>> HandlerList;

		//--- Methods ---//
		//===============//

		EventDelegate(Event<EventData>& owner) : m_Owner(owner) {}

		void Register(EventKey key, EventHandler handler)
		{
			return m_Owner.Register(key, handler);
		}

		void Deregister(EventKey key)
		{
			m_Owner.Deregister(key);
		}

		//-- Variables --//
		//===============//

	private:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		//-- Variables --//
		//===============//

		Event<EventData>& m_Owner;
	};

	//--- Methods ---//
	//===============//

	EventKey GetEventKey();
}
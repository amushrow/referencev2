//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include "ThreadPool.h"

using namespace SK::Threading;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

ThreadPool::ThreadPool(unsigned int size)
: m_Exit(false)
, m_CompleteTasks(true)
{
	m_Workers.reserve(size);
	for(unsigned int i = 0; i < size; ++i)
	{
		m_Workers.emplace_back(&ThreadPool::RunTasks, this);
	}
}

ThreadPool::~ThreadPool()
{
	m_QueueLock.lock();
	m_Exit = true;
	m_QueueLock.unlock();

	m_QueueConditional.notify_all();
	for(Thread& t : m_Workers)
		t.Join();
	
	m_Workers.clear();
}

void SK::Threading::ThreadPool::BeginAddTasks()
{
	m_QueueLock.lock();
}

void SK::Threading::ThreadPool::EndAddTasks()
{
	m_QueueLock.unlock();
	m_QueueConditional.notify_all();
}

void ThreadPool::Stop(bool finishTasks)
{
	if (finishTasks)
		RunTasksInCurrentContext();

	m_QueueLock.lock();
	m_Exit = true;
	m_QueueLock.unlock();

	m_QueueConditional.notify_all();
	for (Thread& t : m_Workers)
		t.Join();
	
	m_Workers.clear();
}

void ThreadPool::RunTasksInCurrentContext()
{
	while (true)
	{
		// Acquire lock
		std::unique_lock<std::mutex> Lock(m_QueueLock);

		if (m_Tasks.empty())
			return;

		std::function<void()> nextTask = m_Tasks.front();
		m_Tasks.pop();

		// Release lock to allow other threads to get work
		Lock.unlock();

		// Do some work
		nextTask();
	}
}

//-- Private Methods --//
//=====================//

void ThreadPool::RunTasks()
{
	while (true)
	{
		// Acquire lock
		std::unique_lock<std::mutex> Lock(m_QueueLock);
		
		while (!m_Exit && m_Tasks.empty())
		{
			// Wait for some work. The lock will be released if the conditional
			// is not ready, and will be reacquired when it is
			m_QueueConditional.wait(Lock);
		}

		if (m_Exit && (m_Tasks.empty() || !m_CompleteTasks))
		{
			return;
		}

		std::function<void()> nextTask = m_Tasks.front();
		m_Tasks.pop();

		// Release lock to allow other threads to get work
		Lock.unlock();

		// Finally do some work
		nextTask();
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <future>
#include <SKSystem/Time/Time.h>

namespace SK
{
	namespace Threading
	{
		//---- Types ----//
		//===============//

		class ThreadId
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			ThreadId();
			ThreadId(const std::thread::id& threadId) : m_InternalId(threadId) {}

			bool operator ==(const ThreadId& other) const
			{
				return m_InternalId == other.m_InternalId;
			}

			bool operator !=(const ThreadId& other) const
			{
				return m_InternalId != other.m_InternalId;
			}

			size_t hash() const;

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//
			
			//-- Variables --//
			//===============//

			std::thread::id m_InternalId;
		};

		class Thread
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Thread() {}

			template<class Fn, class... Args>
			Thread(Fn&& function, Args&&... args) : m_Thread(std::forward<Fn>(function), std::forward<Args>(args)...) {}

			Thread(Thread&& other) throw();
			Thread&  operator=(Thread&& other) throw();

			void     Join();
			void     Detach();
			ThreadId GetThreadId();
			bool     IsValid();
			void*    NativeHandle();

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Thread(const Thread& other) = delete;
			Thread& operator=(const Thread& other) = delete;

			//-- Variables --//
			//===============//

			std::thread m_Thread;
		};

		//--- Methods ---//
		//===============//

		void     Sleep(Time::Duration delay);
		void     Yield();
		ThreadId GetThreadId();

		void     Dispatch();
		void     Invoke(const ThreadId& thread, Time::Duration timeout, std::function<void()> function);
		void     Invoke(const ThreadId& thread, std::function<void()> function);

		template<class Fn, class... Args, class = typename std::enable_if<std::is_same<typename std::result_of<Fn(Args...)>::type, typename std::result_of<Fn(Args...)>::type>::value>::type>
		auto     InvokeEx(const ThreadId& thread, Time::Duration timeout, Fn&& function, Args&&... args) -> typename std::result_of<Fn(Args...)>::type;

		template<class Fn, class... Args, class = typename std::enable_if<!std::is_convertible<Fn, Time::Duration>::value>::type>
		auto     InvokeEx(const ThreadId& Thread, Fn&& function, Args&&... args) -> typename std::result_of<Fn(Args...)>::type;
	}
}

//--- Methods ---//
//===============//

template<class Fn, class... Args, class>
auto SK::Threading::InvokeEx(const ThreadId& thread, Time::Duration timeout, Fn&& function, Args&&... args) -> typename std::result_of<Fn(Args...)>::type
{
	typedef typename std::result_of<Fn(Args...)>::type ReturnType;
	auto task = std::make_shared<std::packaged_task<ReturnType()>>( std::bind(std::forward<Fn>(function), std::forward<Args>(args)...) );
	std::future<ReturnType> Result = task->get_future();

	Invoke(thread, timeout, [task](){ (*task)(); });

	return Result.get();
}

template<class Fn, class... Args, class>
auto SK::Threading::InvokeEx(const ThreadId& thread, Fn&& function, Args&&... args) -> typename std::result_of<Fn(Args...)>::type
{
	typedef typename std::result_of<Fn(Args...)>::type ReturnType;
	auto task = std::make_shared<std::packaged_task<ReturnType()>>( std::bind(std::forward<Fn>(function), std::forward<Args>(args)...) );
	std::future<ReturnType> Result = task->get_future();
	
	Invoke(thread, Time::Duration::Max(), [task](){ (*task)(); });

	return Result.get();
}

//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include "ThreadProc.h"

using namespace SK::Threading;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

ThreadProc::ThreadProc(std::function<void()> func)
: m_Task(func)
, m_Completed(false)
, m_EarlyExit(false)
{

}

ThreadProc::~ThreadProc()
{

}

void ThreadProc::Wait() const
{
	if (!m_Completed)
	{
		Wait(Time::Duration::Max());
	}
}

bool ThreadProc::Wait(SK::Time::Duration timeout) const
{
	if (!m_Completed)
	{
		std::unique_lock<std::mutex> QueueLock(m_mutex);
		std::chrono::microseconds TimeoutTime(timeout.Microseconds());
		std::chrono::nanoseconds CurrentTime = std::chrono::system_clock::now().time_since_epoch();
		if ((CurrentTime + TimeoutTime) > CurrentTime)
		{
			m_QueueConditional.wait_for(QueueLock, TimeoutTime, [this](){ return m_Completed || m_EarlyExit; });
		}
		else
		{
			// Timeout time is past the end of time
			m_QueueConditional.wait(QueueLock, [this](){ return m_Completed || m_EarlyExit; });
		}
		QueueLock.unlock();

		return m_Completed;
	}
	return true;
}

bool ThreadProc::Completed() const
{
	return m_Completed;
}

void ThreadProc::ExecuteTask()
{
	if (!m_Completed)
	{
		// Call task
		m_Task();

		// Flag this task as completed and notify anybody waiting
		m_mutex.lock();
		m_Completed = true;
		m_mutex.unlock();

		m_QueueConditional.notify_all();
	}
}

//-- Private Methods --//
//=====================//

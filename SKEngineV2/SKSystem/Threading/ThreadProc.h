#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <mutex>
#include <queue>
#include <condition_variable>
#include <SKSystem/Time/Time.h>

namespace SK
{
	namespace Threading
	{
		//---- Types ----//
		//===============//

		class ThreadProc
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			ThreadProc(std::function<void()> func);
			~ThreadProc();

			void Wait() const;
			bool Wait(SK::Time::Duration timeout) const;
			bool Completed() const;

			void ExecuteTask();

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//
			
			ThreadProc(const ThreadProc& other) = delete;
			ThreadProc& operator=(const ThreadProc& other) = delete;
			ThreadProc(ThreadProc&& other) = delete;
			ThreadProc& operator=(ThreadProc&& other) = delete;

			//-- Variables --//
			//===============//

			mutable std::mutex              m_mutex;
			mutable std::condition_variable m_QueueConditional;
			std::function<void()>           m_Task;
			bool                            m_Completed;
			bool                            m_EarlyExit;
		};

		//--- Methods ---//
		//===============//
	}
}
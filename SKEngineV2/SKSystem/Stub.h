#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2016 Anthony Mushrow                           //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//

namespace SK
{
	namespace Input
	{
		//---- Types ----//
		//===============//

		class MyClass
		{
		public:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

		private:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

		};

		//--- Methods ---//
		//===============//
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <iostream>
#include <utility>

namespace SK
{
	//---- Types ----//
	//===============//

	class Debug
	{
	public:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//
		
		static void Halt();

		static void Assert(bool condition)
		{
			if (!condition)
			{
				Halt();
			}
		}

		template<typename... Arguments>
		static void Assert(bool condition, Arguments&&... arguments)
		{
			if (!condition)
			{
				Print(std::forward<Arguments>(arguments)...);
				Halt();
			}
		}

		static void Print()
		{
			_DebugOut << std::endl;
		}

		template<typename T, typename... Arguments>
		static void Print(T&& value, Arguments&&... arguments)
		{
			_DebugOut << std::forward<T>(value);
			Print(std::forward<Arguments>(arguments)...);
		}

		//-- Variables --//
		//===============//

	private:

		//---- Types ----//
		//===============//

		template<typename Elem>
		class OutputStream : public std::basic_ostream<Elem>
		{
		public:
			//--- Methods ---//
			//===============//

			OutputStream() : std::basic_ostream<Elem>(&m_StreamBuf) {}

		private:
			//---- Types ----//
			//===============//

			class DebugBuffer : public std::basic_streambuf<Elem>
			{
			public:
				//---- Types ----//
				//===============//

				typedef typename std::basic_streambuf<Elem>::traits_type Traits;
				typedef typename std::basic_streambuf<Elem>::int_type int_type;

			protected:
				//--- Methods ---//
				//===============//

				int_type overflow(int_type c) override;
				int      sync() override;

			private:
				//-- Variables --//
				//===============//

				std::basic_string<Elem> m_Buffer;
			};

			//-- Variables --//
			//===============//

			DebugBuffer m_StreamBuf;
		};

		//-- Variables --//
		//===============//

		static OutputStream<char> _DebugOut;
	};

	//--- Methods ---//
	//===============//
}

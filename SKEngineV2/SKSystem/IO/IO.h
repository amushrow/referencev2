#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstdint>

namespace SK
{
	namespace IO
	{
		//---- Types ----//
		//===============//
		
		enum class Endian
		{
			Any,
			Little,
			Big
		};

		//--- Methods ---//
		//===============//

		inline Endian PlatformEndian()
		{
			uint16_t Number = 0x0001;
			uint8_t* Byte = reinterpret_cast<uint8_t*>(&Number);

			return Byte[0] ? Endian::Little : Endian::Big;
		}

		template<typename T>
		typename std::enable_if<std::is_integral<T>::value && sizeof(T) == 1, T>::type Swizzle(T value)
		{
			return value;
		}

		template<typename T>
		typename std::enable_if<std::is_integral<T>::value && sizeof(T) == 2, T>::type Swizzle(T value)
		{
			return ((value & 0xFF00) >> 8) | ((value & 0x00FF) << 8);
		}

		template<typename T>
		typename std::enable_if<std::is_integral<T>::value && sizeof(T) == 4, T>::type Swizzle(T value)
		{
			return ((value & 0xFF000000) >> 24) | ((value & 0x00FF0000) >> 8) | ((value & 0x0000FF00) << 8) | ((value & 0x000000FF) << 24);
		}

		template<typename T>
		typename std::enable_if<std::is_integral<T>::value && sizeof(T) == 8, T>::type Swizzle(T value)
		{
			return ((value & 0xFF00000000000000) >> 56) | ((value & 0x00FF000000000000) >> 40) | ((value & 0x0000FF0000000000) >> 24) | ((value & 0x000000FF00000000) >> 8) |
				((value & 0x00000000FF000000) << 8) | ((value & 0x0000000000FF0000) << 24) | ((value & 0x000000000000FF00) << 40) | ((value & 0x00000000000000FF) << 56);
		}
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

// Stream.h provides the implementation for Serializer<> which is declared in Serializer/Internal.h
// Each header cannot include the other without creating a circular dependancy so this header is
// provided to include both headers necessary to create custome Serialize functions

//--- Include ---//
//===============//
#include <SKSystem/IO/Serialize/Internal.h>
#include <SKSystem/IO/Stream.h>

#define SerializerInstantiate(type) \
	template bool SK::IO::Serialize<type>::operator()<SK::IO::SerializeMode::Read>(SK::IO::Serializer<SK::IO::SerializeMode::Read>&, type &); \
	template bool SK::IO::Serialize<type>::operator()<SK::IO::SerializeMode::Write>(SK::IO::Serializer<SK::IO::SerializeMode::Write>&, type&);

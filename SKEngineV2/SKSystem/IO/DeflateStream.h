#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/IO/Stream.h>
struct z_stream_s;

namespace SK
{
	namespace IO
	{
		//---- Types ----//
		//===============//

		class DeflateStream : public Stream
		{
		public:
			//---- Types ----//
			//===============//

			enum CompressionMode
			{
				Compress,
				Decompress
			};

			//--- Methods ---//
			//===============//

			DeflateStream(Stream* source, CompressionMode mode, bool owner);
			virtual ~DeflateStream();

			//--Stream Interface
			void     WriteData(const uint8_t* buffer, uint64_t offset, uint64_t length) override;
			uint64_t ReadData(uint8_t* buffer, uint64_t offset, uint64_t length) override;
			uint64_t CopyTo(Stream& rhs);
			uint64_t Length() override;
			void     Length(uint64_t pos) override;
			uint64_t Position() override;
			void     Position(uint64_t pos) override;
			void     Close() override;
			bool     IsOpen() override;

			bool     CanSeek() override  { return false; }
			bool     CanRead() override  { return m_Mode == Decompress; }
			bool     CanWrite() override { return m_Mode == Compress; }
			//--

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			uint8_t         m_Buffer[4096];
			z_stream_s*     m_zlibStream;

			Stream*         m_Source;
			bool            m_Owner;
			bool            m_Initialised;
			CompressionMode m_Mode;
		};
		
		//--- Methods ---//
		//===============//
	}
}
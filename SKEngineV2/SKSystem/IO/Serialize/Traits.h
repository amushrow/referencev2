#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/IO/Serialize/Internal.h>

namespace SK
{
	namespace IO
	{
		//---- Types ----//
		//===============//

		template<SerializeMode Mode, typename T>
		struct has_member_serialize
		{
		private:
			typedef std::true_type yes;
			typedef std::false_type no;

			template<typename U> static auto test(int) -> decltype(std::declval<U>().Serialize(std::declval<Serializer<Mode>>()) == 1, yes());

			template<typename> static no test(...);

		public:

			static const bool value = std::is_same<decltype(test<T>(0)), yes>::value;
		};

		//--- Methods ---//
		//===============//
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright © 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstdint>
#include <memory>
#include <SKSystem/Text/String.h>
#include <SKSystem/IO/IO.h>
#include <SKSystem/IO/Serialize/Internal.h>
#include <SKSystem/IO/Serialize/Traits.h>

namespace SK
{
	namespace IO
	{
		//---- Types ----//
		//===============//

		template<>
		class Serializer<SK::IO::SerializeMode::Write>
		{
		public:

			//--- Methods ---//
			//===============//

			Serializer(Stream& stream) : m_Stream(stream) {}

			template <typename... Arguments>
			bool operator()(Arguments&&... arguments);


		private:

			//-- Variables --//
			//===============//

			Stream& m_Stream;
		};


		template<>
		class Serializer<SK::IO::SerializeMode::Read>
		{
		public:

			//--- Methods ---//
			//===============//

			Serializer(Stream& stream) : m_Stream(stream) {}

			template <typename... Arguments>
			bool operator()(Arguments&&... arguments);

		private:

			//-- Variables --//
			//===============//

			Stream& m_Stream;
		};

		class Stream
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Stream() : m_Endianness(Endian::Any) {}
			virtual ~Stream() {}

			//--Basic stream functions
			virtual void     WriteData(const uint8_t* buffer, uint64_t offset, uint64_t length) = 0;
			virtual uint64_t ReadData(uint8_t* buffer, uint64_t offset, uint64_t length) = 0;
			virtual uint64_t CopyTo(Stream& rhs) = 0;
			virtual uint64_t Length() = 0;
			virtual void     Length(uint64_t length) = 0;
			virtual uint64_t Position() = 0;
			virtual void     Position(uint64_t pos) = 0;
			virtual void     Close() = 0;
			virtual bool     IsOpen() = 0;

			virtual bool     CanSeek() = 0;
			virtual bool     CanRead() = 0;
			virtual bool     CanWrite() = 0;
			//--

			void   Endianness(Endian endian) { m_Endianness = endian; }
			Endian Endianness()              { return m_Endianness; }

			// Basic multi-arg write
			template<typename T, typename... Arguments>
			typename std::enable_if<sizeof...(Arguments) != 0>::type Write(const T& value, const Arguments&... arguments)
			{
				Write(value);
				Write(arguments...);
			}

			// Write using user defined functions for type
			template<typename T>
			typename std::enable_if<!std::is_fundamental<T>::value && !std::is_enum<T>::value>::type Write(const T& value)
			{
				SerializeWrite(value);
			}

			// Write enum by casting to underlying type
			template<typename T>
			typename std::enable_if<std::is_enum<T>::value>::type Write(const T& value)
			{
				Write(*reinterpret_cast<const typename std::underlying_type<T>::type*>(&value));
			}

			// Write built in type
			template<typename T>
			typename std::enable_if<std::is_fundamental<T>::value>::type Write(const T& value)
			{
				T SwizzledValue = Swizzle(value);
				WriteData(reinterpret_cast<const uint8_t*>(&SwizzledValue), 0, sizeof(T));
			}

			// Basic multi-arg read
			template<typename T, typename... Arguments>
			bool Read(T& value, Arguments&... arguments)
			{
				if (Read(value))
				{
					return Read(arguments...);
				}
				return false;
			}

			template<typename T>
			T Read()
			{
				T object;
				if (!Read(object))
				{
					//SKTD: Throw
				}
				return object;
			}

			// Read using user defined functions for type
			template<typename T>
			typename std::enable_if<!std::is_fundamental<T>::value && !std::is_enum<T>::value, bool>::type Read(T& value)
			{
				return SerializeRead(value);
			}

			// Read enum by casting to underlying type
			template<typename T>
			typename std::enable_if<std::is_enum<T>::value, bool>::type Read(T& value)
			{
				return Read(*reinterpret_cast<typename std::underlying_type<T>::type*>(&value));
			}

			// Read built in type
			template<typename T>
			typename std::enable_if<std::is_fundamental<T>::value, bool>::type Read(T& value)
			{
				uint64_t BytesRead = ReadData(reinterpret_cast<uint8_t*>(&value), 0, sizeof(T));
				value = Swizzle(value);
				return BytesRead == sizeof(T);
			}

			void   Write(bool value);
			void   Write(float value);
			void   Write(double value);
			void   Write(const String& value);
			void   Write(const char* value);

			bool   Read(bool& value);
			bool   Read(float& value);
			bool   Read(double& value);
			bool   Read(String& value);

			//-- Variables --//
			//===============//

		private:

			//--- Methods ---//
			//===============//

			template<typename T>
			typename std::enable_if<has_member_serialize<SerializeMode::Write, T>::value>::type SerializeWrite(const T& value)
			{
				const_cast<T&>(value).Serialize(Serializer<SerializeMode::Write>(*this));
			}

			template<typename T>
			typename std::enable_if<!has_member_serialize<SerializeMode::Write, T>::value>::type SerializeWrite(const T& value)
			{
				Serialize<T> T_Serialize;
				Serializer<SerializeMode::Write> Write(*this);
				T_Serialize(Write, const_cast<T&>(value));
			}

			template<typename T>
			typename std::enable_if<has_member_serialize<SerializeMode::Read, T>::value, bool>::type SerializeRead(T& value)
			{
				return value.Serialize(Serializer<SerializeMode::Read>(*this));
			}

			template<typename T>
			typename std::enable_if<!has_member_serialize<SerializeMode::Read, T>::value, bool>::type SerializeRead(T& value)
			{
				Serialize<T> T_Serialize;
				Serializer<SerializeMode::Read> Read(*this);
				return T_Serialize(Read, value);
			}

			template<typename T>
			T Swizzle(const T& value)
			{
				if (m_Endianness != Endian::Any && m_Endianness != PlatformEndian())
				{
					return SK::IO::Swizzle(value);
				}
				return value;
			}

			//-- Variables --//
			//===============//

			Endian          m_Endianness;
		};

	}
}

//--- Methods ---//
//===============//

template <typename... Arguments>
bool SK::IO::Serializer<SK::IO::SerializeMode::Write>::operator()(Arguments&&... arguments)
{
	m_Stream.Write(std::forward<Arguments>(arguments)...);
	return true;
}

template <typename... Arguments>
bool SK::IO::Serializer<SK::IO::SerializeMode::Read>::operator()(Arguments&&... arguments)
{
	m_Stream.Read(std::forward<Arguments>(arguments)...);
	return true;
}
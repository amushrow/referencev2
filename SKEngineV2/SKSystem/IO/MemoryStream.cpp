//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <algorithm>
#include <limits>
#include <SKSystem/Utils.h>
#include <SKSystem/Debug.h>
#include "MemoryStream.h"

using namespace SK::IO;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

MemoryStream::MemoryStream()
: m_Data(new uint8_t[1024])
, m_Length(0)
, m_Pos(0)
, m_BufferSize(1024)
, m_Owner(true)
, m_ReadOnly(false)
{

}

MemoryStream::MemoryStream(uint8_t* data, uint64_t length, bool readOnly, bool owner)
: m_Data(data)
, m_Length(length)
, m_Pos(0)
, m_BufferSize(static_cast<std::size_t>(length))
, m_Owner(owner)
, m_ReadOnly(readOnly)
{
	if (length > std::numeric_limits<std::size_t>::max())
	{
		throw NotEnoughMemoryException("SK::IO::MemoryStream - Data is too large to fit in memory");
	}
}

MemoryStream::MemoryStream(const uint8_t* data, uint64_t length)
: m_Data(const_cast<uint8_t*>(data))
, m_Length(length)
, m_Pos(0)
, m_BufferSize(static_cast<std::size_t>(length))
, m_Owner(false)
, m_ReadOnly(true)
{
	if (length > std::numeric_limits<std::size_t>::max())
	{
		throw NotEnoughMemoryException("SK::IO::MemoryStream - Data is too large to fit in memory");
	}
}

MemoryStream::~MemoryStream()
{
	Close();
}

void MemoryStream::Close()
{
	if (m_Owner)
	{
		delete[] m_Data;
		m_Data = nullptr;
		m_Length = 0;
		m_Pos = 0;
	}
}

bool MemoryStream::IsOpen()
{
	return m_Data != nullptr;
}

void MemoryStream::WriteData(const uint8_t* buffer, uint64_t offset, uint64_t length)
{
	Debug::Assert(!m_ReadOnly, "SK::IO::MemoryStream::WriteData - This stream is not open for writing");
	Debug::Assert(IsOpen(), "SK::IO::MemoryStream::WriteData - The stream is not open");
	
	if (m_Pos + length > m_BufferSize)
	{
		if (m_Owner)
		{
			// Allocate more memory
			do
			{
				if (!MultiplicationIsSafe<std::size_t>(m_BufferSize, 2))
				{
					throw NotEnoughMemoryException("SK::IO::MemoryStream::WriteData - Data is too large to fit in memory");
				}

				m_BufferSize *= 2;
			} while (m_Pos + length > m_BufferSize);
			
			uint8_t* NewData = new uint8_t[m_BufferSize];
			memcpy(NewData, m_Data, static_cast<std::size_t>(m_Length));
			delete[] m_Data;
			m_Data = NewData;
		}
		else
		{
			// Trim data
			length = std::min(m_BufferSize - m_Pos, length);
		}
	}
	memcpy(m_Data + m_Pos, buffer + offset, static_cast<std::size_t>(length));
	m_Pos += length;
	m_Length = std::max(m_Pos, m_Length);
}

uint64_t MemoryStream::ReadData(uint8_t* buffer, uint64_t offset, uint64_t length)
{
	Debug::Assert(IsOpen(), "SK::IO::MemoryStream::ReadData - The stream is not open");

	if (m_Pos + length > m_Length)
	{
		length = int64_t(m_Length - m_Pos);
	}

	memcpy(buffer + offset, m_Data + m_Pos, static_cast<std::size_t>(length));
	m_Pos += length;
	return length;
}

uint64_t MemoryStream::CopyTo(Stream& rhs)
{
	Debug::Assert(this != &rhs, "SK::IO::MemoryStream::CopyTo - Cannot copy a stream to itself");
	Debug::Assert(IsOpen(), "SK::IO::MemoryStream::CopyTo - The stream is not open");
	
	rhs.WriteData(m_Data, 0, m_Length);
	return m_Length;
}

uint64_t MemoryStream::Length()
{
	return m_Length;
}

void MemoryStream::Length(uint64_t length)
{
	Debug::Assert(IsOpen(), "SK::IO::MemoryStream::WriteData - The stream is not open");
	Debug::Assert(length <= UINT32_MAX, "SK::IO::MemoryStream - Invalid argument. Length is too large");

	if (length > m_BufferSize)
	{
		if (length > std::numeric_limits<std::size_t>::max())
		{
			throw NotEnoughMemoryException("SK::IO::MemoryStream::Length - Data is too large to fit in memory");
		}

		uint8_t* NewData = new uint8_t[static_cast<std::size_t>(length)];
		memcpy(NewData, m_Data, m_BufferSize);
		delete[] m_Data;
		m_Data = NewData;
		m_BufferSize = static_cast<std::size_t>(length);
	}
	m_Length = length;
}

uint64_t MemoryStream::Position()
{
	return m_Pos;
}

void MemoryStream::Position(uint64_t pos)
{
	Debug::Assert(IsOpen(), "SK::IO::MemoryStream::WriteData - The stream is not open");

	if (pos > m_Length)
	{
		Length(pos);
	}
	m_Pos = pos;
}

//-- Private Methods --//
//=====================//
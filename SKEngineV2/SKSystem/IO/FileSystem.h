#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstdint>
#include <vector>
#include <memory>
#include <SKSystem/IO/Path.h>
#include <SKSystem/IO/FileStream.h>

namespace SK
{
	namespace IO
	{
		//---- Types ----//
		//===============//

		class File;
		class Directory;
		typedef std::vector<std::shared_ptr<File>> FileList;
		typedef std::vector<std::shared_ptr<Directory>> DirectoryList;
		typedef std::shared_ptr<Directory> DirectoryPtr;

		class File
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			File(const Path& path);

			FileStream         Open();
			void               Move(const Path& destination, bool overwrite)       { Move(m_Path, destination, overwrite); }
			void               Copy(const Path& destination, bool overwrite) const { Copy(m_Path, destination, overwrite); }
			void               Rename(const String& newName);
			void               Delete()                                            { Delete(m_Path); }

			String             Name() const                                        { return m_Filename; }
			String             FullName() const                                    { return m_Path; }
			String             Extension() const                                   { return m_Ext; }
			String             DirectoryPath() const                               { return m_Directory; }

			uint64_t           Size() const;
			const DirectoryPtr Directory() const;

			static bool        Exists(const Path& path);
			static void        Copy(const Path& source, const Path& destination, bool overwrite);
			static void        Move(const Path& source, const Path& destination, bool overwrite);
			static void        Delete(const Path& path);

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			String m_Path;
			String m_Directory;
			String m_Filename;
			String m_Ext;
		};

		class Directory
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Directory(const Path& path);

			void          Move(const Path& destination, bool overwrite)       { Move(m_Path, destination, overwrite); }
			void          Copy(const Path& destination, bool overwrite) const { Copy(m_Path, destination, overwrite); }
			void          Rename(const String& newName);
			void          Delete()                                            { Delete(m_Path); }
			String        Name() const                                        { return m_Name; }
			String        FullName() const                                    { return m_Path; }
			DirectoryList GetDirectories();
			FileList      GetFiles();

			static bool   Exists(const Path& path);
			static void   Create(const Path& path);
			static void   Copy(const Path& source, const Path& destination, bool overwrite);
			static void   Move(const Path& source, const Path& destination, bool overwrite);
			static void   Delete(const Path& path);

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			String m_Path;
			String m_Directory;
			String m_Name;
		};

		//--- Methods ---//
		//===============//
	}
}
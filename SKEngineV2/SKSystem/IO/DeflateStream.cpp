//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/SDK/zlib/zlib.h>
#include <SKSystem/Utils.h>
#include <SKSystem/Debug.h>
#include <SKSystem/IO/DeflateStream.h>

using namespace SK::IO;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

DeflateStream::DeflateStream(Stream* source, CompressionMode mode, bool owner)
: m_Source(source)
, m_Owner(owner)
, m_Initialised(false)
, m_Mode(mode)
{
	Debug::Assert(m_Source != nullptr, "SK::IO::DeflateStream - Invalid Argument, source is null");

	m_zlibStream = new z_stream_s();
	m_zlibStream->zalloc = Z_NULL;
	m_zlibStream->zfree = Z_NULL;
	m_zlibStream->opaque = Z_NULL;

	m_zlibStream->total_in = m_zlibStream->avail_in = 0;
	m_zlibStream->total_out = m_zlibStream->avail_out = 0;
	m_zlibStream->next_in = nullptr;
	m_zlibStream->next_out = nullptr;

	if (m_Mode == Decompress)
	{
		int err = inflateInit(m_zlibStream);
		if (err == Z_OK)
			m_Initialised = true;
	}
	else if (m_Mode == Compress)
	{
		int err = deflateInit(m_zlibStream, Z_DEFAULT_COMPRESSION);
		if (err == Z_OK)
			m_Initialised = true;
	}
}

DeflateStream::~DeflateStream()
{
	Close();
	delete m_zlibStream;

	if (m_Owner)
	{
		m_Source->Close();
		delete m_Source;
	}
}

uint64_t DeflateStream::ReadData(uint8_t* buffer, uint64_t offset, uint64_t count)
{
	Debug::Assert(IsOpen(), "SK::IO::DeflateStream::ReadData - The stream is not open");
	Debug::Assert(m_Mode == Decompress, "SK::IO::DeflateStream::ReadData - This stream cannot be read");

	int ret = Z_OK;
	uint64_t bytesRead = 0;
	uint64_t bytesRemain = count;
	uint64_t curOffset = offset;

	while (true)
	{
		while (m_zlibStream->avail_in > 0)
		{
			m_zlibStream->avail_out = static_cast<uInt>(bytesRemain);
			m_zlibStream->next_out = buffer + curOffset;

			ret = inflate(m_zlibStream, Z_NO_FLUSH);
			bytesRead = bytesRemain - m_zlibStream->avail_out;
			curOffset += bytesRead;
			bytesRemain -= bytesRead;

			//On error, or the end of the stream, escape
			switch (ret)
			{
			case Z_NEED_DICT:
			case Z_DATA_ERROR:
			case Z_MEM_ERROR:
			case Z_STREAM_END:
				inflateEnd(m_zlibStream);
				return count - bytesRemain;
			}

			//If we've filled our output buffer, escape
			if (bytesRemain == 0)
				return count;
		}

		m_zlibStream->avail_in = static_cast<uInt>(m_Source->ReadData(m_Buffer, 0, 4096));
		if (m_zlibStream->avail_in == 0)
			break;
		m_zlibStream->next_in = m_Buffer;
	}

	return count - bytesRemain;
}

void DeflateStream::WriteData(const uint8_t* buffer, uint64_t offset, uint64_t count)
{
	Debug::Assert(IsOpen(), "SK::IO::DeflateStream::WriteData - The stream is not open");
	Debug::Assert(m_Mode == Compress, "SK::IO::DeflateStream::WriteData - This stream is read only");

	uint64_t bytesOut = 0;

	//--Write data
	m_zlibStream->avail_in = static_cast<uInt>(count);
	m_zlibStream->next_in = const_cast<Bytef*>(buffer + offset);

	while (m_zlibStream->avail_in > 0)
	{
		m_zlibStream->avail_out = 4096;
		m_zlibStream->next_out = m_Buffer;
		deflate(m_zlibStream, Z_NO_FLUSH);

		bytesOut = 4096 - m_zlibStream->avail_out;
		m_Source->WriteData(m_Buffer, 0, bytesOut);
	}
	//--
}

uint64_t DeflateStream::CopyTo(Stream& rhs)
{
	Debug::Assert(IsOpen(), "SK::IO::DeflateStream::CopyTo - The stream is not open");
	Debug::Assert(m_Mode == Decompress, "SK::IO::DeflateStream::CopyTo - This stream cannot be read");

	uint8_t Buffer[4096];

	uint64_t BytesWritten = 0;
	uint64_t ChunkSize = 0;
	do
	{
		ChunkSize = ReadData(Buffer, 0, 4096);
		rhs.WriteData(Buffer, 0, ChunkSize);
		BytesWritten += ChunkSize;
	} while (ChunkSize > 0);

	return BytesWritten;
}

uint64_t DeflateStream::Length()
{
	return 0;
}

void DeflateStream::Length(uint64_t length)
{
	Debug::Assert(false, "SK::IO::DeflateStream::Length - Length cannot be explicitly set");
}

uint64_t DeflateStream::Position()
{
	return 0;
}

void DeflateStream::Position(uint64_t pos)
{
	Debug::Assert(false, "SK::IO::DeflateStream::Position - Position cannot be explicitly set");
}

bool DeflateStream::IsOpen()
{
	return m_Initialised && m_Source->IsOpen();
}

void DeflateStream::Close()
{
	if (m_Mode == CompressionMode::Compress && m_Initialised)
	{
		int err = 0;
		do
		{
			m_zlibStream->avail_out = 4096;
			m_zlibStream->next_out = m_Buffer;

			err = deflate(m_zlibStream, Z_FINISH);

			int bytesOut = 4096 - m_zlibStream->avail_out;
			m_Source->WriteData(m_Buffer, 0, bytesOut);
		} while (err != Z_STREAM_END);

		deflateEnd(m_zlibStream);
	}

	m_Initialised = false;
}

//-- Private Methods --//
//=====================//
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Flags.h>
#include <SKSystem/IO/Path.h>
#include <SKSystem/IO/Stream.h>

namespace SK
{
	namespace IO
	{
		//---- Types ----//
		//===============//

		enum class FileMode
		{
			Read = 0x01, // Opens file for reading
			Write = 0x02, // Opens file for writing at beggining
			Append = 0x06, // Opens file for writing at end
			Truncate = 0x0A  // Opens file for writing, discards contents
		};
		typedef Flags<FileMode> FileModeFlags;

		class FileStream : public Stream
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			FileStream(const Path& fileName, FileModeFlags mode);
			virtual ~FileStream();

			//--Stream Interface
			void     WriteData(const uint8_t* buffer, uint64_t offset, uint64_t length) override;
			uint64_t ReadData(uint8_t* buffer, uint64_t offset, uint64_t length) override;
			uint64_t CopyTo(Stream& rhs);
			uint64_t Length() override;
			void     Length(uint64_t pos) override;
			uint64_t Position() override;
			void     Position(uint64_t pos) override;
			void     Close() override;
			bool     IsOpen() override;

			bool     CanSeek() override  { return true; }
			bool     CanRead() override;
			bool     CanWrite() override;
			//--

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//
			
			struct FileStreamData;
			std::shared_ptr<FileStreamData> m_Data;
		};

		//--- Methods ---//
		//===============//
	}
}
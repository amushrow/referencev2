//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <SKSystem/Utils.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/IO/FileSystem.h>

using namespace SK;
using namespace SK::IO;
using namespace SK::Encoding;


//-- Declarations --//
//==================//

namespace
{
	bool PathIsRelative(const String& path);
	String GetSpecialFolder(SpecialFolder folder);
	void ResolvePath(String& path);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

namespace
{
	static char _FolderBuffer[32767];//If somebody has a path longer than 32K then they can sod off.
}

//-- Public Methods --//
//====================//

Path::Path(const char* absolute)
{
	if (PathIsRelative(absolute))
	{
		throw ArgumentException("The supplied path is not absolute");
	}

	m_RootFolder = SpecialFolder::None;
	m_SystemPath = absolute;
}

Path::Path(const String& absolute)
{
	if (PathIsRelative(absolute))
	{
		throw ArgumentException("The supplied path is not absolute");
	}

	m_RootFolder = SpecialFolder::None;
	m_SystemPath = absolute;
}

Path::Path(SpecialFolder root)
: m_RootFolder(root)
, m_SystemPath(GetSpecialFolder(root))
{

}

Path::Path(SpecialFolder root, const String& relative)
: m_RootFolder(root)
, m_SystemPath(GetSpecialFolder(root))
{
	if (root == SpecialFolder::None)
	{
		if (PathIsRelative(relative))
		{
			throw ArgumentException("The supplied path is not absolute");
		}
		m_SystemPath = relative;
	}
	else
	{
		if (!PathIsRelative(relative))
		{
			throw ArgumentException("The supplied path is not relative");
		}

		m_SystemPath.Concat({ "/", relative });
		ResolvePath(m_SystemPath);
	}
}

Path Path::Combine(const Path& root, const String& relative)
{
	String NewPath = String::Join({ root.m_SystemPath, "/", relative });
	ResolvePath(NewPath);
	return Path(NewPath);
}

Path Path::Combine(const Path& root, const std::vector<String>& relativeParts)
{
	String NewPath = String::Join({ root.m_SystemPath, "/" });
	NewPath.Concat(String::Join(relativeParts, "/"));
	ResolvePath(NewPath);
	return Path(NewPath);
}

bool Path::IsRelative(const String& path)
{
	return PathIsRelative(path);
}

//-- Private Methods --//
//=====================//

namespace
{
	bool PathIsRelative(const String& path)
	{
		if (path.StartsWith("/"))
		{
			return false;
		}
		else if (path.StartsWith("~"))
		{
			return false;
		}

		return true;
	}

	String GetSpecialFolder(SpecialFolder folder)
	{
		switch (folder)
		{
		case SpecialFolder::AppData: return "~/.SKEngineV2";
		case SpecialFolder::CommonAppData: return "/var/lib";
		case SpecialFolder::Desktop: return "~/Desktop";
		case SpecialFolder::Documents: return "~/";
		case SpecialFolder::Temporary: return "/tmp";
		case SpecialFolder::Install:
		{
			char ProcPath[32];
			sprintf(ProcPath, "/proc/%d/exe", getpid());
			int NbBytes = readlink(ProcPath, _FolderBuffer, 32767);
			_FolderBuffer[NbBytes] = 0x00;
			
			String FullPath(_FolderBuffer);
			return FullPath.Substring(0, FullPath.LastIndexOf("/"));
		}
		default: return "";
		}

		return "";
	}

	void ResolvePath(String& path)
	{
		path.Replace("\\", "/");

		auto PathParts = path.Split("/");
		auto It = PathParts.begin();
		while (It != PathParts.end())
		{
			if (It->Equals("."))
			{
				It = PathParts.erase(It);
			}
			else if (It->Equals(".."))
			{
				if (It == PathParts.begin())
				{
					throw new ArgumentException("Invalid path");
				}
				It = PathParts.erase(--It);
				It = PathParts.erase(It);
			}
			else
			{
				++It;
			}
		}
		
		PathParts.insert(PathParts.begin(), "/");
		path = std::move(String::Join(PathParts, "/"));
	}
}

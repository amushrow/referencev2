//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <sstream>
#include <iomanip>
#include <libgen.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <SKSystem/IO/FileStream.h>
#include <SKSystem/Utils.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/IO/FileSystem.h>

using namespace SK;
using namespace SK::IO;
using namespace SK::Encoding;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

File::File(const Path& file)
: m_Path(file)
{
	if (!Exists(file))
	{
		throw FileNotFoundException(String::Join({ "'", file, "' could not be found." }));
	}

	String SystemPath = file.GetSystemPath();

	auto DirectoryCopy = UTF8::GetBytes(SystemPath, true);
	auto FilenameCopy = UTF8::GetBytes(SystemPath, true);
	m_Directory = dirname(reinterpret_cast<char*>(DirectoryCopy.Get()));
	m_Filename = basename(reinterpret_cast<char*>(FilenameCopy.Get()));
	m_Ext = SystemPath.Substring(SystemPath.LastIndexOf("."));
}

FileStream File::Open()
{
	return FileStream(m_Path, { FileMode::Read, FileMode::Write });
}

void File::Rename(const String& newName)
{
	Move(String::Join({ m_Directory, "\\", newName }), false);
}

uint64_t File::Size() const
{
	struct stat64 statbuf;
	if (stat64(m_Path, &statbuf) != -1)
	{
		return static_cast<uint64_t>(statbuf.st_size);
	}

	return 0;
}

const DirectoryPtr File::Directory() const
{
	return std::make_shared<SK::IO::Directory>(m_Directory);
}

bool File::Exists(const Path& path)
{
	struct stat64 statbuf;
	if (stat64(path.GetSystemPath(), &statbuf) != -1)
	{
		if (!S_ISDIR(statbuf.st_mode))
		{
			// This is a file
			return true;
		}
	}

	return false;
}

void File::Copy(const Path& source, const Path& destination, bool overwrite)
{
	if (!overwrite && Exists((destination)))
	{
		throw IOException("Unable to copy file. Destination file already exists");
	}

	int SourceFile = open64(source.GetSystemPath(), O_RDONLY, 0);
	int DestinationFile = open64(destination.GetSystemPath(), O_WRONLY | O_CREAT, 0644);

	struct stat64 SourceStat;
	fstat64(SourceFile, &SourceStat);
	std::size_t BytesCopied = sendfile64(DestinationFile, SourceFile, 0, SourceStat.st_size);

	close(SourceFile);
	close(DestinationFile);

	if (BytesCopied == -1)
	{
		std::stringstream ErrorString;
		ErrorString << "Unable to copy file. Error: 0x" << std::hex << std::setfill('0') << std::setw(8) << errno;
		throw IOException(ErrorString.str());
	}
}

void File::Move(const Path& source, const Path& destination, bool overwrite)
{
	Copy(source, destination, overwrite);
	Delete(source);
}

void File::Delete(const Path& path)
{
	if (unlink(path.GetSystemPath()))
	{
		std::stringstream ErrorString;
		ErrorString << "Unable to delete file. Error: 0x" << std::hex << std::setfill('0') << std::setw(8) << errno;
		throw IOException(ErrorString.str());
	}
}

//-- Private Methods --//
//=====================//
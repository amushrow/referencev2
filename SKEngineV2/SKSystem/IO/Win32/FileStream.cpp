//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Win32/Win32.h>
#include <algorithm>
#include <SKSystem/Utils.h>
#include <SKSystem/Debug.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/IO/FileStream.h>

using namespace SK;
using namespace SK::IO;
using namespace SK::Encoding;

//-- Declarations --//
//==================//

struct SK::IO::FileStream::FileStreamData
{
	HANDLE        File;
	FileModeFlags Mode;
};

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

FileStream::FileStream(const Path& fileName, FileModeFlags mode)
: m_Data(new FileStreamData())
{
	Debug::Assert(!mode.HasFlags(FileMode::Append, FileMode::Truncate), "SK::IO::FileStream - Cannot Append and Truncate at the same time");

	DWORD DesiredAccess = 0;
	DWORD Creation = 0;
	if (mode.HasFlags(FileMode::Append)) Creation = OPEN_ALWAYS;
	if (mode.HasFlags(FileMode::Truncate)) Creation = CREATE_ALWAYS;

	if (mode.HasFlags(FileMode::Write))
	{
		DesiredAccess |= GENERIC_WRITE;
		if (Creation == 0)
		{
			Creation = OPEN_ALWAYS;
		}
	}
	if (mode.HasFlags(FileMode::Read))
	{
		DesiredAccess |= GENERIC_READ;
		if (Creation == 0)
		{
			Creation = OPEN_EXISTING;
		}
	}

	m_Data->File = CreateFile(Win32::GetWideString(fileName), DesiredAccess, 0, nullptr, Creation, FILE_ATTRIBUTE_NORMAL, nullptr);
	m_Data->Mode = mode;

	if (mode.HasFlags(FileMode::Append))
	{
		Position(Length());
	}
}

FileStream::~FileStream()
{
	Close();
}

void FileStream::Close()
{
	CloseHandle(m_Data->File);
	m_Data->File = INVALID_HANDLE_VALUE;
}

bool FileStream::IsOpen()
{
	return m_Data->File != INVALID_HANDLE_VALUE;
}

void FileStream::WriteData(const uint8_t* buffer, uint64_t offset, uint64_t length)
{
	Debug::Assert(IsOpen(), "SK::IO::FileStream::WriteData - The stream is not open");
	Debug::Assert(CanWrite(), "SK::IO::FileStream::WriteData - The stream is read only");

	DWORD BytesWritten = 0;
	while (length > std::numeric_limits<DWORD>::max())
	{
		WriteFile(m_Data->File, buffer + offset, std::numeric_limits<DWORD>::max(), &BytesWritten, nullptr);
		length -= std::numeric_limits<DWORD>::max();
		offset += std::numeric_limits<DWORD>::max();
	}
	WriteFile(m_Data->File, buffer + offset, static_cast<DWORD>(length), &BytesWritten, nullptr);
}

uint64_t FileStream::ReadData(uint8_t* buffer, uint64_t offset, uint64_t length)
{
	Debug::Assert(IsOpen(), "SK::IO::FileStream::ReadData - The stream is not open");
	Debug::Assert(CanRead(), "SK::IO::FileStream::WriteData - The stream is write only");
	
	DWORD BytesRead = 0;
	uint64_t TotalBytesRead = 0;
	while (length > std::numeric_limits<DWORD>::max())
	{
		ReadFile(m_Data->File, buffer + offset, std::numeric_limits<DWORD>::max(), &BytesRead, nullptr);
		length -= BytesRead;
		offset += BytesRead;
		TotalBytesRead += BytesRead;
	}
	ReadFile(m_Data->File, buffer + offset, static_cast<DWORD>(length), &BytesRead, nullptr);
	TotalBytesRead += BytesRead;

	return TotalBytesRead;
}

uint64_t FileStream::CopyTo(Stream& rhs)
{
	Debug::Assert(this != &rhs, "SK::IO::FileStream::CopyTo - Cannot copy a stream to itself");
	Debug::Assert(IsOpen(), "SK::IO::FileStream::CopyTo - The stream is not open");
	Debug::Assert(CanRead(), "SK::IO::FileStream::CopyTo - The stream is write only");

	uint8_t Buffer[4096];
	uint64_t OriginalPosition = Position();
	Position(0);

	uint64_t BytesWritten = 0;
	while (BytesWritten < Length())
	{
		uint64_t ChunkSize = ReadData(Buffer, 0, 4096);
		rhs.WriteData(Buffer, 0, ChunkSize);
		BytesWritten += ChunkSize;
	}

	Position(OriginalPosition);
	return BytesWritten;
}

uint64_t FileStream::Length()
{
	LARGE_INTEGER Length;
	GetFileSizeEx(m_Data->File, &Length);

	return static_cast<uint64_t>(Length.QuadPart);
}

void FileStream::Length(uint64_t length)
{
	Debug::Assert(CanWrite(), "SK::IO::FileStream::WriteData - The stream is read only");

	uint64_t OriginalPosition = Position();
	Position(length);
	SetEndOfFile(m_Data->File);

	if (length > OriginalPosition)
	{
		Position(OriginalPosition);
	}
}

uint64_t FileStream::Position()
{
	LARGE_INTEGER Zero = { 0 };
	LARGE_INTEGER Position;
	SetFilePointerEx(m_Data->File, Zero, &Position, FILE_CURRENT);

	return static_cast<uint64_t>(Position.QuadPart);
}

void FileStream::Position(uint64_t position)
{
	LARGE_INTEGER Position;
	Position.QuadPart = static_cast<LONGLONG>(position);
	SetFilePointerEx(m_Data->File, Position, nullptr, FILE_BEGIN);
}

bool FileStream::CanRead()
{
	return m_Data->Mode.HasFlags(FileMode::Read);
}

bool FileStream::CanWrite()
{
	return m_Data->Mode.HasFlags(FileMode::Write);
}

//-- Private Methods --//
//=====================//
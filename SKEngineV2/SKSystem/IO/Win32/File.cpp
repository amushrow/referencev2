//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <sstream>
#include <iomanip>
#include <sys/types.h>
#include <sys/stat.h>
#include <SKSystem/Win32/Win32.h>
#include <SKSystem/IO/FileStream.h>
#include <SKSystem/Utils.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/IO/FileSystem.h>

using namespace SK;
using namespace SK::IO;
using namespace SK::Encoding;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

File::File(const Path& file)
: m_Path(file.GetSystemPath())
{
	const String& SystemPath = file.GetSystemPath();
	if (!Exists(file))
	{
		throw FileNotFoundException(String::Join({ "'", SystemPath, "' could not be found." }));
	}
	
	std::size_t MaxPathLength = SystemPath.Length();
	wchar_t* Drive = new wchar_t[MaxPathLength];
	wchar_t* Directory = new wchar_t[MaxPathLength];
	wchar_t* Filename = new wchar_t[MaxPathLength];
	wchar_t* Extension = new wchar_t[MaxPathLength];
	_wsplitpath_s(Win32::GetWideString(SystemPath), Drive, MaxPathLength, Directory, MaxPathLength, Filename, MaxPathLength, Extension, MaxPathLength);

	wcscat_s(Drive, MaxPathLength, Directory);
	wcscat_s(Filename, MaxPathLength, Extension);
	m_Directory = UTF16::FromBytes(reinterpret_cast<uint8_t*>(Drive));
	m_Filename = UTF16::FromBytes(reinterpret_cast<uint8_t*>(Filename));
	m_Ext = UTF16::FromBytes(reinterpret_cast<uint8_t*>(Extension));

	delete[] Drive;
	delete[] Directory;
	delete[] Filename;
	delete[] Extension;
}

FileStream File::Open()
{
	return FileStream(m_Path, { FileMode::Read, FileMode::Write });
}

void File::Rename(const String& newName)
{
	Move(String::Join({ m_Directory, "\\", newName }), false);
}

uint64_t File::Size() const
{
	struct _stat64 FileStats = { 0 };
	_wstat64(Win32::GetWideString(m_Path), &FileStats);
	return static_cast<uint64_t>(FileStats.st_size);
}

const DirectoryPtr File::Directory() const
{
	return std::make_shared<SK::IO::Directory>(m_Directory);
}

bool File::Exists(const Path& path)
{
	DWORD Attr = GetFileAttributes(Win32::GetWideString(path));
	if (Attr == INVALID_FILE_ATTRIBUTES && GetLastError() == ERROR_FILE_NOT_FOUND)
	{
		// File not found
		return false;
	}
	else if (Attr != INVALID_FILE_ATTRIBUTES && CheckFlags(Attr, FILE_ATTRIBUTE_DIRECTORY))
	{
		// Path is a directory
		return false;
	}

	return true;
}

void File::Copy(const Path& source, const Path& destination, bool overwrite)
{
	if (!CopyFile(Win32::GetWideString(source), Win32::GetWideString(destination), !overwrite))
	{
		std::stringstream ErrorString;
		ErrorString << "Unable to copy file. Error: 0x" << std::hex << std::setfill('0') << std::setw(8) << GetLastError();
		throw IOException(ErrorString.str());
	}
}

void File::Move(const Path& source, const Path& destination, bool overwrite)
{
	DWORD MoveFlags = MOVEFILE_COPY_ALLOWED;
	if (overwrite)
	{
		MoveFlags |= MOVEFILE_REPLACE_EXISTING;
	}
	if (!MoveFileEx(Win32::GetWideString(source), Win32::GetWideString(destination), MoveFlags))
	{
		std::stringstream ErrorString;
		ErrorString << "Unable to move file. Error: 0x" << std::hex << std::setfill('0') << std::setw(8) << GetLastError();
		throw IOException(ErrorString.str());
	}
}

void File::Delete(const Path& path)
{
	if (!DeleteFile(Win32::GetWideString(path)))
	{
		DWORD Error = GetLastError();
		if (Error != ERROR_FILE_NOT_FOUND)
		{
			std::stringstream ErrorString;
			ErrorString << "Unable to delete file. Error: 0x" << std::hex << std::setfill('0') << std::setw(8) << Error;
			throw IOException(ErrorString.str());
		}
	}
}

//-- Private Methods --//
//=====================//
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//

#include <SKSystem/Win32/Win32.h>
#include <Shlwapi.h>
#include <Shlobj.h>
#include <vector>
#include <SKSystem/Utils.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/IO/FileSystem.h>

using namespace SK;
using namespace SK::IO;
using namespace SK::Encoding;

//-- Declarations --//
//==================//

namespace
{
	String GetSpecialFolder(SpecialFolder folder);
	void ResolvePath(String& path);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

namespace
{
	static wchar_t _FolderPath[32767]; //GetModuleFileName has no way to report the length of the path, so we just need to make sure we have buffer big enough.
	//And if somebody has a path longer than 32K then they can sod off.
}

//-- Public Methods --//
//====================//

Path::Path(const char* absolute)
{
	if (PathIsRelative(Win32::GetWideString(absolute)))
	{
		throw ArgumentException("The supplied path is not absolute");
	}

	m_RootFolder = SpecialFolder::None;
	m_SystemPath = absolute;

}
Path::Path(const String& absolute)
{
	if (PathIsRelative(Win32::GetWideString(absolute)))
	{
		throw ArgumentException("The supplied path is not absolute");
	}

	m_RootFolder = SpecialFolder::None;
	m_SystemPath = absolute;
}

Path::Path(SpecialFolder root)
: m_RootFolder(root)
, m_SystemPath(GetSpecialFolder(root))
{

}

Path::Path(SpecialFolder root, const String& relative)
: m_RootFolder(root)
, m_SystemPath(GetSpecialFolder(root))
{
	if (root == SpecialFolder::None)
	{
		if (PathIsRelative(Win32::GetWideString(relative)))
		{
			throw ArgumentException("The supplied path is not absolute");
		}
		m_SystemPath = relative;
	}
	else
	{
		if (!PathIsRelative(Win32::GetWideString(relative)))
		{
			throw ArgumentException("The supplied path is not relative");
		}

		m_SystemPath.Concat({ "\\", relative });
		ResolvePath(m_SystemPath);
	}
}

Path Path::Combine(const Path& root, const String& relative)
{
	String NewPath = String::Join({ root.m_SystemPath, "\\", relative });
	ResolvePath(NewPath);
	return Path(NewPath);
}

Path Path::Combine(const Path& root, const std::vector<String>& relativeParts)
{
	String NewPath = String::Join({ root.m_SystemPath, "\\" });
	NewPath.Concat(String::Join(relativeParts, "\\"));
	ResolvePath(NewPath);
	return Path(NewPath);
}

bool Path::IsRelative(const String& path)
{
	return PathIsRelative(Win32::GetWideString(path));
}

//-- Private Methods --//
//=====================//

namespace
{
	String GetSpecialFolder(SpecialFolder folder)
	{
		switch (folder)
		{
		case SpecialFolder::AppData:
			SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, _FolderPath);
			break;

		case SpecialFolder::CommonAppData:
			SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, SHGFP_TYPE_CURRENT, _FolderPath);
			break;

		case SpecialFolder::Desktop:
			SHGetFolderPath(NULL, CSIDL_DESKTOPDIRECTORY, NULL, SHGFP_TYPE_CURRENT, _FolderPath);
			break;

		case SpecialFolder::Documents:
			SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, _FolderPath);
			break;

		case SpecialFolder::Temporary:
			GetTempPath(32767, _FolderPath);
			break;

		case SpecialFolder::Install:
			{
				GetModuleFileName(NULL, _FolderPath, 32767);
				File Exe(UTF16::FromBytes(reinterpret_cast<uint8_t*>(_FolderPath)));
				return Exe.DirectoryPath();
			}

		case SpecialFolder::None:
			return "";
		}

		return UTF16::FromBytes(reinterpret_cast<uint8_t*>(_FolderPath));
	}

	void ResolvePath(String& path)
	{
		path.Replace("/", "\\");

		auto PathParts = path.Split("\\");
		auto It = PathParts.begin();
		while (It != PathParts.end())
		{
			if (It->Equals(".") || It->IsEmpty())
			{
				It = PathParts.erase(It);
			}
			else if (It->Equals(".."))
			{
				if (It == PathParts.begin())
				{
					throw new ArgumentException("Invalid path");
				}
				It = PathParts.erase(--It);
				It = PathParts.erase(It);
			}
			else
			{
				++It;
			}
		}

		path = std::move(String::Join(PathParts, "\\"));
	}
}
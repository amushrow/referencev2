//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <algorithm>
#include <SKSystem/Debug.h>
#include "EmbeddedStream.h"

using namespace SK::IO;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

EmbeddedStream::EmbeddedStream(Stream* source, uint64_t length)
: m_Source(source)
, m_SourcePos(0)
, m_Length(length)
{
	Debug::Assert(m_Source != nullptr, "SK::IO::EmbeddedStream - Invalid Argument, content is null");
}

EmbeddedStream::EmbeddedStream(Stream* source, uint64_t start, uint64_t length)
: m_Source(source)
, m_SourcePos(start)
, m_Length(length)
{
	Debug::Assert(m_Source != nullptr, "SK::IO::EmbeddedStream - Invalid Argument, content is null");
}

EmbeddedStream::~EmbeddedStream()
{
	Close();
}

void EmbeddedStream::WriteData(const uint8_t* buffer, uint64_t offset, uint64_t length)
{
	Debug::Assert(CanWrite(), "SK::IO::EmbeddedStream::WriteData - Source stream does not support writing");
	Debug::Assert(length <= m_Length - m_Pos, "SK::IO::EmbeddedStream::WriteData - Attempting to write past the end of the stream");

	length = std::min(m_Length - m_Pos, length);
	m_Source->WriteData(buffer, offset, length);
}

uint64_t EmbeddedStream::ReadData(uint8_t* buffer, uint64_t offset, uint64_t length)
{
	Debug::Assert(CanRead(), "SK::IO::EmbeddedStream::WriteData - Source stream does not support writing");

	length = std::min(m_Length - m_Pos, length);
	return m_Source->ReadData(buffer, offset, length);
}

uint64_t EmbeddedStream::CopyTo(Stream& rhs)
{
	Debug::Assert(this != &rhs, "SK::IO::EmbeddedStream::CopyTo - Cannot copy a stream to itself");
	Debug::Assert(CanRead(), "SK::IO::EmbeddedStream::CopyTo - The stream is write only");

	uint8_t Buffer[4096];
	uint64_t OriginalPosition = Position();
	Position(0);

	uint64_t BytesWritten = 0;
	while (BytesWritten < Length())
	{
		uint64_t ChunkSize = ReadData(Buffer, 0, 4096);
		rhs.WriteData(Buffer, 0, ChunkSize);
		BytesWritten += ChunkSize;
	}

	Position(OriginalPosition);
	return BytesWritten;
}

uint64_t EmbeddedStream::Length()
{
	return m_Length;
}

void EmbeddedStream::Length(uint64_t length)
{
	Debug::Assert(false, "SK::IO::EmbeddedStream::Length - Length cannot be explicitly set");
}

uint64_t EmbeddedStream::Position()
{
	return m_Pos;
}

void EmbeddedStream::Position(uint64_t pos)
{
	Debug::Assert(CanSeek(), "SK::IO::EmbeddedStream::Length - Position cannot be explicitly set");
	Debug::Assert(pos <= m_Length, "SK::IO::EmbeddedStream::Length - Attempting to seek past the end of the stream");

	m_Pos = std::min(m_Length, pos);
	m_Source->Position(m_SourcePos + m_Pos);
}

bool EmbeddedStream::IsOpen()
{
	return m_Source != nullptr && m_Source->IsOpen();
}

//-- Private Methods --//
//=====================//
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <algorithm>
#include <cstring>
#include <cstdarg>
#include "Encoding.h"
#include "String.h"

using namespace SK;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

String::Iterator& String::Iterator::operator++()
{
	while (*m_Char != 0)
	{
		++m_Char;
		if ((*m_Char & 0xC0) != 0x80)
		{
			break;
		}
	}

	return *this;
}

String::Iterator& String::Iterator::operator--()
{
	while (m_Char > m_Buffer)
	{
		--m_Char;
		if ((*m_Char & 0xC0) != 0x80)
		{
			break;
		}
	}

	return *this;
}

CodePoint String::Iterator::operator*()
{
	std::size_t Pos = 0;
	return SK::Encoding::UTF8::GetNextCodePoint(reinterpret_cast<const uint8_t*>(m_Char), Pos);
}

String::String()
: m_Buffer(nullptr)
, m_BufferSize(0)
, m_DataSize(0)
, m_StringLength(0)
{

}

String::~String()
{
	delete [] m_Buffer;
}

String::String(const char* str)
: m_StringLength(0)
{
	std::size_t Size = 0;
	while (Encoding::UTF8::GetNextCodePoint(reinterpret_cast<const uint8_t*>(str), Size) > 0)
	{
		++m_StringLength;
	}

	m_Buffer = new char[Size + 1];
	memcpy(m_Buffer, str, Size);
	m_Buffer[Size] = 0x00;

	m_BufferSize = Size + 1;
	m_DataSize = Size;
}

String::String(const std::string& str)
: m_StringLength(0)
{
	std::size_t Size = str.length();
	m_Buffer = new char[Size + 1];
	memcpy(m_Buffer, str.c_str(), str.size());
	m_Buffer[Size] = 0x00;

	m_BufferSize = Size + 1;
	m_DataSize = Size;
	
	std::size_t BufferPosition = 0;
	while (Encoding::UTF8::GetNextCodePoint(reinterpret_cast<uint8_t*>(m_Buffer), BufferPosition, m_BufferSize) > 0)
	{
		++m_StringLength;
	}
}

String::String(const String& other)
: m_Buffer(new char[other.m_DataSize+1])
, m_BufferSize(other.m_DataSize+1)
, m_DataSize(other.m_DataSize)
, m_StringLength(other.m_StringLength)
{
	memcpy(m_Buffer, other.Buffer(), m_BufferSize);
}

String& String::operator = (const String& other)
{
	if (this != &other)
	{
		delete[] m_Buffer;
		m_StringLength = other.m_StringLength;
		m_BufferSize = other.m_DataSize + 1;
		m_DataSize = other.m_DataSize;
		m_Buffer = new char[m_BufferSize];
		memcpy(m_Buffer, other.Buffer(), m_BufferSize);
	}

	return *this;
}

String::String(String&& other)
: m_Buffer(other.m_Buffer)
, m_BufferSize(other.m_BufferSize)
, m_DataSize(other.m_DataSize)
, m_StringLength(other.m_StringLength)
{
	other.m_Buffer = nullptr;
	other.m_BufferSize = 0;
	other.m_DataSize = 0;
	other.m_StringLength = 0;
}

String& String::operator = (String&& other)
{
	if (this != &other)
	{
		delete[] m_Buffer;
		m_Buffer = other.m_Buffer;
		m_BufferSize = other.m_BufferSize;
		m_DataSize = other.m_DataSize;
		m_StringLength = other.m_StringLength;

		other.m_Buffer = nullptr;
		other.m_BufferSize = 0;
		other.m_DataSize = 0;
		other.m_StringLength = 0;
	}

	return *this;
}

void String::Concat(const String& other)
{
	std::size_t RequiredSize = m_DataSize + other.m_DataSize;

	if (RequiredSize >= m_BufferSize)
	{
		char* OldBuffer = m_Buffer;
		m_Buffer = new char[RequiredSize + 1];
		memcpy(m_Buffer, OldBuffer, m_DataSize);
		delete[] OldBuffer;

		m_BufferSize = RequiredSize + 1;
	}

	memcpy(m_Buffer + m_DataSize, other.Buffer(), other.m_DataSize);
	m_Buffer[RequiredSize] = 0x00;
	m_DataSize = RequiredSize;
	m_StringLength += other.m_StringLength;
}

void String::Concat(const std::initializer_list<String>& others)
{
	std::size_t RequiredSize = m_DataSize;
	for (const String& other : others)
	{
		RequiredSize += other.m_DataSize;
	}

	if (RequiredSize > m_BufferSize)
	{
		char* OldBuffer = m_Buffer;
		m_Buffer = new char[RequiredSize + 1];
		memcpy(m_Buffer, OldBuffer, m_DataSize);
		delete[] OldBuffer;

		m_BufferSize = RequiredSize + 1;
	}

	for (const String& other : others)
	{
		memcpy(m_Buffer + m_DataSize, other.Buffer(), other.m_DataSize);
		m_DataSize += other.m_DataSize;
		m_StringLength += other.m_StringLength;
	}
	m_Buffer[RequiredSize] = 0x00;
	m_DataSize = RequiredSize;
}

int String::CompareTo(const String& other) const
{
	return std::strcmp(m_Buffer, other.m_Buffer);
}

int String::CompareToCI(const String& other) const
{
	auto A = begin();
	auto B = other.begin();
	auto End = end();
	while (A != End && Encoding::CaseFolding::Fold(*A) == Encoding::CaseFolding::Fold(*B))
	{
		++A;
		++B;
	}
	
	return *A - *B;
}

bool String::Contains(const String& other) const
{
	return IndexOf(other) != -1;
}

bool String::StartsWith(const String& other) const
{
	if (other.m_DataSize > m_DataSize)
	{
		return false;
	}

	return std::memcmp(m_Buffer, other.m_Buffer, other.m_DataSize) == 0;
}

bool String::EndsWith(const String& other) const
{
	if (other.m_DataSize > m_DataSize)
	{
		return false;
	}

	std::size_t Start = m_DataSize - other.m_DataSize;
	return std::memcmp(m_Buffer + Start, other.m_Buffer, other.m_DataSize) == 0;
}

std::size_t String::IndexOf(const String& other, std::size_t startIndex) const
{
	if (other.Length() > m_StringLength - startIndex)
	{
		return -1;
	}

	std::size_t BufferIndex = 0;
	if (startIndex > 0)
	{
		BufferIndex = CharIndexToBuffer(startIndex);
	}

	const char* Pos = std::strstr(m_Buffer + BufferIndex, other.m_Buffer);

	if (Pos != nullptr)
	{
		std::size_t CharacterIndex = 0;

		const char* C = m_Buffer;
		while (C != Pos && *C != 0)
		{
			if ((*C & 0x80) == 0 || (*C & 0xC0) == 0xC0)
			{
				CharacterIndex++;
			}
			++C;
		}

		return CharacterIndex;
	}

	return -1;
}

std::size_t String::LastIndexOf(const String& other, std::size_t startIndex) const
{
	if (startIndex > m_StringLength)
	{
		startIndex = m_StringLength;
	}

	if (other.Length() > startIndex)
	{
		return -1;
	}

	const char* Pos = nullptr;
	for (const char* s = m_Buffer + startIndex - other.Length(); s >= m_Buffer; --s)
	{
		if (strncmp(s, other.m_Buffer, other.Length()) == 0)
		{
			Pos = s;
			break;
		}
	}

	if (Pos != nullptr)
	{
		std::size_t CharacterIndex = 0;
		const char* C = m_Buffer;
		while (C != Pos && *C != 0)
		{
			if ((*C & 0x80) == 0 || (*C & 0xC0) == 0xC0)
			{
				CharacterIndex++;
			}
			++C;
		}

		return CharacterIndex;
	}

	return -1;
}

String String::Substring(std::size_t index) const
{
	if (index > m_StringLength)
	{
		return String();
	}

	std::size_t StringStart = CharIndexToBuffer(index);
	return String(reinterpret_cast<uint8_t*>(m_Buffer + StringStart), m_DataSize - StringStart, m_StringLength - index);
}

String String::Substring(std::size_t index, std::size_t count) const
{
	if (index > m_StringLength)
	{
		return String();
	}

	count = std::min(count, m_StringLength - index);
	std::size_t StringStart = CharIndexToBuffer(index);
	std::size_t StringEnd = CharIndexToBuffer(index + count);

	return String(reinterpret_cast<uint8_t*>(m_Buffer + StringStart), StringEnd - StringStart, count);
}

std::vector<String> String::Split(const String& delimiter) const
{
	std::vector<String> Strings;
	
	std::size_t StartIndex = 0;
	std::size_t EndIndex = 0;
	while ((EndIndex = IndexOf(delimiter, StartIndex)) != -1)
	{
		Strings.push_back(Substring(StartIndex, EndIndex - StartIndex));
		StartIndex = EndIndex + delimiter.m_StringLength;
	}

	if (StartIndex > 0)
	{
		Strings.push_back(Substring(StartIndex));
	}

	return Strings;
}

String& String::Replace(const String& other, const String& replacement)
{
	if (other.m_DataSize != replacement.m_DataSize)
	{
		//The slow way
		auto Parts = Split(other);
		if (Parts.size() > 1)
		{
			*this = std::move(Join(Parts, replacement));
		}
	}
	else
	{
		//In place replacement
		const char* Pos = std::strstr(m_Buffer, other.m_Buffer);
		while (Pos != nullptr)
		{
			std::size_t Index = Pos - m_Buffer;
			memcpy(m_Buffer + Index, replacement.Buffer(), replacement.m_DataSize);
			Pos = std::strstr(m_Buffer + Index, other.m_Buffer);
		}
	}

	return *this;
}

String String::Join(const std::vector<String>& strings, const String& delimiter)
{
	String Result;

	std::size_t NbDelimiters = strings.size() - 1;
	Result.m_DataSize = NbDelimiters * delimiter.m_DataSize;
	Result.m_StringLength = NbDelimiters * delimiter.m_StringLength;
	for (const String& string : strings)
	{
		Result.m_DataSize += string.m_DataSize;
		Result.m_StringLength += string.m_StringLength;
	}

	Result.m_Buffer = new char[Result.m_DataSize + 1];
	Result.m_BufferSize = Result.m_DataSize + 1;

	std::size_t BufferPos = 0;
	for (const String& string : strings)
	{
		if (BufferPos > 0 && delimiter.m_StringLength > 0)
		{
			memcpy(Result.m_Buffer + BufferPos, delimiter.Buffer(), delimiter.m_DataSize);
			BufferPos += delimiter.m_DataSize;
		}

		memcpy(Result.m_Buffer + BufferPos, string.Buffer(), string.m_DataSize);
		BufferPos += string.m_DataSize;
	}
	Result.m_Buffer[BufferPos] = 0x00;

	return Result;
}

String String::Join(const std::vector<String>& strings)
{
	return String::Join(strings, String());
}

#pragma warning(push)
#pragma warning(disable: 4996)
String String::Format(const char* string, ...)
{
	va_list args;
	va_start(args, string);
	int StringLength = vsnprintf(nullptr, 0, string, args) + 1;
	char* Buffer = new char[StringLength];
	vsprintf(Buffer, string, args);
	va_end(args);

	String Result(Buffer);
	delete [] Buffer;
	return Result;
}
#pragma warning(pop)

//-- Private Methods --//
//=====================//

String::String(const uint8_t* data, std::size_t length, std::size_t stringLength)
: m_Buffer(new char[length + 1])
, m_BufferSize(length + 1)
, m_DataSize(length)
, m_StringLength(stringLength)
{
	memcpy(m_Buffer, data, length);
	m_Buffer[length] = 0x00;
}

std::size_t String::CharIndexToBuffer(std::size_t charIndex) const
{
	std::size_t CharIndex = 0;
	const char* C = m_Buffer;
	while (*C != 0 && CharIndex < charIndex)
	{
		++C;
		if ((*C & 0xC0) != 0x80)
		{
			CharIndex++;
		}
	}

	return C - m_Buffer;
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <xmmintrin.h>
#include <SKSystem/Memory.h>
#include "Types.h"

namespace SK
{
	namespace Math
	{
		//---- Types ----//
		//===============//

		class Matrix;
		class Quaternion;
		class Vector
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Vector() {}
			Vector(__m128 vec) : Data(vec) {}
			Vector(float val);
			Vector(float x, float y, float z, float w);
			Vector(const float4& vec);

			void*         operator new(std::size_t size)   { return SK::AlignedNew(16, size); }
			void*         operator new[](std::size_t size) { return SK::AlignedNew(16, size); }
			void          operator delete(void* mem)       { SK::AlignedDelete(mem); }
			void          operator delete[](void* mem)     { SK::AlignedDelete(mem); }

			void          Set(const float4& vec);
			void          Set(float val);
			void          Set(float x, float y, float z, float w);
			float4        Get() const;
			float         Length() const;
			float         LengthSquared() const;
			float         Normalise();
			const Vector& Cross(const Vector& rhs);

			bool          operator == (const Vector& rhs);
			bool          operator != (const Vector& rhs);
			const Vector& operator += (const Vector& rhs);
			const Vector& operator -= (const Vector& rhs);
			const Vector& operator *= (float f);
			const Vector& operator *= (const Matrix& matrix);
			const Vector& operator *= (const Quaternion& matrix);
			const Vector& operator /= (float f);
			const Vector  operator - () const;
			const Vector  operator + (const Vector& rhs) const;
			const Vector  operator - (const Vector& rhs) const;

			static Vector Cross(const Vector& v1, const Vector& v2);
			static float  Dot(const Vector& v1, const Vector& v2);
			static float  Angle(const Vector& v1, const Vector& v2);
			static Vector Abs(const Vector& rhs);
			static Vector Normalise(const Vector& v1);
			static Vector MakeOrthogonal(const Vector& normal, const Vector& tangent);

			static const Vector& Up();
			static const Vector& Down();
			static const Vector& Right();
			static const Vector& Left();
			static const Vector& Forward();
			static const Vector& Backward();
			static const Vector& Zero();

			//-- Variables --//
			//===============//

			__m128 Data;
		};

		//--- Methods ---//
		//===============//

		const Vector operator * (float f, const Vector& vec);
		const Vector operator * (const Vector& vec, float f);
		const Vector operator / (float f, const Vector& vec);
		const Vector operator / (const Vector& vec, float f);
		const Vector operator * (const Matrix& matrix, const Vector& vec);
		const Vector operator * (const Vector& vec, const Matrix& matrix);
		const Vector operator * (const Quaternion& matrix, const Vector& vec);
		const Vector operator * (const Vector& vec, const Quaternion& matrix);
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//

namespace SK
{
	namespace Math
	{
		//---- Types ----//
		//===============//

		class Euler
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Euler() : Pitch(0), Yaw(0), Roll(0) {}
			Euler(float pitch, float yaw, float roll) : Pitch(pitch), Yaw(yaw), Roll(roll) {}

			//-- Variables --//
			//===============//

			float Pitch;
			float Yaw;
			float Roll;
		};
	}

	//--- Methods ---//
	//===============//
}
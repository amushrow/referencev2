//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <cmath>
#include <smmintrin.h>
#include "Vector.h"
#include "Matrix.h"
#include "Quaternion.h"

using namespace SK::Math;

//-- Declarations --//
//==================//

namespace
{
	static const __m128 SIGNMASK = _mm_castsi128_ps(_mm_set1_epi32(0x80000000));
	static const __m128 XYZMASK = _mm_castsi128_ps(_mm_set_epi32(0x00000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF));
	static const __m128 TWO = _mm_set_ps1(2);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

const Vector& Vector::Up()
{
	static Vector _Up(0, 1, 0, 0);
	return _Up;
}

const Vector& Vector::Down()
{
	static Vector _Down(0, -1, 0, 0);
	return _Down;
}

const Vector& Vector::Right()
{
	static Vector _Right(1, 0, 0, 0);
	return _Right;
}

const Vector& Vector::Left()
{
	static Vector _Left(-1, 0, 0, 0);
	return _Left;
}

const Vector& Vector::Forward()
{
	static Vector _Fwd(0, 0, 1, 0);
	return _Fwd;
}

const Vector& Vector::Backward()
{
	static Vector _Back(0, 0, -1, 0);
	return _Back;
}

const Vector& Vector::Zero()
{
	static Vector _Zero(0, 0, 0, 0);
	return _Zero;
}

Vector::Vector(float val)
{
	Data = _mm_set_ps1(val);
}

Vector::Vector(float x, float y, float z, float w)
{
	Data = _mm_setr_ps(x, y, z, w);
}

Vector::Vector(const float4& vec)
{
	Data = _mm_loadu_ps(vec);
}

void Vector::Set(const float4& vec)
{
	Data = _mm_loadu_ps(vec);
}

void Vector::Set(float val)
{
	Data = _mm_set_ps1(val);
}

void Vector::Set(float x, float y, float z, float w)
{
	Data = _mm_setr_ps(x, y, z, w);
}

float4 Vector::Get() const
{
	float4 Result;
	_mm_storeu_ps(Result, Data);
	return Result;
}

float Vector::Length() const
{
	return _mm_cvtss_f32(_mm_sqrt_ss(_mm_dp_ps(Data, Data, 0x71)));
}

float Vector::LengthSquared() const
{
	return _mm_cvtss_f32(_mm_dp_ps(Data, Data, 0x71));
}

float Vector::Normalise()
{
	__m128 OriginalW = Data;
	__m128 Length = _mm_sqrt_ps(_mm_dp_ps(Data, Data, 0xFF));
	Data = _mm_div_ps(Data, Length);
	return _mm_cvtss_f32(Length);
}

const Vector& Vector::Cross(const Vector& rhs)
{
	Data = _mm_sub_ps(
		_mm_mul_ps(_mm_shuffle_ps(Data, Data, 0xC9), _mm_shuffle_ps(rhs.Data, rhs.Data, 0xD2)),
		_mm_mul_ps(_mm_shuffle_ps(Data, Data, 0xD2), _mm_shuffle_ps(rhs.Data, rhs.Data, 0xC9))
		);
	return *this;
}

bool Vector::operator == (const Vector& rhs)
{
	return _mm_movemask_ps(_mm_cmpneq_ps(Data, rhs.Data)) == 0;
}

bool Vector::operator != (const Vector& rhs)
{
	return _mm_movemask_ps(_mm_cmpneq_ps(Data, rhs.Data)) != 0;
}

const Vector& Vector::operator += (const Vector& rhs)
{
	Data = _mm_add_ps(Data, rhs.Data);
	return *this;
}

const Vector& Vector::operator -= (const Vector& rhs)
{
	Data = _mm_sub_ps(Data, rhs.Data);
	return *this;
}

const Vector& Vector::operator *= (float f)
{
	Data = _mm_mul_ps(Data, _mm_set_ps1(f));
	return *this;
}

const Vector& Vector::operator *= (const Matrix& matrix)
{
	__m128 X = _mm_shuffle_ps(Data, Data, 0x00);
	__m128 Y = _mm_shuffle_ps(Data, Data, 0x55);
	__m128 Z = _mm_shuffle_ps(Data, Data, 0xAA);
	__m128 W = _mm_shuffle_ps(Data, Data, 0xFF);

	Data = _mm_add_ps(
		_mm_add_ps(_mm_mul_ps(X, matrix.Row0), _mm_mul_ps(Y, matrix.Row1)),
		_mm_add_ps(_mm_mul_ps(Z, matrix.Row2), _mm_mul_ps(W, matrix.Row3))
		);

	return *this;
}

const Vector& Vector::operator *= (const Quaternion& quat)
{
	__m128 Q_XYZ = _mm_and_ps(quat.Data, XYZMASK);
	__m128 Cross = _mm_mul_ps(TWO, _mm_sub_ps(
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xC9), _mm_shuffle_ps(Data, Data, 0xD2)),
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xD2), _mm_shuffle_ps(Data, Data, 0xC9))
		));

	__m128 DoubleCross = _mm_sub_ps(
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xC9), _mm_shuffle_ps(Cross, Cross, 0xD2)),
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xD2), _mm_shuffle_ps(Cross, Cross, 0xC9))
		);

	Data = _mm_add_ps(DoubleCross, _mm_add_ps(Data, _mm_mul_ps(Cross, _mm_shuffle_ps(quat.Data, quat.Data, 0xFF))));

	return *this;
}

const Vector& Vector::operator /= (float f)
{
	Data = _mm_div_ps(Data, _mm_set_ps1(f));
	return *this;
}

const Vector Vector::operator - () const
{
	return Vector(_mm_xor_ps(Data, SIGNMASK));
}

const Vector Vector::operator + (const Vector& rhs) const
{
	return Vector(_mm_add_ps(Data, rhs.Data));
}

const Vector Vector::operator - (const Vector& rhs) const
{
	return Vector(_mm_sub_ps(Data, rhs.Data));
}

const Vector SK::Math::operator * (float f, const Vector& vec)
{
	return Vector(_mm_mul_ps(vec.Data, _mm_set_ps1(f)));
}

const Vector SK::Math::operator * (const Vector& vec, float f)
{
	return Vector(_mm_mul_ps(vec.Data, _mm_set_ps1(f)));
}

const Vector SK::Math::operator / (float f, const Vector& vec)
{
	return Vector(_mm_div_ps(vec.Data, _mm_set_ps1(f)));
}

const Vector SK::Math::operator / (const Vector& vec, float f)
{
	return Vector(_mm_div_ps(vec.Data, _mm_set_ps1(f)));
}

Vector Vector::Cross(const Vector& v1, const Vector& v2)
{
	return Vector(_mm_sub_ps(
		_mm_mul_ps(_mm_shuffle_ps(v1.Data, v1.Data, 0xC9), _mm_shuffle_ps(v2.Data, v2.Data, 0xD2)),
		_mm_mul_ps(_mm_shuffle_ps(v1.Data, v1.Data, 0xD2), _mm_shuffle_ps(v2.Data, v2.Data, 0xC9))
		));
}

float Vector::Dot(const Vector& v1, const Vector& v2)
{
	return _mm_cvtss_f32(_mm_dp_ps(v1.Data, v2.Data, 0xFF));
}

float Vector::Angle(const Vector& v1, const Vector& v2)
{
	return acos(_mm_cvtss_f32(
		_mm_mul_ss(_mm_dp_ps(v1.Data, v2.Data, 0x77), _mm_mul_ss(
		_mm_rsqrt_ss(_mm_dp_ps(v1.Data, v1.Data, 0x77)),
		_mm_rsqrt_ss(_mm_dp_ps(v2.Data, v2.Data, 0x77)))
		)));
}

Vector Vector::Abs(const Vector& vec)
{
	return Vector(_mm_andnot_ps(SIGNMASK, vec.Data));
}

Vector Vector::Normalise(const Vector& vec)
{
	Vector Result(vec);
	Result.Normalise();
	return Result;
}

Vector Vector::MakeOrthogonal(const Vector& normal, const Vector& tangent)
{
	float Dot = Vector::Dot(normal, tangent);
	Vector OrthoTangent = tangent - (normal * Dot);
	OrthoTangent.Normalise();
	return OrthoTangent;
}

const Vector SK::Math::operator * (const Matrix& matrix, const Vector& vec)
{
	Vector Result;
	__m128 X = _mm_shuffle_ps(vec.Data, vec.Data, 0x00);
	__m128 Y = _mm_shuffle_ps(vec.Data, vec.Data, 0x55);
	__m128 Z = _mm_shuffle_ps(vec.Data, vec.Data, 0xAA);
	__m128 W = _mm_shuffle_ps(vec.Data, vec.Data, 0xFF);

	Result.Data = _mm_add_ps(
		_mm_add_ps(_mm_mul_ps(X, matrix.Row0), _mm_mul_ps(Y, matrix.Row1)),
		_mm_add_ps(_mm_mul_ps(Z, matrix.Row2), _mm_mul_ps(W, matrix.Row3))
		);

	return Result;
}

const Vector SK::Math::operator * (const Vector& vec, const Matrix& matrix)
{
	Vector Result;
	__m128 X = _mm_shuffle_ps(vec.Data, vec.Data, 0x00);
	__m128 Y = _mm_shuffle_ps(vec.Data, vec.Data, 0x55);
	__m128 Z = _mm_shuffle_ps(vec.Data, vec.Data, 0xAA);
	__m128 W = _mm_shuffle_ps(vec.Data, vec.Data, 0xFF);

	Result.Data = _mm_add_ps(
		_mm_add_ps(_mm_mul_ps(X, matrix.Row0), _mm_mul_ps(Y, matrix.Row1)),
		_mm_add_ps(_mm_mul_ps(Z, matrix.Row2), _mm_mul_ps(W, matrix.Row3))
		);

	return Result;
}

const Vector SK::Math::operator * (const Quaternion& quat, const Vector& vec)
{
	Vector Result;
	__m128 Q_XYZ = _mm_and_ps(quat.Data, XYZMASK);
	__m128 Cross = _mm_mul_ps(TWO, _mm_sub_ps(
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xC9), _mm_shuffle_ps(vec.Data, vec.Data, 0xD2)),
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xD2), _mm_shuffle_ps(vec.Data, vec.Data, 0xC9))
		));

	__m128 DoubleCross = _mm_sub_ps(
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xC9), _mm_shuffle_ps(Cross, Cross, 0xD2)),
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xD2), _mm_shuffle_ps(Cross, Cross, 0xC9))
		);

	Result.Data = _mm_add_ps(DoubleCross, _mm_add_ps(vec.Data, _mm_mul_ps(Cross, _mm_shuffle_ps(quat.Data, quat.Data, 0xFF))));
	return Result;
}

const Vector SK::Math::operator * (const Vector& vec, const Quaternion& quat)
{
	Vector Result;
	__m128 Q_XYZ = _mm_and_ps(quat.Data, XYZMASK);
	__m128 Cross = _mm_mul_ps(TWO, _mm_sub_ps(
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xC9), _mm_shuffle_ps(vec.Data, vec.Data, 0xD2)),
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xD2), _mm_shuffle_ps(vec.Data, vec.Data, 0xC9))
		));

	__m128 DoubleCross = _mm_sub_ps(
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xC9), _mm_shuffle_ps(Cross, Cross, 0xD2)),
		_mm_mul_ps(_mm_shuffle_ps(Q_XYZ, Q_XYZ, 0xD2), _mm_shuffle_ps(Cross, Cross, 0xC9))
		);

	Result.Data = _mm_add_ps(DoubleCross, _mm_add_ps(vec.Data, _mm_mul_ps(Cross, _mm_shuffle_ps(quat.Data, quat.Data, 0xFF))));
	return Result;
}

//-- Private Methods --//
//=====================//
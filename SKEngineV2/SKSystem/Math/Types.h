#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//

namespace SK
{
	namespace Math
	{
		//---- Types ----//
		//===============//

		struct float2
		{
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//
			
			float2() : x(0), y(0) {}
			float2(float x, float y) : x(x), y(y) {}
			explicit float2(float val) : x(val), y(val) {}
			explicit float2(float* vec) : x(vec[0]), y(vec[1]) {}

			operator const float*() const
			{
				return reinterpret_cast<const float*>(this);
			}

			operator float*()
			{
				return reinterpret_cast<float*>(this);
			}

			const float2 operator + (const float2& rhs) const
			{
				float2 Result;
				Result.x = x + rhs.x;
				Result.y = y + rhs.y;
				return Result;
			}

			const float2& operator += (const float2& rhs)
			{
				x += rhs.x;
				y += rhs.y;
				return *this;
			}

			const float2 operator - (const float2& rhs) const
			{
				float2 Result;
				Result.x = x - rhs.x;
				Result.y = y - rhs.y;
				return Result;
			}

			const float2& operator -= (const float2& rhs)
			{
				x -= rhs.x;
				y -= rhs.y;
				return *this;
			}

			const float2 operator * (const float2& rhs) const
			{
				float2 Result;
				Result.x = x * rhs.x;
				Result.y = y * rhs.y;
				return Result;
			}

			const float2& operator *= (const float2& rhs)
			{
				x *= rhs.x;
				y *= rhs.y;
				return *this;
			}

			const float2 operator / (const float2& rhs) const
			{
				float2 Result;
				Result.x = x / rhs.x;
				Result.y = y / rhs.y;
				return Result;
			}

			const float2& operator /= (const float2& rhs)
			{
				x /= rhs.x;
				y /= rhs.y;
				return *this;
			}

			const float2 operator - () const
			{
				float2 Result;
				Result.x = -x;
				Result.y = -y;
				return Result;
			}

			bool operator == (const float2& rhs)
			{
				return (x == rhs.x) && (y == rhs.y);
			}

			bool operator != (const float2& rhs)
			{
				return (x != rhs.x) || (y != rhs.y);
			}

			//-- Variables --//
			//===============//

			float x;
			float y;
		};

		struct float3
		{
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			float3() : x(0), y(0), z(0) {}
			float3(float x, float y, float z) : x(x), y(y), z(z) {}
			float3(const float2& rhs, float z = 0.0f) : x(rhs.x), y(rhs.y), z(z) {}
			explicit float3(float val) : x(val), y(val), z(val) {}
			explicit float3(float* vec) : x(vec[0]), y(vec[1]), z(vec[3]) {}

			operator const float*() const
			{
				return reinterpret_cast<const float*>(this);
			}

			operator float*()
			{
				return reinterpret_cast<float*>(this);
			}

			const float3 operator + (const float3& rhs) const
			{
				float3 Result;
				Result.x = x + rhs.x;
				Result.y = y + rhs.y;
				Result.z = z + rhs.z;
				return Result;
			}

			const float3& operator += (const float3& rhs)
			{
				x += rhs.x;
				y += rhs.y;
				z += rhs.z;
				return *this;
			}

			const float3 operator - (const float3& rhs) const
			{
				float3 Result;
				Result.x = x - rhs.x;
				Result.y = y - rhs.y;
				Result.z = z - rhs.z;
				return Result;
			}

			const float3& operator -= (const float3& rhs)
			{
				x -= rhs.x;
				y -= rhs.y;
				z -= rhs.z;
				return *this;
			}

			const float3 operator * (const float3& rhs) const
			{
				float3 Result;
				Result.x = x * rhs.x;
				Result.y = y * rhs.y;
				Result.z = z * rhs.z;
				return Result;
			}

			const float3& operator *= (const float3& rhs)
			{
				x *= rhs.x;
				y *= rhs.y;
				z *= rhs.z;
				return *this;
			}

			const float3 operator / (const float3& rhs) const
			{
				float3 Result;
				Result.x = x / rhs.x;
				Result.y = y / rhs.y;
				Result.z = z / rhs.z;
				return Result;
			}

			const float3& operator /= (const float3& rhs)
			{
				x /= rhs.x;
				y /= rhs.y;
				z /= rhs.z;
				return *this;
			}

			const float3 operator - () const
			{
				float3 Result;
				Result.x = -x;
				Result.y = -y;
				Result.z = -z;
				return Result;
			}

			bool operator == (const float3 rhs)
			{
				return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
			}

			bool operator != (const float3& rhs)
			{
				return (x != rhs.x) || (y != rhs.y) || (z != rhs.z);
			}

			const float2 xy() { return float2(x, y); }
			const float2 xz() { return float2(x, z); }

			//-- Variables --//
			//===============//

			float x;
			float y;
			float z;
		};

		struct float4
		{
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			float x;
			float y;
			float z;
			float w;

			float4() : x(0), y(0), z(0), w(0) {}
			float4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}
			float4(const float3& rhs, float w = 0.0f) : x(rhs.x), y(rhs.y), z(rhs.z), w(w) {}
			float4(const float2& rhs, float z = 0.0f, float w = 0.0f) : x(rhs.x), y(rhs.y), z(z), w(w) {}
			explicit float4(float val) : x(val), y(val), z(val), w(0) {}
			explicit float4(float* vec) : x(vec[0]), y(vec[1]), z(vec[2]), w(vec[3]) {}

			operator const float*() const
			{
				return reinterpret_cast<const float*>(this);
			}

			operator float*()
			{
				return reinterpret_cast<float*>(this);
			}

			const float4 operator + (const float4& rhs) const
			{
				float4 Result;
				Result.x = x + rhs.x;
				Result.y = y + rhs.y;
				Result.z = z + rhs.z;
				Result.w = w + rhs.w;
				return Result;
			}

			const float4& operator += (const float4& rhs)
			{
				x += rhs.x;
				y += rhs.y;
				z += rhs.z;
				w += rhs.w;
				return *this;
			}

			const float4 operator - (const float4& rhs) const
			{
				float4 Result;
				Result.x = x - rhs.x;
				Result.y = y - rhs.y;
				Result.z = z - rhs.z;
				Result.w = w - rhs.w;
				return Result;
			}

			const float4& operator -= (const float4& rhs)
			{
				x -= rhs.x;
				y -= rhs.y;
				z -= rhs.z;
				w -= rhs.w;
				return *this;
			}

			const float4 operator * (const float4& rhs) const
			{
				float4 Result;
				Result.x = x * rhs.x;
				Result.y = y * rhs.y;
				Result.z = z * rhs.z;
				Result.w = w * rhs.w;
				return Result;
			}

			const float4& operator *= (const float4& rhs)
			{
				x *= rhs.x;
				y *= rhs.y;
				z *= rhs.z;
				w *= rhs.w;
				return *this;
			}

			const float4 operator / (const float4& rhs) const
			{
				float4 Result;
				Result.x = x / rhs.x;
				Result.y = y / rhs.y;
				Result.z = z / rhs.z;
				Result.w = w / rhs.w;
				return Result;
			}

			const float4& operator /= (const float4& rhs)
			{
				x /= rhs.x;
				y /= rhs.y;
				z /= rhs.z;
				w /= rhs.w;
				return *this;
			}

			const float4 operator - () const
			{
				float4 Result;
				Result.x = -x;
				Result.y = -y;
				Result.z = -z;
				Result.w = -w;
				return Result;
			}

			bool operator == (const float4& rhs)
			{
				return (x == rhs.x) && (y == rhs.y) && (z == rhs.z) && (w == rhs.w);
			}

			bool operator != (const float4& rhs)
			{
				return (x != rhs.x) || (y != rhs.y) || (z != rhs.z) || (w != rhs.w);
			}

			const float3 xyz() { return float3(x, y, z); }
			const float2 xy() { return float2(x, y); }
			const float2 xz() { return float2(x, z); }
		};

		struct float4x4
		{
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			float4 row0;
			float4 row1;
			float4 row2;
			float4 row3;

			float4x4() {}
			float4x4(float4 row0, float4 row1, float4 row2, float4 row3) : row0(row0), row1(row1), row2(row2), row3(row3) {}
			float4x4(float* mat) : row0(mat), row1(mat+4), row2(mat+8), row3(mat+12) {}

			operator const float*() const
			{
				return reinterpret_cast<const float*>(this);
			}

			operator float*()
			{
				return reinterpret_cast<float*>(this);
			}

			bool operator == (const float4x4& rhs)
			{
				return (row0 == rhs.row0) && (row1 == rhs.row1) && (row2 == rhs.row2) && (row3 == rhs.row3);
			}

			bool operator != (const float4x4& rhs)
			{
				return (row0 != rhs.row0) || (row1 != rhs.row1) || (row2 != rhs.row2) || (row3 != rhs.row3);
			}
		};

		struct int2
		{
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			int2() : x(0), y(0) {}
			int2(int x, int y) : x(x), y(y) {}
			explicit int2(int val) : x(val), y(val) {}
			explicit int2(int* vec) : x(vec[0]), y(vec[1]) {}

			operator const int*() const
			{
				return reinterpret_cast<const int*>(this);
			}

			operator int*()
			{
				return reinterpret_cast<int*>(this);
			}

			const int2 operator + (const int2& rhs) const
			{
				int2 Result;
				Result.x = x + rhs.x;
				Result.y = y + rhs.y;
				return Result;
			}

			const int2& operator += (const int2& rhs)
			{
				x += rhs.x;
				y += rhs.y;
				return *this;
			}

			const int2 operator - (const int2& rhs) const
			{
				int2 Result;
				Result.x = x - rhs.x;
				Result.y = y - rhs.y;
				return Result;
			}

			const int2& operator -= (const int2& rhs)
			{
				x -= rhs.x;
				y -= rhs.y;
				return *this;
			}

			const int2 operator - () const
			{
				int2 Result;
				Result.x = -x;
				Result.y = -y;
				return Result;
			}

			bool operator == (const int2& rhs)
			{
				return (x == rhs.x) && (y == rhs.y);
			}

			bool operator != (const int2& rhs)
			{
				return (x != rhs.x) || (y != rhs.y);
			}

			//-- Variables --//
			//===============//

			int x;
			int y;
		};

		struct int3
		{
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			int3() : x(0), y(0) {}
			int3(int x, int y, int z) : x(x), y(y), z(z) {}
			int3(const int2& rhs, int z = 0) : x(rhs.x), y(rhs.y), z(z) {}
			explicit int3(int val) : x(val), y(val), z(val) {}
			explicit int3(int* vec) : x(vec[0]), y(vec[1]), z(vec[2]) {}

			operator const int*() const
			{
				return reinterpret_cast<const int*>(this);
			}

			operator int*()
			{
				return reinterpret_cast<int*>(this);
			}

			const int3 operator + (const int3& rhs) const
			{
				int3 Result;
				Result.x = x + rhs.x;
				Result.y = y + rhs.y;
				Result.z = z + rhs.z;
				return Result;
			}

			const int3& operator += (const int3& rhs)
			{
				x += rhs.x;
				y += rhs.y;
				z += rhs.z;
				return *this;
			}

			const int3 operator - (const int3& rhs) const
			{
				int3 Result;
				Result.x = x - rhs.x;
				Result.y = y - rhs.y;
				Result.z = z - rhs.z;
				return Result;
			}

			const int3& operator -= (const int3& rhs)
			{
				x -= rhs.x;
				y -= rhs.y;
				z -= rhs.z;
				return *this;
			}

			const int3 operator - () const
			{
				int3 Result;
				Result.x = -x;
				Result.y = -y;
				Result.z = -z;
				return Result;
			}

			bool operator == (const int3& rhs)
			{
				return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
			}

			bool operator != (const int3& rhs)
			{
				return (x != rhs.x) || (y != rhs.y) || (z != rhs.z);
			}

			//-- Variables --//
			//===============//

			int x;
			int y;
			int z;
		};

		//--- Methods ---//
		//===============//

		inline const float4 operator * (float f, const float4& rhs) { float4 Result; Result.x = rhs.x * f; Result.y = rhs.y * f; Result.z = rhs.z * f; Result.w = rhs.w * f; return Result; }
		inline const float4 operator * (const float4& lhs, float f) { float4 Result; Result.x = lhs.x * f; Result.y = lhs.y * f; Result.z = lhs.z * f; Result.w = lhs.w * f; return Result; }
		inline const float4 operator / (float f, const float4& rhs) { float4 Result; Result.x = rhs.x / f; Result.y = rhs.y / f; Result.z = rhs.z / f; Result.w = rhs.w / f; return Result; }
		inline const float4 operator / (const float4& lhs, float f) { float4 Result; Result.x = lhs.x / f; Result.y = lhs.y / f; Result.z = lhs.z / f; Result.w = lhs.w / f; return Result; }

		inline const float3 operator * (float f, const float3& rhs) { float3 Result; Result.x = rhs.x * f; Result.y = rhs.y * f; Result.z = rhs.z * f; return Result; }
		inline const float3 operator * (const float3& lhs, float f) { float3 Result; Result.x = lhs.x * f; Result.y = lhs.y * f; Result.z = lhs.z * f; return Result; }
		inline const float3 operator / (float f, const float3& rhs) { float3 Result; Result.x = rhs.x / f; Result.y = rhs.y / f; Result.z = rhs.z / f; return Result; }
		inline const float3 operator / (const float3& lhs, float f) { float3 Result; Result.x = lhs.x / f; Result.y = lhs.y / f; Result.z = lhs.z / f; return Result; }

		inline const float2 operator * (float f, const float2& rhs) { float2 Result; Result.x = rhs.x * f; Result.y = rhs.y * f; return Result; }
		inline const float2 operator * (const float2& lhs, float f) { float2 Result; Result.x = lhs.x * f; Result.y = lhs.y * f; return Result; }
		inline const float2 operator / (float f, const float2& rhs) { float2 Result; Result.x = rhs.x / f; Result.y = rhs.y / f; return Result; }
		inline const float2 operator / (const float2& lhs, float f) { float2 Result; Result.x = lhs.x / f; Result.y = lhs.y / f; return Result; }
	}
}
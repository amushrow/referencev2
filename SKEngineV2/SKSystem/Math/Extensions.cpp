//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <emmintrin.h>
#include <SKSystem/Math/Extensions.h>

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

namespace
{
	static const __m128 SIGNMASK = _mm_castsi128_ps(_mm_set1_epi32(0x80000000));
	static const __m128 FOUR_PI = _mm_set_ps1(1.27323954473516f);
	static const __m128 ONE = _mm_set_ps1(1);
	static const __m128 HALF = _mm_set_ps1(0.5f);
	static const __m128 DP1 = _mm_set_ps1(-0.78515625f);
	static const __m128 DP2 = _mm_set_ps1(-2.4187564849853515625e-4f);
	static const __m128 DP3 = _mm_set_ps1(-3.77489497744594108e-8f);

	static const __m128 COSCOEF1 = _mm_set_ps1(2.443315711809948e-5f);
	static const __m128 COSCOEF2 = _mm_set_ps1(-1.388731625493765e-3f);
	static const __m128 COSCOEF3 = _mm_set_ps1(4.166664568298827e-2f);
	static const __m128 SINCOEF1 = _mm_set_ps1(-1.9515295891e-4f);
	static const __m128 SINCOEF2 = _mm_set_ps1(8.3321608736e-3f);
	static const __m128 SINCOEF3 = _mm_set_ps1(-1.6666654611e-1f);

	static const __m128i INT_ONE = _mm_set1_epi32(1);
	static const __m128i INT_INVONE = _mm_set1_epi32(~1);
	static const __m128i INT_TWO = _mm_set1_epi32(2);
	static const __m128i INT_FOUR = _mm_set1_epi32(4);
}

//-- Public Methods --//
//====================//

__m128 _mm_sin_ps(__m128 _A)
{
	__m128 sign_bit = _mm_and_ps(_A, SIGNMASK);
	_A = _mm_andnot_ps(SIGNMASK, _A);
	
	__m128i emm2 = _mm_and_si128(_mm_add_epi32(_mm_cvttps_epi32(_mm_mul_ps(_A, FOUR_PI)), INT_ONE), INT_INVONE);
	__m128 y = _mm_cvtepi32_ps(emm2);

	// get the swap sign flag
	__m128 swap_sign_bit = _mm_castsi128_ps(_mm_slli_epi32(_mm_and_si128(emm2, INT_FOUR), 29));
	__m128 poly_mask = _mm_castsi128_ps(_mm_cmpeq_epi32(_mm_and_si128(emm2, INT_TWO), _mm_setzero_si128()));
	sign_bit = _mm_xor_ps(sign_bit, swap_sign_bit);

	 // Extended precision modular arithmetic
	_A = _mm_add_ps(_A, _mm_mul_ps(y, DP1));
	_A = _mm_add_ps(_A, _mm_mul_ps(y, DP2));
	_A = _mm_add_ps(_A, _mm_mul_ps(y, DP3));

	// Evaluate the first polynom  (0 <= x <= Pi/4)
	__m128 z = _mm_mul_ps(_A, _A);

	y = _mm_add_ps(_mm_mul_ps(z, _mm_add_ps(_mm_mul_ps(z, COSCOEF1), COSCOEF2)), COSCOEF3);	
	y = _mm_mul_ps(y, _mm_mul_ps(z, z));

	__m128 tmp = _mm_mul_ps(z, HALF);
	y = _mm_sub_ps(y, tmp);
	y = _mm_add_ps(y, ONE);

	// Evaluate the second polynom  (Pi/4 <= x <= 0)
	__m128 y2 = _mm_add_ps(_mm_mul_ps(z, _mm_add_ps(_mm_mul_ps(z, SINCOEF1), SINCOEF2)), SINCOEF3);
	y2 = _mm_add_ps(_mm_mul_ps(_mm_mul_ps(y2, z), _A), _A);

	// select the correct result from the two polynoms
	__m128 xmm3 = poly_mask;
	y2 = _mm_and_ps(xmm3, y2);
	y = _mm_andnot_ps(xmm3, y);
	y = _mm_add_ps(y, y2);
	
	// update the sign
	y = _mm_xor_ps(y, sign_bit);
	
	return y;
}

__m128 _mm_cos_ps(__m128 _A)
{	
	_A = _mm_andnot_ps(SIGNMASK, _A);

	__m128i emm2 = _mm_and_si128(_mm_add_epi32(_mm_cvttps_epi32(_mm_mul_ps(_A, FOUR_PI)), INT_ONE), INT_INVONE);
	__m128 y = _mm_cvtepi32_ps(emm2);

	// get the polynom selection mask
	emm2 = _mm_sub_epi32(emm2, INT_TWO);
	__m128 sign_bit = _mm_castsi128_ps(_mm_slli_epi32(_mm_andnot_si128(emm2, INT_FOUR), 29));
	__m128 poly_mask = _mm_castsi128_ps(_mm_cmpeq_epi32(_mm_and_si128(emm2, INT_TWO), _mm_setzero_si128()));

	// Extended precision modular arithmetic
	_A = _mm_add_ps(_mm_add_ps(_mm_add_ps(_A, _mm_mul_ps(y, DP1)), _mm_mul_ps(y, DP2)), _mm_mul_ps(y, DP3));

	// Evaluate the first polynom  (0 <= x <= Pi/4)
	__m128 z = _mm_mul_ps(_A, _A);
	y = _mm_add_ps(_mm_mul_ps(z, _mm_add_ps(_mm_mul_ps(z, COSCOEF1), COSCOEF2)), COSCOEF3);
	y = _mm_mul_ps(y, _mm_mul_ps(z, z));
	
	__m128 tmp = _mm_mul_ps(z, HALF);
	y = _mm_sub_ps(y, tmp);
	y = _mm_add_ps(y, ONE);

	// Evaluate the second polynom  (Pi/4 <= x <= 0)
	__m128 y2 = _mm_add_ps(_mm_mul_ps(z, _mm_add_ps(_mm_mul_ps(z, SINCOEF1), SINCOEF2)), SINCOEF3);
	y2 = _mm_add_ps(_mm_mul_ps(_mm_mul_ps(y2, z), _A), _A);

	// select the correct result from the two polynoms
	y2 = _mm_and_ps(poly_mask, y2);
	y = _mm_andnot_ps(poly_mask, y);
	y = _mm_add_ps(y, y2);

	// update the sign
	y = _mm_xor_ps(y, sign_bit);

	return y;
}

void _mm_sincos_ps(__m128 _A, __m128& out_Sin, __m128& out_Cos)
{
	__m128 sign_bit_sin = _A;
	_A = _mm_andnot_ps(SIGNMASK, _A);
	sign_bit_sin = _mm_and_ps(sign_bit_sin, SIGNMASK);
	
	__m128i emm2 = _mm_and_si128(_mm_add_epi32(_mm_cvttps_epi32(_mm_mul_ps(_A, FOUR_PI)), INT_ONE), INT_INVONE);
	__m128 swap_sign_bit_sin = _mm_castsi128_ps(_mm_slli_epi32(_mm_and_si128(emm2, INT_FOUR), 29));
	sign_bit_sin = _mm_xor_ps(sign_bit_sin, swap_sign_bit_sin);
	__m128 sign_bit_cos = _mm_castsi128_ps(_mm_slli_epi32(_mm_andnot_si128(_mm_sub_epi32(emm2, INT_TWO), INT_FOUR), 29));
	__m128 poly_mask = _mm_castsi128_ps(_mm_cmpeq_epi32(_mm_and_si128(emm2, INT_TWO), _mm_setzero_si128()));

	// Extended precision modular arithmetic
	__m128 y = _mm_cvtepi32_ps(emm2);
	_A = _mm_add_ps(_mm_add_ps(_mm_add_ps(_A, _mm_mul_ps(y, DP1)), _mm_mul_ps(y, DP2)), _mm_mul_ps(y, DP3));

	// Evaluate the first polynom  (0 <= x <= Pi/4)
	__m128 z = _mm_mul_ps(_A, _A);
	y = COSCOEF1;

	y = _mm_add_ps(_mm_mul_ps(z, _mm_add_ps(_mm_mul_ps(z, COSCOEF1), COSCOEF2)), COSCOEF3);
	y = _mm_mul_ps(y, _mm_mul_ps(z, z));
	__m128 tmp = _mm_mul_ps(z, HALF);
	y = _mm_sub_ps(y, tmp);
	y = _mm_add_ps(y, ONE);

	// Evaluate the second polynom  (Pi/4 <= x <= 0)
	__m128 y2 = _mm_add_ps(_mm_mul_ps(z, _mm_add_ps(_mm_mul_ps(z, SINCOEF1), SINCOEF2)), SINCOEF3);
	y2 = _mm_mul_ps(y2, z);
	y2 = _mm_mul_ps(y2, _A);
	y2 = _mm_add_ps(y2, _A);

	// select the correct result from the two polynoms
	__m128 xmm3 = poly_mask;
	__m128 ysin2 = _mm_and_ps(xmm3, y2);
	__m128 ysin1 = _mm_andnot_ps(xmm3, y);
	y2 = _mm_sub_ps(y2, ysin2);
	y = _mm_sub_ps(y, ysin1);

	__m128 xmm1 = _mm_add_ps(ysin1, ysin2);
	__m128 xmm2 = _mm_add_ps(y, y2);

	// update the sign
	out_Sin = _mm_xor_ps(xmm1, sign_bit_sin);
	out_Cos = _mm_xor_ps(xmm2, sign_bit_cos);
}

//-- Private Methods --//
//=====================//
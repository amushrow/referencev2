//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <cmath>
#include <pmmintrin.h>
#include <immintrin.h>
#include <SKSystem/Math/Extensions.h>
#include <SKSystem/Math/Types.h>
#include <SKSystem/Math/Vector.h>
#include <SKSystem/Math/Quaternion.h>
#include <SKSystem/Math/Matrix.h>
#include <SKSystem/Debug.h>
using namespace SK::Math;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

namespace
{
	static const __m128 ONE = _mm_set_ps1(1);
	static const __m128 ZERO_W = _mm_setr_ps(0, 0, 0, 1);

	static const __m128 QUAT2MAT_FLIP0 = _mm_castsi128_ps(_mm_set_epi32(0x80000000, 0, 0, 0x80000000));
	static const __m128 QUAT2MAT_FLIP1 = _mm_castsi128_ps(_mm_set_epi32(0x80000000, 0x80000000, 0x80000000, 0));
	static const __m128 CLEAR_X = _mm_castsi128_ps(_mm_set_epi32(-1, -1, -1, 0));
}

//-- Public Methods --//
//====================//

const Matrix& Matrix::Identity()
{
	static Matrix _Identity(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1);
	return _Identity;
}

Matrix::Matrix(const float* data)
{
	Row0 = _mm_loadu_ps(data);
	Row1 = _mm_loadu_ps(data + 4);
	Row2 = _mm_loadu_ps(data + 8);
	Row3 = _mm_loadu_ps(data + 12);
}

Matrix::Matrix(const float4& row0, const float4& row1, const float4& row2, const float4& row3)
{
	Row0 = _mm_loadu_ps(row0);
	Row1 = _mm_loadu_ps(row1);
	Row2 = _mm_loadu_ps(row2);
	Row3 = _mm_loadu_ps(row3);
}

Matrix::Matrix(float xx, float xy, float xz, float xw,
	float yx, float yy, float yz, float yw,
	float zx, float zy, float zz, float zw,
	float wx, float wy, float wz, float ww)
{
	Row0 = _mm_setr_ps(xx, xy, xz, xw);
	Row1 = _mm_setr_ps(yx, yy, yz, yw);
	Row2 = _mm_setr_ps(zx, zy, zz, zw);
	Row3 = _mm_setr_ps(wx, wy, wz, ww);
}

float4x4 Matrix::Get() const
{
	float4x4 Result;
	_mm_storeu_ps(&Result[0], Row0);
	_mm_storeu_ps(&Result[4], Row1);
	_mm_storeu_ps(&Result[8], Row2);
	_mm_storeu_ps(&Result[12], Row3);

	return Result;
}

float4 Matrix::Row(int row) const
{
	SK::Debug::Assert(0 <= row && row <= 3, "Matrix::Row: Invalid row");
	float4 Row;
	switch (row)
	{
	case 0: _mm_storeu_ps(Row, Row0); break;
	case 1: _mm_storeu_ps(Row, Row1); break;
	case 2: _mm_storeu_ps(Row, Row2); break;
	case 3: _mm_storeu_ps(Row, Row3); break;
	}
	return Row;
}

void Matrix::Row(int row, float4 data)
{
	SK::Debug::Assert(0 <= row && row <= 3, "Matrix::Row: Invalid row");
	switch (row)
	{
	case 0: Row0 = _mm_loadu_ps(data); break;
	case 1: Row1 = _mm_loadu_ps(data); break;
	case 2: Row2 = _mm_loadu_ps(data); break;
	case 3: Row3 = _mm_loadu_ps(data); break;
	}
}

Matrix Matrix::Transpose() const
{
	Matrix Result;
	__m128 Tmp4, Tmp3, Tmp2, Tmp1;
	Tmp1 = _mm_shuffle_ps((Row0), (Row1), 0x44);
	Tmp2 = _mm_shuffle_ps((Row0), (Row1), 0xEE);
	Tmp3 = _mm_shuffle_ps((Row2), (Row3), 0x44);
	Tmp4 = _mm_shuffle_ps((Row2), (Row3), 0xEE);
	Result.Row0 = _mm_shuffle_ps(Tmp1, Tmp2, 0x88);
	Result.Row1 = _mm_shuffle_ps(Tmp1, Tmp2, 0xDD);
	Result.Row2 = _mm_shuffle_ps(Tmp3, Tmp4, 0x88);
	Result.Row3 = _mm_shuffle_ps(Tmp3, Tmp4, 0xDD);

	return Result;
}

Matrix Matrix::Inverse() const
{
	Matrix Result;
	static const __m128 SignA = _mm_set_ps(1.0f, -1.0f, 1.0f, -1.0f);
	static const __m128 SignB = _mm_set_ps(-1.0f, 1.0f, -1.0f, 1.0f);

	__m128 Fac0;
	{
		__m128 Swp0a = _mm_shuffle_ps(Row3, Row2, 0xFF);
		__m128 Swp0b = _mm_shuffle_ps(Row3, Row2, 0xAA);

		__m128 Swp00 = _mm_shuffle_ps(Row2, Row1, 0xAA);
		__m128 Swp01 = _mm_shuffle_ps(Swp0a, Swp0a, 0x80);
		__m128 Swp02 = _mm_shuffle_ps(Swp0b, Swp0b, 0x80);
		__m128 Swp03 = _mm_shuffle_ps(Row2, Row1, 0xFF);

		__m128 Mul00 = _mm_mul_ps(Swp00, Swp01);
		__m128 Mul01 = _mm_mul_ps(Swp02, Swp03);
		Fac0 = _mm_sub_ps(Mul00, Mul01);
	}

	__m128 Fac1;
	{
		__m128 Swp0a = _mm_shuffle_ps(Row3, Row2, 0xFF);
		__m128 Swp0b = _mm_shuffle_ps(Row3, Row2, 0x55);

		__m128 Swp00 = _mm_shuffle_ps(Row2, Row1, 0x55);
		__m128 Swp01 = _mm_shuffle_ps(Swp0a, Swp0a, 0x80);
		__m128 Swp02 = _mm_shuffle_ps(Swp0b, Swp0b, 0x80);
		__m128 Swp03 = _mm_shuffle_ps(Row2, Row1, 0xFF);

		__m128 Mul00 = _mm_mul_ps(Swp00, Swp01);
		__m128 Mul01 = _mm_mul_ps(Swp02, Swp03);
		Fac1 = _mm_sub_ps(Mul00, Mul01);
	}


	__m128 Fac2;
	{
		__m128 Swp0a = _mm_shuffle_ps(Row3, Row2, 0xAA);
		__m128 Swp0b = _mm_shuffle_ps(Row3, Row2, 0x55);

		__m128 Swp00 = _mm_shuffle_ps(Row2, Row1, 0x55);
		__m128 Swp01 = _mm_shuffle_ps(Swp0a, Swp0a, 0x80);
		__m128 Swp02 = _mm_shuffle_ps(Swp0b, Swp0b, 0x80);
		__m128 Swp03 = _mm_shuffle_ps(Row2, Row1, 0xAA);

		__m128 Mul00 = _mm_mul_ps(Swp00, Swp01);
		__m128 Mul01 = _mm_mul_ps(Swp02, Swp03);
		Fac2 = _mm_sub_ps(Mul00, Mul01);
	}

	__m128 Fac3;
	{
		__m128 Swp0a = _mm_shuffle_ps(Row3, Row2, 0xFF);
		__m128 Swp0b = _mm_shuffle_ps(Row3, Row2, 0x00);

		__m128 Swp00 = _mm_shuffle_ps(Row2, Row1, 0x00);
		__m128 Swp01 = _mm_shuffle_ps(Swp0a, Swp0a, 0x80);
		__m128 Swp02 = _mm_shuffle_ps(Swp0b, Swp0b, 0x80);
		__m128 Swp03 = _mm_shuffle_ps(Row2, Row1, 0xFF);

		__m128 Mul00 = _mm_mul_ps(Swp00, Swp01);
		__m128 Mul01 = _mm_mul_ps(Swp02, Swp03);
		Fac3 = _mm_sub_ps(Mul00, Mul01);
	}

	__m128 Fac4;
	{
		__m128 Swp0a = _mm_shuffle_ps(Row3, Row2, 0xAA);
		__m128 Swp0b = _mm_shuffle_ps(Row3, Row2, 0x00);

		__m128 Swp00 = _mm_shuffle_ps(Row2, Row1, 0x00);
		__m128 Swp01 = _mm_shuffle_ps(Swp0a, Swp0a, 0x80);
		__m128 Swp02 = _mm_shuffle_ps(Swp0b, Swp0b, 0x80);
		__m128 Swp03 = _mm_shuffle_ps(Row2, Row1, 0xAA);

		__m128 Mul00 = _mm_mul_ps(Swp00, Swp01);
		__m128 Mul01 = _mm_mul_ps(Swp02, Swp03);
		Fac4 = _mm_sub_ps(Mul00, Mul01);
	}

	__m128 Fac5;
	{
		__m128 Swp0a = _mm_shuffle_ps(Row3, Row2, 0x55);
		__m128 Swp0b = _mm_shuffle_ps(Row3, Row2, 0x00);

		__m128 Swp00 = _mm_shuffle_ps(Row2, Row1, 0x00);
		__m128 Swp01 = _mm_shuffle_ps(Swp0a, Swp0a, 0x80);
		__m128 Swp02 = _mm_shuffle_ps(Swp0b, Swp0b, 0x80);
		__m128 Swp03 = _mm_shuffle_ps(Row2, Row1, 0x55);

		__m128 Mul00 = _mm_mul_ps(Swp00, Swp01);
		__m128 Mul01 = _mm_mul_ps(Swp02, Swp03);
		Fac5 = _mm_sub_ps(Mul00, Mul01);
	}

	__m128 Temp0 = _mm_shuffle_ps(Row1, Row0, 0x00);
	__m128 Vec0 = _mm_shuffle_ps(Temp0, Temp0, 0xA8);

	__m128 Temp1 = _mm_shuffle_ps(Row1, Row0, 0x55);
	__m128 Vec1 = _mm_shuffle_ps(Temp1, Temp1, 0xA8);

	__m128 Temp2 = _mm_shuffle_ps(Row1, Row0, 0xAA);
	__m128 Vec2 = _mm_shuffle_ps(Temp2, Temp2, 0xA8);

	__m128 Temp3 = _mm_shuffle_ps(Row1, Row0, 0xFF);
	__m128 Vec3 = _mm_shuffle_ps(Temp3, Temp3, 0xA8);

	__m128 Mul00 = _mm_mul_ps(Vec1, Fac0);
	__m128 Mul01 = _mm_mul_ps(Vec2, Fac1);
	__m128 Mul02 = _mm_mul_ps(Vec3, Fac2);
	__m128 Sub00 = _mm_sub_ps(Mul00, Mul01);
	__m128 Add00 = _mm_add_ps(Sub00, Mul02);
	__m128 Inv0 = _mm_mul_ps(SignB, Add00);

	__m128 Mul03 = _mm_mul_ps(Vec0, Fac0);
	__m128 Mul04 = _mm_mul_ps(Vec2, Fac3);
	__m128 Mul05 = _mm_mul_ps(Vec3, Fac4);
	__m128 Sub01 = _mm_sub_ps(Mul03, Mul04);
	__m128 Add01 = _mm_add_ps(Sub01, Mul05);
	__m128 Inv1 = _mm_mul_ps(SignA, Add01);

	__m128 Mul06 = _mm_mul_ps(Vec0, Fac1);
	__m128 Mul07 = _mm_mul_ps(Vec1, Fac3);
	__m128 Mul08 = _mm_mul_ps(Vec3, Fac5);
	__m128 Sub02 = _mm_sub_ps(Mul06, Mul07);
	__m128 Add02 = _mm_add_ps(Sub02, Mul08);
	__m128 Inv2 = _mm_mul_ps(SignB, Add02);

	__m128 Mul09 = _mm_mul_ps(Vec0, Fac2);
	__m128 Mul10 = _mm_mul_ps(Vec1, Fac4);
	__m128 Mul11 = _mm_mul_ps(Vec2, Fac5);
	__m128 Sub03 = _mm_sub_ps(Mul09, Mul10);
	__m128 Add03 = _mm_add_ps(Sub03, Mul11);
	__m128 Inv3 = _mm_mul_ps(SignA, Add03);

	__m128 Row00 = _mm_shuffle_ps(Inv0, Inv1, 0x00);
	__m128 Row10 = _mm_shuffle_ps(Inv2, Inv3, 0x00);
	__m128 Row20 = _mm_shuffle_ps(Row00, Row10, 0x88);

	__m128 Det0 = _mm_dp_ps(Row0, Row20, 0xff);
	__m128 Rcp0 = _mm_div_ps(ONE, Det0);

	//  Inverse /= Determinant;
	Result.Row0 = _mm_mul_ps(Inv0, Rcp0);
	Result.Row1 = _mm_mul_ps(Inv1, Rcp0);
	Result.Row2 = _mm_mul_ps(Inv2, Rcp0);
	Result.Row3 = _mm_mul_ps(Inv3, Rcp0);

	return Result;
}

const Matrix Matrix::operator * (const Matrix& rhs) const
{
	Matrix Result;

	// First Row
	__m128 XX = _mm_shuffle_ps(Row0, Row0, 0x00);
	__m128 XY = _mm_shuffle_ps(Row0, Row0, 0x55);
	__m128 XZ = _mm_shuffle_ps(Row0, Row0, 0xAA);
	__m128 XW = _mm_shuffle_ps(Row0, Row0, 0xFF);
	Result.Row0 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(XX, rhs.Row0), _mm_mul_ps(XY, rhs.Row1)), _mm_add_ps(_mm_mul_ps(XZ, rhs.Row2), _mm_mul_ps(XW, rhs.Row3)));

	// Second Row
	XX = _mm_shuffle_ps(Row1, Row1, 0x00);
	XY = _mm_shuffle_ps(Row1, Row1, 0x55);
	XZ = _mm_shuffle_ps(Row1, Row1, 0xAA);
	XW = _mm_shuffle_ps(Row1, Row1, 0xFF);
	Result.Row1 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(XX, rhs.Row0), _mm_mul_ps(XY, rhs.Row1)), _mm_add_ps(_mm_mul_ps(XZ, rhs.Row2), _mm_mul_ps(XW, rhs.Row3)));

	// Third Row
	XX = _mm_shuffle_ps(Row2, Row2, 0x00);
	XY = _mm_shuffle_ps(Row2, Row2, 0x55);
	XZ = _mm_shuffle_ps(Row2, Row2, 0xAA);
	XW = _mm_shuffle_ps(Row2, Row2, 0xFF);
	Result.Row2 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(XX, rhs.Row0), _mm_mul_ps(XY, rhs.Row1)), _mm_add_ps(_mm_mul_ps(XZ, rhs.Row2), _mm_mul_ps(XW, rhs.Row3)));

	// Fourth Row
	XX = _mm_shuffle_ps(Row3, Row3, 0x00);
	XY = _mm_shuffle_ps(Row3, Row3, 0x55);
	XZ = _mm_shuffle_ps(Row3, Row3, 0xAA);
	XW = _mm_shuffle_ps(Row3, Row3, 0xFF);
	Result.Row3 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(XX, rhs.Row0), _mm_mul_ps(XY, rhs.Row1)), _mm_add_ps(_mm_mul_ps(XZ, rhs.Row2), _mm_mul_ps(XW, rhs.Row3)));

	return Result;
}

void Matrix::operator *= (const Matrix& rhs)
{
	// First Row
	__m128 XX = _mm_shuffle_ps(Row0, Row0, 0x00);
	__m128 XY = _mm_shuffle_ps(Row0, Row0, 0x55);
	__m128 XZ = _mm_shuffle_ps(Row0, Row0, 0xAA);
	__m128 XW = _mm_shuffle_ps(Row0, Row0, 0xFF);
	Row0 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(XX, rhs.Row0), _mm_mul_ps(XY, rhs.Row1)), _mm_add_ps(_mm_mul_ps(XZ, rhs.Row2), _mm_mul_ps(XW, rhs.Row3)));


	// Second Row
	XX = _mm_shuffle_ps(Row1, Row1, 0x00);
	XY = _mm_shuffle_ps(Row1, Row1, 0x55);
	XZ = _mm_shuffle_ps(Row1, Row1, 0xAA);
	XW = _mm_shuffle_ps(Row1, Row1, 0xFF);
	Row1 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(XX, rhs.Row0), _mm_mul_ps(XY, rhs.Row1)), _mm_add_ps(_mm_mul_ps(XZ, rhs.Row2), _mm_mul_ps(XW, rhs.Row3)));

	// Third Row
	XX = _mm_shuffle_ps(Row2, Row2, 0x00);
	XY = _mm_shuffle_ps(Row2, Row2, 0x55);
	XZ = _mm_shuffle_ps(Row2, Row2, 0xAA);
	XW = _mm_shuffle_ps(Row2, Row2, 0xFF);
	Row2 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(XX, rhs.Row0), _mm_mul_ps(XY, rhs.Row1)), _mm_add_ps(_mm_mul_ps(XZ, rhs.Row2), _mm_mul_ps(XW, rhs.Row3)));

	// Fourth Row
	XX = _mm_shuffle_ps(Row3, Row3, 0x00);
	XY = _mm_shuffle_ps(Row3, Row3, 0x55);
	XZ = _mm_shuffle_ps(Row3, Row3, 0xAA);
	XW = _mm_shuffle_ps(Row3, Row3, 0xFF);
	Row3 = _mm_add_ps(_mm_add_ps(_mm_mul_ps(XX, rhs.Row0), _mm_mul_ps(XY, rhs.Row1)), _mm_add_ps(_mm_mul_ps(XZ, rhs.Row2), _mm_mul_ps(XW, rhs.Row3)));
}

bool Matrix::operator == (const Matrix& rhs) const
{
	return _mm_movemask_ps(_mm_cmpneq_ps(Row0, rhs.Row0)) == 0 &&
	       _mm_movemask_ps(_mm_cmpneq_ps(Row1, rhs.Row1)) == 0 &&
	       _mm_movemask_ps(_mm_cmpneq_ps(Row2, rhs.Row2)) == 0 &&
	       _mm_movemask_ps(_mm_cmpneq_ps(Row3, rhs.Row3)) == 0;
}

bool Matrix::operator != (const Matrix& rhs) const
{
	return _mm_movemask_ps(_mm_cmpneq_ps(Row0, rhs.Row0)) != 0 ||
	       _mm_movemask_ps(_mm_cmpneq_ps(Row1, rhs.Row1)) != 0 ||
	       _mm_movemask_ps(_mm_cmpneq_ps(Row2, rhs.Row2)) != 0 ||
	       _mm_movemask_ps(_mm_cmpneq_ps(Row3, rhs.Row3)) != 0;
}

Matrix Matrix::RotationX(float a)
{
	Matrix Result = Identity();
	float CosA = cos(a);
	float SinA = sin(a);

	Result.Row1 = _mm_setr_ps(0, CosA, SinA, 0);
	Result.Row2 = _mm_setr_ps(0, -SinA, CosA, 0);

	return Result;
}

Matrix Matrix::RotationY(float a)
{
	Matrix Result = Identity();
	float CosA = cos(a);
	float SinA = sin(a);

	Result.Row0 = _mm_setr_ps(CosA, 0, -SinA, 0);
	Result.Row2 = _mm_setr_ps(SinA, 0, CosA, 0);

	return Result;
}

Matrix Matrix::RotationZ(float a)
{
	Matrix Result = Identity();
	float CosA = cos(a);
	float SinA = sin(a);

	Result.Row0 = _mm_setr_ps(CosA, SinA, 0, 0);
	Result.Row1 = _mm_setr_ps(-SinA, CosA, 0, 0);

	return Result;
}

Matrix Matrix::Rotation(const Euler& rhs)
{
	Matrix Result = Identity();

	ALIGN(16) float4 SinData;
	ALIGN(16) float4 CosData;

	__m128 Sin, Cos;
	_mm_sincos_ps(_mm_setr_ps(rhs.Pitch, rhs.Yaw, rhs.Roll, 0), Sin, Cos);
	_mm_store_ps(SinData, Sin);
	_mm_store_ps(CosData, Cos);

	Result.Row0 = _mm_setr_ps(CosData.y*CosData.z, CosData.y*SinData.z, -SinData.y, 0);
	Result.Row1 = _mm_setr_ps(SinData.x*SinData.y*CosData.z + CosData.x*-SinData.x,  SinData.x*SinData.y*SinData.z + CosData.x*CosData.z, SinData.x*CosData.y, 0);
	Result.Row2 = _mm_setr_ps(CosData.x*SinData.y*CosData.z + -SinData.x*-SinData.z, CosData.x*SinData.y*SinData.z + -SinData.x*CosData.z, CosData.x*CosData.y, 0);

	return Result;
}

Matrix Matrix::RotationArbi(const Vector& axis, float a)
{
	static const __m128 SignA = _mm_setr_ps(1.0f, -1.0f, 1.0f, 0.0f);
	static const __m128 SignB = _mm_setr_ps(1.0f, 1.0f, -1.0f, 0.0f);
	static const __m128 SignC = _mm_setr_ps(-1.0f, 1.0f, 1.0f, 0.0f);

	Matrix Result;
	float Cos = cos(a);
	float Sin = sin(a);
	float Sum = 1.0f - Cos;

	__m128 SumPS = _mm_set_ps1(Sum);
	__m128 CosPS = _mm_set_ps1(Cos);
	__m128 SinAxis = _mm_mul_ps(_mm_set_ps1(Sin), axis.Data);

	__m128 Tmp = _mm_shuffle_ps(SinAxis, CosPS, _MM_SHUFFLE(0, 0, 1, 2));
	Result.Row0 = _mm_mul_ps(_mm_mul_ps(axis.Data, _mm_shuffle_ps(axis.Data, axis.Data, 0x00)), SumPS);
	Result.Row0 = _mm_add_ps(Result.Row0, _mm_mul_ps(_mm_shuffle_ps(Tmp, Tmp, _MM_SHUFFLE(3, 1, 0, 3)), SignA));

	Tmp = _mm_shuffle_ps(SinAxis, CosPS, _MM_SHUFFLE(0, 0, 0, 2));
	Result.Row1 = _mm_mul_ps(_mm_mul_ps(axis.Data, _mm_shuffle_ps(axis.Data, axis.Data, 0x55)), SumPS);
	Result.Row1 = _mm_add_ps(Result.Row1, _mm_mul_ps(_mm_shuffle_ps(Tmp, Tmp, _MM_SHUFFLE(3, 1, 2, 0)), SignB));

	Result.Row2 = _mm_mul_ps(_mm_mul_ps(axis.Data, _mm_shuffle_ps(axis.Data, axis.Data, 0xAA)), SumPS);
	Result.Row2 = _mm_add_ps(Result.Row2, _mm_mul_ps(_mm_shuffle_ps(SinAxis, CosPS, _MM_SHUFFLE(3, 3, 0, 1)), SignC));

	Result.Row3 = ZERO_W;

	return Result;
}

Matrix Matrix::Translation(float dx, float dy, float dz)
{
	Matrix Result = Identity();
	Result.Row3 = _mm_setr_ps(dx, dy, dz, 1);
	return Result;
}

Matrix Matrix::Translation(float4 translation)
{
	Matrix Result = Identity();
	Result.Row3 = _mm_setr_ps(translation.x, translation.y, translation.z, 1);
	return Result;
}

Matrix Matrix::Scale(float x, float y, float z)
{
	Matrix Result;
	Result.Row0 = _mm_setr_ps(x, 0, 0, 0);
	Result.Row1 = _mm_setr_ps(0, y, 0, 0);
	Result.Row2 = _mm_setr_ps(0, 0, z, 0);
	Result.Row3 = ZERO_W;
	return Result;
}

Matrix Matrix::Scale(float4 scale)
{
	Matrix Result;
	Result.Row0 = _mm_setr_ps(scale.x, 0, 0, 0);
	Result.Row1 = _mm_setr_ps(0, scale.y, 0, 0);
	Result.Row2 = _mm_setr_ps(0, 0, scale.z, 0);
	Result.Row3 = ZERO_W;
	return Result;
}

Matrix Matrix::FromQuaternion(const Quaternion& rhs)
{
	Matrix Result;

	/*
	//--Q2
	x2 = x + x; 
	y2 = y + y; 
	z2 = z + z;
	//--

	//--QA
	xx = x * x2;   
	xy = x * y2;  
	xz = x * z2;
	//--

	//--QB
	yy = y * y2;   
	yz = y * z2;   
	zz = z * z2;
	//--

	//--QC
	wx = w * x2;   
	wy = w * y2;   
	wz = w * z2;
	//--

	Row0.x = 1.0f - (QB.x + QB.z);
	Row0.y = QA.y + QC.z;
	Row0.z = QA.z - QC.y;
	Row0.w = 0;

	Row1.x = QA.y - QC.z;
	Row1.y = 1.0f - (QA.x + QB.z);
	Row1.z = QB.y + QC.x;
	Row1.w = 0;

	Row2.x = QA.z + QC.y;
	Row2.y = QB.y - QC.x;
	Row2.z = 1.0f - (QA.x + QB.x);
	Row2.w = 0;*/

	__m128 Q2 = _mm_add_ps(rhs.Data, rhs.Data);
	__m128 QA = _mm_mul_ps(Q2, _mm_shuffle_ps(rhs.Data, rhs.Data, 0x00));
	__m128 QB = _mm_mul_ps(_mm_shuffle_ps(Q2, Q2, _MM_SHUFFLE(0, 2, 1, 1)), _mm_shuffle_ps(rhs.Data, rhs.Data, _MM_SHUFFLE(0, 2, 2, 1)));
	__m128 QC = _mm_mul_ps(Q2, _mm_shuffle_ps(rhs.Data, rhs.Data, 0xFF));
	__m128 xmm7 = _mm_and_ps(CLEAR_X, _mm_sub_ps(ONE, _mm_add_ps(_mm_shuffle_ps(QB, QA, 0x00), _mm_shuffle_ps(QB, QB, _MM_SHUFFLE(0, 2, 2, 2)))));
	__m128 xmm3 = _mm_xor_ps(QUAT2MAT_FLIP1, _mm_shuffle_ps(QC, xmm7, _MM_SHUFFLE(0, 0, 1, 2)));
	__m128 xmm4 = _mm_add_ps(_mm_shuffle_ps(QA, xmm7, _MM_SHUFFLE(0, 1, 2, 1)), xmm3);
	Result.Row0 = _mm_shuffle_ps(xmm4, xmm4, _MM_SHUFFLE(3, 1, 0, 2));
	__m128 xmm0 = _mm_add_ps(_mm_shuffle_ps(QA, QB, _MM_SHUFFLE(1, 1, 2, 1)), _mm_xor_ps(QUAT2MAT_FLIP0, _mm_shuffle_ps(QC, QC, _MM_SHUFFLE(0, 0, 1, 2))));
	Result.Row2 = _mm_shuffle_ps(xmm0, xmm7, _MM_SHUFFLE(0, 3, 3, 1));
	__m128 Tmp = _mm_shuffle_ps(xmm0, xmm7, _MM_SHUFFLE(0, 2, 0, 2));
	Result.Row1 = _mm_shuffle_ps(Tmp, Tmp, _MM_SHUFFLE(3, 0, 2, 1));
	Result.Row3 = ZERO_W;
	
	return Result;
}

//-- Private Methods --//
//=====================//
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2017 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cmath>
#include <xmmintrin.h>
#include <SKSystem/Memory.h>
#include <SKSystem/Math/Euler.h>
#include <SKSystem/Math/Types.h>

namespace SK
{
	namespace Math
	{

		//---- Types ----//
		//===============//

		template<int32_t base, int32_t exp, typename Enable=void>
		struct pow;

		// Positive exponent
		template<int32_t base, int32_t exp>
		struct pow<base, exp, std::enable_if_t<(exp > 0)>>
		{
			static const int32_t value = base * pow<base, exp - 1>::value;
		};

		template<uint32_t base>
		struct pow<base, 0, void>
		{
			static const int32_t value = 1;
		};

		//--- Methods ---//
		//===============//
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

// Forced to use pointers and a Release function to avoid MS bug
// http://connect.microsoft.com/VisualStudio/feedback/details/747145

namespace SK
{
	//---- Types ----//
	//===============//

	template<typename T>
	class Singleton
	{
	public:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		static T& Instance()
		{
			if (!m_Instance)
			{
				m_Instance = new T();
			}

			return *m_Instance;
		}

		static void Release()
		{
			delete m_Instance;
			m_Instance = nullptr;
		}
		
		//-- Variables --//
		//===============//

	protected:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		Singleton() {}

		//-- Variables --//
		//===============//

	private:
		//---- Types ----//
		//===============//

		//--- Methods ---//
		//===============//

		Singleton(const Singleton& rhs) = delete;

		//-- Variables --//
		//===============//

		static T* m_Instance;
	};

	template<typename T>
	T* Singleton<T>::m_Instance = nullptr;
}
﻿##=============================================================##
##                                                             ##
##                         SKEngineV2                          ##
##   -------------------------------------------------------   ##
##  Copyright © 2013 - 2014 Anthony Mushrow                    ##
##  All rights reserved                                        ##
##                                                             ##
##  This code is licensed under The Code Project Open License  ##
##  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     ##
##  for more information.                                      ##
##                                                             ##

##=============================================================##
import bpy
from bpy.props import EnumProperty, StringProperty, PointerProperty, BoolProperty

class SKMaterialProps(bpy.types.PropertyGroup):
    @classmethod
    def register(cls):

        bpy.types.Material.SKMaterialProps = PointerProperty(
            name="SKProperties",
            description="Custom material properties for SKEngine",
            type=cls,
            )

        cls.sortOrder = EnumProperty(name="Sort Order", default="1", items = [("0", "First", "", 0), ("1", "Normal", "", 1), ("2", "Last", "", 2)])
        cls.vertexFormat = EnumProperty(name="Vertex Format", default="1", items = [("0", "Vertex2D", "", 0), ("1", "Vertex3D", "", 1), ("2", "Vertex3D_TC", "", 2)])
        cls.vertexShader = StringProperty(name="Vertex Shader", default="Standard 3D")
        cls.pixelShader = StringProperty(name="Pixel Shader", default="Standard Lit")
        cls.cullable = BoolProperty(name="Cullable", default=True);

    @classmethod
    def unregister(cls):
        del bpy.types.Material.SKMaterialProps

def register():
    bpy.utils.register_class(SKMaterialProps)

def unregister():
    bpy.utils.unregister_class(SKMaterialProps)
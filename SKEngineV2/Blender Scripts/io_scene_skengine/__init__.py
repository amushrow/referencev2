﻿##=============================================================##
##                                                             ##
##                         SKEngineV2                          ##
##   -------------------------------------------------------   ##
##  Copyright © 2013 - 2014 Anthony Mushrow                    ##
##  All rights reserved                                        ##
##                                                             ##
##  This code is licensed under The Code Project Open License  ##
##  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     ##
##  for more information.                                      ##
##                                                             ##
##=============================================================##

bl_info = {
    "name":         "SKEngine - Exporter",
    "author":       "Anthony Mushrow",
    "blender":      (2,6,7),
    "version":      (0,0,1),
    "location":     "File > Import-Export",
    "description":  "Export meshes to SKEngine format",
    "category":     "Import-Export"
}
        
import bpy
from io_scene_skengine import exporter

def menu_func(self, context):
    self.layout.operator("export_skengine_mesh.skmesh", text="SKEngine Mesh(.skmesh)");

def register():
    bpy.utils.register_module(__name__);
    bpy.types.INFO_MT_file_export.append(menu_func);
    
def unregister():
    bpy.utils.unregister_module(__name__);
    bpy.types.INFO_MT_file_export.remove(menu_func);

if __name__ == "__main__":
    register()
﻿##=============================================================##
##                                                             ##
##                         SKEngineV2                          ##
##   -------------------------------------------------------   ##
##  Copyright © 2013 - 2014 Anthony Mushrow                    ##
##  All rights reserved                                        ##
##                                                             ##
##  This code is licensed under The Code Project Open License  ##
##  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     ##
##  for more information.                                      ##
##                                                             ##
##=============================================================##

import struct;
import bpy
from bpy_extras.io_utils import ExportHelper, axis_conversion
import mathutils

AXIS_TRANSFORM = mathutils.Matrix(((1,0,0,0),(0,0,1,0),(0,1,0,0),(0,0,0,1)))
REWIND_TRIS = True

class float2:
    def __init__(self, x = 0.0, y = 0.0):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def write(self, file):
        file.write(struct.pack("<ff", self.x, self.y))

class float3:
    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
        self.x = x
        self.y = y
        self.z = z

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z

    def write(self, file):
        file.write(struct.pack("<fff", self.x, self.y, self.z))

class float4:
    def __init__(self, x = 0.0, y = 0.0, z = 0.0, w = 0.0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z and self.w == other.w

    def write(self, file):
        file.write(struct.pack("<ffff", self.x, self.y, self.z, self.w))

class Matrix:
    def __init__(self):
        self.row0 = float4()
        self.row1 = float4()
        self.row2 = float4()
        self.row3 = float4()

    def write(self, file):
        self.row0.write(file)
        self.row1.write(file)
        self.row2.write(file)
        self.row3.write(file)

class Vertex_All:
    def __init__(self):
        self.Position = float3()
        self.Normal   = float3()
        self.TexCoord = float2()
        self.Tangent  = float3()
        self.Colour   = float4()
        self.Reserved = 0.0

        self.Duplicate = -1
        self.InUse = False

class SKMaterial:
    def __init__(self):
        self.Cullable = False
        self.VertexShader = ""
        self.PixelShader = ""
        self.SortOrder = 1
        self.VertexFormat = 1
        self.Textures = [None] * 16

    def write(self, file):
        file.write(struct.pack("<?II", self.Cullable, self.SortOrder, self.VertexFormat))
        ShaderBytes = bytes(self.VertexShader, "utf-8")
        file.write(struct.pack("<Q", len(ShaderBytes)))
        file.write(struct.pack("<{0}s".format(len(ShaderBytes)), ShaderBytes))
        ShaderBytes = bytes(self.PixelShader, "utf-8")
        file.write(struct.pack("<Q", len(ShaderBytes)))
        file.write(struct.pack("<{0}s".format(len(ShaderBytes)), ShaderBytes))

        for i in range(0, 16):
            if (self.Textures[i] != None):
                TextureBytes = bytes(self.Textures[i], "utf-8")
                file.write(struct.pack("<Q", len(TextureBytes)))
                file.write(struct.pack("<{0}s".format(len(TextureBytes)), TextureBytes))
            else:
                file.write(struct.pack("<Q", 0))

class SubMesh:
    def __init__(self):
        self.Offset = 0
        self.Count = 0
    
    def write(self, file):
        file.write(struct.pack("<II", self.Offset, self.Count))

class Bone:
    def __init__(self):
        self.InvPoseTransform =  Matrix()
        self.LocalTransform = Matrix()
        self.Children = []
        self.Name = ""
        self.BlenderBone = None

    def write(self, file):
        NameBytes = bytes(self.Name, "utf-8")
        file.write(struct.pack("<Q", len(NameBytes)))
        file.write(struct.pack("<{0}s".format(len(NameBytes)), NameBytes))
        self.InvPoseTransform.write(file)
        self.LocalTransform.write(file)
        for i in range(len(self.Children)):
            self.Children[i].write(file)

class Mesh:
    def __init__(self):
        self.Name = ""
        self.Materials = []
        self.SubMeshes = []
        self.Transform = Matrix()
        self.NumVerts = 0
        self.NumIndis = 0
        self.NumChildren = 0
        self.NumBones = 0
        self.Verts = []
        self.Indis = []
        self.Children = []
        self.Bones = []
        self.BoundingBox = [float3()] * 8
        self.Radius = 0.0

    def write(self, file):
        file.write(struct.pack("<IIII", self.NumVerts, self.NumIndis, self.NumChildren, self.NumBones))
        NameBytes = bytes(self.Name, "utf-8")
        file.write(struct.pack("<Q", len(NameBytes)))
        file.write(struct.pack("<{0}s".format(len(NameBytes)), NameBytes))

        file.write(struct.pack("<I", len(self.Materials)))
        for i in range(len(self.Materials)):
            self.Materials[i].write(file)
            self.SubMeshes[i].write(file)

        self.Transform.write(file)

        for i in range(0,8):
            self.BoundingBox[i].write(file)

        file.write(struct.pack("<f", self.Radius))

        if self.Materials[0].VertexFormat == 0:
            #Vertex2D
            for vertex in self.Verts:
                file.write(struct.pack("<ff", vertex.Position.x, vertex.Position.y))
                vertex.TexCoord.write(file)
                vertex.Colour.write(file)

        elif self.Materials[0].VertexFormat == 1:
            #Vertex3D
            for vertex in self.Verts:
                vertex.Position.write(file)
                vertex.Normal.write(file)
                vertex.TexCoord.write(file)
        else:
            #Vertex3D_TC
            for vertex in self.Verts:
                vertex.Position.write(file)
                vertex.Normal.write(file)
                vertex.TexCoord.write(file)
                vertex.Tangent.write(file)
                vertex.Colour.write(file)
                file.write(struct.pack("<f", vertex.Reserved))

        for index in self.Indis:
            file.write(struct.pack("<H", index))

        for bone in self.Bones:
            bone.write(file)

        for child in self.Children:
            child.write(file)

class MeshFile:
    def __init__(self):
        self.NumMeshes = 0
        self.Meshes = []

    def write(self, file):
        file.write(struct.pack("<6s", b"SKMESH"))
        file.write(struct.pack('<I', self.NumMeshes))
        for mesh in self.Meshes:
            mesh.write(file)

def GetEmpty(object, context, mat = None):
    NewMesh = Mesh()
    NewMesh.Name = object.name
    NewMesh.Materials.append(SKMaterial())
    NewMesh.SubMeshes.append(SubMesh())

    WorldMatrix = (AXIS_TRANSFORM * (AXIS_TRANSFORM * object.matrix_world).inverted()).inverted().transposed()
    
    NewMesh.Transform.row0.x = WorldMatrix.row[0].x
    NewMesh.Transform.row0.y = WorldMatrix.row[0].y
    NewMesh.Transform.row0.z = WorldMatrix.row[0].z
    NewMesh.Transform.row0.w = WorldMatrix.row[0].w
    NewMesh.Transform.row1.x = WorldMatrix.row[1].x
    NewMesh.Transform.row1.y = WorldMatrix.row[1].y
    NewMesh.Transform.row1.z = WorldMatrix.row[1].z
    NewMesh.Transform.row1.w = WorldMatrix.row[1].w
    NewMesh.Transform.row2.x = WorldMatrix.row[2].x
    NewMesh.Transform.row2.y = WorldMatrix.row[2].y
    NewMesh.Transform.row2.z = WorldMatrix.row[2].z
    NewMesh.Transform.row2.w = WorldMatrix.row[2].w
    NewMesh.Transform.row3.x = WorldMatrix.row[3].x
    NewMesh.Transform.row3.y = WorldMatrix.row[3].y
    NewMesh.Transform.row3.z = WorldMatrix.row[3].z
    NewMesh.Transform.row3.w = WorldMatrix.row[3].w

    for child in object.children:
       if child.type == 'MESH':
           NewMesh.Children.append(GetMesh(child, context))
           NewMesh.NumChildren += 1
       elif child.type == 'EMPTY':
           NewMesh.Children.append(GetEmpty(child, context))
           NewMesh.NumChildren += 1

    return NewMesh

def FixupChildBoneIds(boneList):
    for bone in boneList:
        for childBone in bone.BlenderBone.children:
            for i in range(len(boneList)):
                if (boneList[i].BlenderBone == childBone):
                    bone.Children.append(i)
                    break

def GetBones(armature):
    Bones = []
    ParentId = -1
    for bone in Armature.data.bones:
        NewBone = Bone()
        NewBone.Name = bone.name
        NewBone.BlenderBone = bone

        WorldMatrix = (AXIS_TRANSFORM * (AXIS_TRANSFORM * bone.matrix).inverted()).inverted().transposed()

        NewMesh.Transform.row0.x = WorldMatrix.row[0].x
        NewMesh.Transform.row0.y = WorldMatrix.row[0].y
        NewMesh.Transform.row0.z = WorldMatrix.row[0].z
        NewMesh.Transform.row0.w = WorldMatrix.row[0].w
        NewMesh.Transform.row1.x = WorldMatrix.row[1].x
        NewMesh.Transform.row1.y = WorldMatrix.row[1].y
        NewMesh.Transform.row1.z = WorldMatrix.row[1].z
        NewMesh.Transform.row1.w = WorldMatrix.row[1].w
        NewMesh.Transform.row2.x = WorldMatrix.row[2].x
        NewMesh.Transform.row2.y = WorldMatrix.row[2].y
        NewMesh.Transform.row2.z = WorldMatrix.row[2].z
        NewMesh.Transform.row2.w = WorldMatrix.row[2].w
        NewMesh.Transform.row3.x = WorldMatrix.row[3].x
        NewMesh.Transform.row3.y = WorldMatrix.row[3].y
        NewMesh.Transform.row3.z = WorldMatrix.row[3].z
        NewMesh.Transform.row3.w = WorldMatrix.row[3].w

        Bones.append(NewBone)

    FixupChildBoneIds(Bones)

    return Bones

def GetMesh(object, context):
    NewMesh = Mesh()
    NewMesh.Name = object.name

    for i in range(0,8):
        Vec = mathutils.Vector((object.bound_box[i][0], object.bound_box[i][1], object.bound_box[i][2]))
        Vec = AXIS_TRANSFORM * Vec;
        NewMesh.BoundingBox[i] = float3(Vec.x, Vec.y, Vec.z)
            
        if Vec.length > NewMesh.Radius:
            NewMesh.Radius = Vec.length

    WorldMatrix = (AXIS_TRANSFORM * (AXIS_TRANSFORM * object.matrix_world).inverted()).inverted().transposed()
   
    NewMesh.Transform.row0.x = WorldMatrix.row[0].x
    NewMesh.Transform.row0.y = WorldMatrix.row[0].y
    NewMesh.Transform.row0.z = WorldMatrix.row[0].z
    NewMesh.Transform.row0.w = WorldMatrix.row[0].w
    NewMesh.Transform.row1.x = WorldMatrix.row[1].x
    NewMesh.Transform.row1.y = WorldMatrix.row[1].y
    NewMesh.Transform.row1.z = WorldMatrix.row[1].z
    NewMesh.Transform.row1.w = WorldMatrix.row[1].w
    NewMesh.Transform.row2.x = WorldMatrix.row[2].x
    NewMesh.Transform.row2.y = WorldMatrix.row[2].y
    NewMesh.Transform.row2.z = WorldMatrix.row[2].z
    NewMesh.Transform.row2.w = WorldMatrix.row[2].w
    NewMesh.Transform.row3.x = WorldMatrix.row[3].x
    NewMesh.Transform.row3.y = WorldMatrix.row[3].y
    NewMesh.Transform.row3.z = WorldMatrix.row[3].z
    NewMesh.Transform.row3.w = WorldMatrix.row[3].w

    for Material in object.data.materials:
        if Material != None:
            SKProps = Material.SKMaterialProps
            NewMat = SKMaterial()
            NewMat.Cullable = SKProps.cullable
            NewMat.SortOrder = int(SKProps.sortOrder)
            NewMat.VertexFormat = int(SKProps.vertexFormat)
            NewMat.VertexShader = SKProps.vertexShader
            NewMat.PixelShader = SKProps.pixelShader

            TextureSlots = Material.texture_slots
            for i in range(0,16):
                if TextureSlots[i] and TextureSlots[i].texture.type == "IMAGE":
                    NewMat.Textures[i] = TextureSlots[i].texture.image.filepath
                else:
                    NewMat.Textures[i] = ""

            NewMesh.Materials.append(NewMat)
            NewMesh.SubMeshes.append(SubMesh())

    MeshData = object.to_mesh(context.scene, True, 'PREVIEW')

    has_uv = bool(MeshData.uv_textures)
    has_vertexCol = bool(MeshData.vertex_colors) and (NewMesh.Materials[0].VertexFormat != 1)

    if has_uv:
        if MeshData.uv_layers.active:
            active_uv_layer = MeshData.uv_layers.active.data
        else:
            has_uv = False

    if has_vertexCol:
        if MeshData.vertex_colors.active:
            active_col_layer = MeshData.vertex_colors.active.data
        else:
            has_vertexCol = False

    #Separate out each material
    MaterialFaces = []
    for i in range(len(MeshData.materials)):
        MaterialFaces.append([])

    for face in MeshData.polygons:
        MaterialFaces[face.material_index].append(face)  

    #SKTD: Calc tangent
    for vertex in MeshData.vertices:
        NewVert = Vertex_All()
        VertexPos = AXIS_TRANSFORM * vertex.co
        NewVert.Position.x = VertexPos.x
        NewVert.Position.y = VertexPos.y
        NewVert.Position.z = VertexPos.z
        NewMesh.Verts.append(NewVert)

    for MaterialIndex, Faces in enumerate(MaterialFaces):
        NewMesh.SubMeshes[MaterialIndex].Offset = NewMesh.NumIndis
        for face in Faces:
            UV = float2()
            Normal = float3()
            Colour = float4()

            FaceIndices = []
            for i in face.loop_indices:
                Vi = MeshData.loops[i].vertex_index
                if face.use_smooth:
                    MeshNormal = AXIS_TRANSFORM * MeshData.vertices[Vi].normal
                    Normal = float3(MeshNormal.x, MeshNormal.y, MeshNormal.z)
                else:
                    MeshNormal = AXIS_TRANSFORM * face.normal
                    Normal = float3(MeshNormal.x, MeshNormal.y, MeshNormal.z)

                if has_uv:
                    UV = float2(active_uv_layer[i].uv.x, 1.0 - active_uv_layer[i].uv.y)
                if has_vertexCol:
                    Colour = float4(1.0, active_col_layer[i].color.r, active_uv_layer[i].color.g, active_uv_layer[i].color.b)

                while Vi != -1:
                    if (NewMesh.Verts[Vi].InUse == False or (NewMesh.Verts[Vi].Normal == Normal and NewMesh.Verts[Vi].Colour == Colour and NewMesh.Verts[Vi].TexCoord == UV)):
                        #Same vertex data, we can share it
                        NewMesh.Verts[Vi].Colour = Colour
                        NewMesh.Verts[Vi].TexCoord = UV
                        NewMesh.Verts[Vi].Normal = Normal
                        NewMesh.Verts[Vi].InUse = True
                        FaceIndices.append(Vi)
                        break
                    else:
                        # See if we have a copy of this vertex with different attributes
                        Vi = NewMesh.Verts[Vi].Duplicate
            
                if Vi == -1:
                    #Different vertex data, make a copy
                    OldVi = MeshData.loops[i].vertex_index

                    NewVert = Vertex_All()
                    NewVert.Position.x = NewMesh.Verts[OldVi].Position.x
                    NewVert.Position.y = NewMesh.Verts[OldVi].Position.y
                    NewVert.Position.z = NewMesh.Verts[OldVi].Position.z
                    NewVert.Colour = Colour
                    NewVert.TexCoord = UV
                    NewVert.Normal = Normal
                    NewVert.InUse = True
                    NewVert.Duplicate = -1

                    Vi = len(NewMesh.Verts)
                    NewMesh.Verts[OldVi].Duplicate = Vi
                    NewMesh.Verts.append(NewVert)
                    FaceIndices.append(Vi)
                        

            if len(FaceIndices) == 3:
                if REWIND_TRIS:
                    NewMesh.Indis.append(FaceIndices[0])
                    NewMesh.Indis.append(FaceIndices[2])
                    NewMesh.Indis.append(FaceIndices[1])
                else:
                    NewMesh.Indis.append(FaceIndices[0])
                    NewMesh.Indis.append(FaceIndices[1])
                    NewMesh.Indis.append(FaceIndices[2])
                NewMesh.NumIndis += 3

            elif len(FaceIndices) == 4:
                if REWIND_TRIS:
                    NewMesh.Indis.append(FaceIndices[0])
                    NewMesh.Indis.append(FaceIndices[2])
                    NewMesh.Indis.append(FaceIndices[1])
                    NewMesh.Indis.append(FaceIndices[0])
                    NewMesh.Indis.append(FaceIndices[3])
                    NewMesh.Indis.append(FaceIndices[2])
                else:
                    NewMesh.Indis.append(FaceIndices[0])
                    NewMesh.Indis.append(FaceIndices[1])
                    NewMesh.Indis.append(FaceIndices[2])
                    NewMesh.Indis.append(FaceIndices[0])
                    NewMesh.Indis.append(FaceIndices[2])
                    NewMesh.Indis.append(FaceIndices[3])
                NewMesh.NumIndis += 6
           
        NewMesh.SubMeshes[MaterialIndex].Count = NewMesh.NumIndis - NewMesh.SubMeshes[MaterialIndex].Offset

    NewMesh.NumVerts = len(NewMesh.Verts)

    #Get bones
    Armature = object.find_armature()
    if Armature != None:
        NewMesh.Bones = GetBones(Armature)
        NewMesh.NumBones = len(NewMesh.Bones)

    print("Num Verts in mesh: {0}".format(NewMesh.NumVerts))
    for child in object.children:
        if child.type == 'MESH':
            NewMesh.Children.append(GetMesh(child, context))
            NewMesh.NumChildren += 1
        elif child.type == 'EMPTY':
            NewMesh.Children.append(GetEmpty(child, context))
            NewMesh.NumChildren += 1

    return NewMesh

class SKEngineMeshExporter(bpy.types.Operator, ExportHelper):
    bl_idname       = "export_skengine_mesh.skmesh";
    bl_label        = "Export SKMesh";
    bl_options      = {'PRESET'};
    
    filename_ext    = ".skmesh";
    
    def execute(self, context):
        MeshFileInst = MeshFile()
        
        for object in context.scene.objects:
            if object.parent == None:
                if object.type == 'EMPTY':
                    MeshFileInst.Meshes.append(GetEmpty(object, context))
                    MeshFileInst.NumMeshes += 1
                elif object.type == 'MESH':
                    MeshFileInst.Meshes.append(GetMesh(object, context))
                    MeshFileInst.NumMeshes += 1

        OutputFile = open(self.filepath, 'bw')
        MeshFileInst.write(OutputFile)
        OutputFile.close()

        return {'FINISHED'};
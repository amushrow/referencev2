#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKGraphics/Renderable.h>
#include <SKSystem/Math/Vector.h>
#include <SKSystem/Events.h>
#include <SKSystem/Array.h>
#include <vector>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		class RenderPass;
		class Renderable;

		class TreeUpdatedArgs
		{
		public:
			TreeUpdatedArgs(Renderable* child) { }

			Renderable* Child() { return m_Child; }

		private:

			Renderable* m_Child;
		};

		class RenderTree
		{
		public:

			//---- Types ----//
			//===============//

			friend class Renderable;

			//--- Methods ---//
			//===============//

			RenderTree(uint32_t maxRenderables);

			void     Add(Renderable* child);
			void     Remove(Renderable* child);
			void     Update();
			uint32_t MaxRenderables() const { return m_MaxRenderables; }
			uint32_t NumRenderables() const { return m_NumRenderables; }

			std::vector<Renderable*>::iterator begin() { return m_Tree.begin(); }
			std::vector<Renderable*>::iterator end()   { return m_Tree.end(); }

			//-- Variables --//
			//===============//

			EventDelegate<TreeUpdatedArgs> ChildAdded;
			EventDelegate<TreeUpdatedArgs> ChildRemoved;

		private:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//
			
			void OnChildAdded(Renderable* newChild);
			void OnChildRemoved(Renderable* oldChild);

			void AddChildToList(Renderable* child);
			void RemoveChildFromList(Renderable* child);

			//-- Variables --//
			//===============//
			
			Event<TreeUpdatedArgs>   m_ChildAdded;
			Event<TreeUpdatedArgs>   m_ChildRemoved;
			std::vector<Renderable*> m_Tree;
			uint32_t                 m_MaxRenderables;
			uint32_t                 m_NumRenderables;
		};

		//--- Methods ---//
		//===============//
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Math/Matrix.h>
#include <SKSystem/Math/Quaternion.h>
#include <SKSystem/Math/Vector.h>
#include <SKGraphics/Culling.h>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		class Camera
		{
		public:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			Camera();

			Math::Vector     Position() const { return m_Position; }
			void             Position(const Math::Vector& position);
			Math::Quaternion Orientation() const { return m_Orientation; }
			void             Orientation(const Math::Quaternion& orientation);
			float            FoV() const { return m_FoV; }
			void             FoV(float fov);
			float            Near() const { return m_Near; }
			float            Far() const { return m_Far; }
			void             Aspect(float aspect);
			void             ClippingPlanes(float near, float far);
			void             LookAt(const Math::Vector& target, const Math::Vector& up = Math::Vector::Up());

			Math::Matrix     ProjectionMatrix();
			Math::Matrix     BackwardsProjectionMatrix();
			Math::Matrix     ViewMatrix();
			Math::Matrix     ViewProjMatrix();
			const Frustum&   Frustum();

			//-- Variables --//
			//===============//

		private:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			Math::Quaternion  m_Orientation;
			Math::Vector      m_Position;
			
			float             m_FoV;
			float             m_Near;
			float             m_Far;
			float             m_Aspect;
			bool              m_RecalcProjection;
			bool              m_RecalcView;
			bool              m_RecalcViewProj;
			bool              m_RecalcFrustum;

			Math::Matrix      m_ProjectionMatrix;
			Math::Matrix      m_ViewMatrix;
			Math::Matrix      m_ViewProjMatrix;

			Graphics::Frustum m_Frustum;
		};

		//--- Methods ---//
		//===============//
	}
}
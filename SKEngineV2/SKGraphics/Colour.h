#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstdint>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		class ColourABGR;
		class ColourARGB;
		class ColourFloat;

		class ColourARGB
		{
		public:

			//--- Methods ---//
			//===============//

			ColourARGB() : m_Colour(0xFF000000)	{ }

			ColourARGB(uint8_t a, uint8_t r, uint8_t g, uint8_t b)
			: m_Colour((a << 24) | (r << 16) | (g << 8) | b)
			{

			}

			explicit ColourARGB(const ColourABGR& other);
			explicit ColourARGB(const ColourFloat& other);

			uint32_t Value() const { return m_Colour; }
			uint8_t  A() const     { return (m_Colour & 0xFF000000) >> 24; }
			uint8_t  R() const     { return (m_Colour & 0x00FF0000) >> 16; }
			uint8_t  G() const     { return (m_Colour & 0x0000FF00) >> 8; }
			uint8_t  B() const     { return m_Colour & 0x000000FF; }

		private:

			//-- Variables --//
			//===============//

			uint32_t m_Colour;

		};

		class ColourABGR
		{
		public:

			//--- Methods ---//
			//===============//

			ColourABGR() : m_Colour(0xFF000000)	{ }

			ColourABGR(uint8_t a, uint8_t b, uint8_t g, uint8_t r)
			: m_Colour((a << 24) | (b << 16) | (g << 8) | r)
			{

			}

			explicit ColourABGR(const ColourARGB& other);
			explicit ColourABGR(const ColourFloat& other);

			uint32_t Value() const { return m_Colour; }
			uint8_t  A() const     { return (m_Colour & 0xFF000000) >> 24; }
			uint8_t  B() const     { return (m_Colour & 0x00FF0000) >> 16; }
			uint8_t  G() const     { return (m_Colour & 0x0000FF00) >> 8; }
			uint8_t  R() const     { return m_Colour & 0x000000FF; }

		private:

			//-- Variables --//
			//===============//

			uint32_t m_Colour;
		};

		class ColourFloat
		{
		public:

			//--- Methods ---//
			//===============//

			ColourFloat()
			: m_R(0.0f), m_G(0.0f), m_B(0.0f), m_A(1.0f)
			{

			}

			ColourFloat(float a, float r, float g, float b)
			: m_R(r), m_G(g), m_B(b), m_A(a)
			{

			}

			explicit ColourFloat(const ColourARGB& other);
			explicit ColourFloat(const ColourABGR& other);

			float A() const { return m_A; }
			float R() const { return m_R; }
			float G() const { return m_G; }
			float B() const { return m_B; }

		private:

			//-- Variables --//
			//===============//

			float m_R;
			float m_G;
			float m_B;
			float m_A;
		};
	}
}

//--- Methods ---//
//===============//

inline SK::Graphics::ColourARGB::ColourARGB(const SK::Graphics::ColourABGR& other)
{
	uint32_t ABGR = other.Value();
	m_Colour = ((ABGR >> 24) << 24) | ((ABGR >> 16) & 0xFF) | ((ABGR >> 8) & 0xFF) << 8 | ((ABGR) & 0xFF) << 16;
}

inline SK::Graphics::ColourARGB::ColourARGB(const SK::Graphics::ColourFloat& other)
{
	uint32_t A = static_cast<uint32_t>(other.A() * 255);
	uint32_t R = static_cast<uint32_t>(other.R() * 255);
	uint32_t G = static_cast<uint32_t>(other.G() * 255);
	uint32_t B = static_cast<uint32_t>(other.B() * 255);

	m_Colour = (A >> 24) | (R >> 16) | (G >> 8) << 8 | B;
}

inline SK::Graphics::ColourABGR::ColourABGR(const SK::Graphics::ColourARGB& other)
{
	uint32_t ARGB = other.Value();
	m_Colour = ((ARGB >> 24) << 24) | ((ARGB >> 16) & 0xFF) | ((ARGB >> 8) & 0xFF) << 8 | ((ARGB) & 0xFF) << 16;
}

inline SK::Graphics::ColourABGR::ColourABGR(const SK::Graphics::ColourFloat& other)
{
	uint32_t A = static_cast<uint32_t>(other.A() * 255);
	uint32_t B = static_cast<uint32_t>(other.B() * 255);
	uint32_t G = static_cast<uint32_t>(other.G() * 255);
	uint32_t R = static_cast<uint32_t>(other.R() * 255);

	m_Colour = (A >> 24) | (B >> 16) | (G >> 8) << 8 | R;
}

inline SK::Graphics::ColourFloat::ColourFloat(const SK::Graphics::ColourARGB& other)
{
	m_A = static_cast<float>(other.A()) / 255.0f;
	m_R = static_cast<float>(other.R()) / 255.0f;
	m_G = static_cast<float>(other.G()) / 255.0f;
	m_B = static_cast<float>(other.B()) / 255.0f;
}

inline SK::Graphics::ColourFloat::ColourFloat(const SK::Graphics::ColourABGR& other)
{
	m_A = static_cast<float>(other.A()) / 255.0f;
	m_R = static_cast<float>(other.R()) / 255.0f;
	m_G = static_cast<float>(other.G()) / 255.0f;
	m_B = static_cast<float>(other.B()) / 255.0f;
}
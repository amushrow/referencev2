//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2016 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Time/Clocks.h>
#include <SKGraphics/RenderPass.h>
#include <SKSystem/Memory.h>
#include <algorithm>
using namespace SK::Graphics;

//-- Declarations --//
//==================//

namespace
{
	struct
	{
		SK::Math::Vector CameraPosition;
	} _SortData;
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

RenderPass::RenderPass(uint32_t threadPoolSize)
: m_ThreadPool(threadPoolSize)
, m_RenderTree(nullptr)
, m_Camera(nullptr)
, m_Data(nullptr)
, m_OcclusionData(nullptr)
, m_RenderTarget(nullptr)
, m_DepthBuffer(nullptr)
, m_OverrideVertex(INVALID_ID)
, m_OverridePixel(INVALID_ID)
, m_EventKey(SK::GetEventKey())
{
	
}

RenderPass::~RenderPass()
{
	delete m_OcclusionData;
	delete m_Data;
}

void RenderPass::SetCamera(Camera* camera)
{
	m_Camera = camera;
}

void RenderPass::SetRenderTree(RenderTree* tree)
{
	if (m_RenderTree != tree)
	{
		if (m_RenderTree)
		{
			m_RenderTree->ChildAdded.Deregister(m_EventKey);
			m_RenderTree->ChildRemoved.Deregister(m_EventKey);
		}

		m_RenderTree = tree;

		m_Data = new Data(m_RenderTree->MaxRenderables());

		if (m_RenderTree)
		{
			using std::placeholders::_1;
			using std::placeholders::_2;
			m_RenderTree->ChildAdded.Register(m_EventKey, std::bind(&RenderPass::ChildAdded, this, _1, _2));
			m_RenderTree->ChildRemoved.Register(m_EventKey, std::bind(&RenderPass::ChildRemoved, this, _1, _2));
		}
	}
}

void RenderPass::SetRenderTarget(TexturePtr texture)
{
	m_RenderTarget = texture;
}

void RenderPass::SetDepthBuffer(TexturePtr texture)
{
	m_DepthBuffer = texture;
}

void RenderPass::SetOverrideTechnique(size_t vertexShaderId, size_t pixelShaderId)
{
	m_OverrideVertex = vertexShaderId;
	m_OverridePixel = pixelShaderId;
}

void RenderPass::EnableOcclusionCulling(SK::Math::int2& resolution, uint32_t maxRenderedTris)
{
	delete m_OcclusionData;
	m_OcclusionData = new OcclusionData(resolution, m_RenderTree->MaxRenderables(), maxRenderedTris);
}

void RenderPass::DisableOcclusionCulling()
{
	delete m_OcclusionData;
	m_OcclusionData = nullptr;
}

void RenderPass::Prepare()
{
	SK::Debug::Assert(m_RenderTree != nullptr, "RenderPass::Prepare - No render tree has been set.");
	_SortData.CameraPosition = m_Camera->Position();
	
	if (m_RenderTree)
	{
		if (m_Data->Rebuild)
		{
			for (auto& child : *m_RenderTree)
				AddRenderable(child);

			m_Data->Boxes.InitialiseHiBoxes();
		}

		auto& RefList = m_Data->Ref;
		const uint32_t Length = m_Data->NumRenderables;

		memset(m_Data->Occluder, 0, Length);

		/*SK::Time::HighPerformanceClock Clock;
		auto StartTime = Clock.Time();
		static float Least = FLT_MAX;
		Least = std::min(Least, (Clock.Time() - StartTime).Milliseconds<float>());*/

		FrustumCulling();
		
		
		// Mark PreCulled objects
		// Process 16 flags at a time
		uint32_t IterCount = Length >> 4;
		for (uint32_t i = 0; i < IterCount; ++i)
		{
			__m128i Culled = _mm_load_si128(reinterpret_cast<__m128i*>(&m_Data->Boxes.Culled[i << 4]));
			__m128i PreCulled = _mm_load_si128(reinterpret_cast<__m128i*>(&m_Data->PreCulled[i << 4]));
			_mm_store_si128(reinterpret_cast<__m128i*>(&m_Data->Boxes.Culled[i << 4]), _mm_or_si128(Culled, PreCulled));
		}
		for (uint32_t i = IterCount << 4; i < Length; ++i)
		{
			m_Data->Boxes.Culled[i] |= m_Data->PreCulled[i];
		}

		GetCameraDistance();

		if (m_OcclusionData)
		{
			// Sort occluders front to back
			uint32_t* Occluder32 = reinterpret_cast<uint32_t*>(m_Data->Occluder);
			m_OcclusionData->OccluderListSize = 0;
			IterCount = Length >> 2;
			for (uint32_t i = 0; i < IterCount; ++i)
			{
				if (!Occluder32[i])
					continue;
			
				uint32_t Index = i << 2;
				for (uint32_t k = 0; k < 4; ++k, ++Index)
				{
					if (m_Data->Occluder[Index] && (!m_Data->Boxes.Culled[Index] || !m_Data->Cullable[Index]))
					{
						m_OcclusionData->OccluderList[m_OcclusionData->OccluderListSize++] = { m_Data->CameraDistance[Index], Index };
					}
				}
			}
			for (uint32_t i = IterCount << 2; i < Length; ++i)
			{
				if (m_Data->Occluder[i] && (!m_Data->Boxes.Culled[i] || !m_Data->Cullable[i]))
				{
					m_OcclusionData->OccluderList[m_OcclusionData->OccluderListSize++] = { m_Data->CameraDistance[i], i };
				}
			}

			std::sort(m_OcclusionData->OccluderList, m_OcclusionData->OccluderList + m_OcclusionData->OccluderListSize, CompareOccluderItem);

			RenderOcclusionBuffer();
			CullOccludedObjects();
		}	

		IterCount = Length >> 4;
		for (size_t i = 0; i < IterCount; ++i)
		{
			__m128i Culled = _mm_load_si128(reinterpret_cast<__m128i*>(&m_Data->Boxes.Culled[i << 4]));
			__m128i Cullable = _mm_load_si128(reinterpret_cast<__m128i*>(&m_Data->Cullable[i << 4]));
			_mm_store_si128(reinterpret_cast<__m128i*>(&m_Data->Boxes.Culled[i << 4]), _mm_and_si128(Culled, Cullable));
		}
		for (size_t i = IterCount << 4; i < Length; ++i)
		{
			m_Data->Boxes.Culled[i] &= m_Data->Cullable[i];
		}

		//if (Once)
		{
			uint32_t* Culled32 = reinterpret_cast<uint32_t*>(m_Data->Boxes.Culled);
			m_Data->RenderListSize = 0;
			IterCount = Length >> 2;
			for (size_t i = 0; i < IterCount; ++i)
			{
				if (Culled32[i] == 0x01010101)
					continue;

				size_t Index = i << 2;
				for (size_t k = 0; k < 4; ++k, ++Index)
				{
					if (!m_Data->Boxes.Culled[Index])
					{
						m_Data->Key[Index].Parts.Culled = 0;
						m_Data->Key[Index].Parts.CameraDistance = std::min(static_cast<uint16_t>((m_Data->CameraDistance[Index] / (m_Camera->Far() * 5.0f)) * UINT16_MAX), UINT16_MAX);
						m_Data->RenderList[m_Data->RenderListSize++] = { m_Data->Key[Index], m_Data->Ref[Index] };
					}
				}
			}

			for (size_t i = IterCount << 2; i < Length; ++i)
			{
				if (m_Data->Boxes.Culled[i])
					continue;

				m_Data->Key[i].Parts.Culled = m_Data->Boxes.Culled[i];
				m_Data->Key[i].Parts.CameraDistance = std::min(static_cast<uint16_t>((m_Data->CameraDistance[i] / (m_Camera->Far() * 5.0f)) * UINT16_MAX), UINT16_MAX);
				m_Data->RenderList[m_Data->RenderListSize++] = { m_Data->Key[i], m_Data->Ref[i] };
			}
			
			std::sort(m_Data->RenderList, m_Data->RenderList + m_Data->RenderListSize, CompareRenderItem);
			m_Data->Rebuild = false;
		}
	}
}

void RenderPass::Render(Renderer& renderer)
{
	SK::Time::HighPerformanceClock Clock;
	auto Start = Clock.Time();

	Math::Matrix ViewProj = m_Camera->ViewMatrix() * m_Camera->ProjectionMatrix();
	Math::Matrix View = m_Camera->ViewMatrix();
	auto WVP = renderer.GetShaderParamId("WorldViewProj");
	auto ViewProjId = renderer.GetShaderParamId("ViewProj");
	auto WorldId = renderer.GetShaderParamId("World");
	auto ViewId = renderer.GetShaderParamId("View");
	
	renderer.SetShaderParam(ViewId, View);
	renderer.SetShaderParam(ViewProjId, ViewProj);
	
	const Frustum& CameraFrustum = m_Camera->Frustum();

	size_t NumRenderables = m_Data->RenderListSize;
	for (size_t i = 0; i < NumRenderables; ++i)
	{
		m_Data->RenderList[i].Object->Render(renderer);
	}
}

//-- Private Methods --//
//=====================//

void RenderPass::ChildAdded(void* sender, TreeUpdatedArgs& e)
{
	AddRenderable(e.Child());
}

void RenderPass::ChildRemoved(void* sender, TreeUpdatedArgs& e)
{
	RemoveRenderable(e.Child());
}

void RenderPass::AddRenderable(Renderable* renderable)
{
	uint32_t Index = m_Data->NumRenderables;

	m_Data->Ref[Index] = renderable;
	m_Data->Key[Index].Value = 0;
	m_Data->Key[Index].Parts.SortOrder = static_cast<uint8_t>(renderable->SortOrder());
	m_Data->Key[Index].Parts.InstanceId = renderable->InstanceTypeId();
	if (renderable->Visible())
	{
		m_Data->PreCulled[Index] = 0;
		m_Data->Cullable[Index] = renderable->Cullable();
	}
	else
	{
		m_Data->PreCulled[Index] = 1;
		m_Data->Cullable[Index] = 1;
	}

	if (!m_Data->PreCulled[Index] && m_Data->Cullable[Index])
	{
		const AABB& Box = renderable->AABB();
		Math::float4 Centre = Box.Centre.Get();
		Math::float4 Size = Box.Size.Get();
		m_Data->Boxes.SetBox(Index, Centre, Size);
	}
	else
	{
		m_Data->Boxes.SetBox(Index, Math::float4(0.0f), Math::float4(FLT_MAX));
	}
	++m_Data->NumRenderables;

	for (auto child = renderable->Child(); child; child = child->Next())
		AddRenderable(child);
}

void RenderPass::RemoveRenderable(Renderable* renderable)
{
	//SKTD
}

#if SKGRAPHICS_SHOWOCCLUSIONBUFFER
void RenderPass::CopyOcclusionBuffer(Renderer& renderer, TexturePtr renderTarget)
{
	const int DataSize = m_OcclusionData->Resolution.x*m_OcclusionData->Resolution.y;
	float *pixels = new float[m_OcclusionData->Resolution.x*m_OcclusionData->Resolution.y];
	m_OcclusionData->Occlusion.ComputePixelDepthBuffer(pixels);

	float maxdepth = -1, mindepth = FLT_MAX;
	for (int i = 0; i < DataSize; ++i)
	{
		if (pixels[i] > 0.0f)
		{
			maxdepth = std::max(maxdepth, pixels[i]);
			mindepth = std::min(mindepth, pixels[i]);
		}
	}

	uint8_t* Tex = static_cast<uint8_t*>(renderer.LockTexture(renderTarget, 0));

	for (int i = 0; i < DataSize; ++i)
	{
		unsigned char intensity = 0;
		if (pixels[i] > 0.0f)
			intensity = (unsigned char) (((pixels[i] - mindepth) / (maxdepth - mindepth))*192.0f + 63.0f);

		Tex[i] = intensity;
	}

	delete [] pixels;
	renderer.UnlockTexture(renderTarget, 0);
}
#endif

// Cull AABB's against the view frustum
void RenderPass::FrustumCulling()
{
	const Frustum& CameraFrustum = m_Camera->Frustum();
	const size_t NumThreads = m_ThreadPool.GetThreadCount();
	const uint32_t Length = m_Data->NumRenderables;
	if (NumThreads)
	{
		const uint32_t MaxAsyncTasks = 16;
		const uint32_t NumBlocks = Length / m_Data->Boxes.HiBlockSize;
		const uint32_t BoxesPerTask = std::max((NumBlocks / MaxAsyncTasks), 4u) * m_Data->Boxes.HiBlockSize;

		auto CullFunc = [this, &CameraFrustum](uint32_t start, uint32_t end)
		{
			m_Data->Boxes.Cull(CameraFrustum, start, end);
		};

		SK::Threading::FutureGroup<void, MaxAsyncTasks + 1> Tasks;

		uint32_t StartBox = 0;
		while ((StartBox + BoxesPerTask) < Length)
		{
			auto CullTask = m_ThreadPool.AddTask(CullFunc, StartBox, StartBox + BoxesPerTask);
			StartBox += BoxesPerTask;
			Tasks.Add(CullTask);
		}
		auto CullTask = m_ThreadPool.AddTask(CullFunc, StartBox, Length);
		Tasks.Add(std::move(CullTask));
	
		m_ThreadPool.RunTasksInCurrentContext();
		Tasks.Wait();
	}
	else
	{
		m_Data->Boxes.Cull(CameraFrustum);
	}
}

// Find the camera distance for each object, and cull those that are too small to be seen
void RenderPass::GetCameraDistance()
{
	const float FoVConst = tan(m_Camera->FoV() * 0.5f);
	const float Threshold = 0.01f;
	const float OccluderThreshold = 0.5f;
	auto& AABBList = m_Data->Boxes.Box;

	auto CameraDist = [this, &AABBList, FoVConst, Threshold, OccluderThreshold](uint32_t start, uint32_t end)
	{
		uint32_t* Culled32 = reinterpret_cast<uint32_t*>(m_Data->Boxes.Culled);
		uint32_t* Cullable32 = reinterpret_cast<uint32_t*>(m_Data->Cullable);

		__m128 PX = _mm_load_ps1(&_SortData.CameraPosition.Data.m128_f32[0]);
		__m128 PY = _mm_load_ps1(&_SortData.CameraPosition.Data.m128_f32[1]);
		__m128 PZ = _mm_load_ps1(&_SortData.CameraPosition.Data.m128_f32[2]);
		for (uint32_t i = start; i < end; ++i)
		{
			if ((Culled32[i] & Cullable32[i]) == 0x01010101)
				continue;

			__m128 CX = _mm_sub_ps(_mm_load_ps(&AABBList.CentreX[i << 2]), PX);
			__m128 CZ = _mm_sub_ps(_mm_load_ps(&AABBList.CentreZ[i << 2]), PZ);
			__m128 CY = _mm_sub_ps(_mm_load_ps(&AABBList.CentreY[i << 2]), PY);
			__m128 Result = _mm_add_ps(_mm_mul_ps(CX, CX), _mm_mul_ps(CY, CY));
			Result = _mm_add_ps(Result, _mm_mul_ps(CZ, CZ));
			Result = _mm_sqrt_ps(Result);
			_mm_store_ps(&m_Data->CameraDistance[i << 2], Result);
		}

		__m128 vecFoV = _mm_load_ps1(&FoVConst);
		__m128 vecOccluderThreshold = _mm_load_ps1(&OccluderThreshold);
		__m128 vecThreshold = _mm_load_ps1(&Threshold);
		for (uint32_t i = start; i < end; ++i)
		{
			if ((Culled32[i] & Cullable32[i]) == 0x01010101)
				continue;

			__m128 Radius = _mm_load_ps(&AABBList.Radius[i << 2]);
			__m128 Distance = _mm_load_ps(&m_Data->CameraDistance[i << 2]);

			__m128 Size = _mm_div_ps(Radius, _mm_mul_ps(Distance, vecFoV));
			__m128 Culled = _mm_sub_ps(Size, vecThreshold);
			__m128 Occluder = _mm_sub_ps(vecOccluderThreshold, Size);
			int OccluderFlags = _mm_movemask_ps(Occluder);
			int CulledFlags = _mm_movemask_ps(Culled);

			m_Data->Boxes.Culled[(i << 2) + 0] |= (CulledFlags >> 0) & 1;
			m_Data->Boxes.Culled[(i << 2) + 1] |= (CulledFlags >> 1) & 1;
			m_Data->Boxes.Culled[(i << 2) + 2] |= (CulledFlags >> 2) & 1;
			m_Data->Boxes.Culled[(i << 2) + 3] |= (CulledFlags >> 3) & 1;

			m_Data->Occluder[(i << 2) + 0] = (OccluderFlags >> 0) & 1;
			m_Data->Occluder[(i << 2) + 1] = (OccluderFlags >> 1) & 1;
			m_Data->Occluder[(i << 2) + 2] = (OccluderFlags >> 2) & 1;
			m_Data->Occluder[(i << 2) + 3] = (OccluderFlags >> 3) & 1;
		}
	};

	const uint32_t MaxAsyncTasks = 16;
	SK::Threading::FutureGroup<void, MaxAsyncTasks + 1> Tasks;

	uint32_t IterCount = m_Data->NumRenderables >> 2;
	const size_t NumThreads = m_ThreadPool.GetThreadCount();
	if (NumThreads)
	{
		
		const uint32_t ChunkSize = 4096;
		const uint32_t NumBlocks = IterCount / ChunkSize;
		const uint32_t ItemsPerTask = std::max((NumBlocks / MaxAsyncTasks), 1u) * ChunkSize;

		uint32_t WorkStart = 0;
		while ((WorkStart + ItemsPerTask) < IterCount)
		{
			Tasks.Add(m_ThreadPool.AddTask(CameraDist, WorkStart, WorkStart + ChunkSize));
			WorkStart += ChunkSize;
		}
		Tasks.Add(m_ThreadPool.AddTask(CameraDist, WorkStart, IterCount));

		m_ThreadPool.RunTasksInCurrentContext();
	}
	else
	{
		CameraDist(0, IterCount);
	}
	
	for (uint32_t i = IterCount << 2; i < m_Data->NumRenderables; ++i)
	{
		Math::Vector Centre(AABBList.CentreX[i], AABBList.CentreY[i], AABBList.CentreZ[i], 1.0f);
		m_Data->CameraDistance[i] = (Centre - _SortData.CameraPosition).Length();
		float Size = AABBList.Radius[i] / m_Data->CameraDistance[i] * FoVConst;
		m_Data->Boxes.Culled[i] |= (Size < Threshold) ? 1 : 0;
		m_Data->Occluder[i] |= (Size > OccluderThreshold) ? 1 : 0;
	}

	Tasks.Wait();
}

void RenderPass::RenderOcclusionBuffer()
{
	static const Math::Matrix viewportMatrix(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, -1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
		);

	Math::Matrix ViewProjPort = m_Camera->ViewMatrix() * m_Camera->BackwardsProjectionMatrix() * viewportMatrix;
	m_OcclusionData->NumScreenSpaceTris = 0;

	uint16_t NumVerts = 0;
	uint16_t NumIndices = 0;
	size_t Length = m_OcclusionData->OccluderListSize;
	for (size_t i = 0; i < Length; ++i)
	{
		size_t Index = m_OcclusionData->OccluderList[i].Index;

		auto Results = &m_OcclusionData->ScreenSpaceOcclusionGeom[NumVerts];
		auto WVP = m_Data->Ref[Index]->WorldTransformDirect() * ViewProjPort;
		auto Mesh = m_Data->Ref[Index]->GetOcclusionMesh();
		for (size_t k = 0; k < Mesh.Vertices.Length(); ++k)
		{
			__m128 xVal = _mm_load1_ps(&Mesh.Vertices[k].x);
			__m128 yVal = _mm_load1_ps(&Mesh.Vertices[k].y);
			__m128 zVal = _mm_load1_ps(&Mesh.Vertices[k].z);
				
			__m128 xform = WVP.Row3;
			xform = _mm_add_ps(xform, _mm_mul_ps(WVP.Row0, _mm_load1_ps(&Mesh.Vertices[k].x)));
			xform = _mm_add_ps(xform, _mm_mul_ps(WVP.Row1, _mm_load1_ps(&Mesh.Vertices[k].y)));
			xform = _mm_add_ps(xform, _mm_mul_ps(WVP.Row2, _mm_load1_ps(&Mesh.Vertices[k].z)));
				
			//__m128 xform = _mm_fmadd_ps(WVP.Row0, xVal, _mm_fmadd_ps(WVP.Row1, yVal, _mm_fmadd_ps(WVP.Row2, zVal, WVP.Row3)));
			_mm_store_ps(Results[k], xform);
		}

		size_t IterCount = Mesh.Indices.Length() >> 2;
		for (size_t k = 0; k < IterCount; ++k)
		{
			__m128i OffsetIndeces = _mm_loadu_si128(reinterpret_cast<__m128i*>(&Mesh.Indices[k << 2]));
			OffsetIndeces = _mm_add_epi32(OffsetIndeces, _mm_set1_epi32(NumVerts));
			_mm_storeu_si128(reinterpret_cast<__m128i*>(&m_OcclusionData->OcclusionGeomIndices[NumIndices]), OffsetIndeces);
			NumIndices += 4;
		}

		for (size_t k = IterCount << 2; k < Mesh.Indices.Length(); ++k)
			m_OcclusionData->OcclusionGeomIndices[NumIndices++] = Mesh.Indices[k] + NumVerts;

		NumVerts += static_cast<uint16_t>(Mesh.Vertices.Length());
	}

	m_OcclusionData->NumScreenSpaceTris = NumIndices / 3;

	// Rasterize
	m_OcclusionData->Occlusion.ClearBuffer();

	const int NumThreads = static_cast<int>(m_ThreadPool.GetThreadCount() + 1);
	const MaskedOcclusionCullingSSE::VertexLayout vtxLayout(16, 4, 12);
	SK::Threading::FutureGroup<MaskedOcclusionCullingSSE::CullingResult, 8> Tasks;
	MaskedOcclusionCullingSSE::ScissorRect Rects[8];

	if (NumThreads == 1)
	{
		// No threading
		m_OcclusionData->Occlusion.RenderTriangles(reinterpret_cast<float*>(m_OcclusionData->ScreenSpaceOcclusionGeom), m_OcclusionData->OcclusionGeomIndices, m_OcclusionData->NumScreenSpaceTris);
	}
	else if (NumThreads < 4)
	{
		// Single row (up to 3)
		auto Res = m_OcclusionData->Resolution;
		int BlockWidth = ((Res.x / NumThreads) / 32) * 32;
		for (int i = 0; i < NumThreads; ++i)
		{
			int LeftEdge = i * BlockWidth;
			int RightEdge = (i+1 == NumThreads) ? Res.x : LeftEdge + BlockWidth;
			Rects[i] = { LeftEdge, 0, RightEdge, Res.y };
			auto Task = m_ThreadPool.AddTask(&MaskedOcclusionCullingSSE::RenderTriangles, &m_OcclusionData->Occlusion, reinterpret_cast<float*>(m_OcclusionData->ScreenSpaceOcclusionGeom), m_OcclusionData->OcclusionGeomIndices, m_OcclusionData->NumScreenSpaceTris, MaskedOcclusionCullingSSE::CLIP_PLANE_ALL, &Rects[i], vtxLayout);
			Tasks.Add(Task);
		}
		
		m_ThreadPool.RunTasksInCurrentContext();
		Tasks.Wait();
	}
	else
	{
		// Two rows (up to 4x2)
		auto Res = m_OcclusionData->Resolution;
		int BlocksOnRow = std::min(NumThreads / 2, 4);
		int BlockWidth = ((Res.x / BlocksOnRow) / 32) * 32;

		// Top row
		for (int i = 0; i < BlocksOnRow; ++i)
		{
			int LeftEdge = i * BlockWidth;
			int RightEdge = (i + 1 == NumThreads) ? Res.x : LeftEdge + BlockWidth;
			Rects[i] = { LeftEdge, 0, RightEdge, Res.y/2 };
			auto Task = m_ThreadPool.AddTask(&MaskedOcclusionCullingSSE::RenderTriangles, &m_OcclusionData->Occlusion, reinterpret_cast<float*>(m_OcclusionData->ScreenSpaceOcclusionGeom), m_OcclusionData->OcclusionGeomIndices, m_OcclusionData->NumScreenSpaceTris, MaskedOcclusionCullingSSE::CLIP_PLANE_ALL, &Rects[i], vtxLayout);
			Tasks.Add(Task);
		}

		// Bottom row
		BlocksOnRow = std::min(NumThreads - BlocksOnRow, 4);
		BlockWidth = ((Res.x / BlocksOnRow) / 32) * 32;
		for (int i = 0; i < BlocksOnRow; ++i)
		{
			int LeftEdge = i * BlockWidth;
			int RightEdge = (i + 1 == NumThreads) ? Res.x : LeftEdge + BlockWidth;
			Rects[4+i] = { LeftEdge, Res.y / 2, RightEdge, Res.y };
			auto Task = m_ThreadPool.AddTask(&MaskedOcclusionCullingSSE::RenderTriangles, &m_OcclusionData->Occlusion, reinterpret_cast<float*>(m_OcclusionData->ScreenSpaceOcclusionGeom), m_OcclusionData->OcclusionGeomIndices, m_OcclusionData->NumScreenSpaceTris, MaskedOcclusionCullingSSE::CLIP_PLANE_ALL, &Rects[4 + i], vtxLayout);
			Tasks.Add(Task);
		}

		m_ThreadPool.RunTasksInCurrentContext();
		Tasks.Wait();
	}
}

void RenderPass::CullOccludedObjects()
{
	static const Math::Matrix viewportMatrix(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, -1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
		);
	Math::Matrix ViewProjPort = m_Camera->ViewMatrix() * m_Camera->BackwardsProjectionMatrix() * viewportMatrix;
	uint32_t IterCount = m_Data->NumRenderables >> 2;
	uint32_t Length = m_Data->NumRenderables;

	auto OccludeRegion = [this, &ViewProjPort](uint32_t start, uint32_t end)
	{
		uint32_t* Culled32 = reinterpret_cast<uint32_t*>(m_Data->Boxes.Culled);
		for (size_t Iter = start; Iter < end; ++Iter)
		{
			if ((Culled32[Iter]) == 0x01010101)
				continue;

			if (!m_Data->Boxes.Culled[(Iter << 2) + 0])
				OcclusionTest((Iter << 2) + 0, ViewProjPort);
			if (!m_Data->Boxes.Culled[(Iter << 2) + 1])
				OcclusionTest((Iter << 2) + 1, ViewProjPort);
			if (!m_Data->Boxes.Culled[(Iter << 2) + 2])
				OcclusionTest((Iter << 2) + 2, ViewProjPort);
			if (!m_Data->Boxes.Culled[(Iter << 2) + 3])
				OcclusionTest((Iter << 2) + 3, ViewProjPort);
		}
	};

	const size_t NumThreads = m_ThreadPool.GetThreadCount();
	const uint32_t MaxAsyncTasks = 16;
	SK::Threading::FutureGroup<void, MaxAsyncTasks + 1> Tasks;
	
	if (NumThreads)
	{
		const uint32_t ChunkSize = 4096;
		const uint32_t NumBlocks = IterCount / ChunkSize;
		const uint32_t ItemsPerTask = std::max((NumBlocks / MaxAsyncTasks), 1u) * ChunkSize;

		uint32_t WorkStart = 0;
		while ((WorkStart + ItemsPerTask) < IterCount)
		{
			Tasks.Add(m_ThreadPool.AddTask(OccludeRegion, WorkStart, WorkStart + ChunkSize));
			WorkStart += ChunkSize;
		}
		Tasks.Add(m_ThreadPool.AddTask(OccludeRegion, WorkStart, IterCount));

		m_ThreadPool.RunTasksInCurrentContext();
	}
	else
	{
		OccludeRegion(0, IterCount);
	}

	// Do the last few that weren't queued
	for (size_t Iter = IterCount << 2; Iter < Length; ++Iter)
	{
		if (!m_Data->Boxes.Culled[Iter])
			OcclusionTest((Iter << 2) + 0, ViewProjPort);
	}

	Tasks.Wait();
}

void RenderPass::OcclusionTest(size_t i, const SK::Math::Matrix& ViewProjPort)
{
	// w ends up being garbage, but it doesn't matter - we ignore it anyway.
	__m128 vCenter = _mm_setr_ps(m_Data->Boxes.Box.CentreX[i], m_Data->Boxes.Box.CentreY[i], m_Data->Boxes.Box.CentreZ[i], 0);
	__m128 vHalf = _mm_setr_ps(m_Data->Boxes.Box.SizeX[i], m_Data->Boxes.Box.SizeY[i], m_Data->Boxes.Box.SizeZ[i], 0);

	__m128 vMin = _mm_sub_ps(vCenter, vHalf);
	__m128 vMax = _mm_add_ps(vCenter, vHalf);

	// transforms
	__m128 xRow[2], yRow[2], zRow[2];
	xRow[0] = _mm_mul_ps(_mm_shuffle_ps(vMin, vMin, 0x00), ViewProjPort.Row0);
	xRow[1] = _mm_mul_ps(_mm_shuffle_ps(vMax, vMax, 0x00), ViewProjPort.Row0);
	yRow[0] = _mm_mul_ps(_mm_shuffle_ps(vMin, vMin, 0x55), ViewProjPort.Row1);
	yRow[1] = _mm_mul_ps(_mm_shuffle_ps(vMax, vMax, 0x55), ViewProjPort.Row1);
	zRow[0] = _mm_mul_ps(_mm_shuffle_ps(vMin, vMin, 0xaa), ViewProjPort.Row2);
	zRow[1] = _mm_mul_ps(_mm_shuffle_ps(vMax, vMax, 0xaa), ViewProjPort.Row2);

	// Find the minimum of each component
	__m128 minvert = _mm_add_ps(ViewProjPort.Row3, _mm_add_ps(_mm_add_ps(_mm_min_ps(xRow[0], xRow[1]), _mm_min_ps(yRow[0], yRow[1])), _mm_min_ps(zRow[0], zRow[1])));
	float minW = minvert.m128_f32[3];
	if (minW < 0.00000001f)
		return; //return ePT_VISIBLE;
	
	__m128 vertA = _mm_add_ps(ViewProjPort.Row3, _mm_add_ps(xRow[0], _mm_add_ps(yRow[1], zRow[1])));
	__m128 vertB = _mm_add_ps(ViewProjPort.Row3, _mm_add_ps(xRow[0], _mm_add_ps(yRow[1], zRow[0])));
	__m128 screenMin = _mm_div_ps(_mm_shuffle_ps(vertA, vertB, _MM_SHUFFLE(1, 0, 1, 0)), _mm_shuffle_ps(vertA, vertB, 0xff));
	__m128 screenMax = screenMin;

	vertA = _mm_add_ps(ViewProjPort.Row3, _mm_add_ps(xRow[0], _mm_add_ps(yRow[0], zRow[1])));
	vertB = _mm_add_ps(ViewProjPort.Row3, _mm_add_ps(xRow[0], _mm_add_ps(yRow[0], zRow[0])));
	__m128 xformedPos = _mm_div_ps(_mm_shuffle_ps(vertA, vertB, _MM_SHUFFLE(1, 0, 1, 0)), _mm_shuffle_ps(vertA, vertB, 0xff));
	screenMin = _mm_min_ps(screenMin, xformedPos);
	screenMax = _mm_max_ps(screenMax, xformedPos);

	vertA = _mm_add_ps(ViewProjPort.Row3, _mm_add_ps(xRow[1], _mm_add_ps(yRow[0], zRow[0])));
	vertB = _mm_add_ps(ViewProjPort.Row3, _mm_add_ps(xRow[1], _mm_add_ps(yRow[0], zRow[1])));
	xformedPos = _mm_div_ps(_mm_shuffle_ps(vertA, vertB, _MM_SHUFFLE(1, 0, 1, 0)), _mm_shuffle_ps(vertA, vertB, 0xff));
	screenMin = _mm_min_ps(screenMin, xformedPos);
	screenMax = _mm_max_ps(screenMax, xformedPos);

	vertA = _mm_add_ps(ViewProjPort.Row3, _mm_add_ps(xRow[1], _mm_add_ps(yRow[1], zRow[1])));
	vertB = _mm_add_ps(ViewProjPort.Row3, _mm_add_ps(xRow[1], _mm_add_ps(yRow[1], zRow[0])));
	xformedPos = _mm_div_ps(_mm_shuffle_ps(vertA, vertB, _MM_SHUFFLE(1, 0, 1, 0)), _mm_shuffle_ps(vertA, vertB, 0xff));
	screenMin = _mm_min_ps(screenMin, xformedPos);
	screenMax = _mm_max_ps(screenMax, xformedPos);

	static const __m128 SIGNMASK = _mm_castsi128_ps(_mm_set_epi32(0x80000000, 0x80000000, 0, 0));
	__m128 Top = _mm_xor_ps(_mm_shuffle_ps(screenMin, screenMax, _MM_SHUFFLE(1, 0, 1, 0)), SIGNMASK);
	__m128 Bottom = _mm_xor_ps(_mm_shuffle_ps(screenMin, screenMax, _MM_SHUFFLE(3, 2, 3, 2)), SIGNMASK);
	__m128 Result = _mm_xor_ps(_mm_min_ps(Top, Bottom), SIGNMASK);

	MaskedOcclusionCullingSSE::CullingResult res = m_OcclusionData->Occlusion.TestRect(Result.m128_f32[0], Result.m128_f32[1], Result.m128_f32[2], Result.m128_f32[3], minW);
	if (res == MaskedOcclusionCullingSSE::OCCLUDED)
		m_Data->Boxes.Culled[i] = 1;
}

RenderPass::Data::Data(uint32_t maxSize)
: Ref(SK::AlignedNew<Renderable*>(16, maxSize))
, Boxes(maxSize)
, CameraDistance(SK::AlignedNew<float>(16, maxSize))
, PreCulled(SK::AlignedNew<uint8_t>(16, maxSize))
, Cullable(SK::AlignedNew<uint8_t>(16, maxSize))
, Dirty(SK::AlignedNew<uint8_t>(16, maxSize))
, Occluder(SK::AlignedNew<uint8_t>(16, maxSize))
, Key(SK::AlignedNew<SortKey>(16, maxSize))
, RenderList( new RenderItem[maxSize] )
, RenderListSize(0)
, NumRenderables(0)
, Rebuild(true)
{
	
}

RenderPass::Data::~Data()
{
	delete [] RenderList;
	SK::AlignedDelete(Key);
	SK::AlignedDelete(Occluder);
	SK::AlignedDelete(Dirty);
	SK::AlignedDelete(Cullable);
	SK::AlignedDelete(PreCulled);
	SK::AlignedDelete(CameraDistance);
	SK::AlignedDelete(Ref);
}

RenderPass::OcclusionData::OcclusionData(SK::Math::int2& resolution, uint32_t maxOccluders, uint32_t maxRenderedTris)
: ScreenSpaceOcclusionGeom(SK::AlignedNew<Math::float4>(32, maxRenderedTris * 3))
, OcclusionGeomIndices(SK::AlignedNew<uint32_t>(32, maxRenderedTris * 3))
, NumScreenSpaceTris(0)
, OccluderList(new OccluderItem[maxOccluders])
, OccluderListSize(0)
, Resolution(resolution)
{
	Occlusion.SetResolution(resolution.x, resolution.y);
}

RenderPass::OcclusionData::~OcclusionData()
{
	delete [] OccluderList;
	SK::AlignedDelete(OcclusionGeomIndices);
	SK::AlignedDelete(ScreenSpaceOcclusionGeom);
}
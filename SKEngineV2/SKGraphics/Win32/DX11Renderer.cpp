//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2016 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKGraphics/Win32/DX11Renderer.h>
#include <SKGraphics/Win32/DX11DDSTextureLoader.h>
#include <SKGraphics/Win32/DX11Texture.h>
#include <d3dcompiler.h>
#include <algorithm>

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

using namespace SK::Graphics;
using namespace SK;

//-- Declarations --//
//==================//

namespace
{
	template<typename T>
	struct DX11BufferDeleter
	{
		void operator ()(T* buffer)
		{
			ID3D11Buffer* DXBuffer = reinterpret_cast<ID3D11Buffer*>(buffer->Handle());
			DXBuffer->Release();

			delete buffer;
		}
	};

	template<typename Resource>
	struct DX11ResourceDeleter
	{
		void operator ()(Resource* resource)
		{
			ID3D11Resource* DXResource = reinterpret_cast<ID3D11Resource*>(resource->Handle());
			DXResource->Release();

			delete resource;
		}
	};

	D3D11_INPUT_ELEMENT_DESC Vertex2DDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	D3D11_INPUT_ELEMENT_DESC Vertex3DDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	D3D11_INPUT_ELEMENT_DESC Vertex3D_TCDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 2, DXGI_FORMAT_R32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	D3D11_INPUT_ELEMENT_DESC Instanced_VertexColourDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "INSTANCE_TRANSFORM", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCE_TRANSFORM", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCE_TRANSFORM", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCE_TRANSFORM", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "TEXCOORD", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
	};

	D3D11_INPUT_ELEMENT_DESC Instanced_VertexDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "INSTANCE_TRANSFORM", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCE_TRANSFORM", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCE_TRANSFORM", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCE_TRANSFORM", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
	};

	D3D11_DEPTH_STENCIL_DESC DepthStencilStateDesc[] =
	{
		{ false, D3D11_DEPTH_WRITE_MASK_ZERO, D3D11_COMPARISON_LESS_EQUAL, false, D3D11_DEFAULT_STENCIL_READ_MASK, D3D11_DEFAULT_STENCIL_WRITE_MASK, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_COMPARISON_ALWAYS },
		{ true, D3D11_DEPTH_WRITE_MASK_ZERO, D3D11_COMPARISON_LESS_EQUAL, false, D3D11_DEFAULT_STENCIL_READ_MASK, D3D11_DEFAULT_STENCIL_WRITE_MASK, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_COMPARISON_ALWAYS },
		{ false, D3D11_DEPTH_WRITE_MASK_ALL, D3D11_COMPARISON_LESS_EQUAL, false, D3D11_DEFAULT_STENCIL_READ_MASK, D3D11_DEFAULT_STENCIL_WRITE_MASK, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_COMPARISON_ALWAYS },
		{ true, D3D11_DEPTH_WRITE_MASK_ALL, D3D11_COMPARISON_LESS_EQUAL, false, D3D11_DEFAULT_STENCIL_READ_MASK, D3D11_DEFAULT_STENCIL_WRITE_MASK, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_STENCIL_OP_KEEP, D3D11_COMPARISON_ALWAYS }
	};

	D3D11_RASTERIZER_DESC RasterizerStateDesc[] =
	{
		{ D3D11_FILL_SOLID, D3D11_CULL_NONE, false, 0, 0.0f, 0.0f, true, false, true, false },
		{ D3D11_FILL_SOLID, D3D11_CULL_BACK, true, 0, 0.0f, 0.0f, true, false, true, false },
		{ D3D11_FILL_SOLID, D3D11_CULL_BACK, false, 0, 0.0f, 0.0f, true, false, true, false }
	};

	D3D11_RENDER_TARGET_BLEND_DESC BlendStateDesc[] =
	{
		{ false, D3D11_BLEND_ONE, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD, D3D11_BLEND_ONE, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD, D3D11_COLOR_WRITE_ENABLE_ALL },
		{ true, D3D11_BLEND_SRC_ALPHA, D3D11_BLEND_INV_SRC_ALPHA, D3D11_BLEND_OP_ADD, D3D11_BLEND_ONE, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD, D3D11_COLOR_WRITE_ENABLE_ALL },
		{ true, D3D11_BLEND_SRC_ALPHA, D3D11_BLEND_ONE, D3D11_BLEND_OP_ADD, D3D11_BLEND_ONE, D3D11_BLEND_ONE, D3D11_BLEND_OP_ADD, D3D11_COLOR_WRITE_ENABLE_ALL },
		{ true, D3D11_BLEND_ZERO, D3D11_BLEND_SRC_COLOR, D3D11_BLEND_OP_ADD, D3D11_BLEND_ONE, D3D11_BLEND_ONE, D3D11_BLEND_OP_MAX, D3D11_COLOR_WRITE_ENABLE_ALL },
	};

	D3D11_SAMPLER_DESC SamplerStateDesc[] =
	{
		{ D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_WRAP,  D3D11_TEXTURE_ADDRESS_WRAP,  D3D11_TEXTURE_ADDRESS_WRAP,  -0.5f, 1, D3D11_COMPARISON_NEVER, {1.0f, 1.0f, 1.0f, 1.0f}, -FLT_MAX, FLT_MAX},
		{ D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_CLAMP, D3D11_TEXTURE_ADDRESS_CLAMP, D3D11_TEXTURE_ADDRESS_CLAMP, -0.5f, 1, D3D11_COMPARISON_NEVER, {1.0f, 1.0f, 1.0f, 1.0f}, -FLT_MAX, FLT_MAX},
		{ D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_WRAP,  D3D11_TEXTURE_ADDRESS_WRAP,  D3D11_TEXTURE_ADDRESS_WRAP,  -0.5f, 4, D3D11_COMPARISON_NEVER, {1.0f, 1.0f, 1.0f, 1.0f}, -FLT_MAX, FLT_MAX},
		{ D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_CLAMP, D3D11_TEXTURE_ADDRESS_CLAMP, D3D11_TEXTURE_ADDRESS_CLAMP, -0.5f, 4, D3D11_COMPARISON_NEVER, {1.0f, 1.0f, 1.0f, 1.0f}, -FLT_MAX, FLT_MAX},
		{ D3D11_FILTER_MIN_MAG_MIP_POINT,  D3D11_TEXTURE_ADDRESS_WRAP,  D3D11_TEXTURE_ADDRESS_WRAP,  D3D11_TEXTURE_ADDRESS_WRAP,  0.0f, 1, D3D11_COMPARISON_NEVER, {1.0f, 1.0f, 1.0f, 1.0f}, -FLT_MAX, FLT_MAX},
		
		{ D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT, D3D11_TEXTURE_ADDRESS_CLAMP, D3D11_TEXTURE_ADDRESS_CLAMP, D3D11_TEXTURE_ADDRESS_CLAMP, 0.0f, 1, D3D11_COMPARISON_LESS, { 1.0f, 1.0f, 1.0f, 1.0f }, -FLT_MAX, FLT_MAX },
	};

	enum class MultisampleType
	{
		None,

		MSAAx2,
		MSAAx4,
		MSAAx8,

		//ATI
		EQAAx2f4,
		EQAAx4f8,
		EQAAx4f16,
		EQAAx8f16,

		//nvidia
		CSAAx8,
		CSAAx8Q,
		CSAAx16,
		CSAAx16Q
	};

	DXGI_SAMPLE_DESC MultisampleDescs[] =
	{
		{ 1, 0 },  //None
		{ 2, 0 },  //MSAAx2
		{ 4, 0 },  //MSAAx4
		{ 8, 0 },  //MSAAx8

		{ 2, 4 },  //EQAAx2f4
		{ 4, 8 },  //EQAAx4f8
		{ 4, 16 }, //EQAAx4f16
		{ 8, 16 }, //EQAAx8f16

		{ 4, 8 },  //CSAAx8
		{ 8, 8 },  //CSAAx8Q
		{ 4, 16 }, //CSAAx16
		{ 8, 16 }, //CSAAx16Q
		{ 8, 32 }, //CSAAx32
	};

	template<typename TextureDesc>
	TextureDesc GetTextureDesc(TextureFormat format, TextureFlags flags);
	DXGI_FORMAT GetDXFormat(TextureFormat format);
	TextureFormat GetSKFormat(DXGI_FORMAT format);
	void SetRenderTargetsImpl(TexturePtr textures[4], int faces[4], TexturePtr depthStencil);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Renderer::PlatformData::PlatformData()
: Initialised(false)
, ActiveVertexShader(VertexShaders.end())
, ActivePixelShader(PixelShaders.end())
, ActiveIndexBuffer(nullptr)
, ActiveInputLayout(VertexType::Vertex2D)
, DepthStencilTexture(nullptr)
, NumRenderTargets(0)
, SwapChain(nullptr)
, Device(nullptr)
, DeviceContext(nullptr)
, BackBuffer(nullptr)
, BackBufferView(nullptr)
, DepthBuffer(nullptr)
, DepthBufferView(nullptr)
{
	this->ActiveVertexBuffer[0] = this->ActiveVertexBuffer[1] = nullptr;

	for (int i = 0; i < NUM_INPUT_LAYOUTS; ++i)
		InputLayout[i] = nullptr;

	for (int i = 0; i < 4; ++i)
		DepthStencilStates[i] = nullptr;
	
	for (int i = 0; i < 4; ++i)
		BlendStates[i] = nullptr;

	for (int i = 0; i < 3; ++i)
		RasterizerStates[i] = nullptr;

	for (int i = 0; i < 6; ++i)
		SamplerStates[i] = nullptr;
}

Renderer::Renderer()
: m_Data(new PlatformData)
{

}

Renderer::~Renderer()
{
	m_Data->Release();
	delete m_Data;
}

void Renderer::GetCaps(RenderCaps& caps)
{
	caps.DisplayModes.clear();
	caps.MultisampleTypes.clear();

	DXGI_ADAPTER_DESC DisplayAdapterDesc;
	ID3D11Device* D3DDevice;
	ID3D11DeviceContext* D3DContext;

	// Get display adapter
	IDXGIFactory* DXGIFactory;
	CreateDXGIFactory(__uuidof(IDXGIFactory), (void**) &DXGIFactory);

	IDXGIAdapter* DisplayAdapter;
	DXGIFactory->EnumAdapters(0, &DisplayAdapter);

	DisplayAdapter->GetDesc(&DisplayAdapterDesc);

	// Find display modes for default monitor
	IDXGIOutput* DisplayOutput;
	DisplayAdapter->EnumOutputs(0, &DisplayOutput);

	UINT NumDisplayModes = 0;
	DisplayOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, 0, &NumDisplayModes, nullptr);

	DXGI_MODE_DESC* DisplayModes = new DXGI_MODE_DESC[NumDisplayModes];
	DisplayOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, 0, &NumDisplayModes, DisplayModes);
	for (UINT i = 0; i < NumDisplayModes; ++i)
	{
		if (DisplayModes[i].Scaling == DXGI_MODE_SCALING_UNSPECIFIED)
		{
			DisplayMode Mode;
			Mode.Width = static_cast<uint16_t>(DisplayModes[i].Width);
			Mode.Height = static_cast<uint16_t>(DisplayModes[i].Height);
			Mode.RefreshRate = static_cast<double>(DisplayModes[i].RefreshRate.Numerator) / static_cast<double>(DisplayModes[i].RefreshRate.Denominator);
			Mode.RationalRefreshRate.x = static_cast<int>(DisplayModes[i].RefreshRate.Numerator);
			Mode.RationalRefreshRate.y = static_cast<int>(DisplayModes[i].RefreshRate.Denominator);

			caps.DisplayModes.push_back(Mode);
		}
	}

	delete[] DisplayModes;
	DisplayOutput->Release();
	DisplayAdapter->Release();
	DXGIFactory->Release();

	D3D_FEATURE_LEVEL OutFeatureLevel;
	D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, 0, nullptr, 0, D3D11_SDK_VERSION, &D3DDevice, &OutFeatureLevel, &D3DContext);

	UINT QualityLevels = 0;
	if (DisplayAdapterDesc.VendorId == 0x10DE)
	{
		// nvidia
		D3DDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 2, &QualityLevels);
		if (QualityLevels > 0)
		{
			caps.MultisampleTypes.push_back(MultisampleType::MSAAx2);
		}

		D3DDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &QualityLevels);
		if (QualityLevels > 0)
		{
			caps.MultisampleTypes.push_back(MultisampleType::MSAAx4);
		}
		if (QualityLevels > 8)
		{
			caps.MultisampleTypes.push_back(MultisampleType::CSAAx8);
		}
		if (QualityLevels > 16)
		{
			caps.MultisampleTypes.push_back(MultisampleType::CSAAx16);
		}

		D3DDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 8, &QualityLevels);
		if (QualityLevels > 0)
		{
			caps.MultisampleTypes.push_back(MultisampleType::MSAAx8);
		}
		if (QualityLevels > 8)
		{
			caps.MultisampleTypes.push_back(MultisampleType::CSAAx8Q);
		}
		if (QualityLevels > 16)
		{
			caps.MultisampleTypes.push_back(MultisampleType::CSAAx16Q);
		}
		if (QualityLevels > 32)
		{
			caps.MultisampleTypes.push_back(MultisampleType::CSAAx32);
		}
	}
	else if (DisplayAdapterDesc.VendorId == 0x1002)
	{
		// ATI
		D3DDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 2, &QualityLevels);
		if (QualityLevels > 0)
		{
			caps.MultisampleTypes.push_back(MultisampleType::MSAAx2);
		}
		if (QualityLevels > 4)
		{
			caps.MultisampleTypes.push_back(MultisampleType::EQAAx2f4);
		}

		D3DDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &QualityLevels);
		if (QualityLevels > 0)
		{
			caps.MultisampleTypes.push_back(MultisampleType::MSAAx4);
		}
		if (QualityLevels > 8)
		{
			caps.MultisampleTypes.push_back(MultisampleType::EQAAx4f8);
		}
		if (QualityLevels > 16)
		{
			caps.MultisampleTypes.push_back(MultisampleType::EQAAx4f16);
		}

		D3DDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 8, &QualityLevels);
		if (QualityLevels > 0)
		{
			caps.MultisampleTypes.push_back(MultisampleType::MSAAx8);
		}
		if (QualityLevels > 16)
		{
			caps.MultisampleTypes.push_back(MultisampleType::EQAAx8f16);
		}
	}

	D3DDevice->Release();
	D3DContext->Release();
}

bool Renderer::Initialise(const SK::GUI::Window& window, const RenderSettings& settings)
{
	SK::Debug::Assert(m_Data->Initialised == false, "Renderer::Initialise - Already initialised");

	for (int i = 0; i < NUM_INPUT_LAYOUTS; ++i)
	{
		m_Data->InputLayout[i] = nullptr;
	}
	
	m_Data->BackBufferView = nullptr;
	m_Data->BackBuffer = nullptr;
	m_Data->DepthBufferView = nullptr;
	m_Data->DepthBuffer = nullptr;
	m_Data->SwapChain = nullptr;
	m_Data->DeviceContext = nullptr;
	m_Data->Device = nullptr;

	for (int i = 0; i < 4; ++i)
	{
		m_Data->DepthStencilStates[i] = nullptr;
		m_Data->BlendStates[i] = nullptr;
	}

	for (int i = 0; i < 3; ++i)
		m_Data->RasterizerStates[i] = nullptr;

	for (int i = 0; i < 6; ++i)
		m_Data->SamplerStates[i] = nullptr;

	m_Data->SwapChainDesc.BufferCount = 2;
	m_Data->SwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	m_Data->SwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	m_Data->SwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	m_Data->SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	m_Data->SwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	m_Data->SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	m_Data->SwapChainDesc.Windowed = settings.Windowed;
	m_Data->SwapChainDesc.OutputWindow = window.Data()->Wnd;

	m_Data->SwapChainDesc.BufferDesc.RefreshRate = { static_cast<UINT>(settings.RationalRefreshRate.x), static_cast<UINT>(settings.RationalRefreshRate.y) };
	m_Data->SwapChainDesc.BufferDesc.Width = settings.Width > 0 ? settings.Width : window.Size().x;
	m_Data->SwapChainDesc.BufferDesc.Height = settings.Height > 0 ? settings.Height : window.Size().y;

	int MSIndex = static_cast<int>(settings.Multisample);
	m_Data->SwapChainDesc.SampleDesc.Count = MultisampleDescs[MSIndex].Count;
	m_Data->SwapChainDesc.SampleDesc.Quality = MultisampleDescs[MSIndex].Quality;

	UINT Flags = 0;
#ifdef _DEBUG
	Flags = D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_FEATURE_LEVEL OutFeatureLevel;
	if (FAILED(D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, Flags, nullptr, 0, D3D11_SDK_VERSION,
		&m_Data->SwapChainDesc, &m_Data->SwapChain, &m_Data->Device, &OutFeatureLevel, &m_Data->DeviceContext)))
	{
		m_Data->Release();
		return false;
	}

	for (int i = 0; i < 4; ++i)
	{
		if (FAILED(m_Data->Device->CreateDepthStencilState(&DepthStencilStateDesc[i], &m_Data->DepthStencilStates[i])))
		{
			m_Data->Release();
			return false;
		}

		D3D11_BLEND_DESC Desc;
		Desc.AlphaToCoverageEnable = false;
		Desc.IndependentBlendEnable = false;
		for (int j = 0; j < 8; ++j)
		{
			memcpy(&Desc.RenderTarget[j], &BlendStateDesc[i], sizeof(BlendStateDesc[i]));
		}

		if (FAILED(m_Data->Device->CreateBlendState(&Desc, &m_Data->BlendStates[i])))
		{
			m_Data->Release();
			return false;
		}
	}

	for (int i = 0; i < 3; ++i)
	{
		if (FAILED(m_Data->Device->CreateRasterizerState(&RasterizerStateDesc[i], &m_Data->RasterizerStates[i])))
		{
			m_Data->Release();
			return false;
		}
	}

	for (int i = 0; i < 6; ++i)
	{
		if (FAILED(m_Data->Device->CreateSamplerState(&SamplerStateDesc[i], &m_Data->SamplerStates[i])))
		{
			m_Data->Release();
			return false;
		}
	}

	D3D11_TEXTURE2D_DESC DepthDesc;
	memset(&DepthDesc, 0, sizeof(DepthDesc));
	DepthDesc.Width = m_Data->SwapChainDesc.BufferDesc.Width;
	DepthDesc.Height = m_Data->SwapChainDesc.BufferDesc.Height;
	DepthDesc.MipLevels = 1;
	DepthDesc.ArraySize = 1;
	DepthDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	DepthDesc.SampleDesc = m_Data->SwapChainDesc.SampleDesc;
	DepthDesc.Usage = D3D11_USAGE_DEFAULT;
	DepthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	DepthDesc.CPUAccessFlags = 0;
	DepthDesc.MiscFlags = 0;

	if (FAILED(m_Data->Device->CreateTexture2D(&DepthDesc, nullptr, &m_Data->DepthBuffer)))
	{
		m_Data->Release();
		return false;
	}

	if (FAILED(m_Data->Device->CreateDepthStencilView(m_Data->DepthBuffer, nullptr, &m_Data->DepthBufferView)))
	{
		m_Data->Release();
		return false;
	}

	m_Data->SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&m_Data->BackBuffer));

	if (FAILED(m_Data->Device->CreateRenderTargetView(m_Data->BackBuffer, nullptr, &m_Data->BackBufferView)))
	{
		m_Data->Release();
		return false;
	}

	m_Data->DeviceContext->OMSetRenderTargets(1, &m_Data->BackBufferView, m_Data->DepthBufferView);

	D3D11_VIEWPORT vp;
	vp.Width = static_cast<float>(window.Size().x);
	vp.Height = static_cast<float>(window.Size().y);
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	m_Data->DeviceContext->RSSetViewports(1, &vp);

	m_Data->CurrentSettings = settings;
	m_Data->Initialised = true;
	return true;
}

VertexBufferPtr Renderer::CreateVertexBuffer(VertexType vertexType, uint32_t nbVerts, bool dynamic, void* vertexData, uint32_t dataLength)
{
	Debug::Assert(vertexData != nullptr || dynamic, "CreateVertexBuffer: Can't make a buffer that is not dynamic without any data.");
	Debug::Assert(vertexData == nullptr || dataLength >= VertexSize(vertexType) * nbVerts, "CreateVertexBuffer: Vertex buffer is too small.");

	D3D11_BUFFER_DESC VBuffDesc;
	memset(&VBuffDesc, 0, sizeof(VBuffDesc));
	VBuffDesc.ByteWidth = VertexSize(vertexType) * nbVerts;
	VBuffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	if (dynamic)
	{
		VBuffDesc.Usage = D3D11_USAGE_DYNAMIC;
		VBuffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		VBuffDesc.Usage = D3D11_USAGE_IMMUTABLE;
		VBuffDesc.CPUAccessFlags = 0;
	}

	D3D11_SUBRESOURCE_DATA VertexData;
	VertexData.pSysMem = vertexData;
	VertexData.SysMemPitch = 0;
	VertexData.SysMemSlicePitch = 0;

	ID3D11Buffer* DXVertexBuffer = nullptr;
	if (vertexData)
		m_Data->Device->CreateBuffer(&VBuffDesc, &VertexData, &DXVertexBuffer);
	else
		m_Data->Device->CreateBuffer(&VBuffDesc, NULL, &DXVertexBuffer);

	return VertexBufferPtr(new VertexBuffer(DXVertexBuffer, vertexType, nbVerts, dynamic), DX11BufferDeleter<VertexBuffer>());
}

IndexBufferPtr Renderer::CreateIndexBuffer(uint32_t nbIndis, bool dynamic, void* indexData, uint32_t dataLength)
{
	Debug::Assert(indexData != nullptr || dynamic, "CreateIndexBuffer: Can't make a buffer that is not dynamic without any data.");
	Debug::Assert(indexData == nullptr || dataLength >= sizeof(uint16_t) * nbIndis, "CreateIndexBuffer: Index buffer is too small.");

	D3D11_BUFFER_DESC IBuffDesc;
	memset(&IBuffDesc, 0, sizeof(IBuffDesc));
	IBuffDesc.ByteWidth = sizeof(uint16_t) * nbIndis;
	IBuffDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	if (dynamic)
	{
		IBuffDesc.Usage = D3D11_USAGE_DYNAMIC;
		IBuffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		IBuffDesc.Usage = D3D11_USAGE_IMMUTABLE;
		IBuffDesc.CPUAccessFlags = 0;
	}

	D3D11_SUBRESOURCE_DATA IndexData;
	IndexData.pSysMem = indexData;
	IndexData.SysMemPitch = 0;
	IndexData.SysMemSlicePitch = 0;

	ID3D11Buffer* DXIndexBuffer = nullptr;
	m_Data->Device->CreateBuffer(&IBuffDesc, &IndexData, &DXIndexBuffer);

	return IndexBufferPtr(new IndexBuffer(DXIndexBuffer, nbIndis, dynamic), DX11BufferDeleter<IndexBuffer>());
}

TexturePtr Renderer::CreateTexture(const Math::int3& size, TextureFormat format, TextureFlags flags, void* textureData, uint32_t dataLength)
{
	SK::Debug::Assert(flags.HasFlags(TextureFlag::Staging) || flags.HasFlags(TextureFlag::Lockable) || flags.HasFlags(TextureFlag::RenderTarget) || textureData != nullptr, "DX11Renderer::CreateTexture - Immutable texture can't be initialized without data.");
	SK::Debug::Assert(!(format == TextureFormat::Depth_32 || format == TextureFormat::DepthStencil_32) || flags.HasFlags(TextureFlag::RenderTarget), "DX11Renderer::CreateTexture - Depth buffers should have the RenderTarget flag set.");

	DX11Texture* TextureHandle = new DX11Texture(size, format, flags);

	D3D11_SUBRESOURCE_DATA TextureData;
	TextureData.pSysMem = textureData;
	TextureData.SysMemPitch = size.x * TextureSampleSize(format);
	TextureData.SysMemSlicePitch = size.x * size.y * TextureSampleSize(format);

	if (size.x > 0 && size.y > 0 && size.z > 0)
	{
		//3D Texture
		D3D11_TEXTURE3D_DESC Desc = GetTextureDesc<D3D11_TEXTURE3D_DESC>(format, flags);
		Desc.Width = size.x;
		Desc.Height = size.y;
		Desc.Depth = size.z;

		ID3D11Texture3D* DXTexture;
		m_Data->Device->CreateTexture3D(&Desc, textureData ? &TextureData : nullptr, &DXTexture);

		TextureHandle->DXGIFormat = Desc.Format;
		TextureHandle->MainTexture = DXTexture;
	}
	else if (size.x > 0 && size.y > 0)
	{
		//2D Texture
		D3D11_TEXTURE2D_DESC Desc = GetTextureDesc<D3D11_TEXTURE2D_DESC>(format, flags);
		Desc.Width = size.x;
		Desc.Height = size.y;

		Desc.ArraySize = flags.HasFlags(TextureFlag::Cube) ? 6 : 1;
		Desc.SampleDesc.Count = flags.HasFlags(TextureFlag::MSAA) ? 4 : 1;
		Desc.SampleDesc.Quality = flags.HasFlags(TextureFlag::MSAA) ? D3D11_STANDARD_MULTISAMPLE_PATTERN : 0;

		if (flags.HasFlags(TextureFlag::Cube))
		{
			Desc.ArraySize = 6;
			Desc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;
		}
		else
		{
			Desc.ArraySize = 1;
		}

		ID3D11Texture2D* DXResolve = nullptr;
		ID3D11Texture2D* DXTexture;
		m_Data->Device->CreateTexture2D(&Desc, textureData ? &TextureData : nullptr, &DXTexture);

		if (flags.HasFlags(TextureFlag::MSAA))
		{
			Desc.SampleDesc.Count = 1;
			Desc.SampleDesc.Quality = 0;
			m_Data->Device->CreateTexture2D(&Desc, textureData ? &TextureData : nullptr, &DXResolve);

			m_Data->Device->CreateShaderResourceView(DXResolve, nullptr, &TextureHandle->SRV);
		}
		else if (!flags.HasFlags(TextureFlag::Staging))
		{
			m_Data->Device->CreateShaderResourceView(DXTexture, nullptr, &TextureHandle->SRV);
		}

		TextureHandle->DXGIFormat = Desc.Format;
		TextureHandle->MainTexture = DXTexture;
		TextureHandle->ResolveTexture = DXResolve;

		if (flags.HasFlags(TextureFlag::RenderTarget))
		{
			if (format == TextureFormat::Depth_32 || format == TextureFormat::DepthStencil_32)
			{
				m_Data->Device->CreateDepthStencilView(DXTexture, nullptr, &TextureHandle->DSV);
			}
			else if (flags.HasFlags(TextureFlag::Cube))
			{
				D3D11_RENDER_TARGET_VIEW_DESC RTVDesc;
				RTVDesc.Format = DXGI_FORMAT_UNKNOWN;
				RTVDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
				RTVDesc.Texture2DArray.ArraySize = 1;
				RTVDesc.Texture2DArray.MipSlice = 0;

				for (int i = 0; i < 6; ++i)
				{
					RTVDesc.Texture2DArray.FirstArraySlice = i;
					m_Data->Device->CreateRenderTargetView(DXTexture, &RTVDesc, &TextureHandle->RTV[i]);
				}
			}
			else
			{
				m_Data->Device->CreateRenderTargetView(DXTexture, nullptr, &TextureHandle->RTV[0]);
			}
		}
	}
	else if (size.x > 0)
	{
		//1D Texture
		D3D11_TEXTURE1D_DESC Desc = GetTextureDesc<D3D11_TEXTURE1D_DESC>(format, flags);
		Desc.Width = size.x;

		ID3D11Texture1D* DXTexture;
		m_Data->Device->CreateTexture1D(&Desc, textureData ? &TextureData : nullptr, &DXTexture);

		TextureHandle->DXGIFormat = Desc.Format;
		TextureHandle->MainTexture = DXTexture;
	}
	else
	{
		SK::Debug::Assert(false, "DX11Renderer::CreateTexture - Invalid texture size");
	}

	return TexturePtr(TextureHandle);
}

TexturePtr Renderer::CreateTextureFromDDS(const uint8_t* data, uint32_t dataLength)
{
	ID3D11Resource* DXTexture;
	DirectX::CreateDDSTextureFromMemoryEx(m_Data->Device, nullptr, data, dataLength, 0, D3D11_USAGE_IMMUTABLE, D3D11_BIND_SHADER_RESOURCE, 0, 0, false, &DXTexture, nullptr);

	D3D11_RESOURCE_DIMENSION TextureType = D3D11_RESOURCE_DIMENSION_UNKNOWN;
	DXTexture->GetType(&TextureType);

	Math::int3 Size(0);
	TextureFormat Format = TextureFormat::Unknown;
	TextureFlags Flags;
	DXGI_FORMAT DXFormat;

	switch (TextureType)
	{
		case D3D11_RESOURCE_DIMENSION_TEXTURE1D:
		{
			ID3D11Texture1D* Texture1D = static_cast<ID3D11Texture1D*>(DXTexture);
			D3D11_TEXTURE1D_DESC Desc;
			Texture1D->GetDesc(&Desc);

			Size.x = Desc.Width;
			Format = GetSKFormat(Desc.Format);
			if (Desc.MipLevels != 1)
			{
				Flags |= TextureFlag::MipMapped;
			}

			DXFormat = Desc.Format;
		}
		break;

		case D3D11_RESOURCE_DIMENSION_TEXTURE2D:
		{
			ID3D11Texture2D* Texture2D = static_cast<ID3D11Texture2D*>(DXTexture);
			D3D11_TEXTURE2D_DESC Desc;
			Texture2D->GetDesc(&Desc);

			Size.x = Desc.Width;
			Size.y = Desc.Height;
			Format = GetSKFormat(Desc.Format);
			if (Desc.MipLevels != 1)
			{
				Flags |= TextureFlag::MipMapped;
			}

			if (Desc.ArraySize == 6)
			{
				Flags |= TextureFlag::Cube;
			}

			DXFormat = Desc.Format;
		}
		break;

		case D3D11_RESOURCE_DIMENSION_TEXTURE3D:
		{
			ID3D11Texture3D* Texture3D = static_cast<ID3D11Texture3D*>(DXTexture);
			D3D11_TEXTURE3D_DESC Desc;
			Texture3D->GetDesc(&Desc);

			Size.x = Desc.Width;
			Size.y = Desc.Height;
			Size.z = Desc.Depth;
			Format = GetSKFormat(Desc.Format);
			if (Desc.MipLevels != 1)
			{
				Flags |= TextureFlag::MipMapped;
			}

			DXFormat = Desc.Format;
		}
		break;
	}

	DX11Texture* Texture = new DX11Texture(Size, Format, Flags);
	Texture->DXGIFormat = DXFormat;
	Texture->MainTexture = DXTexture;
	m_Data->Device->CreateShaderResourceView(DXTexture, nullptr, &Texture->SRV);

	return TexturePtr(Texture);
}

void* Renderer::LockBuffer(IndexBufferPtr buffer)
{
	ID3D11Resource* D3DBuffer = reinterpret_cast<ID3D11Resource*>(buffer->Handle());
	D3D11_MAPPED_SUBRESOURCE LockedResource;
	m_Data->DeviceContext->Map(D3DBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &LockedResource);

	return LockedResource.pData;
}

void* Renderer::LockBuffer(VertexBufferPtr buffer)
{
	ID3D11Resource* D3DBuffer = reinterpret_cast<ID3D11Resource*>(buffer->Handle());
	D3D11_MAPPED_SUBRESOURCE LockedResource;
	m_Data->DeviceContext->Map(D3DBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &LockedResource);

	return LockedResource.pData;
}

void* Renderer::LockTexture(TexturePtr texture, uint32_t face)
{
	DX11Texture* D3DTexture = static_cast<DX11Texture*>(texture.get());
	D3D11_MAPPED_SUBRESOURCE LockedResource;

	if (texture->Flags().HasFlags(TextureFlag::Lockable))
		m_Data->DeviceContext->Map(D3DTexture->MainTexture, D3D11CalcSubresource(0, face, 0), D3D11_MAP_WRITE_DISCARD, 0, &LockedResource);
	else if (texture->Flags().HasFlags(TextureFlag::Staging))
		m_Data->DeviceContext->Map(D3DTexture->MainTexture, D3D11CalcSubresource(0, face, 0), D3D11_MAP_READ, 0, &LockedResource);

	return LockedResource.pData;
}

void Renderer::UnlockBuffer(IndexBufferPtr buffer)
{
	ID3D11Resource* D3DBuffer = reinterpret_cast<ID3D11Resource*>(buffer->Handle());
	m_Data->DeviceContext->Unmap(D3DBuffer, 0);
}

void Renderer::UnlockBuffer(VertexBufferPtr buffer)
{
	ID3D11Resource* D3DBuffer = reinterpret_cast<ID3D11Resource*>(buffer->Handle());
	m_Data->DeviceContext->Unmap(D3DBuffer, 0);
}

void Renderer::UnlockTexture(TexturePtr texture, uint32_t face)
{
	DX11Texture* D3DTexture = static_cast<DX11Texture*>(texture.get());
	m_Data->DeviceContext->Unmap(D3DTexture->MainTexture, D3D11CalcSubresource(0, face, 0));
}

void Renderer::CopyTexture(TexturePtr srcTexture, TexturePtr dstTexture)
{
	DX11Texture* SrcD3DTexture = static_cast<DX11Texture*>(srcTexture.get());
	DX11Texture* DstD3DTexture = static_cast<DX11Texture*>(dstTexture.get());
	m_Data->DeviceContext->CopyResource(DstD3DTexture->MainTexture, SrcD3DTexture->MainTexture);
}

Array<uint8_t> Renderer::CompileShader(const uint8_t* shaderSource, uint32_t dataLength, const SK::String& entryPoint, FeatureLevel targetVersion, SK::String* errorMsg)
{
	ID3DBlob* CompiledShader;
	ID3DBlob* ErrorMessages;
	UINT Flags = 0;
#ifdef DEBUG
	Flags |= 3DCOMPILE_DEBUG;
#endif

	const char* Target = nullptr;
	switch (targetVersion)
	{
	case FeatureLevel::cs_5_0: Target = "cs_5_0"; break;
	case FeatureLevel::cs_4_1: Target = "cs_4_1"; break;
	case FeatureLevel::cs_4_0: Target = "cs_4_0"; break;
	case FeatureLevel::ds_5_0: Target = "ds_5_0"; break;
	case FeatureLevel::gs_5_0: Target = "gs_5_0"; break;
	case FeatureLevel::gs_4_1: Target = "gs_4_1"; break;
	case FeatureLevel::gs_4_0: Target = "gs_4_0"; break;
	case FeatureLevel::hs_5_0: Target = "hs_5_0"; break;
	case FeatureLevel::ps_5_0: Target = "ps_5_0"; break;
	case FeatureLevel::ps_4_1: Target = "ps_4_1"; break;
	case FeatureLevel::ps_4_0: Target = "ps_4_0"; break;
	case FeatureLevel::ps_2_0: Target = "ps_4_0_level_9_1"; break;
	case FeatureLevel::vs_5_0: Target = "vs_5_0"; break;
	case FeatureLevel::vs_4_1: Target = "vs_4_1"; break;
	case FeatureLevel::vs_4_0: Target = "vs_4_0"; break;
	case FeatureLevel::vs_2_0: Target = "vs_4_0_level_9_1"; break;
	}

	const char* EntryPoint = reinterpret_cast<const char*>(entryPoint.Buffer());
	D3DCompile(shaderSource, dataLength, nullptr, nullptr, nullptr, EntryPoint, Target, Flags, 0, &CompiledShader, &ErrorMessages);

	if (ErrorMessages)
	{
		if (errorMsg)
		{
			*errorMsg = SK::String(reinterpret_cast<const char*>(ErrorMessages->GetBufferPointer()));
		}
		ErrorMessages->Release();
	}

	size_t BufferSize = 0;
	void*  Buffer = nullptr;
	if (CompiledShader != nullptr)
	{
		BufferSize = CompiledShader->GetBufferSize();
		Buffer = CompiledShader->GetBufferPointer();
	}

	Array<uint8_t> Result(BufferSize);
	memcpy(Result.Get(), Buffer, BufferSize);

	if (CompiledShader != nullptr)
	{
		CompiledShader->Release();
	}

	return Result;
}

size_t Renderer::CreateVertexShader(const SK::String& name, const uint8_t* compiledSource, uint32_t dataLength, InputLayout layout)
{
	std::hash<SK::String> StringHash;
	size_t ShaderId = StringHash(name);
	SK::Debug::Assert(m_Data->VertexShaders.count(ShaderId) == 0, "Renderer::CreateVertexShader - A shader with that name already exists");

	int InputIndex = static_cast<int>(layout);
	if (m_Data->InputLayout[InputIndex] == nullptr)
	{
		D3D11_INPUT_ELEMENT_DESC* InputDesc = nullptr;
		UINT NumElements = 0;

		switch (layout)
		{
		case InputLayout::Vertex2D:
			InputDesc = Vertex2DDesc;
			NumElements = sizeof(Vertex2DDesc) / sizeof(D3D11_INPUT_ELEMENT_DESC);
			break;

		case InputLayout::Vertex3D:
			InputDesc = Vertex3DDesc;
			NumElements = sizeof(Vertex3DDesc) / sizeof(D3D11_INPUT_ELEMENT_DESC);
			break;

		case InputLayout::Vertex3D_TC:
			InputDesc = Vertex3D_TCDesc;
			NumElements = sizeof(Vertex3D_TCDesc) / sizeof(D3D11_INPUT_ELEMENT_DESC);
			break;

		case InputLayout::Vertex3D_InstanceMatCol:
			InputDesc = Instanced_VertexColourDesc;
			NumElements = sizeof(Instanced_VertexColourDesc) / sizeof(D3D11_INPUT_ELEMENT_DESC);
			break;

		case InputLayout::Vertex3D_InstanceMat:
			InputDesc = Instanced_VertexDesc;
			NumElements = sizeof(Instanced_VertexDesc) / sizeof(D3D11_INPUT_ELEMENT_DESC);
			break;
		}

		m_Data->Device->CreateInputLayout(InputDesc, NumElements, compiledSource, dataLength, &m_Data->InputLayout[InputIndex]);
	}

	DX11VertexShader VertexShader;
	m_Data->Device->CreateVertexShader(compiledSource, dataLength, nullptr, &VertexShader.Shader);

	ID3D11ShaderReflection* Reflection;
	D3DReflect(compiledSource, dataLength, IID_ID3D11ShaderReflection, reinterpret_cast<void**>(&Reflection));
	m_Data->SetupShaderParams(VertexShader, Reflection);
	Reflection->Release();

	m_Data->VertexShaders[ShaderId] = std::move(VertexShader);

	return ShaderId;
}

void Renderer::SetVertexShader(size_t vshaderId)
{
	auto ShaderIterator = m_Data->VertexShaders.find(vshaderId);
	
	if (m_Data->ActiveVertexShader != ShaderIterator)
	{
		if (ShaderIterator != m_Data->VertexShaders.end())
		{
			DX11VertexShader& VertexShader = ShaderIterator->second;
			ID3D11VertexShader* DXVertexShader = ShaderIterator == m_Data->VertexShaders.end() ? nullptr : ShaderIterator->second.Shader;
			m_Data->DeviceContext->VSSetShader(VertexShader.Shader, nullptr, 0);

			for (auto ParamBuffer : VertexShader.ParamBuffers)
			{
				m_Data->DeviceContext->VSSetConstantBuffers(ParamBuffer->Slot, 1, &ParamBuffer->DXBuffer);
			}
			m_Data->ActiveVertexShader = ShaderIterator;
		}
	}
}

size_t Renderer::CreatePixelShader(const SK::String& name, const uint8_t* compiledSource, uint32_t dataLength)
{
	std::hash<SK::String> StringHash;
	size_t ShaderId = StringHash(name);
	SK::Debug::Assert(m_Data->PixelShaders.count(ShaderId) == 0, "Renderer::CreateVertexShader - A shader with that name already exists");

	DX11PixelShader PixelShader;
	m_Data->Device->CreatePixelShader(compiledSource, dataLength, nullptr, &PixelShader.Shader);

	ID3D11ShaderReflection* Reflection;
	D3DReflect(compiledSource, dataLength, IID_ID3D11ShaderReflection, reinterpret_cast<void**>(&Reflection));
	m_Data->SetupShaderParams(PixelShader, Reflection);
	Reflection->Release();

	m_Data->PixelShaders[ShaderId] = std::move(PixelShader);

	return ShaderId;
}

void Renderer::SetPixelShader(size_t pshaderId)
{
	auto ShaderIterator = m_Data->PixelShaders.find(pshaderId);

	if (m_Data->ActivePixelShader != ShaderIterator)
	{
		if (ShaderIterator != m_Data->PixelShaders.end())
		{
			DX11PixelShader& PixelShader = ShaderIterator->second;
			m_Data->DeviceContext->PSSetShader(PixelShader.Shader, nullptr, 0);

			for (auto ParamBuffer : PixelShader.ParamBuffers)
			{
				m_Data->DeviceContext->PSSetConstantBuffers(ParamBuffer->Slot, 1, &ParamBuffer->DXBuffer);
			}

			for (int i = 0; i < MAX_TEXTURE_UNITS; ++i)
			{
				if (PixelShader.Textures[i].InUse)
				{
					PixelShader.Textures[i].Dirty = true;
					PixelShader.Samplers[i].Dirty = true;
				}
			}

			m_Data->ActivePixelShader = ShaderIterator;
		}
	}
}

size_t Renderer::GetShaderParamId(const SK::String& name)
{
	std::hash<SK::String> Hash;
	return Hash(name);
}

void Renderer::SetShaderParam(size_t id, void* data, uint32_t dataSize)
{
	if (m_Data->ActivePixelShader != m_Data->PixelShaders.end())
	{
		DX11PixelShader& PixelShader = m_Data->ActivePixelShader->second;
		if (PixelShader.Params.count(id) > 0)
		{
			auto& Param = PixelShader.Params[id];
			SK::Debug::Assert(dataSize == Param.Size, "DX11Renderer::SetShaderParam: Data sizes do not match.");
			memcpy(Param.ParamBuffer->Buffer + Param.Offset, data, Param.Size);
			Param.ParamBuffer->Dirty = true;
		}
	}

	if (m_Data->ActiveVertexShader != m_Data->VertexShaders.end())
	{
		DX11VertexShader& VertexShader = m_Data->ActiveVertexShader->second;
		if (VertexShader.Params.count(id) > 0)
		{
			auto& Param = VertexShader.Params[id];
			SK::Debug::Assert(dataSize == Param.Size, "DX11Renderer::SetShaderParam: Data sizes do not match.");
			memcpy(Param.ParamBuffer->Buffer + Param.Offset, data, Param.Size);
			Param.ParamBuffer->Dirty = true;
		}
	}
}

void Renderer::SetShaderParam(size_t id, const SK::Math::Matrix& data)
{
	if (m_Data->ActivePixelShader != m_Data->PixelShaders.end())
	{
		DX11PixelShader& PixelShader = m_Data->ActivePixelShader->second;
		if (PixelShader.Params.count(id) > 0)
		{
			auto& Param = PixelShader.Params[id];
			SK::Debug::Assert(sizeof(SK::Math::Matrix) == Param.Size, "DX11Renderer::SetShaderParam: Data sizes do not match.");
			_mm_store_ps(reinterpret_cast<float*>(Param.ParamBuffer->Buffer + Param.Offset), data.Row0);
			_mm_store_ps(reinterpret_cast<float*>(Param.ParamBuffer->Buffer + Param.Offset + 16), data.Row1);
			_mm_store_ps(reinterpret_cast<float*>(Param.ParamBuffer->Buffer + Param.Offset + 32), data.Row2);
			_mm_store_ps(reinterpret_cast<float*>(Param.ParamBuffer->Buffer + Param.Offset + 48), data.Row3);
			//memcpy(Param.ParamBuffer->Buffer + Param.Offset, data, Param.Size);
			Param.ParamBuffer->Dirty = true;
		}
	}

	if (m_Data->ActiveVertexShader != m_Data->VertexShaders.end())
	{
		DX11VertexShader& VertexShader = m_Data->ActiveVertexShader->second;
		if (VertexShader.Params.count(id) > 0)
		{
			auto& Param = VertexShader.Params[id];
			SK::Debug::Assert(sizeof(SK::Math::Matrix) == Param.Size, "DX11Renderer::SetShaderParam: Data sizes do not match.");
			_mm_store_ps(reinterpret_cast<float*>(Param.ParamBuffer->Buffer + Param.Offset), data.Row0);
			_mm_store_ps(reinterpret_cast<float*>(Param.ParamBuffer->Buffer + Param.Offset + 16), data.Row1);
			_mm_store_ps(reinterpret_cast<float*>(Param.ParamBuffer->Buffer + Param.Offset + 32), data.Row2);
			_mm_store_ps(reinterpret_cast<float*>(Param.ParamBuffer->Buffer + Param.Offset + 48), data.Row3);
			//memcpy(Param.ParamBuffer->Buffer + Param.Offset, data, Param.Size);
			Param.ParamBuffer->Dirty = true;
		}
	}
}

void Renderer::SetShaderParam(size_t id, const SK::Math::Vector& data)
{
	if (m_Data->ActivePixelShader != m_Data->PixelShaders.end())
	{
		DX11PixelShader& PixelShader = m_Data->ActivePixelShader->second;
		if (PixelShader.Params.count(id) > 0)
		{
			auto& Param = PixelShader.Params[id];
			SK::Debug::Assert(sizeof(SK::Math::Vector) == Param.Size, "DX11Renderer::SetShaderParam: Data sizes do not match.");
			_mm_store_ps(reinterpret_cast<float*>(Param.ParamBuffer->Buffer + Param.Offset), data.Data);
			//memcpy(Param.ParamBuffer->Buffer + Param.Offset, data, Param.Size);
			Param.ParamBuffer->Dirty = true;
		}
	}

	if (m_Data->ActiveVertexShader != m_Data->VertexShaders.end())
	{
		DX11VertexShader& VertexShader = m_Data->ActiveVertexShader->second;
		if (VertexShader.Params.count(id) > 0)
		{
			auto& Param = VertexShader.Params[id];
			SK::Debug::Assert(sizeof(SK::Math::Vector) == Param.Size, "DX11Renderer::SetShaderParam: Data sizes do not match.");
			_mm_store_ps(reinterpret_cast<float*>(Param.ParamBuffer->Buffer + Param.Offset), data.Data);
			//memcpy(Param.ParamBuffer->Buffer + Param.Offset, data, Param.Size);
			Param.ParamBuffer->Dirty = true;
		}
	}
}

void Renderer::SetShaderTexture(int textureUnit, TexturePtr texture)
{
	SK::Debug::Assert(textureUnit >= 0 && textureUnit < MAX_TEXTURE_UNITS, "Renderer::SetShaderTexture - Invalid texture unit");
	DX11Texture* DXTexture = static_cast<DX11Texture*>(texture.get());
	ID3D11ShaderResourceView* ResourceView = DXTexture != nullptr ? DXTexture->SRV : nullptr;

	if (m_Data->ActivePixelShader != m_Data->PixelShaders.end())
	{
		DX11PixelShader& PixelShader = m_Data->ActivePixelShader->second;
		//SK::Debug::Assert(PixelShader.Textures[textureUnit].InUse, "Renderer::SetShaderTexture - Texture unit is not in use");

		if (PixelShader.Textures[textureUnit].ResourceView != ResourceView)
		{
			PixelShader.Textures[textureUnit].ResourceView = ResourceView;
			PixelShader.Textures[textureUnit].Dirty = true;
		}
	}
}

void Renderer::SetShaderSampler(int samplerUnit, SamplerState sampler)
{
	SK::Debug::Assert(samplerUnit >= 0 && samplerUnit < MAX_TEXTURE_UNITS, "Renderer::SetShaderTexture - Invalid sampler unit");
	
	if (m_Data->ActivePixelShader != m_Data->PixelShaders.end())
	{
		DX11PixelShader& PixelShader = m_Data->ActivePixelShader->second;
		if (PixelShader.Samplers[samplerUnit].Sampler != sampler)
		{
			PixelShader.Samplers[samplerUnit].Sampler = sampler;
			PixelShader.Samplers[samplerUnit].Dirty = true;
		}
	}
}

void Renderer::CommitShaderParams()
{
	if (m_Data->ActivePixelShader != m_Data->PixelShaders.end())
	{
		DX11PixelShader& PixelShader = m_Data->ActivePixelShader->second;
		for (auto ParamBuffer : PixelShader.ParamBuffers)
		{
			if (ParamBuffer->Dirty)
			{
				m_Data->DeviceContext->UpdateSubresource(ParamBuffer->DXBuffer, 0, nullptr, ParamBuffer->Buffer, 0, 0);
				ParamBuffer->Dirty = false;
			}
		}

		for (int i = 0; i < MAX_TEXTURE_UNITS; ++i)
		{
			if (PixelShader.Textures[i].Dirty)
			{
				m_Data->DeviceContext->PSSetShaderResources(i, 1, &PixelShader.Textures[i].ResourceView);
				PixelShader.Textures[i].Dirty = false;
			}

			if (PixelShader.Samplers[i].Dirty)
			{
				int SamplerIndex = static_cast<int>(PixelShader.Samplers[i].Sampler);
				m_Data->DeviceContext->PSSetSamplers(i, 1, &m_Data->SamplerStates[SamplerIndex]);
				PixelShader.Samplers[i].Dirty = false;
			}
		}
	}

	if (m_Data->ActiveVertexShader != m_Data->VertexShaders.end())
	{
		DX11VertexShader& VertexShader = m_Data->ActiveVertexShader->second;
		for (auto ParamBuffer : VertexShader.ParamBuffers)
		{
			if (ParamBuffer->Dirty)
			{
				m_Data->DeviceContext->UpdateSubresource(ParamBuffer->DXBuffer, 0, nullptr, ParamBuffer->Buffer, 0, 0);
				ParamBuffer->Dirty = false;
			}
		}
	}
}

void Renderer::SetRenderTarget(TexturePtr texture, int face, TexturePtr depthTexture)
{
	SetRenderTargets(&texture, &face, 1, depthTexture);
}

void Renderer::SetRenderTargets(TexturePtr* textures, int* faces, int numTargets, TexturePtr depthTexture)
{
	bool Different = numTargets != m_Data->NumRenderTargets;
	for (int i = 0; i < numTargets; ++i)
	{
		if (textures[i] != m_Data->RenderTargetTexture[i] || faces[i] != m_Data->RenderTargetFace[i])
		{
			Different = true;
			if (m_Data->RenderTargetTexture[i] && i < m_Data->NumRenderTargets)
				m_Data->ResolveRenderTarget(m_Data->RenderTargetTexture[i], m_Data->RenderTargetFace[i]);
		}
		else if (depthTexture != m_Data->DepthStencilTexture)
		{
			Different = true;
		}
	}

	for (int i = numTargets; i < m_Data->NumRenderTargets; ++i)
	{
		if (m_Data->RenderTargetTexture[i])
			m_Data->ResolveRenderTarget(m_Data->RenderTargetTexture[i], m_Data->RenderTargetFace[i]);
	}

	if (Different)
	{
		//Set new render targets
		ID3D11DepthStencilView* DepthStencil = depthTexture ? static_cast<DX11Texture*>(depthTexture.get())->DSV : nullptr;
		ID3D11RenderTargetView* RTS[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];

		int NumRTS = std::min(numTargets, D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT);
		for (int i = 0; i < NumRTS; ++i)
		{
			DX11Texture* D3DTexture = static_cast<DX11Texture*>(textures[i].get());
			RTS[i] = D3DTexture->RTV[faces[i]];
		}
		m_Data->DeviceContext->OMSetRenderTargets(NumRTS, RTS, DepthStencil);

		//Update references
		for (int i = 0; i < NumRTS; ++i)
		{
			m_Data->RenderTargetTexture[i] = textures[i];
			m_Data->RenderTargetFace[i] = faces[i];
		}
		for (int i = NumRTS; i < m_Data->NumRenderTargets; ++i)
			m_Data->RenderTargetTexture[i] = nullptr;
		m_Data->DepthStencilTexture = depthTexture;
		m_Data->NumRenderTargets = NumRTS;
	}
}

void Renderer::SetDefaultRenderTarget()
{
	if (m_Data->NumRenderTargets > 0)
	{
		for (int i = 0; i < m_Data->NumRenderTargets; ++i)
			m_Data->ResolveRenderTarget(m_Data->RenderTargetTexture[i], m_Data->RenderTargetFace[i]);

		m_Data->DeviceContext->OMSetRenderTargets(1, &m_Data->BackBufferView, m_Data->DepthBufferView);

		for (int i = 0; i < m_Data->NumRenderTargets; ++i)
			m_Data->RenderTargetTexture[i] = nullptr;
		m_Data->DepthStencilTexture = nullptr;
		m_Data->NumRenderTargets = 0;
	}
}

void Renderer::SetViewport(const SK::Math::int2& origin, const SK::Math::int2& size)
{
	D3D11_VIEWPORT vp;
	vp.Width = size.x;
	vp.Height = size.y;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = origin.x;
	vp.TopLeftY = origin.y;
	m_Data->DeviceContext->RSSetViewports(1, &vp);
}

void Renderer::SetDepthStencilMode(DepthStencilMode mode)
{
	//SKTD: Don't set same state again
	int StateIndex = static_cast<int>(mode);
	m_Data->DeviceContext->OMSetDepthStencilState(m_Data->DepthStencilStates[StateIndex], 0);
}

void Renderer::SetCullingMode(CullMode mode)
{
	int StateIndex = static_cast<int>(mode);
	m_Data->DeviceContext->RSSetState(m_Data->RasterizerStates[StateIndex]);
}

void Renderer::SetBlendingMode(BlendMode mode)
{
	int StateIndex = static_cast<int>(mode);
	static const float White[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	m_Data->DeviceContext->OMSetBlendState(m_Data->BlendStates[StateIndex], White, 0xFFFFFFFF);
}

void Renderer::SetInputLayout(InputLayout layout)
{
	int LayoutIndex = static_cast<int>(layout);
	m_Data->DeviceContext->IASetInputLayout(m_Data->InputLayout[LayoutIndex]);
}

void Renderer::Clear(const ColourFloat& clearColour)
{
	float col[4] = { clearColour.R(), clearColour.G(), clearColour.B(), 1.0f };
	if (m_Data->NumRenderTargets > 0)
	{
		for (int i = 0; i < m_Data->NumRenderTargets; ++i)
		{
			DX11Texture* D3DTexture = static_cast<DX11Texture*>(m_Data->RenderTargetTexture[i].get());
			m_Data->DeviceContext->ClearRenderTargetView(D3DTexture->RTV[m_Data->RenderTargetFace[i]], col);
		}
	}
	else
	{
		m_Data->DeviceContext->ClearRenderTargetView(m_Data->BackBufferView, col);
	}

	m_Data->DeviceContext->ClearDepthStencilView(m_Data->DepthBufferView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

void Renderer::DrawBuffer(PrimType primType, VertexBufferPtr vbuff, uint32_t numVerts, uint32_t offset)
{
	m_Data->SetVertexBuffer(primType, vbuff);
	m_Data->DeviceContext->Draw(static_cast<UINT>(numVerts), static_cast<UINT>(offset));
}

void Renderer::DrawBufferIndexed(PrimType primType, VertexBufferPtr vbuff, IndexBufferPtr ibuff, uint32_t numIndis, uint32_t indexOffset, uint32_t vertexOffset)
{
	m_Data->SetVertexBuffer(primType, vbuff);
	m_Data->SetIndexBuffer(ibuff);
	m_Data->DeviceContext->DrawIndexed(static_cast<UINT>(numIndis), static_cast<UINT>(indexOffset), static_cast<UINT>(vertexOffset));
}

void Renderer::DrawBufferInstanced(PrimType primType, VertexBufferPtr vbuff, uint32_t numVerts, uint32_t vertexOffset, VertexBufferPtr instanceBuff, uint32_t numInstances, uint32_t instanceOffset)
{
	m_Data->SetInstancedVertexBuffer(primType, vbuff, instanceBuff);
	m_Data->DeviceContext->DrawInstanced(static_cast<UINT>(numVerts), static_cast<UINT>(numInstances), static_cast<UINT>(vertexOffset), static_cast<UINT>(instanceOffset));
}

void Renderer::DrawBufferIndexedInstanced(PrimType primType, VertexBufferPtr vbuff, IndexBufferPtr ibuff, uint32_t numIndis, uint32_t indexOffset, uint32_t vertexOffset, VertexBufferPtr instanceBuff, uint32_t numInstances, uint32_t instanceOffset)
{
	m_Data->SetInstancedVertexBuffer(primType, vbuff, instanceBuff);
	m_Data->SetIndexBuffer(ibuff);
	m_Data->DeviceContext->DrawIndexedInstanced(static_cast<UINT>(numIndis), static_cast<UINT>(numInstances), static_cast<UINT>(indexOffset), static_cast<UINT>(vertexOffset), 0);
}

void Renderer::Present()
{
#ifdef _DEBUG
	m_Data->SwapChain->Present(0, 0);
#else
	m_Data->SwapChain->Present(m_Data->CurrentSettings.PresentInterval, 0);
#endif
}

void Renderer::Flush()
{
	m_Data->DeviceContext->Flush();
}

//-- Private Methods --//
//=====================//

void Renderer::PlatformData::SetVertexBuffer(PrimType primType, VertexBufferPtr vertexBuffer)
{
	ID3D11Buffer* Buffer = reinterpret_cast<ID3D11Buffer*>(vertexBuffer->Handle());
	if (this->ActiveVertexBuffer[0] != Buffer || this->ActiveVertexBuffer[1] != nullptr)
	{
		UINT Stride = VertexSize(vertexBuffer->Type());
		UINT Offset = 0;
		this->DeviceContext->IASetVertexBuffers(0, 1, &Buffer, &Stride, &Offset);

		this->ActiveVertexBuffer[0] = Buffer;
		this->ActiveVertexBuffer[1] = nullptr;
	}

	switch (primType)
	{
	case PrimType::PointList: this->DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST); break;
	case PrimType::LineList: this->DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST); break;
	case PrimType::LineStrip: this->DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINESTRIP); break;
	case PrimType::TriangleList: this->DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST); break;
	case PrimType::TriangleStrip: this->DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP); break;
	}
}

void Renderer::PlatformData::SetInstancedVertexBuffer(PrimType primType, VertexBufferPtr vertexBuffer, VertexBufferPtr instanceBuffer)
{
	ID3D11Buffer* VertexBuffer = reinterpret_cast<ID3D11Buffer*>(vertexBuffer->Handle());
	ID3D11Buffer* InstanceBuffer = reinterpret_cast<ID3D11Buffer*>(instanceBuffer->Handle());
	if (this->ActiveVertexBuffer[0] != VertexBuffer || this->ActiveVertexBuffer[1] != InstanceBuffer)
	{
		ID3D11Buffer* Buffers[2] = { VertexBuffer, InstanceBuffer };
		UINT Strides[2] = { VertexSize(vertexBuffer->Type()), VertexSize(instanceBuffer->Type()) };
		UINT Offsets[2] = { 0, 0 };
		this->DeviceContext->IASetVertexBuffers(0, 2, Buffers, Strides, Offsets);

		this->ActiveVertexBuffer[0] = VertexBuffer;
		this->ActiveVertexBuffer[1] = InstanceBuffer;
	}

	switch (primType)
	{
	case PrimType::PointList: this->DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST); break;
	case PrimType::LineList: this->DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST); break;
	case PrimType::LineStrip: this->DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINESTRIP); break;
	case PrimType::TriangleList: this->DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST); break;
	case PrimType::TriangleStrip: this->DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP); break;
	}
}

void Renderer::PlatformData::SetIndexBuffer(IndexBufferPtr indexBuffer)
{
	ID3D11Buffer* Buffer = reinterpret_cast<ID3D11Buffer*>(indexBuffer->Handle());
	if (this->ActiveIndexBuffer != Buffer)
	{
		this->DeviceContext->IASetIndexBuffer(Buffer, DXGI_FORMAT_R16_UINT, 0);
		this->ActiveIndexBuffer = Buffer;
	}
}

void Renderer::PlatformData::ResolveRenderTarget(TexturePtr texture, int face)
{
	DX11Texture* DXTexture = static_cast<DX11Texture*>(texture.get());

	if (texture->Flags().HasFlags(TextureFlag::MSAA))
	{
		UINT Subresource = D3D11CalcSubresource(0, face, 1);
		this->DeviceContext->ResolveSubresource(DXTexture->ResolveTexture, Subresource, DXTexture->MainTexture, Subresource, DXTexture->DXGIFormat);
	}

	if (texture->Flags().HasFlags(TextureFlag::MipMapped))
	{
		this->DeviceContext->GenerateMips(DXTexture->SRV);
	}
}

void Renderer::PlatformData::SetupShaderParams(DX11VertexShader& shader, ID3D11ShaderReflection* reflection)
{
	for (auto ParamBuffer : shader.ParamBuffers)
	{
		ParamBuffer->DXBuffer->Release();
		SK::AlignedDelete(ParamBuffer->Buffer);
		//delete [] ParamBuffer->Buffer;
		delete ParamBuffer;
	}
	shader.ParamBuffers.clear();
	shader.Params.clear();

	D3D11_SHADER_DESC ShaderDesc;
	reflection->GetDesc(&ShaderDesc);
	for (UINT i = 0; i < ShaderDesc.BoundResources; ++i)
	{
		D3D11_SHADER_INPUT_BIND_DESC BindingDesc;
		reflection->GetResourceBindingDesc(i, &BindingDesc);

		if (BindingDesc.Type == D3D_SIT_CBUFFER)
		{
			GetParams(reflection, BindingDesc, shader.Params, shader.ParamBuffers);
		}
	}
}

void Renderer::PlatformData::SetupShaderParams(DX11PixelShader& shader, ID3D11ShaderReflection* reflection)
{
	for (int i = 0; i < MAX_TEXTURE_UNITS; ++i)
	{
		shader.Textures[i].Dirty = false;
		shader.Textures[i].InUse = false;
		shader.Textures[i].ResourceView = nullptr;

		shader.Samplers[i].Dirty = false;
		shader.Samplers[i].Sampler = SamplerState::TriLinear;
	}

	for (auto ParamBuffer : shader.ParamBuffers)
	{
		ParamBuffer->DXBuffer->Release();
		SK::AlignedDelete(ParamBuffer->Buffer);
		//delete [] ParamBuffer->Buffer;
		delete ParamBuffer;
	}
	shader.ParamBuffers.clear();
	shader.Params.clear();

	D3D11_SHADER_DESC ShaderDesc;
	reflection->GetDesc(&ShaderDesc);
	for (UINT i = 0; i < ShaderDesc.BoundResources; ++i)
	{
		D3D11_SHADER_INPUT_BIND_DESC BindingDesc;
		reflection->GetResourceBindingDesc(i, &BindingDesc);

		if (BindingDesc.Type == D3D_SIT_CBUFFER)
		{
			GetParams(reflection, BindingDesc, shader.Params, shader.ParamBuffers);
		}
		else if (BindingDesc.Type == D3D_SIT_TEXTURE)
		{
			shader.Textures[BindingDesc.BindPoint].InUse = true;
			shader.Textures[BindingDesc.BindPoint].Dirty = true;
			
			shader.Samplers[BindingDesc.BindPoint].Dirty = true;
		}
	}
}

void Renderer::PlatformData::GetParams(ID3D11ShaderReflection* reflection, D3D11_SHADER_INPUT_BIND_DESC& bindingDesc, ParamMap& params, ParamBufferList& paramBufferList)
{
	D3D11_SHADER_BUFFER_DESC ShaderBufferDesc;
	auto ConstBuffer = reflection->GetConstantBufferByName(bindingDesc.Name);
	ConstBuffer->GetDesc(&ShaderBufferDesc);
	reflection->GetResourceBindingDescByName(ShaderBufferDesc.Name, &bindingDesc);

	ShaderParamBuffer* ParamBuffer = new ShaderParamBuffer;
	ParamBuffer->Buffer = static_cast<uint8_t*>(SK::AlignedNew(16, ShaderBufferDesc.Size));
	//ParamBuffer->Buffer = new uint8_t[ShaderBufferDesc.Size];
	ParamBuffer->Size = ShaderBufferDesc.Size;
	ParamBuffer->Slot = bindingDesc.BindPoint;

	D3D11_BUFFER_DESC DXBufferDesc;
	memset(&DXBufferDesc, 0, sizeof(DXBufferDesc));
	DXBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	DXBufferDesc.ByteWidth = ShaderBufferDesc.Size;
	DXBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	Device->CreateBuffer(&DXBufferDesc, nullptr, &ParamBuffer->DXBuffer);

	std::hash<SK::String> NameHash;
	for (UINT j = 0; j < ShaderBufferDesc.Variables; ++j)
	{
		D3D11_SHADER_VARIABLE_DESC ParamDesc;
		auto DXParam = ConstBuffer->GetVariableByIndex(j);
		DXParam->GetDesc(&ParamDesc);

		ShaderParam Param;
		Param.ParamBuffer = ParamBuffer;
		Param.Offset = ParamDesc.StartOffset;
		Param.Size = ParamDesc.Size;
		size_t Hash = NameHash(SK::String(ParamDesc.Name));

		// Add param to map
		params[Hash] = Param;

		// Fill out default param
		if (ParamDesc.DefaultValue)
		{
			memcpy(ParamBuffer->Buffer + Param.Offset, ParamDesc.DefaultValue, Param.Size);
		}
	}

	ParamBuffer->Dirty = true;
	paramBufferList.push_back(ParamBuffer);
}

void Renderer::PlatformData::Release()
{
	if (this->BackBufferView != nullptr)
	{
		this->BackBufferView->Release();
	}

	if (this->BackBuffer != nullptr)
	{
		this->BackBuffer->Release();
	}

	if (this->DepthBufferView != nullptr)
	{
		this->DepthBufferView->Release();
	}

	if (this->DepthBuffer != nullptr)
	{
		this->DepthBuffer->Release();
	}

	for (int i = 0; i < NUM_INPUT_LAYOUTS; ++i)
	{
		if (this->InputLayout[i] != nullptr)
		{
			this->InputLayout[i]->Release();
		}
	}

	for (int i = 0; i < 4; ++i)
	{
		if (this->DepthStencilStates[i] != nullptr)
		{
			this->DepthStencilStates[i]->Release();
		}

		if (this->BlendStates[i] != nullptr)
		{
			this->BlendStates[i]->Release();
		}
	}

	for (int i = 0; i < 3; ++i)
	{
		if (this->RasterizerStates[i] != nullptr)
		{
			this->RasterizerStates[i]->Release();
		}
	}

	for (int i = 0; i < 6; ++i)
	{
		if (this->SamplerStates[i] != nullptr)
		{
			this->SamplerStates[i]->Release();
		}
	}

	if (this->SwapChain != nullptr)
	{
		this->SwapChain->Release();
	}

	if (this->DeviceContext != nullptr)
	{
		this->DeviceContext->ClearState();
		this->DeviceContext->Flush();
		this->DeviceContext->Release();
	}

	if (this->Device != nullptr)
	{
		this->Device->Release();
	}
}

namespace
{
	template<typename TextureDesc>
	TextureDesc GetTextureDesc(TextureFormat format, TextureFlags flags)
	{
		bool IsDepthBuffer = format == TextureFormat::Depth_32 || format == TextureFormat::DepthStencil_32;

		TextureDesc Desc = { 0 };
		Desc.Format = GetDXFormat(format);
		Desc.MipLevels = (flags.HasFlags(TextureFlag::MSAA) || flags.HasFlags(TextureFlag::Lockable) || flags.HasFlags(TextureFlag::Staging) || flags.HasFlags(TextureFlag::RenderTarget)) ? 1 : 0;

		if (flags.HasFlags(TextureFlag::Lockable))
		{
			Desc.Usage = D3D11_USAGE_DYNAMIC;
			Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		}
		else if (flags.HasFlags(TextureFlag::RenderTarget) || IsDepthBuffer)
		{
			Desc.Usage = D3D11_USAGE_DEFAULT;
			Desc.CPUAccessFlags = 0;
		}
		else if (flags.HasFlags(TextureFlag::Staging))
		{
			Desc.Usage = D3D11_USAGE_STAGING;
			Desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
		}
		else
		{
			// Not a render target, and not lockable. Just a static resource
			Desc.Usage = D3D11_USAGE_IMMUTABLE;
			Desc.CPUAccessFlags = 0;
		}

		if (IsDepthBuffer)
		{
			Desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		}
		else if (!flags.HasFlags(TextureFlag::Staging))
		{
			Desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

			if (flags.HasFlags(TextureFlag::RenderTarget))
				Desc.BindFlags |= D3D11_BIND_RENDER_TARGET;

			if (flags.HasFlags(TextureFlag::MipMapped))
				Desc.MiscFlags |= D3D11_RESOURCE_MISC_GENERATE_MIPS;
		}

		return Desc;
	}

	DXGI_FORMAT GetDXFormat(TextureFormat format)
	{
		switch (format)
		{
		case TextureFormat::RGBA_32: return DXGI_FORMAT_R8G8B8A8_UNORM;
		case TextureFormat::RGBA_64: return DXGI_FORMAT_R16G16B16A16_FLOAT;
		case TextureFormat::RGBA_128: return DXGI_FORMAT_R32G32B32A32_FLOAT;
		case TextureFormat::RG_32: return DXGI_FORMAT_R16G16_FLOAT;
		case TextureFormat::RG_64: return DXGI_FORMAT_R32G32_FLOAT;
		case TextureFormat::Red_8: return DXGI_FORMAT_R8_UNORM;
		case TextureFormat::Red_16: return DXGI_FORMAT_R16_FLOAT;
		case TextureFormat::Red_32: return DXGI_FORMAT_R32_FLOAT;
		case TextureFormat::Depth_32: return DXGI_FORMAT_D32_FLOAT;
		case TextureFormat::DepthStencil_32: return DXGI_FORMAT_D24_UNORM_S8_UINT;
		case TextureFormat::RGBA_UINT32: return DXGI_FORMAT_R8G8B8A8_UINT;
		case TextureFormat::RGBA_UINT64: return DXGI_FORMAT_R16G16B16A16_UINT;
		case TextureFormat::RGBA_UINT128: return DXGI_FORMAT_R32G32B32A32_UINT;
		case TextureFormat::RG_UINT32: return DXGI_FORMAT_R16G16_UINT;
		case TextureFormat::RG_UINT64: return DXGI_FORMAT_R32G32_UINT;
		}

		return DXGI_FORMAT_UNKNOWN;
	}

	TextureFormat GetSKFormat(DXGI_FORMAT format)
	{
		switch (format)
		{
		case DXGI_FORMAT_R8G8B8A8_UNORM: return TextureFormat::RGBA_32;
		case DXGI_FORMAT_R16G16B16A16_FLOAT: return TextureFormat::RGBA_64;
		case DXGI_FORMAT_R32G32B32A32_FLOAT: return TextureFormat::RGBA_128;
		case DXGI_FORMAT_R16G16_FLOAT: return TextureFormat::RG_32;
		case DXGI_FORMAT_R32G32_FLOAT: return TextureFormat::RG_64;
		case DXGI_FORMAT_R8_UNORM: return TextureFormat::Red_8;
		case DXGI_FORMAT_R16_FLOAT: return TextureFormat::Red_16;
		case DXGI_FORMAT_R32_FLOAT: return TextureFormat::Red_32;
		case DXGI_FORMAT_D32_FLOAT: return TextureFormat::Depth_32;
		case DXGI_FORMAT_D24_UNORM_S8_UINT: return TextureFormat::DepthStencil_32;
		case DXGI_FORMAT_R8G8B8A8_UINT: return TextureFormat::RGBA_UINT32;
		case DXGI_FORMAT_R16G16B16A16_UINT: return TextureFormat::RGBA_UINT64;
		case DXGI_FORMAT_R32G32B32A32_UINT: return TextureFormat::RGBA_UINT128;
		case DXGI_FORMAT_R16G16_UINT: return TextureFormat::RG_UINT32;
		case DXGI_FORMAT_R32G32_UINT: return TextureFormat::RG_UINT64;
		}

		return TextureFormat::Unknown;
	}
}
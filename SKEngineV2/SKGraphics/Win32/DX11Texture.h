#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKGraphics/Texture.h>
#include <d3d11_1.h>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		class DX11Texture : public Texture
		{
		public:

			//---- Types ----//
			//===============//

			DX11Texture(const Math::int3& size, TextureFormat format, TextureFlags flags)
			: MainTexture(nullptr), ResolveTexture(nullptr), DSV(nullptr), SRV(nullptr), Texture(size, format, flags)
			{
				for (int i = 0; i < 6; ++i)
				{
					RTV[i] = nullptr;
				}
			}

			~DX11Texture()
			{
				MainTexture->Release();

				if (ResolveTexture != nullptr)
				{
					ResolveTexture->Release();
				}

				for (int i = 0; i < 6; ++i)
				{
					if (RTV[i] != nullptr)
					{
						RTV[i]->Release();
					}
				}

				if (DSV != nullptr)
				{
					DSV->Release();
				}

				if (SRV != nullptr)
				{
					SRV->Release();
				}
			}

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			DXGI_FORMAT               DXGIFormat;
			ID3D11Resource*           MainTexture;
			ID3D11Resource*           ResolveTexture;
			ID3D11RenderTargetView*   RTV[6];
			ID3D11DepthStencilView*   DSV;
			ID3D11ShaderResourceView* SRV;
		};

		//--- Methods ---//
		//===============//
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <d3d11_1.h>
#include <vector>
#include <map>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//
		
		struct ShaderParamBuffer;

		struct ShaderParam
		{
			size_t             Offset;
			size_t             Size;
			ShaderParamBuffer* ParamBuffer;
		};

		struct ShaderResource
		{
			ShaderResource() : ResourceView(nullptr), Dirty(false), InUse(false) {}

			ID3D11ShaderResourceView* ResourceView;
			bool                      Dirty;
			bool                      InUse;
		};

		struct ShaderSampler
		{
			SamplerState Sampler;
			bool         Dirty;
		};

		struct ShaderParamBuffer
		{
			ShaderParamBuffer() : Buffer(nullptr), Size(0), Dirty(false) {}

			uint8_t*      Buffer;
			uint32_t      Size;
			uint32_t      Slot;
			ID3D11Buffer* DXBuffer;
			bool          Dirty;
		};

		typedef std::vector<ShaderParamBuffer*>  ParamBufferList;
		typedef std::map<size_t, ShaderParam>    ParamMap;
		typedef std::map<size_t, ShaderResource> ResourceMap;

		class DX11VertexShader
		{
		public:

			//--- Methods ---//
			//===============//

			DX11VertexShader() : Shader(nullptr)
			{

			}

			DX11VertexShader(DX11VertexShader&& rhs) throw()
			: Shader(rhs.Shader)
			, ParamBuffers(std::move(rhs.ParamBuffers))
			, Params(std::move(rhs.Params))
			{
				rhs.Shader = nullptr;
			}

			DX11VertexShader& operator=(DX11VertexShader&& rhs) throw()
			{
				Shader = rhs.Shader;
				ParamBuffers = std::move(rhs.ParamBuffers);
				Params = std::move(rhs.Params);
				rhs.Shader = nullptr;

				return *this;
			}
		
			~DX11VertexShader()
			{
				for (auto ParamBuffer : ParamBuffers)
				{
					ParamBuffer->DXBuffer->Release();
					SK::AlignedDelete(ParamBuffer->Buffer);
					//delete[] ParamBuffer->Buffer;
					delete ParamBuffer;
				}

				if (Shader)
				{
					Shader->Release();
				}
			}

			//-- Variables --//
			//===============//

			ID3D11VertexShader* Shader;
			ParamBufferList     ParamBuffers;
			ParamMap            Params;

		private:
			DX11VertexShader(const DX11VertexShader& other) = delete;
			DX11VertexShader& operator=(const DX11VertexShader& other) = delete;
		};

		class DX11PixelShader
		{
		public:

			//-- Variables --//
			//===============//

			DX11PixelShader() : Shader(nullptr) {}

			DX11PixelShader(DX11PixelShader&& rhs) throw()
			: Shader(rhs.Shader)
			, ParamBuffers(std::move(rhs.ParamBuffers))
			, Params(std::move(rhs.Params))
			{
				rhs.Shader = nullptr;

				for (int i = 0; i < MAX_TEXTURE_UNITS; ++i)
				{
					Textures[i] = rhs.Textures[i];
					Samplers[i] = rhs.Samplers[i];

					rhs.Textures[i].ResourceView = nullptr;
					rhs.Textures[i].InUse = false;
				}
			}

			DX11PixelShader& operator=(DX11PixelShader&& rhs) throw()
			{
				Shader = rhs.Shader;
				ParamBuffers = std::move(rhs.ParamBuffers);
				Params = std::move(rhs.Params);
				rhs.Shader = nullptr;

				for (int i = 0; i < MAX_TEXTURE_UNITS; ++i)
				{
					Textures[i] = rhs.Textures[i];
					Samplers[i] = rhs.Samplers[i];

					rhs.Textures[i].ResourceView = nullptr;
					rhs.Textures[i].InUse = false;
				}

				return *this;
			}

			~DX11PixelShader()
			{
				for (auto ParamBuffer : ParamBuffers)
				{
					ParamBuffer->DXBuffer->Release();
					SK::AlignedDelete(ParamBuffer->Buffer);
					//delete[] ParamBuffer->Buffer;
					delete ParamBuffer;
				}

				if (Shader)
				{
					Shader->Release();
				}
			}

			ID3D11PixelShader* Shader;
			ParamBufferList    ParamBuffers;
			ParamMap           Params;

			ShaderResource     Textures[MAX_TEXTURE_UNITS];
			ShaderSampler      Samplers[MAX_TEXTURE_UNITS];
		
		private:
			DX11PixelShader(const DX11PixelShader& other) = delete;
			DX11PixelShader& operator=(const DX11PixelShader& other) = delete;
		};

		//--- Methods ---//
		//===============//
	}
}
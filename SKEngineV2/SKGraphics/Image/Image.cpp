//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKGraphics/Image/BitmapFileFormat.h>
#include <SKSystem/Cryptography/CRC.h>
#include <SKSystem/IO/DeflateStream.h>
#include <SKSystem/IO/MemoryStream.h>
#include <SKSystem/Array.h>
#include <SKGraphics/Image/lodepng.h>
#include "Image.h"

using namespace SK::IO;
using namespace SK::Graphics;

//-- Declarations --//
//==================//

namespace
{
	bool IsPng(uint8_t signature[8]);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Image Image::Load(SK::IO::Stream& input, uint64_t length)
{
	Image NewImage;

	uint64_t StreamStartPosition = input.Position();
	uint8_t Signature[8];
	input.ReadData(Signature, 0, 8);

	if (IsPng(Signature))
	{
		LodePNGState State;
		lodepng_state_init(&State);
		input.Position(StreamStartPosition);
		lodepng_inspect(&NewImage.m_Width, &NewImage.m_Height, &State, input);
		switch (State.info_png.color.colortype)
		{
		case LodePNGColorType::LCT_GREY:
		case LodePNGColorType::LCT_RGB:
		case LodePNGColorType::LCT_BGR:
		case LodePNGColorType::LCT_PALETTE:
			State.info_png.color.colortype = LodePNGColorType::LCT_RGB;
			NewImage.m_Format = InternalFormat::RGB;
			break;

		case LodePNGColorType::LCT_GREY_ALPHA:
		case LodePNGColorType::LCT_RGBA:
		case LodePNGColorType::LCT_BGRA:
			State.info_png.color.colortype = LodePNGColorType::LCT_RGBA;
			NewImage.m_Format = InternalFormat::RGBA;
			break;
		}
		
		input.Position(StreamStartPosition);
		lodepng::decode(&NewImage.m_Data, NewImage.m_Width, NewImage.m_Height, input, State.info_png.color.colortype, State.info_png.color.bitdepth);
		NewImage.m_BitDepth = State.info_png.color.bitdepth;
	}
	else
	{
		input.Position(StreamStartPosition);
		GetImageData(input, length, NewImage);
	}

	return NewImage;
}

Image::Image(int width, int height, InternalFormat format)
: m_Width(width)
, m_Height(height)
, m_Format(format)
, m_BitDepth(8)
{
	switch (format)
	{
	case InternalFormat::BGR:
	case InternalFormat::RGB: m_Data = new uint8_t[m_Width * m_Height * 3]; break;
	case InternalFormat::BGRA:
	case InternalFormat::RGBA: m_Data = new uint8_t[m_Width * m_Height * 4]; break;
	}
}

Image::Image(const Image& other)
: m_Width(other.m_Width)
, m_Height(other.m_Height)
, m_Format(other.m_Format)
, m_BitDepth(other.m_BitDepth)
{
	int DataSize = 0;
	switch (m_Format)
	{
	case InternalFormat::BGR:
	case InternalFormat::RGB: DataSize = m_Width * m_Height * 3 * (m_BitDepth / 8); break;
	case InternalFormat::BGRA:
	case InternalFormat::RGBA: DataSize = m_Width * m_Height * 4 * (m_BitDepth / 8); break;
	}

	m_Data = new uint8_t[DataSize];
	memcpy(m_Data, other.m_Data, DataSize);
}

Image& Image::operator=(const Image& other)
{
	delete [] m_Data;

	m_Width = other.m_Width;
	m_Height = other.m_Height;
	m_Format = other.m_Format;
	m_BitDepth = other.m_BitDepth;

	int DataSize = 0;
	switch (m_Format)
	{
	case InternalFormat::BGR:
	case InternalFormat::RGB: DataSize = m_Width * m_Height * 3 * (m_BitDepth / 8); break;
	case InternalFormat::BGRA:
	case InternalFormat::RGBA: DataSize = m_Width * m_Height * 4 * (m_BitDepth / 8); break;
	}

	m_Data = new uint8_t[DataSize];
	memcpy(m_Data, other.m_Data, DataSize);

	return *this;
}

Image::Image(Image&& other)
: m_Width(other.m_Width)
, m_Height(other.m_Height)
, m_Format(other.m_Format)
, m_BitDepth(other.m_BitDepth)
, m_Data(other.m_Data)
{
	other.m_Data = nullptr;
	other.m_Width = 0;
	other.m_Height = 0;
}

Image& Image::operator = (Image&& other)
{
	delete [] m_Data;

	m_Width = other.m_Width;
	m_Height = other.m_Height;
	m_Format = other.m_Format;
	m_BitDepth = other.m_BitDepth;
	m_Data = other.m_Data;

	other.m_Data = nullptr;
	other.m_Width = 0;
	other.m_Height = 0;

	return *this;
}

Image::~Image()
{
	delete [] m_Data;
}

void Image::Save(Stream& output, FileFormat format) const
{
	switch (format)
	{
	case FileFormat::BMP24: WriteBitmap24(output); break;
	case FileFormat::PNG: WritePng(output); break;
	}
}

//-- Private Methods --//
//=====================//

void Image::WriteBitmap24(Stream& output) const
{
	int BitmapPadding = 0;
	int BitmapStride = (m_Width * 3);
	if (BitmapStride % 4)
	{
		// Need to pad the ends
		BitmapPadding = 4 - (BitmapStride % 4);
		BitmapStride += BitmapPadding;
	}

	BitmapFileHeader FileHeader;
	FileHeader.FileType = 0x4D42;
	FileHeader.Length = sizeof(BitmapFileHeader) +sizeof(BitmapInfoHeader) +(BitmapStride*m_Height);
	FileHeader.Reserved0 = 0;
	FileHeader.Reserved1 = 0;
	FileHeader.ImageDataOffset = sizeof(BitmapFileHeader) +sizeof(BitmapInfoHeader);

	BitmapInfoHeader InfoHeader;
	InfoHeader.HeaderLength = sizeof(BitmapInfoHeader);
	InfoHeader.Width = m_Width;
	InfoHeader.Height = m_Height;
	InfoHeader.Planes = 1;
	InfoHeader.Bpp = 24;
	InfoHeader.Compression = 0;
	InfoHeader.BufferSize = BitmapStride * m_Height;
	InfoHeader.XPelsPerMeter = 0;
	InfoHeader.YPelsPerMeter = 0;
	InfoHeader.NbColoursUsed = 0;
	InfoHeader.NbColoursRequired = 0;

	output.WriteData(reinterpret_cast<uint8_t*>(&FileHeader), 0, sizeof(BitmapFileHeader));
	output.WriteData(reinterpret_cast<uint8_t*>(&InfoHeader), 0, sizeof(BitmapInfoHeader));

	if (m_Format == InternalFormat::BGR)
	{
		int Stride = (m_Width * 3);
		for (uint32_t Row = m_Height - 1; Row != 0xFFFFFFFF; --Row)
		{
			int Offset = Row * Stride;
			output.WriteData(m_Data, Offset, Stride);
			for (int Pad = 0; Pad < BitmapPadding; ++Pad)
			{
				output.Write(uint8_t(0));
			}
		}
	}
	else if (m_Format == InternalFormat::RGB)
	{
		int Stride = (m_Width * 3);
		for (uint32_t Row = m_Height - 1; Row != 0xFFFFFFFF; --Row)
		{
			for (uint32_t Col = 0; Col < m_Width; ++Col)
			{
				int Offset = (Row * Stride) + Col * 3;

				output.Write(m_Data[Offset + 2]);
				output.Write(m_Data[Offset + 1]);
				output.Write(m_Data[Offset]);
			}

			for (int Pad = 0; Pad < BitmapPadding; ++Pad)
			{
				output.Write(uint8_t(0));
			}
		}
	}
	else if (m_Format == InternalFormat::BGRA || m_Format == InternalFormat::RGBA)
	{
		// Need to sort alpha out
		int Stride = (m_Width * 4);
		for (uint32_t Row = m_Height - 1; Row != 0xFFFFFFFF; --Row)
		{
			for (uint32_t Col = 0; Col < m_Width; ++Col)
			{
				std::size_t Index = (Row * Stride) + (Col * 4);
				uint8_t Red = m_Data[Index];
				uint8_t Green = m_Data[Index + 1];
				uint8_t Blue = m_Data[Index + 2];
				uint8_t Alpha = m_Data[Index + 3];

				if (Alpha == 0x00)
				{
					Red = Green = Blue = 0xFF;
				}
				else if (Alpha != 0xFF)
				{
					// Multiply alpha'd parts with white
					int Temp = Alpha * (Red - 0xFF);
					Temp = ((((Temp) >> 8) + (Temp)) >> 8);
					Red = 0xFF + Temp;

					Temp = Alpha * (Green - 0xFF);
					Temp = ((((Temp) >> 8) + (Temp)) >> 8);
					Green = 0xFF + Temp;

					Temp = Alpha * (Blue - 0xFF);
					Temp = ((((Temp) >> 8) + (Temp)) >> 8);
					Blue = 0xFF + Temp;
				}

				if (m_Format == InternalFormat::BGRA)
				{
					output.Write(Red);
					output.Write(Green);
					output.Write(Blue);
				}
				else
				{
					output.Write(Blue);
					output.Write(Green);
					output.Write(Red);
				}
			}

			for (int Pad = 0; Pad < BitmapPadding; ++Pad)
			{
				output.Write(uint8_t(0));
			}
		}
	}
}

void Image::WritePng(Stream& output) const
{
	lodepng::State PngState;
	PngState.info_raw.bitdepth = 8;
	PngState.info_png.color.bitdepth = 8;

	switch (m_Format)
	{
	case InternalFormat::RGBA:
		PngState.info_raw.colortype = LCT_RGBA;
		PngState.info_png.color.colortype = LCT_RGBA;
		break;

	case InternalFormat::RGB:
		PngState.info_raw.colortype = LCT_RGB;
		PngState.info_png.color.colortype = LCT_RGB;
		break;

	case InternalFormat::BGRA:
		PngState.info_raw.colortype = LCT_BGRA;
		PngState.info_png.color.colortype = LCT_RGBA;
		break;

	case InternalFormat::BGR:
		PngState.info_raw.colortype = LCT_BGR;
		PngState.info_png.color.colortype = LCT_RGB;
		break;
	}

	lodepng::encode(output, m_Data, m_Width, m_Height, PngState);
}

namespace
{
	bool IsPng(uint8_t signature[8])
	{
		const uint8_t PngSignature[8] = { 137, 80, 78, 71, 13, 10, 26, 10 };
		bool Png = true;
		for (int i = 0; i < 8; ++i)
		{
			if (PngSignature[i] != signature[i])
			{
				Png = false;
				break;
			}
		}

		return Png;
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKGraphics/Mesh.h>
#include <SKGraphics/Renderer.h>
#include <SKSystem/IO/Stream.h>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		class MeshLoader
		{
		public:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			static MeshPtr LoadMesh(SK::IO::Stream& meshData, Renderer& renderer);

			//-- Variables --//
			//===============//

		private:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			static MeshPtr ReadMesh(SK::IO::Stream& meshData, Renderer& renderer);

			//-- Variables --//
			//===============//

		};

		//--- Methods ---//
		//===============//
	}
}
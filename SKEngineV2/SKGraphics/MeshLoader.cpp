//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKGraphics/MeshLoader.h>
#include <algorithm>
using namespace SK::Graphics;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

MeshPtr MeshLoader::LoadMesh(SK::IO::Stream& meshData, Renderer& renderer)
{
	uint32_t NumMeshes;
	uint8_t FileType[6];
	meshData.ReadData(FileType, 0, 6);

	if (memcmp(FileType, "SKMESH", 6) != 0)
	{
		SK::Debug::Assert(false, "MeshLoader::LoadMesh: Not a valid mesh file.");
		return nullptr;
	}
	meshData.Read(NumMeshes);

	if (NumMeshes == 1)
	{
		return ReadMesh(meshData, renderer);
	}
	else
	{
		Mesh* RootMesh = new Mesh();
		
		for (uint32_t i = 0; i < NumMeshes; ++i)
		{
			MeshPtr ChildMesh = ReadMesh(meshData, renderer);
			ChildMesh->m_Parent = RootMesh;
			RootMesh->m_Children.push_back(ChildMesh);
		}

		return MeshPtr(RootMesh);
	}
}

//-- Private Methods --//
//=====================//

MeshPtr MeshLoader::ReadMesh(SK::IO::Stream& meshData, Renderer& renderer)
{
	Mesh* NewMesh = new Mesh();
	uint32_t NumVerts;
	uint32_t NumIndis;
	uint32_t NumChildren;
	meshData.Read(NumVerts, NumIndis, NumChildren, NewMesh->m_Name);

	// Read materials
	uint32_t VertexFormat;

	uint32_t NumMaterials;
	meshData.Read(NumMaterials);

	std::hash<SK::String> StringHash;
	for (uint32_t i = 0; i < NumMaterials; ++i)
	{
		Mesh::Material Mat;
		uint32_t SortOrder;
		SK::String VertexShader;
		SK::String PixelShader;
		meshData.Read(Mat.Cullable, SortOrder, VertexFormat, VertexShader, PixelShader);

		for (uint32_t i = 0; i < MAX_TEXTURE_UNITS; ++i)
		{
			meshData.Read(Mat.TextureNames[i]);
		}

		Mat.SortOrder = static_cast<RenderSortOrder>(SortOrder);
		Mat.PixelShaderId = StringHash(PixelShader);
		Mat.VertexShaderId = StringHash(VertexShader);

		meshData.Read(Mat.Offset, Mat.Count);

		NewMesh->m_Materials.push_back(Mat);
	}

	NewMesh->m_PrimType = PrimType::TriangleList;

	// Read Transform
	float MatrixData[16];
	for (uint32_t i = 0; i < 16; ++i)
	{
		meshData.Read(MatrixData[i]);
	}

	NewMesh->m_Transform.Row0 = _mm_setr_ps(MatrixData[0], MatrixData[1], MatrixData[2], MatrixData[3]);
	NewMesh->m_Transform.Row1 = _mm_setr_ps(MatrixData[4], MatrixData[5], MatrixData[6], MatrixData[7]);
	NewMesh->m_Transform.Row2 = _mm_setr_ps(MatrixData[8], MatrixData[9], MatrixData[10], MatrixData[11]);
	NewMesh->m_Transform.Row3 = _mm_setr_ps(MatrixData[12], MatrixData[13], MatrixData[14], MatrixData[15]);

	// Read bounding box
	Math::float3 Min(FLT_MAX), Max(-FLT_MAX);
	for (int i = 0; i<8; ++i)
	{
		float X, Y, Z;
		meshData.Read(X, Y, Z);
		NewMesh->m_Extents.Corners[i].Set(X, Y, Z, 1.0f);
	}

	float Radius;
	meshData.Read(Radius);

	const float SKTDOcclusionScale = 0.8f;
	Array<Math::float3> Vertices(8);
	Vertices[0] = (NewMesh->m_Extents.Corners[0] * SKTDOcclusionScale).Get().xyz();
	Vertices[1] = (NewMesh->m_Extents.Corners[1] * SKTDOcclusionScale).Get().xyz();
	Vertices[2] = (NewMesh->m_Extents.Corners[2] * SKTDOcclusionScale).Get().xyz();
	Vertices[3] = (NewMesh->m_Extents.Corners[3] * SKTDOcclusionScale).Get().xyz();
	Vertices[4] = (NewMesh->m_Extents.Corners[4] * SKTDOcclusionScale).Get().xyz();
	Vertices[5] = (NewMesh->m_Extents.Corners[5] * SKTDOcclusionScale).Get().xyz();
	Vertices[6] = (NewMesh->m_Extents.Corners[6] * SKTDOcclusionScale).Get().xyz();
	Vertices[7] = (NewMesh->m_Extents.Corners[7] * SKTDOcclusionScale).Get().xyz();

	Array<uint32_t> Indices(36);

	//Left
	Indices[0] = 2;
	Indices[1] = 1;
	Indices[2] = 0;
	Indices[3] = 0;
	Indices[4] = 3;
	Indices[5] = 2;

	// Front
	Indices[6] = 6;
	Indices[7] = 2;
	Indices[8] = 3;
	Indices[9] = 3;
	Indices[10] = 7;
	Indices[11] = 6;

	// Back
	Indices[12] = 5;
	Indices[13] = 4;
	Indices[14] = 0;
	Indices[15] = 0;
	Indices[16] = 1;
	Indices[17] = 5;

	// Right
	Indices[18] = 6;
	Indices[19] = 7;
	Indices[20] = 4;
	Indices[21] = 4;
	Indices[22] = 5;
	Indices[23] = 6;

	// Top
	Indices[24] = 1;
	Indices[25] = 2;
	Indices[26] = 6;
	Indices[27] = 6;
	Indices[28] = 5;
	Indices[29] = 1;

	// Bottom
	Indices[30] = 7;
	Indices[31] = 3;
	Indices[32] = 0;
	Indices[33] = 0;
	Indices[34] = 4;
	Indices[35] = 7;

	NewMesh->m_OcclusionMesh.Vertices = Vertices;
	NewMesh->m_OcclusionMesh.Indices = Indices;

	// Read vertex data
	if (NumVerts > 0)
	{
		uint32_t VertexDataSize = VertexSize(static_cast<VertexType>(VertexFormat)) * NumVerts;
		uint8_t* VertexData = new uint8_t[VertexDataSize];
		meshData.ReadData(VertexData, 0, VertexDataSize);
		NewMesh->m_VertexBuffer = renderer.CreateVertexBuffer(static_cast<VertexType>(VertexFormat), NumVerts, false, VertexData, VertexDataSize);
		delete [] VertexData;
	}
	else
	{
		NewMesh->m_VertexBuffer = nullptr;
	}

	// Read index data
	if (NumIndis > 0)
	{
		uint32_t IndexDataSize = sizeof(uint16_t) * NumIndis;
		uint8_t* IndexData = new uint8_t[IndexDataSize];
		meshData.ReadData(IndexData, 0, IndexDataSize);
		NewMesh->m_IndexBuffer = renderer.CreateIndexBuffer(NumIndis, false, IndexData, IndexDataSize);
		delete [] IndexData;
	}
	else
	{
		NewMesh->m_IndexBuffer = nullptr;
	}

	for (uint32_t i = 0; i < NumChildren; ++i)
	{
		MeshPtr ChildMesh = ReadMesh(meshData, renderer);
		ChildMesh->m_Parent = NewMesh;
		NewMesh->m_Children.push_back(ChildMesh);
	}

	return MeshPtr(NewMesh);
}
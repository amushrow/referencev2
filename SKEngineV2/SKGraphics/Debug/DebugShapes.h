#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2016 Anthony Mushrow                           //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem\Math\Types.h>
#include <SKSystem\Math\Matrix.h>
#include <SKGraphics\Colour.h>
#include <SKGraphics\Renderer.h>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		class DebugShapes
		{
		public:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//
			DebugShapes(uint32_t maxLines, uint32_t maxBoxes);

			void Initialise(Renderer& renderer);
			
			void Clear();
			void Render(Renderer& renderer);

			bool AddLine(const Math::float3& a, const Math::float3& b, const ColourFloat& colour = ColourFloat(1.0f, 0.0f, 1.0f, 0.5f));
			bool AddBox(const Math::Matrix& matrix, const Math::float3& extents, const ColourFloat& colour = ColourFloat(0.5f, 1.0f, 0.0f, 1.0f));

			//-- Variables --//
			//===============//

		private:

			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			//-- Variables --//
			//===============//

			bool            m_Initialised;
			uint32_t        m_NumBoxes;
			uint32_t        m_MaxBoxes;
			VertexBufferPtr m_BoxesBuffer;
			VertexBufferPtr m_BoxesInstanceBuffer;
			InstanceMatCol* m_BoxInstances;
		};

		//--- Methods ---//
		//===============//
	}
}
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKGraphics/Mesh.h>
#include <SKGraphics/ResourceCache.h>
#include <SKGraphics/RenderTree.h>
using namespace SK::Graphics;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Mesh::Mesh()
: m_Transform(Math::Matrix::Identity())
, m_Name("Root")
, m_VertexBuffer(nullptr)
, m_IndexBuffer(nullptr)
, m_PrimType(PrimType::TriangleList)
, m_Parent(nullptr)
{

}

MeshInstance* Mesh::CreateInstance(MeshInstanceManager& instanceManager, Renderer& renderer)
{
	MeshInstance* RootInst = new MeshInstance();
	RootInst->m_Mesh = this;
	
	std::hash<SK::String> NameHash;
	auto& TextureCache = ResourceCache<Texture>::Instance();
	if (m_VertexBuffer)
	{
		// Set up first mesh / material
		RootInst->InstanceTypeId(reinterpret_cast<uint32_t>(this) << 8);
		RootInst->Cullable(m_Materials[0].Cullable);
		RootInst->SortOrder(m_Materials[0].SortOrder);

		MeshInstanceManager::Bin* Bin = instanceManager.FindInstanceBin(RootInst->InstanceTypeId());
		if (!Bin)
		{
			Bin = instanceManager.CreateInstanceBin(RootInst->InstanceTypeId(), renderer);

			Bin->Offset = m_Materials[0].Offset;
			Bin->Count = m_Materials[0].Count;
			Bin->MaterialIndex = 0;
			Bin->Mesh = this;

			Bin->TextureCount = 0;

			for (int i = 0; i < MAX_TEXTURE_UNITS; ++i)
			{
				auto& TextureName = m_Materials[0].TextureNames[i];
				if (TextureName.Length() > 0)
				{
					Bin->Textures[i] = TextureCache.GetResource(NameHash(TextureName));
					++Bin->TextureCount;
				}
				else
				{
					break;
				}
			}
			
		}
		
		RootInst->m_Bin = Bin;

		
	}
	else
	{
		// This is an empty mesh
		RootInst->m_Offset = 0;
		RootInst->m_Count = 0;
		RootInst->m_MaterialIndex = 0;
		RootInst->Visible(false);
		RootInst->Cullable(false);
		RootInst->InstanceTypeId(0);
		RootInst->m_TextureCount = 0;
		RootInst->m_Bin = nullptr;
	}

	RootInst->LocalTransform(m_Transform);
	RootInst->LocalExtents(Extents());

	// Add any additional instances for other materials
	uint32_t MaterialsCount = static_cast<uint32_t>(m_Materials.size());
	for (uint32_t i = 1; i < MaterialsCount; ++i)
	{
		MeshInstance* OtherMaterial = new MeshInstance();
		OtherMaterial->m_Mesh = this;
		OtherMaterial->Cullable(m_Materials[i].Cullable);
		OtherMaterial->SortOrder(m_Materials[i].SortOrder);
		OtherMaterial->InstanceTypeId(reinterpret_cast<uint32_t>(this) << 8 | i);
		OtherMaterial->LocalTransform(m_Transform);
		OtherMaterial->LocalExtents(Extents());
		OtherMaterial->Attach(RootInst);

		MeshInstanceManager::Bin* Bin = instanceManager.FindInstanceBin(OtherMaterial->InstanceTypeId());
		if (!Bin)
		{
			Bin = instanceManager.CreateInstanceBin(OtherMaterial->InstanceTypeId(), renderer);

			Bin->Offset = m_Materials[i].Offset;
			Bin->Count = m_Materials[i].Count;
			Bin->MaterialIndex = i;
			Bin->Mesh = this;

			Bin->TextureCount = 0;

			for (int k = 0; k < MAX_TEXTURE_UNITS; ++k)
			{
				auto& TextureName = m_Materials[i].TextureNames[k];
				if (TextureName.Length() > 0)
				{
					Bin->Textures[k] = TextureCache.GetResource(NameHash(TextureName));
					++Bin->TextureCount;
				}
				else
				{
					break;
				}
			}

		}
	}

	for (auto Child : m_Children)
	{
		Child->CreateInstance(instanceManager, renderer)->Attach(RootInst);
	}

	return RootInst;
}

MeshInstance* Mesh::CreateInstance()
{
	MeshInstance* RootInst = new MeshInstance();
	RootInst->m_Mesh = this;
	RootInst->m_Bin = nullptr;
	
	std::hash<SK::String> NameHash;
	auto& TextureCache = ResourceCache<Texture>::Instance();
	if (m_VertexBuffer)
	{
		// Set up first mesh / material
		RootInst->m_Offset = m_Materials[0].Offset;
		RootInst->m_Count = m_Materials[0].Count;
		RootInst->m_MaterialIndex = 0;
		RootInst->Cullable(m_Materials[0].Cullable);
		RootInst->SortOrder(m_Materials[0].SortOrder);
		RootInst->InstanceTypeId(reinterpret_cast<uint32_t>(this) << 8);

		RootInst->m_TextureCount = 0;
		for (int i = 0; i < MAX_TEXTURE_UNITS; ++i)
		{
			auto& TextureName = m_Materials[0].TextureNames[i];
			if (TextureName.Length() > 0)
			{
				RootInst->m_Textures[i] = TextureCache.GetResource(NameHash(TextureName));
				++RootInst->m_TextureCount;
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		// This is an empty mesh
		RootInst->m_Offset = 0;
		RootInst->m_Count = 0;
		RootInst->m_MaterialIndex = 0;
		RootInst->m_TextureCount = 0;
		RootInst->Visible(false);
	}

	RootInst->LocalTransform(m_Transform);
	RootInst->LocalExtents(Extents());

	// Add any additional instances for other materials
	uint32_t MaterialsCount = static_cast<uint32_t>(m_Materials.size());
	for (uint32_t i = 1; i < MaterialsCount; ++i)
	{
		MeshInstance* OtherMaterial = new MeshInstance();
		OtherMaterial->m_Mesh = this;
		OtherMaterial->m_Bin = nullptr;

		OtherMaterial->m_Offset = m_Materials[i].Offset;
		OtherMaterial->m_Count = m_Materials[i].Count;
		OtherMaterial->m_MaterialIndex = i;
		OtherMaterial->Cullable(m_Materials[i].Cullable);
		OtherMaterial->SortOrder(m_Materials[i].SortOrder);
		OtherMaterial->InstanceTypeId(reinterpret_cast<uint32_t>(this) << 8 | i);
		OtherMaterial->LocalTransform(m_Transform);
		OtherMaterial->LocalExtents(Extents());

		OtherMaterial->m_TextureCount = 0;
		for (int j = 0; j < MAX_TEXTURE_UNITS; ++j)
		{
			auto& TextureName = m_Materials[i].TextureNames[j];
			if (TextureName.Length() > 0)
			{
				OtherMaterial->m_Textures[j] = TextureCache.GetResource(NameHash(TextureName));
				++OtherMaterial->m_TextureCount;
			}
			else
			{
				break;
			}
		}

		OtherMaterial->Attach(RootInst);
	}

	for (auto Child : m_Children)
	{
		Child->CreateInstance()->Attach(RootInst);
	}

	return RootInst;
}

void MeshInstance::Render(Renderer& renderer)
{
	if (m_Bin)
	{
		static size_t NumInstances = 0;
		NumInstances = m_Bin->NumInstances;
		if (m_Bin->NumInstances < m_Bin->MaxInstances && !m_Bin->renderTest)
		{
			auto& Instance = m_Bin->Instances[m_Bin->NumInstances++];
			Instance.Transform = this->WorldTransform().Get();
		}
	}
	else
	{
		static auto WorldId = renderer.GetShaderParamId("World");
		renderer.SetShaderParam(WorldId, this->WorldTransform());
		for (uint32_t i = 0; i < m_TextureCount; ++i)
		{
			renderer.SetShaderTexture(i, m_Textures[i]);
		}
		renderer.CommitShaderParams();

		IndexBufferPtr IndexBuffer = m_Mesh->IndexBuffer();
		VertexBufferPtr VertexBuffer = m_Mesh->VertexBuffer();
		if (IndexBuffer)
		{
			uint32_t NumIndis = m_Mesh->IndexBuffer()->NbIndis();
			renderer.DrawBufferIndexed(m_Mesh->PrimType(), VertexBuffer, IndexBuffer, m_Count, m_Offset, 0);
		}
		else if (VertexBuffer)
		{
			uint32_t NumVerts = m_Mesh->VertexBuffer()->NbVerts();
			renderer.DrawBuffer(m_Mesh->PrimType(), VertexBuffer, NumVerts, 0);
		}
		else
		{
			// No buffers, stop rendering this mesh
			Visible(false);
		}
	}
}

MeshInstanceManager::Bin* MeshInstanceManager::FindInstanceBin(uint32_t instanceTypeId)
{
	auto& It = m_InstanceBins.find(instanceTypeId);
	if (It != m_InstanceBins.end())
		return &It->second;
	return nullptr;
}

MeshInstanceManager::Bin* MeshInstanceManager::CreateInstanceBin(uint32_t instanceTypeId, Renderer& renderer)
{
	Bin& NewBin = m_InstanceBins[instanceTypeId];
	NewBin.InstanceBuffer = renderer.CreateVertexBuffer(VertexType::InstanceMat, 8192, true);
	NewBin.Instances = SK::AlignedNew<SK::Graphics::InstanceMat>(32, 8192);
	NewBin.MaxInstances = 8192;
	NewBin.NumInstances = 0;
	NewBin.renderTest = false;
	return &NewBin;
}

void MeshInstanceManager::Render(SK::Graphics::Renderer& renderer)
{
	for (auto& Bin : m_InstanceBins)
	{
		static size_t NumInstances = 0;
		NumInstances = Bin.second.NumInstances;

		void* InstanceBuffer = renderer.LockBuffer(Bin.second.InstanceBuffer);
		memcpy(InstanceBuffer, Bin.second.Instances, VertexSize(VertexType::InstanceMat) * Bin.second.NumInstances);
		renderer.UnlockBuffer(Bin.second.InstanceBuffer);

		for (uint32_t i = 0; i < Bin.second.TextureCount; ++i)
		{
			renderer.SetShaderTexture(i, Bin.second.Textures[i]);
		}
		renderer.CommitShaderParams();

		IndexBufferPtr IndexBuffer = Bin.second.Mesh->IndexBuffer();
		VertexBufferPtr VertexBuffer = Bin.second.Mesh->VertexBuffer();
		if (IndexBuffer)
		{
			uint32_t NumIndis = Bin.second.Mesh->IndexBuffer()->NbIndis();
			renderer.DrawBufferIndexedInstanced(Bin.second.Mesh->PrimType(), VertexBuffer, IndexBuffer, Bin.second.Count, Bin.second.Offset, 0, Bin.second.InstanceBuffer, Bin.second.NumInstances, 0);
		}
		else if (VertexBuffer)
		{
			uint32_t NumVerts = Bin.second.Mesh->VertexBuffer()->NbVerts();
			renderer.DrawBufferInstanced(Bin.second.Mesh->PrimType(), VertexBuffer, NumVerts, Bin.second.Offset, Bin.second.InstanceBuffer, Bin.second.NumInstances, 0);
		}
		
		//SKTD: Not the best place I think
		Bin.second.NumInstances = 0;
		//Bin.second.renderTest = true;
	}
}

//-- Private Methods --//
//=====================//

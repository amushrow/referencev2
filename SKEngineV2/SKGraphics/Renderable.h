#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Array.h>
#include <SKSystem/Memory.h>
#include <SKSystem/Object.h>
#include <SKSystem/Text/String.h>
#include <SKSystem/Math/Matrix.h>
#include <SKSystem/Math/Vector.h>
#include <SKGraphics/Culling.h>
#include <functional>
#include <vector>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		class RenderTree;
		class Renderer;
		class Renderable;
		
		struct OcclusionMesh
		{
			SK::Array<Math::float3> Vertices;
			SK::Array<uint32_t>     Indices;
		};


		enum class RenderSortOrder
		{
			First,
			Normal,
			Last
		};

		class Renderable : public Object<Renderable>
		{
		public:
			//---- Types ----//
			//===============//

			friend class RenderTree;

			enum class DirtyFlags
			{
				BoundingBox = 0x02,
				Resync = 0x04
			};

			//--- Methods ---//
			//===============//

			Renderable();

			virtual void          Render(Renderer& renderer) = 0;
			virtual SK::String    Name() const = 0;

			bool                  Cullable() const                                  { return m_Cullable; }
			void                  Cullable(bool cull)                               { m_Cullable = cull; }
			void                  Cullable(bool cull, bool recursive);
			RenderSortOrder       SortOrder() const                                 { return m_SortOrder; }
			void                  SortOrder(RenderSortOrder order)                  { m_SortOrder = order; }
			void                  SortOrder(RenderSortOrder order, bool recursive);
			bool                  Visible() const                                   { return m_Visible; }
			void                  Visible(bool visible)                             { m_Visible = visible; }
			void                  Visible(bool visible, bool recursive);
			uint32_t              InstanceTypeId() const                            { return m_InstanceTypeId; }
			void                  InstanceTypeId(uint32_t id)                       { m_InstanceTypeId = id; }

			void                  LocalExtents(const Graphics::Extents& extents);
			const Extents&        LocalExtents() const                              { return m_LocalExtents; }
			const Extents&        Extents();
			const Graphics::AABB& AABB();
			const Graphics::AABB& AABBDirect() const                                { return m_AABB; }

			RenderTree*           Root() const                                      { return m_Root; }
			size_t                RefId() const                                     { return m_RefId; }
			void                  RefId(size_t id)                                  { m_RefId = id; }

			virtual OcclusionMesh GetOcclusionMesh() const                          { return OcclusionMesh(); };

		private:
			//--- Methods ---//
			//===============//

			void UpdateExtents();
			void Detached() override;
			void Attached(Renderable* parent) override;
			void SetRoot(RenderTree* root);

			//-- Variables --//
			//===============//

			Graphics::Extents m_LocalExtents;
			Graphics::Extents m_Extents;
			Graphics::AABB    m_AABB;
			RenderTree*       m_Root;
			size_t            m_RefId;
			uint32_t          m_InstanceTypeId;
			RenderSortOrder   m_SortOrder;
			bool              m_Cullable;
			bool              m_Visible;
		};

		//--- Methods ---//
		//===============//
	}
}
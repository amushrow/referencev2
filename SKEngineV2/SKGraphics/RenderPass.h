#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKGraphics/MaskedOcclusionCulling/MaskedOcclusionCullingSSE.h>
#include <SKSystem/Math/Vector.h>
#include <SKSystem/Threading/ThreadPool.h>
#include <SKSystem/Memory.h>
#include <SKSystem/Utils.h>
#include <SKGraphics/Camera.h>
#include <SKGraphics/Renderer.h>
#include <SKGraphics/RenderTree.h>
#include <SKGraphics/Texture.h>

#define SKGRAPHICS_SHOWOCCLUSIONBUFFER 1

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//
		class RenderPass
		{
		public:
			//--- Methods ---//
			//===============//

			RenderPass(uint32_t threadPoolSize);
			~RenderPass();

			void SetCamera(Camera* camera);
			void SetRenderTree(RenderTree* tree);
			void SetRenderTarget(TexturePtr texture);
			void SetDepthBuffer(TexturePtr texture);
			void SetOverrideTechnique(size_t vertexShaderId, size_t pixelShaderId);
			
			void EnableOcclusionCulling(Math::int2& resolution, uint32_t maxRenderedTris);
			void DisableOcclusionCulling();

			void Prepare();
			void Render(Renderer& renderer);

#if SKGRAPHICS_SHOWOCCLUSIONBUFFER
			void CopyOcclusionBuffer(Renderer& renderer, TexturePtr renderTarget);
#endif

		private:
			//---- Types ----//
			//===============//

			union SortKey
			{
				struct
				{
					uint64_t   CameraDistance : 16;
					uint64_t   InstanceId : 32;
					uint64_t   SortOrder : 4;
					uint64_t   Culled : 1;
					uint64_t   Pad : 11;
				} Parts;
				uint64_t       Value;
			};

			struct OccluderItem
			{
				float    CamDistance;
				uint32_t Index;
			};

			struct RenderItem
			{
				SortKey     Key;
				Renderable* Object;
			};

			struct OcclusionData
			{
				OcclusionData(Math::int2& resolution, uint32_t maxOccluders, uint32_t maxRenderedTris);
				~OcclusionData();

				MaskedOcclusionCullingSSE Occlusion;
				Math::float4* const       ScreenSpaceOcclusionGeom;
				uint32_t* const           OcclusionGeomIndices;
				uint32_t                  NumScreenSpaceTris;
				OccluderItem* const       OccluderList;
				uint32_t                  OccluderListSize;
				SK::Math::int2            Resolution;
			};

			struct Data
			{
				Data(uint32_t maxSize);
				~Data();

				Renderable** const   Ref;

				CulledBoxes<16, 2>   Boxes;
				float* const         CameraDistance;
				uint8_t* const       PreCulled;
				uint8_t* const       Cullable;
				uint8_t* const       Dirty;
				uint8_t* const       Occluder;
				SortKey* const       Key;
				RenderItem* const    RenderList;
				uint32_t             RenderListSize;
				uint32_t             NumRenderables;
				bool                 Rebuild;
			};

			//--- Methods ---//
			//===============//

			void ChildAdded(void* sender, TreeUpdatedArgs& e);
			void ChildRemoved(void* sender, TreeUpdatedArgs& e);
			void AddRenderable(Renderable* renderable);
			void RemoveRenderable(Renderable* renderable);
			static bool CompareRenderItem(const RenderItem& lhs, const RenderItem& rhs) { return lhs.Key.Value < rhs.Key.Value; }
			static bool CompareOccluderItem(const OccluderItem& lhs, const OccluderItem& rhs) { return lhs.CamDistance < rhs.CamDistance; }

			void FrustumCulling();
			void GetCameraDistance();

			void RenderOcclusionBuffer();
			void CullOccludedObjects();
			void OcclusionTest(size_t i, const Math::Matrix& viewThingy);

			//-- Variables --//
			//===============//



			SK::Threading::ThreadPool m_ThreadPool;
			RenderTree*               m_RenderTree;
			Camera*                   m_Camera;
			Data*                     m_Data;
			OcclusionData*            m_OcclusionData;
			TexturePtr                m_RenderTarget;
			TexturePtr                m_DepthBuffer;
			size_t                    m_OverrideVertex;
			size_t                    m_OverridePixel;
			EventKey                  m_EventKey;
		};

		//--- Methods ---//
		//===============//
	}
}
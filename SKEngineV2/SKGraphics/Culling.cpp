//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2016 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKGraphics/Culling.h>
#include <SKSystem/Math/Types.h>
#include <algorithm>
#include <intrin.h>
using namespace SK::Graphics;
using namespace SK::Math;

//-- Declarations --//
//==================//
namespace
{
	static const __m128 SIGNMASK = _mm_castsi128_ps(_mm_set1_epi32(0x80000000));
	static const __m256 SIGNMASK256 = _mm256_castsi256_ps(_mm256_set1_epi32(0x80000000));
	static const __m128 ALLMASK = _mm_castsi128_ps(_mm_set_epi32(0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF));
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

AABBList::AABBList(size_t size)
: CentreX(SK::AlignedNew<float>(32, size))
, CentreY(SK::AlignedNew<float>(32, size))
, CentreZ(SK::AlignedNew<float>(32, size))
, SizeX(SK::AlignedNew<float>(32, size))
, SizeY(SK::AlignedNew<float>(32, size))
, SizeZ(SK::AlignedNew<float>(32, size))
, Radius(SK::AlignedNew<float>(32, size))
, Size(size)
{
}

AABBList::~AABBList()
{
	SK::AlignedDelete(CentreX);
	SK::AlignedDelete(CentreY);
	SK::AlignedDelete(CentreZ);
	SK::AlignedDelete(SizeX);
	SK::AlignedDelete(SizeY);
	SK::AlignedDelete(SizeZ);
	SK::AlignedDelete(Radius);
}

AABB AABB::FromExtents(const Extents& extents)
{
	AABB Box;

	Math::float3 Min(FLT_MAX), Max(-FLT_MAX);
	for (int i = 0; i < 8; ++i)
	{
		auto Corner = extents.Corners[i].Get();
		for (int k = 0; k < 3; ++k)
		{
			if (Corner[k] < Min[k])
				Min[k] = Corner[k];
			if (Corner[k] > Max[k])
				Max[k] = Corner[k];
		}
	}

	Box.Centre = Math::float4((Min + Max) / 2, 1.0f);
	Box.Size = Math::float4((Max - Min) / 2, 0.0f);
	Box.Size.Data.m128_f32[3] = Box.Size.Length();

	return Box;
}

void Frustum::Contains(const AABBList& aabb, uint8_t* results, uint32_t numBoxes) const
{
	Contains(aabb, results, 0, numBoxes);
}

#define AVX
#ifdef AVX
void Frustum::Contains(const AABBList& aabb, uint8_t* results, uint32_t start, uint32_t end) const
{
	__declspec(align(16)) struct
	{
		Math::float4 SignFlip;
		Math::float4 Plane;
	} Planes[6];

	for (int i = 0; i < 6; ++i)
	{
		_mm_store_ps(Planes[i].Plane, m_Planes[i].Data);
		_mm_store_ps(Planes[i].SignFlip, _mm_and_ps(m_Planes[i].Data, SIGNMASK));
	}

	const __m256 BitMask = _mm256_castsi256_ps(_mm256_set1_epi32(0x80000000));
	size_t NumIterations = end >> 3;

	uint64_t* results64 = reinterpret_cast<uint64_t*>(results);
	for (size_t Iter = start >> 3; Iter < NumIterations; ++Iter)
	{
		if (results64[Iter] == 0x0101010101010101ull)
			continue;

		__m256 Reshizzle = _mm256_set1_ps(0);

		__m256 cx = _mm256_load_ps(&aabb.CentreX[Iter << 3]);
		__m256 cy = _mm256_load_ps(&aabb.CentreY[Iter << 3]);
		__m256 cz = _mm256_load_ps(&aabb.CentreZ[Iter << 3]);
		__m256 sx = _mm256_load_ps(&aabb.SizeX[Iter << 3]);
		__m256 sy = _mm256_load_ps(&aabb.SizeY[Iter << 3]);
		__m256 sz = _mm256_load_ps(&aabb.SizeZ[Iter << 3]);

		for (size_t Plane = 0; Plane < 6; ++Plane)
		{
			__m256 apx = _mm256_add_ps(_mm256_xor_ps(_mm256_broadcast_ss(&Planes[Plane].SignFlip.x), sx), cx);
			__m256 apy = _mm256_add_ps(_mm256_xor_ps(_mm256_broadcast_ss(&Planes[Plane].SignFlip.y), sy), cy);
			__m256 apz = _mm256_add_ps(_mm256_xor_ps(_mm256_broadcast_ss(&Planes[Plane].SignFlip.z), sz), cz);

			apx = _mm256_mul_ps(_mm256_broadcast_ss(&Planes[Plane].Plane.x), apx);
			apy = _mm256_mul_ps(_mm256_broadcast_ss(&Planes[Plane].Plane.y), apy);
			apz = _mm256_mul_ps(_mm256_broadcast_ss(&Planes[Plane].Plane.z), apz);

			__m256 Dot = _mm256_add_ps(_mm256_broadcast_ss(&Planes[Plane].Plane.w), _mm256_add_ps(apx, _mm256_add_ps(apy, apz)));
			Reshizzle = _mm256_or_ps(Reshizzle, Dot);
			if (_mm256_testc_ps(Reshizzle, BitMask))
				break;
		}

		//results[Iter] = _mm256_movemask_ps(Reshizzle);

		int mask = _mm256_movemask_ps(Reshizzle);
		results[(Iter << 3) + 0] = (mask >> 0) & 1;
		results[(Iter << 3) + 1] = (mask >> 1) & 1;
		results[(Iter << 3) + 2] = (mask >> 2) & 1;
		results[(Iter << 3) + 3] = (mask >> 3) & 1;
		results[(Iter << 3) + 4] = (mask >> 4) & 1;
		results[(Iter << 3) + 5] = (mask >> 5) & 1;
		results[(Iter << 3) + 6] = (mask >> 6) & 1;
		results[(Iter << 3) + 7] = (mask >> 7) & 1;
	}


	for (size_t i = NumIterations << 3; i < end; ++i)
	{
		__m128 Size = _mm_setr_ps(aabb.SizeX[i], aabb.SizeY[i], aabb.SizeZ[i], 0.0f);
		__m128 Centre = _mm_setr_ps(aabb.CentreX[i], aabb.CentreY[i], aabb.CentreZ[i], 1.0f);
		//for (size_t Plane = 0; Plane < 6; ++Plane)
		{
			__m128 D = _mm_xor_ps(Size, _mm_load_ps(Planes[0].SignFlip));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[0].Plane), 0xFF)) < 0.0f)
			{
				//Culled
				//results[i / 8] |= (1 << (i % 8));
				results[i] = 1;
				continue;
			}

			D = _mm_xor_ps(Size, _mm_load_ps(Planes[1].SignFlip));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[1].Plane), 0xFF)) < 0.0f)
			{
				//Culled
				//results[i / 8] |= (1 << (i % 8));
				results[i] = 1;
				continue;
			}

			D = _mm_xor_ps(Size, _mm_load_ps(Planes[2].SignFlip));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[2].Plane), 0xFF)) < 0.0f)
			{
				//Culled
				//results[i / 8] |= (1 << (i % 8));
				results[i] = 1;
				continue;
			}

			D = _mm_xor_ps(Size, _mm_load_ps(Planes[3].SignFlip));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[3].Plane), 0xFF)) < 0.0f)
			{
				//Culled
				//results[i / 8] |= (1 << (i % 8));
				results[i] = 1;
				continue;
			}

			D = _mm_xor_ps(Size, _mm_load_ps(Planes[4].SignFlip));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[4].Plane), 0xFF)) < 0.0f)
			{
				//Culled
				//results[i / 8] |= (1 << (i % 8));
				results[i] = 1;
				continue;
			}

			D = _mm_xor_ps(Size, _mm_load_ps(Planes[5].SignFlip));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[5].Plane), 0xFF)) < 0.0f)
			{
				//Culled
				//results[i / 8] |= (1 << (i % 8));
				results[i] = 1;
				continue;
			}
		}
	}
}
#else
void Frustum::Contains(const AABBList& aabb, uint8_t* results, uint32_t start, uint32_t end) const
{
	__declspec(align(16)) Math::float4 SignFlipPlanes[6];
	__declspec(align(16)) Math::float4 Planes[6];

	for (int i = 0; i < 6; ++i)
	{
		_mm_store_ps(Planes[i], m_Planes[i].Data);
		_mm_store_ps(SignFlipPlanes[i], _mm_and_ps(m_Planes[i].Data, SIGNMASK));
	}

	const __m128i BitMask = _mm_set1_epi32(0x80000000);
	
	size_t NumIterations = end >> 2;
	for (size_t Iter = start >> 2; Iter < NumIterations; ++Iter)
	{
		__m128 Reshizzle = _mm_set1_ps(0);
		__m128 cx = _mm_load_ps(&aabb.CentreX[Iter << 2]);
		__m128 cy = _mm_load_ps(&aabb.CentreY[Iter << 2]);
		__m128 cz = _mm_load_ps(&aabb.CentreZ[Iter << 2]);
		__m128 sx = _mm_load_ps(&aabb.SizeX[Iter << 2]);
		__m128 sy = _mm_load_ps(&aabb.SizeY[Iter << 2]);
		__m128 sz = _mm_load_ps(&aabb.SizeZ[Iter << 2]);

		for (size_t Plane = 0; Plane < 6; ++Plane)
		{
			__m128 apx = _mm_add_ps(cx, _mm_xor_ps(_mm_broadcast_ss(&SignFlipPlanes[Plane].x), sx));
			__m128 apy = _mm_add_ps(cy, _mm_xor_ps(_mm_broadcast_ss(&SignFlipPlanes[Plane].y), sy));
			__m128 apz = _mm_add_ps(cz, _mm_xor_ps(_mm_broadcast_ss(&SignFlipPlanes[Plane].z), sz));

			apx = _mm_mul_ps(apx, _mm_broadcast_ss(&Planes[Plane].x));
			apy = _mm_mul_ps(apy, _mm_broadcast_ss(&Planes[Plane].y));
			apz = _mm_mul_ps(apz, _mm_broadcast_ss(&Planes[Plane].z));

			__m128 Dot = _mm_add_ps(_mm_broadcast_ss(&Planes[Plane].w), _mm_add_ps(apx, _mm_add_ps(apy, apz)));
			Reshizzle = _mm_or_ps(Reshizzle, Dot);
			if (_mm_testc_si128(_mm_castps_si128(Reshizzle), BitMask))
				break;
		}

		int mask = _mm_movemask_ps(Reshizzle);
		for (int k = 0; k < 4; ++k)
			results[(Iter << 2) + k] = (mask & (1 << k)) != 0 ? 1 : 0;
	}
	
	for (size_t i = NumIterations << 2; i < end; ++i)
	{
		__m128 Size = _mm_setr_ps(aabb.SizeX[i], aabb.SizeY[i], aabb.SizeZ[i], 0.0f);
		__m128 Centre = _mm_setr_ps(aabb.CentreX[i], aabb.CentreY[i], aabb.CentreZ[i], 1.0f);
		//for (size_t Plane = 0; Plane < 6; ++Plane)
		{
			__m128 D = _mm_xor_ps(Size, _mm_load_ps(SignFlipPlanes[0]));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[0]), 0xFF)) < 0.0f)
			{
				//Culled
				results[i] = 1;
				continue;
			}

			D = _mm_xor_ps(Size, _mm_load_ps(SignFlipPlanes[1]));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[1]), 0xFF)) < 0.0f)
			{
				//Culled
				results[i] = 1;
				continue;
			}

			D = _mm_xor_ps(Size, _mm_load_ps(SignFlipPlanes[2]));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[2]), 0xFF)) < 0.0f)
			{
				//Culled
				results[i] = 1;
				continue;
			}

			D = _mm_xor_ps(Size, _mm_load_ps(SignFlipPlanes[3]));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[3]), 0xFF)) < 0.0f)
			{
				//Culled
				results[i] = 1;
				continue;
			}

			D = _mm_xor_ps(Size, _mm_load_ps(SignFlipPlanes[4]));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[4]), 0xFF)) < 0.0f)
			{
				//Culled
				results[i] = 1;
				continue;
			}

			D = _mm_xor_ps(Size, _mm_load_ps(SignFlipPlanes[5]));
			D = _mm_add_ps(D, Centre);
			if (_mm_cvtss_f32(_mm_dp_ps(D, _mm_load_ps(Planes[5]), 0xFF)) < 0.0f)
			{
				//Culled
				results[i] = 1;
				continue;
			}
		}
	}
}
#endif

bool Frustum::Contains(const AABB& aabb) const
{
	Vector AbsPlanes[6];
	for (int i = 0; i < 6; ++i)
	{
		AbsPlanes[i] = Vector::Abs(m_Planes[i]);
		AbsPlanes[i].Data.m128_f32[3] = 0.0f;
	}

	for (int i = 0; i < 6; ++i)
	{
		float D = Vector::Dot(aabb.Centre, m_Planes[i]);
		float R = Vector::Dot(aabb.Size, AbsPlanes[i]);
		if (D + R < 0)
		{
			return false;
		}
	}

	return true;
}

void Frustum::SetViewProj(const Matrix& viewProj)
{
	float X, Y, Z, W;
	float4 Row0 = viewProj.Row(0);
	float4 Row1 = viewProj.Row(1);
	float4 Row2 = viewProj.Row(2);
	float4 Row3 = viewProj.Row(3);

	// Near
	X = Row0.w + Row0.z;
	Y = Row1.w + Row1.z;
	Z = Row2.w + Row2.z;
	W = Row3.w + Row3.z;
	m_Planes[0].Set(X, Y, Z, W);

	// Left
	X = Row0.w + Row0.x;
	Y = Row1.w + Row1.x;
	Z = Row2.w + Row2.x;
	W = Row3.w + Row3.x;
	m_Planes[1].Set(X, Y, Z, W);

	// Right
	X = Row0.w - Row0.x;
	Y = Row1.w - Row1.x;
	Z = Row2.w - Row2.x;
	W = Row3.w - Row3.x;
	m_Planes[2].Set(X, Y, Z, W);

	// Far
	X = Row0.w - Row0.z;
	Y = Row1.w - Row1.z;
	Z = Row2.w - Row2.z;
	W = Row3.w - Row3.z;
	m_Planes[3].Set(X, Y, Z, W);

	// Bottom
	X = Row0.w + Row0.y;
	Y = Row1.w + Row1.y;
	Z = Row2.w + Row2.y;
	W = Row3.w + Row3.y;
	m_Planes[4].Set(X, Y, Z, W);

	// Top
	X = Row0.w - Row0.y;
	Y = Row1.w - Row1.y;
	Z = Row2.w - Row2.y;
	W = Row3.w - Row3.y;
	m_Planes[5].Set(X, Y, Z, W);
}

//-- Private Methods --//
//=====================//

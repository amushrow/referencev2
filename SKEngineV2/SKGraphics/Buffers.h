#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKGraphics/Vertex.h>
#include <cstdint>
#include <memory>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		class VertexBuffer
		{
		public:

			//--- Methods ---//
			//===============//

			VertexBuffer(void* handle, VertexType type, uint32_t length, bool dynamic)
			: m_Handle(handle), m_Type(type), m_Length(length), m_Dynamic(dynamic)
			{

			}

			uint32_t   Size() const    { return VertexSize(m_Type) * m_Length; }
			uint32_t   NbVerts() const { return m_Length; }
			VertexType Type() const    { return m_Type; }
			bool       Dynamic() const { return m_Dynamic; }
			
			void*      Handle() const  { return m_Handle; }

		private:

			//-- Variables --//
			//===============//

			void*      m_Handle;
			VertexType m_Type;
			uint32_t   m_Length;
			bool       m_Dynamic;
		};

		class IndexBuffer
		{
		public:

			//--- Methods ---//
			//===============//

			IndexBuffer(void* handle, uint32_t length, bool dynamic)
				: m_Handle(handle), m_Length(length), m_Dynamic(dynamic)
			{

			}

			uint32_t   Size() const    { return m_Length * sizeof(uint16_t); }
			uint32_t   NbIndis() const { return m_Length; }
			bool       Dynamic() const { return m_Dynamic; }

			void*      Handle() const  { return m_Handle; }

		private:

			//-- Variables --//
			//===============//

			void*      m_Handle;
			uint32_t   m_Length;
			bool       m_Dynamic;
		};

		typedef std::shared_ptr<VertexBuffer> VertexBufferPtr;
		typedef std::shared_ptr<IndexBuffer> IndexBufferPtr;
	}
}
#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Array.h>
#include <SKSystem/GUI/Window.h>
#include <SKGraphics/Buffers.h>
#include <SKGraphics/RendererState.h>
#include <SKGraphics/Texture.h>
#include <SKGraphics/Vertex.h>
#include <SKSystem/Math/Matrix.h>
#include <SKSystem/Math/Vector.h>
#include <cstdint>
#include <memory>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		const size_t INVALID_ID = -1;

		class Renderer
		{
		public:
		
			//--- Methods ---//
			//===============//

			Renderer();
			~Renderer();

			void            GetCaps(RenderCaps& caps);
			bool            Initialise(const SK::GUI::Window& window, const RenderSettings& settings);

			VertexBufferPtr CreateVertexBuffer(VertexType vertexType, uint32_t nbVerts, bool dynamic, void* vertexData = nullptr, uint32_t dataLength = 0);
			IndexBufferPtr  CreateIndexBuffer(uint32_t nbIndis, bool dynamic, void* indexData = nullptr, uint32_t dataLength = 0);
			TexturePtr      CreateTexture(const Math::int3& size, TextureFormat format, TextureFlags flags, void* textureData = nullptr, uint32_t dataLength = 0);
			TexturePtr      CreateTextureFromDDS(const uint8_t* data, uint32_t dataLength);

			void*           LockBuffer(IndexBufferPtr buffer);
			void*           LockBuffer(VertexBufferPtr buffer);
			void*           LockTexture(TexturePtr texture, uint32_t face);
			void            UnlockBuffer(IndexBufferPtr buffer);
			void            UnlockBuffer(VertexBufferPtr buffer);
			void            UnlockTexture(TexturePtr texture, uint32_t face);
			void            CopyTexture(TexturePtr srcTexture, TexturePtr dstTexture);

			Array<uint8_t>  CompileShader(const uint8_t* shaderSource, uint32_t dataLength, const SK::String& entryPoint, FeatureLevel targetVersion, SK::String* errorMsg = nullptr);
			
			size_t          CreateVertexShader(const SK::String& name, const uint8_t* compiledSource, uint32_t dataLength, InputLayout inputLayout);
			void            SetVertexShader(size_t vshaderId);
			size_t          CreatePixelShader(const SK::String& name, const uint8_t* compiledSource, uint32_t dataLength);
			void            SetPixelShader(size_t pshaderId);

			size_t          GetShaderParamId(const SK::String& name);
			void            SetShaderParam(size_t id, void* data, uint32_t dataLength);
			void            SetShaderParam(size_t id, const SK::Math::Matrix& data);
			void            SetShaderParam(size_t id, const SK::Math::Vector& data);
			void            SetShaderTexture(int textureUnit, TexturePtr texture);
			void            SetShaderSampler(int samplerUnit, SamplerState sampler);
			void            CommitShaderParams();

			void            SetRenderTarget(TexturePtr texture, int face, TexturePtr depthTexture);
			void            SetRenderTargets(TexturePtr* textures, int* faces, int numTargets, TexturePtr depthTexture);
			void            SetDefaultRenderTarget();
			void            SetViewport(const SK::Math::int2& origin, const SK::Math::int2& size);

			void            SetDepthStencilMode(DepthStencilMode mode);
			void            SetCullingMode(CullMode mode);
			void            SetBlendingMode(BlendMode mode);
			void            SetInputLayout(InputLayout vertex);

			void            Clear(const ColourFloat& clearColour);
			void            DrawBuffer(PrimType primType, VertexBufferPtr vbuff, uint32_t numVerts, uint32_t offset);
			void            DrawBufferIndexed(PrimType primType, VertexBufferPtr vbuff, IndexBufferPtr ibuff, uint32_t numIndis, uint32_t indexOffset, uint32_t vertexOffset);
			void            DrawBufferInstanced(PrimType primType, VertexBufferPtr vbuff, uint32_t numVerts, uint32_t vertexOffset, VertexBufferPtr instanceBuff, uint32_t numInstances, uint32_t instanceOffset);
			void            DrawBufferIndexedInstanced(PrimType primType, VertexBufferPtr vbuff, IndexBufferPtr ibuff, uint32_t numIndis, uint32_t indexOffset, uint32_t vertexOffset, VertexBufferPtr instanceBuff, uint32_t numInstances, uint32_t instanceOffset);
			void            Present();
			void            Flush();

		private:

			//---- Types ----//
			//===============//

			struct PlatformData;

			//-- Variables --//
			//===============//

			PlatformData* m_Data;
		};
	}
}
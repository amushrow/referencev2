#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <cstdint>
#include <vector>

namespace SK
{
	namespace Graphics
	{
		//---- Types ----//
		//===============//

		const int MAX_TEXTURE_UNITS = 16;

		enum class FeatureLevel
		{
			cs_5_0,
			cs_4_1,
			cs_4_0,
			ds_5_0,
			gs_5_0,
			gs_4_1,
			gs_4_0,
			hs_5_0,
			ps_5_0,
			ps_4_1,
			ps_4_0,
			ps_3_0,
			ps_2_0,
			vs_5_0,
			vs_4_1,
			vs_4_0,
			vs_3_0,
			vs_2_0,
			vs_1_1
		};

		enum class PrimType
		{
			PointList,
			LineList,
			LineStrip,
			TriangleList,
			TriangleStrip
		};

		enum class DepthStencilMode
		{
			Disabled,
			ReadOnly,
			WriteOnly,
			Enabled
		};

		enum class CullMode
		{
			None,
			Clockwise,
			CounterClockwise
		};

		enum class BlendMode
		{
			None,
			Normal,
			Add,
			Multiply
		};

		enum class MultisampleType
		{
			None,

			MSAAx2,
			MSAAx4,
			MSAAx8,

			//ATI
			EQAAx2f4,
			EQAAx4f8,
			EQAAx4f16,
			EQAAx8f16,

			//nvidia
			CSAAx8,
			CSAAx8Q,
			CSAAx16,
			CSAAx16Q,
			CSAAx32
		};

		enum class SamplerState
		{
			TriLinear,
			TriLinearClamped,
			Anisotropic,
			AnisotropicClamped,
			Point,
			Shadow
		};

		struct DisplayMode
		{
			uint16_t   Width;
			uint16_t   Height;
			double     RefreshRate;
			Math::int2 RationalRefreshRate;
		};

		struct RenderCaps
		{
			std::vector<DisplayMode>     DisplayModes;
			std::vector<MultisampleType> MultisampleTypes;
		};

		struct RenderSettings
		{
			uint16_t        Width;
			uint16_t        Height;
			Math::int2      RationalRefreshRate;
			MultisampleType Multisample;
			uint16_t        PresentInterval;
			bool            Windowed;
		};
	}
}
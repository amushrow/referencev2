//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKGraphics/Renderable.h>
#include <SKGraphics/RenderTree.h>
using namespace SK;
using namespace SK::Graphics;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Renderable::Renderable()
: Object(GenericFlags::Or(ObjectDirty::Transform, DirtyFlags::BoundingBox, DirtyFlags::Resync))
, m_SortOrder(RenderSortOrder::Normal)
, m_Cullable(true)
, m_Visible(true)
, m_Root(nullptr)
{

}

void Renderable::Cullable(bool cull, bool recursive)
{
	m_Cullable = cull;

	if (recursive)
	{
		for (Renderable* child = Child(); child; child = child->Next())
			child->Cullable(cull, true);
	}
}

void Renderable::SortOrder(RenderSortOrder sortOrder, bool recursive)
{
	m_SortOrder = sortOrder;

	if (recursive)
	{
		for (Renderable* child = Child(); child; child = child->Next())
			child->SortOrder(sortOrder, true);
	}
}

void Renderable::Visible(bool visible, bool recursive)
{
	m_Visible = visible;

	if (recursive)
	{
		for (Renderable* child = Child(); child; child = child->Next())
			child->Visible(visible, true);
	}
}

void Renderable::LocalExtents(const Graphics::Extents& extents)
{
	m_LocalExtents = extents;
	m_DirtyFlags |= DirtyFlags::BoundingBox;
}

const Extents& Renderable::Extents()
{
	if (m_DirtyFlags.HasFlags(DirtyFlags::BoundingBox))
		UpdateExtents();

	return m_Extents;
}

const AABB& Renderable::AABB()
{
	if (m_DirtyFlags.HasFlags(DirtyFlags::BoundingBox))
		UpdateExtents();

	return m_AABB;
}

//-- Private Methods --//
//=====================//

void Renderable::UpdateExtents()
{
	const Math::Matrix& World = WorldTransform();
	for (int i = 0; i < 8; ++i)
		m_Extents.Corners[i] = World * m_LocalExtents.Corners[i];
	m_AABB = AABB::FromExtents(m_Extents);

	m_DirtyFlags &= ~static_cast<int>(DirtyFlags::BoundingBox);
}

void Renderable::Detached()
{
	if (m_Root)
	{
		m_Root->OnChildRemoved(this);
		SetRoot(nullptr);
	}
}

void Renderable::Attached(Renderable* parent)
{
	SetRoot(parent->m_Root);
	if (m_Root != nullptr)
	{
		m_Root->OnChildAdded(this);
	}
}

void Renderable::SetRoot(RenderTree* root)
{
	m_Root = root;
	for (Renderable* child = Child(); child; child = child->Next())
		child->SetRoot(root);
}
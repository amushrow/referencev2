//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKInput/Device.h>

using namespace SK::Input;


//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

Device::Device(const SK::String& name, const SK::Guid& guid, const SK::Input::DeviceInfo& devInfo)
: m_Id(-1)
, m_Name(name)
, m_Guid(guid)
, m_DevInfo(devInfo)
, ButtonUp(m_ButtonUp)
, ButtonDown(m_ButtonDown)
, AxisChanged(m_AxisChanged)
{

}

SK::String Device::GetKeyName(Key key) const
{
	return Input::GetKeyName(key);
}

void Device::SetForceFeedback(ForceFeedbackAxis axis, float magnitude)
{
	SetForceFeedback({AxisMagnitude(axis, magnitude)});
}

//-- Private Methods --//
//=====================//

void Device::OnButtonUp(Key key)
{
	KeyEventArgs Data(key, 0.0f);
	m_ButtonUp.OnEvent(this, Data);
}

void Device::OnButtonDown(Key key)
{
	KeyEventArgs Data(key, 1.0f);
	m_ButtonDown.OnEvent(this, Data);
}

void Device::OnAxisChanged(Key key, float value)
{
	KeyEventArgs Data(key, value);
	m_AxisChanged.OnEvent(this, Data);
}

//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKInput/DeviceManager.h>
#include <SKInput/GameKeys.h>
using namespace SK::Input;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

KeyBinding::KeyBinding(unsigned int device, Key deviceKey)
: m_DeadZone(0.0f)
, m_Value(0.0f)
, m_PrevValue(0.0f)
{
	m_Bindings.push_back(DeviceKeyPair(&DeviceManager::Instance()[device], deviceKey));
}

KeyBinding::KeyBinding(SpecialDevice device, Key deviceKey)
: m_DeadZone(0.0f)
, m_Value(0.0f)
, m_PrevValue(0.0f)
{
	m_Bindings.push_back(DeviceKeyPair(&DeviceManager::Instance()[device], deviceKey));
}

KeyBinding::KeyBinding(std::vector<DeviceKeyPair> bindings)
: m_Bindings(bindings)
, m_DeadZone(0.0f)
, m_Value(0.0f)
, m_PrevValue(0.0f)
{

}

void KeyBinding::DeadZone(float dead)
{
	m_DeadZone = dead;
}

void KeyBinding::AddBinding(unsigned int device, Key deviceKey)
{
	m_Bindings.push_back(DeviceKeyPair(&DeviceManager::Instance()[device], deviceKey));
}

void KeyBinding::AddBinding(SpecialDevice device, Key deviceKey)
{
	m_Bindings.push_back(DeviceKeyPair(&DeviceManager::Instance()[device], deviceKey));
}

void KeyBinding::AddBinding(const KeyBinding& other)
{
	for (auto& NewBinding : other.m_Bindings)
	{
		bool Duplicate = false;
		for (auto& ExistingBinding : m_Bindings)
		{
			if (ExistingBinding.Device == NewBinding.Device
				&& ExistingBinding.Key == NewBinding.Key)
			{
				Duplicate = true;
				break;
			}
		}

		if (!Duplicate)
		{
			m_Bindings.push_back(NewBinding);
		}
	}
}

void KeyBinding::RemoveBinding(unsigned int device, Key deviceKey)
{
	for (auto It = m_Bindings.begin(); It != m_Bindings.end(); ++It)
	{
		if (DeviceManager::Instance()[device] == *It->Device
			&& It->Key == deviceKey)
		{
			m_Bindings.erase(It);
			break;
		}
	}
}

void KeyBinding::RemoveBinding(SpecialDevice device, Key deviceKey)
{
	for (auto It = m_Bindings.begin(); It != m_Bindings.end(); ++It)
	{
		if (DeviceManager::Instance()[device] == *It->Device
			&& It->Key == deviceKey)
		{
			m_Bindings.erase(It);
			break;
		}
	}
}

void KeyBinding::ClearBindings()
{
	m_Bindings.clear();
}

void KeyBinding::Update()
{
	m_PrevValue = m_Value;
	m_Value = 0.0f;
	for (auto& Binding : m_Bindings)
	{
		float BindingValue = Binding.Device->GetValue(Binding.Key);
		if (std::abs(BindingValue) > std::abs(m_Value))
		{
			m_Value = BindingValue;
		}
	}
}

bool KeyBinding::Pressed()
{
	return std::abs(m_Value) > m_DeadZone;
}

bool KeyBinding::JustPressed()
{
	return std::abs(m_Value) > m_DeadZone && std::abs(m_PrevValue) <= m_DeadZone;
}

bool KeyBinding::JustReleased()
{
	return std::abs(m_Value) <= m_DeadZone && std::abs(m_PrevValue) > m_DeadZone;
}

float KeyBinding::Value()
{
	return m_Value;
}

//-- Private Methods --//
//=====================//

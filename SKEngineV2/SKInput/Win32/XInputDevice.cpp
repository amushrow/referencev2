//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKInput/Win32/XInputDevice.h>
#include <SKSystem/Utils.h>

using namespace SK::Input;

//-- Declarations --//
//==================//

namespace
{
	float GetAxisValue(SHORT x);
	SK::Guid GetXInputGuid(const XINPUT_CAPABILITIES& caps);

	Key XInputButtonMap[16] =
	{
		Key::DPad_Up,
		Key::DPad_Down,
		Key::DPad_Left,
		Key::DPad_Right,
		Key::Button1,
		Key::Button2,
		Key::Button3,
		Key::Button4,
		Key::Button5,
		Key::Button6,
		Key::Button31, // These two aren't actually used
		Key::Button32, //
		Key::Button7,
		Key::Button8,
		Key::Button9,
		Key::Button10,
	};

	Key XInputAxisMap[6] =
	{
		Key::Axis_X,
		Key::Axis_Y,
		Key::Axis_Rx,
		Key::Axis_Ry,
		Key::Slider1,
		Key::Slider2
	};
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

namespace
{
	SK::Guid PadGuid("8A3297B1-B97A-45BB-B34D-B9B75D6605B0");
	SK::Guid WheelGuid("17C0C10B-4C84-4BD1-A242-1BDAD67E77DC");
	SK::Guid StickGuid("90EE64DC-9971-4866-B537-238334E92812");
}

//-- Public Methods --//
//====================//

XInputDevice::XInputDevice(uint16_t index, const XINPUT_CAPABILITIES& caps)
: Device(String::Format("Xbox 360 Peripheral [%u]", index), GetXInputGuid(caps), { 0, 0, index })
, m_PadIndex(index)
, m_Type(Input::Type::Joypad)
{
#if (WINVER >= _WIN32_WINNT_WIN8)
	switch (caps.SubType)
	{
	case XINPUT_DEVSUBTYPE_WHEEL:
		m_Type = Input::Type::Wheel; break;
	case XINPUT_DEVSUBTYPE_FLIGHT_STICK:
	case XINPUT_DEVSUBTYPE_ARCADE_STICK:
		m_Type = Input::Type::Joystick; break;
	}
#endif

	memset(m_Buttons, 0, sizeof(m_Buttons));
	memset(m_Axis, 0, sizeof(m_Axis));

	if (caps.Vibration.wLeftMotorSpeed)
	{
		m_AvailableFFAxis |= ForceFeedbackAxis::X;
	}
	if (caps.Vibration.wRightMotorSpeed)
	{
		m_AvailableFFAxis |= ForceFeedbackAxis::Y;
	}
}

XInputDevice::~XInputDevice()
{

}

void XInputDevice::ClearInput()
{
	for (int i = 0; i < 16; ++i)
	{
		if (m_Buttons[i])
		{
			m_Buttons[i] = false;
			OnButtonUp(XInputButtonMap[i]);
		}
	}

	for (int i = 0; i < 6; ++i)
	{
		if (m_Axis[i] != 0.0f)
		{
			m_Axis[i] = 0.0f;
			OnAxisChanged(XInputAxisMap[i], 0.0f);
		}
	}
}

float XInputDevice::GetValue(Key key) const
{
	switch (key)
	{
	case Key::DPad_Up:    return m_Buttons[0] ? 1.0f : 0.0f;
	case Key::DPad_Down:  return m_Buttons[1] ? 1.0f : 0.0f;
	case Key::DPad_Left:  return m_Buttons[2] ? 1.0f : 0.0f;
	case Key::DPad_Right: return m_Buttons[3] ? 1.0f : 0.0f;
	case Key::Button1:    return m_Buttons[4] ? 1.0f : 0.0f;
	case Key::Button2:	  return m_Buttons[5] ? 1.0f : 0.0f;
	case Key::Button3:	  return m_Buttons[6] ? 1.0f : 0.0f;
	case Key::Button4:	  return m_Buttons[7] ? 1.0f : 0.0f;
	case Key::Button5:	  return m_Buttons[8] ? 1.0f : 0.0f;
	case Key::Button6:	  return m_Buttons[9] ? 1.0f : 0.0f;
	case Key::Button7:	  return m_Buttons[12] ? 1.0f : 0.0f;
	case Key::Button8:	  return m_Buttons[13] ? 1.0f : 0.0f;
	case Key::Button9:	  return m_Buttons[14] ? 1.0f : 0.0f;
	case Key::Button10:	  return m_Buttons[15] ? 1.0f : 0.0f;

	case Key::Axis_X:  return m_Axis[0];
	case Key::Axis_Y:  return m_Axis[1];
	case Key::Axis_Rx:  return m_Axis[2];
	case Key::Axis_Ry:  return m_Axis[3];
	case Key::Slider1:    return m_Axis[4];
	case Key::Slider2:    return m_Axis[5];
	}

	return 0.0f;
}

SK::String XInputDevice::GetKeyName(Key key) const
{
	switch (key)
	{
	case Key::DPad_Up:    return "D-Pad Up";
	case Key::DPad_Down:  return "D-Pad Down";
	case Key::DPad_Left:  return "D-Pad Left";
	case Key::DPad_Right: return "D-Pad Right";
	case Key::Button1:    return "Start";
	case Key::Button2:    return "Back";
	case Key::Button3:    return "Left Thumb";
	case Key::Button4:    return "Right Thumb";
	case Key::Button5:    return "Left Shoulder";
	case Key::Button6:    return "Right Shoulder";
	case Key::Button7:    return "A";
	case Key::Button8:    return "B";
	case Key::Button9:    return "X";
	case Key::Button10:   return "Y";
	case Key::Axis_X:  return "Left Thumb X Axis";
	case Key::Axis_Y:  return "Left Thumb Y Axis";
	case Key::Axis_Rx:  return "Right Thumb X Axis";
	case Key::Axis_Ry:  return "Right Thumb Y Axis";
	case Key::Slider1:    return "Left Trigger";
	case Key::Slider2:    return "Right Trigger";
	}

	return "Invalid";
}

void XInputDevice::SetForceFeedback(const std::vector<AxisMagnitude>& axes)
{
	XINPUT_VIBRATION Vibration = { 0, 0 };
	for (std::size_t i = 0; i < axes.size(); ++i)
	{
		if (axes[i].Axis == ForceFeedbackAxis::X)
		{
			Vibration.wLeftMotorSpeed = static_cast<WORD>(std::abs(axes[i].Magnitude) * 65535.0f);
		}
		else if (axes[i].Axis == ForceFeedbackAxis::Y)
		{
			Vibration.wRightMotorSpeed = static_cast<WORD>(std::abs(axes[i].Magnitude) * 65535.0f);
		}
	}

	XInputSetState(m_PadIndex, &Vibration);
}

void XInputDevice::ClearForceFeedback()
{
	XINPUT_VIBRATION Clear = { 0, 0 };
	XInputSetState(m_PadIndex, &Clear);
}

//-- Private Methods --//
//=====================//

void XInputDevice::Update()
{
	XINPUT_STATE State;
	if (XInputGetState(m_PadIndex, &State) != ERROR_SUCCESS)
	{
		memset(&State, 0, sizeof(State));
	}

	for (int i = 0; i < 16; ++i)
	{
		int XInputButton = (1 << i);
		if (State.Gamepad.wButtons & XInputButton)
		{
			if (!m_Buttons[i])
			{
				m_Buttons[i] = true;
				OnButtonDown(XInputButtonMap[i]);
			}
		}
		else if (m_Buttons[i])
		{
			m_Buttons[i] = false;
			OnButtonUp(XInputButtonMap[i]);
		}
	}

	float NewAxis[6] =
	{
		GetAxisValue(State.Gamepad.sThumbLX),
		GetAxisValue(State.Gamepad.sThumbLY),
		GetAxisValue(State.Gamepad.sThumbRX),
		GetAxisValue(State.Gamepad.sThumbRY),
		float(State.Gamepad.bLeftTrigger) / 255.0f,
		float(State.Gamepad.bRightTrigger) / 255.0f
	};

	for (int i = 0; i < 6; ++i)
	{
		if (m_Axis[i] != NewAxis[i])
		{
			m_Axis[i] = NewAxis[i];
			OnAxisChanged(XInputAxisMap[i], m_Axis[i]);
		}
	}
}

namespace
{
	float GetAxisValue(SHORT x)
	{
		return x > 0 ? float(x) / 32767.0f : float(x) / 32768.0f;
	}

	SK::Guid GetXInputGuid(const XINPUT_CAPABILITIES& caps)
	{
#if (WINVER >= _WIN32_WINNT_WIN8)
		if (caps.SubType == XINPUT_DEVSUBTYPE_WHEEL)
		{
			return WheelGuid;
		}
		else if (caps.SubType == XINPUT_DEVSUBTYPE_FLIGHT_STICK ||
			caps.SubType == XINPUT_DEVSUBTYPE_ARCADE_STICK)
		{
			return StickGuid;
		}
#endif

		return PadGuid;
	}
}
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Utils.h>
#include <SKSystem/Debug.h>
#include <SKInput/Win32/RawInputDevices.h>

using namespace SK::Input;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

SystemMouse::SystemMouse()
: RawInputDevice("System Mouse", SK::Guid("82FF81BF-6341-4433-81DA-1118DCB86424"), { 0, 0, 0 })
, m_DeltaX(0)
, m_DeltaY(0)
, m_PosX(0)
, m_PosY(0)
{

}

void SystemMouse::ClearInput()
{
	m_DeltaX = 0;
	m_DeltaY = 0;

	m_Mice.clear();
}

float SystemMouse::GetValue(Key key) const
{
	switch (key)
	{
	case Key::MouseXAxis: return static_cast<float>(m_DeltaX);
	case Key::MouseYAxis: return static_cast<float>(m_DeltaY);
	case Key::MouseXPos:  return static_cast<float>(m_PosX);
	case Key::MouseYPos:  return static_cast<float>(m_PosY);
	case Key::MouseLeftButton:
	case Key::MouseRightButton:
	case Key::MouseMiddleButton:
	case Key::MouseButton1:
	case Key::MouseButton2:
		{
			int Index = static_cast<int>(key) -static_cast<int>(Key::MouseLeftButton);
			for (auto& KVP : m_Mice)
			{
				if (KVP.second->Buttons[Index] != 0)
				{
					return 1.0f;
				}
			}
		}
		break;
	case Key::MouseWheel:
		{
			short WheelDelta = 0;
			for (auto& KVP : m_Mice)
			{
				WheelDelta += KVP.second->WheelDelta;
			}
			return static_cast<float>(WheelDelta);
		}
		break;
	}

	return 0.0f;
}

void SystemMouse::InputEvent(const RAWINPUT& eventData)
{
	HANDLE MapKey = eventData.header.hDevice;
	if (m_Mice.find(MapKey) == m_Mice.end())
	{
		// New device
		MouseData* Data = new MouseData();
		memset(Data, 0, sizeof(MouseData));
		m_Mice.insert({ MapKey, Data });
	}

	if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_LEFT_BUTTON_DOWN))
	{
		m_Mice[MapKey]->Buttons[0] = 0x03;
	}
	else if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_LEFT_BUTTON_UP))
	{
		m_Mice[MapKey]->Buttons[0] &= ~0x01;
	}

	if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_RIGHT_BUTTON_DOWN))
	{
		m_Mice[MapKey]->Buttons[1] = 0x03;
	}
	else if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_RIGHT_BUTTON_UP))
	{
		m_Mice[MapKey]->Buttons[1] &= ~0x01;
	}

	if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_MIDDLE_BUTTON_DOWN))
	{
		m_Mice[MapKey]->Buttons[2] = 0x03;
	}
	else if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_MIDDLE_BUTTON_UP))
	{
		m_Mice[MapKey]->Buttons[2] &= ~0x01;
	}

	if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_BUTTON_4_DOWN))
	{
		m_Mice[MapKey]->Buttons[3] = 0x03;
	}
	else if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_BUTTON_4_UP))
	{
		m_Mice[MapKey]->Buttons[3] &= ~0x01;
	}

	if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_BUTTON_5_DOWN))
	{
		m_Mice[MapKey]->Buttons[4] = 0x03;
	}
	else if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_BUTTON_5_UP))
	{
		m_Mice[MapKey]->Buttons[4] &= ~0x01;
	}

	if (SK::CheckFlags(eventData.data.mouse.usButtonFlags, RI_MOUSE_WHEEL))
	{
		m_Mice[MapKey]->WheelDelta += static_cast<short>(eventData.data.mouse.usButtonData);
	}
}

void SystemMouse::DeviceRemoved(HANDLE handle)
{
	auto KVP = m_Mice.find(handle);
	if (KVP != m_Mice.end())
	{
		memset(KVP->second, 0, sizeof(MouseData));
	}
}

//-- Private Methods --//
//=====================//

void SystemMouse::Update()
{
	POINT CursorPos;
	GetCursorPos(&CursorPos);
	m_DeltaX = CursorPos.x - m_PosX;
	m_DeltaY = CursorPos.y - m_PosY;
	m_PosX = CursorPos.x;
	m_PosY = CursorPos.y;

	for (auto& KVP : m_Mice)
	{
		for (int i = 0; i < 5; ++i)
		{
			KVP.second->Buttons[i] &= ~0x02;
		}

		KVP.second->WheelDelta = 0;
	}
}
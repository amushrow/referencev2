#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Win32/Win32.h>
#include <Xinput.h>
#include <SKInput/Device.h>

namespace SK
{
	namespace Input
	{
		//---- Types ----//
		//===============//

		class XInputDevice : public Device
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			XInputDevice(uint16_t index, const XINPUT_CAPABILITIES& caps);
			~XInputDevice();

			void        ClearInput() override;
			Input::Type Type() const override { return m_Type; }
			float       GetValue(Key key) const override;
			String      GetKeyName(Key key) const override;

			void        SetForceFeedback(const std::vector<AxisMagnitude>& axes) override;
			void        SetForceFeedback(ForceFeedbackAxis axis, float magnitude);
			void        ClearForceFeedback() override;
			FFAxisFlags AvailableFFAxis() const override { return m_AvailableFFAxis; }

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			void        Update() override;

			//-- Variables --//
			//===============//

			uint16_t     m_PadIndex;
			Input::Type  m_Type;
			FFAxisFlags  m_AvailableFFAxis;

			bool         m_Buttons[16];
			float        m_Axis[6];
		};

		//--- Methods ---//
		//===============//
	}
}
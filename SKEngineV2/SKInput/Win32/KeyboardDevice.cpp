//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKInput/Win32/RawInputDevices.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/Utils.h>

using namespace SK;
using namespace SK::Input;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

KeyboardDevice::KeyboardDevice(HANDLE deviceHandle)
: RawInputDevice(GetDeviceName(GetDevicePath(deviceHandle), false), GetGuid(deviceHandle), GetDevInfo(deviceHandle))
, m_Handle(deviceHandle)
{
	memset(m_Buttons, 0, sizeof(m_Buttons));
}

KeyboardDevice::~KeyboardDevice()
{
	
}

void KeyboardDevice::ClearInput()
{
	for (int i = 0; i < 256; ++i)
	{
		if (ClearButtonState(m_Buttons[i]))
		{
			OnButtonUp(static_cast<Key>(i));
		}
	}
}

float KeyboardDevice::GetValue(Key key) const
{
	int Index = static_cast<int>(key);
	if (Index < 256)
	{
		return m_Buttons[Index] ? 1.0f : 0.0f;
	}
	return 0.0f;
}

void KeyboardDevice::InputEvent(const RAWINPUT& eventData)
{
	int VK = GetVirtualKey(eventData.data.keyboard);
	if (eventData.data.keyboard.Flags & RI_KEY_BREAK)
	{
		if (SetButtonStateUp(m_Buttons[VK]))
		{
			OnButtonUp(static_cast<Key>(VK));
		}
	}
	else
	{
		if (SetButtonStateDown(m_Buttons[VK]))
		{
			OnButtonDown(static_cast<Key>(VK));
		}
	}
}

void KeyboardDevice::DeviceRemoved(HANDLE handle)
{
	ClearInput();
}


//-- Private Methods --//
//=====================//

void KeyboardDevice::Update()
{
	for (int i = 0; i < 256; ++i)
	{
		if (UpdateButtonState(m_Buttons[i]))
		{
			OnButtonUp(static_cast<Key>(i));
		}
	}
}
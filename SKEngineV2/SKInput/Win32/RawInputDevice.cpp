//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Win32/Win32.h>
#include <SetupAPI.h>
#include <devguid.h>
#include <hidsdi.h>
#include <SKInput/Win32/RawInputDevices.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/Debug.h>
#include <SKSystem/Utils.h>

#ifndef WINDOWS_XP
#define INITGUID
#include <devpkey.h>
#endif

#pragma comment(lib, "Setupapi.lib")
#pragma comment(lib, "Hid.lib")

using namespace SK::Input;

//-- Declarations --//
//==================//

namespace
{
	enum class HIDType
	{
		Interface,
		Mouse,
		Keyboard
	};

	bool FindDevData(const SK::String& devicePath, HIDType type, HDEVINFO& outDevInfo, SP_DEVINFO_DATA& outDevInfoData);
}

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

SK::String RawInputDevice::GetKeyName(Key key) const
{
	if (Type() == Type::Keyboard)
	{
		int VK = static_cast<int>(key);
		if (VK < 256)
		{
			switch (key)
			{
			case Key::OEM_1:
			case Key::OEM_2:
			case Key::OEM_3:
			case Key::OEM_4:
			case Key::OEM_5:
			case Key::OEM_6:
			case Key::OEM_7:
			case Key::OEM_8:
			{
				if (key == Key::NumpadEnter)
				{
					VK = VK_RETURN;
				}
				int ScanCode = MapVirtualKey(VK, MAPVK_VK_TO_VSC);

				switch (VK)
				{
				case VK_LEFT:
				case VK_UP:
				case VK_RIGHT:
				case VK_DOWN:
				case VK_PRIOR:
				case VK_NEXT:
				case VK_END:
				case VK_HOME:
				case VK_INSERT:
				case VK_DELETE:
				case VK_DIVIDE:
				case VK_NUMLOCK:
				case VK_RCONTROL:
				case VK_RMENU:
				case VK_RWIN:
					ScanCode |= 0x100; // set extended bit
					break;
				}

				wchar_t Name[64];
				if (GetKeyNameText(ScanCode << 16, Name, 64))
				{
					return SK::Encoding::UTF16::FromBytes(reinterpret_cast<uint8_t*>(Name));
				}
			}
			break;
			}

			return Input::GetKeyName(key);
		}
	}
	else if (Type() == Type::Mouse)
	{
		if (key >= Key::MouseLeftButton && key <= Key::MouseYPos)
		{
			return Input::GetKeyName(key);
		}
	}

	// Not a valid device key
	return "Invalid";
}

int RawInputDevice::GetVirtualKey(const RAWKEYBOARD& keyboard)
{
	int VK = keyboard.VKey;
	const bool E0 = (keyboard.Flags & RI_KEY_E0) != 0;

	if (VK == VK_SHIFT)
	{
		VK = MapVirtualKey(keyboard.MakeCode, MAPVK_VSC_TO_VK_EX);
	}

	switch (VK)
	{
	case VK_CONTROL: VK = static_cast<int>(E0 ? Key::RightCtrl : Key::LeftCtrl); break;
	case VK_MENU:    VK = static_cast<int>(E0 ? Key::AltGr : Key::Alt); break;
	case VK_RETURN:  VK = static_cast<int>(E0 ? Key::NumpadEnter : Key::Return); break;
	case VK_INSERT:  VK = static_cast<int>(E0 ? Key::Insert : Key::Numpad0); break;
	case VK_DELETE:  VK = static_cast<int>(E0 ? Key::Delete : Key::Decimal); break;
	case VK_HOME:    VK = static_cast<int>(E0 ? Key::Home : Key::Numpad7); break;
	case VK_END:     VK = static_cast<int>(E0 ? Key::End : Key::Numpad1); break;
	case VK_PRIOR:   VK = static_cast<int>(E0 ? Key::Prior : Key::Numpad9); break;
	case VK_NEXT:    VK = static_cast<int>(E0 ? Key::Next : Key::Numpad3); break;
	case VK_LEFT:    VK = static_cast<int>(E0 ? Key::Left : Key::Numpad4); break;
	case VK_RIGHT:   VK = static_cast<int>(E0 ? Key::Right : Key::Numpad6); break;
	case VK_UP:      VK = static_cast<int>(E0 ? Key::Up : Key::Numpad8); break;
	case VK_DOWN:    VK = static_cast<int>(E0 ? Key::Down : Key::Numpad2); break;
	case VK_CLEAR:   VK = static_cast<int>(E0 ? Key::Clear : Key::Numpad5); break;
	}

	return VK;
}

bool RawInputDevice::SetButtonStateDown(uint8_t& button)
{
	bool Event = button == 0;

	// Set key down and update bits
	button = 0x03;

	return Event;
}

bool RawInputDevice::SetButtonStateUp(uint8_t& button)
{
	bool Event = button != 0;

	// Remove key down bit
	button &= ~0x01;

	return Event && button == 0;
}

bool RawInputDevice::UpdateButtonState(uint8_t& button)
{
	bool Event = button != 0;

	// Remove update bit
	button &= ~0x02;

	return Event && button == 0;
}

bool RawInputDevice::ClearButtonState(uint8_t& button)
{
	bool Event = button != 0;

	// Clear all the bits
	button = 0;

	return Event;
}

//-- Private Methods --//
//=====================//

SK::String SK::Input::RawInputDevice::GetDeviceName(const SK::String& devicePath, bool isMouse)
{
	String Result;

	auto DevicePathParts = devicePath.Substring(4).Split("#");
	SK::String InstanceId = SK::String::Format("%s\\%s\\%s", DevicePathParts[0].Buffer(), DevicePathParts[1].Buffer(), DevicePathParts[2].Buffer());

	HDEVINFO DevInfo;
	SP_DEVINFO_DATA DevInfoData;

	if (FindDevData(InstanceId, isMouse ? HIDType::Mouse : HIDType::Keyboard, DevInfo, DevInfoData))
	{
		uint8_t FriendlyName[MAX_PATH * sizeof(wchar_t)];
		DWORD DataType;
		DWORD Size = 0;
		if (!SetupDiGetDeviceRegistryProperty(DevInfo, &DevInfoData, SPDRP_FRIENDLYNAME, &DataType, FriendlyName, sizeof(FriendlyName), &Size))
		{
			SetupDiGetDeviceRegistryProperty(DevInfo, &DevInfoData, SPDRP_DEVICEDESC, &DataType, FriendlyName, sizeof(FriendlyName), &Size);
		}

		Result = SK::Encoding::UTF16::FromBytes(FriendlyName);

#ifndef WINDOWS_XP
		if (Result.Equals("HID-compliant mouse") || Result.Equals("HID Keyboard Device"))
		{
			//Device name is not very helpful, try digging a little deeper

			// Get parent HID interface
			uint8_t ParentBuffer[1024];
			DEVPROPTYPE PropType;
			DWORD RequiredSize;
			SetupDiGetDeviceProperty(DevInfo, &DevInfoData, &DEVPKEY_Device_Parent, &PropType, ParentBuffer, 1024, &RequiredSize, 0);

			HDEVINFO ParentDevInfo;
			SP_DEVINFO_DATA ParentDevInfoData;
			if (FindDevData(SK::Encoding::UTF16::FromBytes(ParentBuffer), HIDType::Interface, ParentDevInfo, ParentDevInfoData))
			{
				uint8_t FriendlyName[MAX_PATH * sizeof(wchar_t)];
				DWORD DataType;
				DWORD Size = 0;
				if (!SetupDiGetDeviceRegistryProperty(ParentDevInfo, &ParentDevInfoData, SPDRP_FRIENDLYNAME, &DataType, FriendlyName, sizeof(FriendlyName), &Size))
				{
					SetupDiGetDeviceRegistryProperty(ParentDevInfo, &ParentDevInfoData, SPDRP_DEVICEDESC, &DataType, FriendlyName, sizeof(FriendlyName), &Size);
				}

				if (SK::Encoding::UTF16::FromBytes(FriendlyName).Equals("USB Input Device"))
				{
					// Also not helpful. Last chance, use the bus reported name instead
					SetupDiGetDeviceProperty(ParentDevInfo, &ParentDevInfoData, &DEVPKEY_Device_BusReportedDeviceDesc, &PropType, FriendlyName, sizeof(FriendlyName), &RequiredSize, 0);
				}
				Result = SK::Encoding::UTF16::FromBytes(FriendlyName);

				SetupDiDestroyDeviceInfoList(ParentDevInfo);
			}
		}
#endif
		SetupDiDestroyDeviceInfoList(DevInfo);
		return Result;
	}

	return "Unknown";
}

SK::String RawInputDevice::GetDevicePath(HANDLE deviceHandle)
{
	UINT BufferLength = 1024;
	uint8_t DeviceNameBuffer[1024 * sizeof(wchar_t)];
	GetRawInputDeviceInfo(deviceHandle, RIDI_DEVICENAME, DeviceNameBuffer, &BufferLength);
	return SK::Encoding::UTF16::FromBytes(DeviceNameBuffer);
}

SK::Guid RawInputDevice::GetGuid(HANDLE deviceHandle)
{
	String DevicePath = GetDevicePath(deviceHandle);
	return SK::Guid(DevicePath.Substring(DevicePath.LastIndexOf("#") + 1));
}

DeviceInfo RawInputDevice::GetDevInfo(HANDLE deviceHandle)
{
	UINT BufferLength = 1024;
	uint8_t DeviceNameBuffer[1024 * sizeof(wchar_t)];
	GetRawInputDeviceInfo(deviceHandle, RIDI_DEVICENAME, DeviceNameBuffer, &BufferLength);
	String DevicePath = SK::Encoding::UTF16::FromBytes(DeviceNameBuffer);

	//Get Vid & Pid
	HANDLE HIDDevice = CreateFile(reinterpret_cast<wchar_t*>(DeviceNameBuffer), 0, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, 0, nullptr);
	if (HIDDevice == INVALID_HANDLE_VALUE)
	{
		throw SK::IOException("MouseDevice - Unable to open device");
	}
	HIDD_ATTRIBUTES Attributes;
	HidD_GetAttributes(HIDDevice, &Attributes);

	Input::DeviceInfo DevInfo = { Attributes.VendorID, Attributes.ProductID, 0 };

	auto DeviceData = DevicePath.Split("#")[1].Split("&");
	for (String& Info : DeviceData)
	{
		if (Info.StartsWith("MI_"))
		{
			DevInfo.Interface = atoi(reinterpret_cast<const char*>(Info.Substring(3).Buffer()));
			break;
		}
	}
	CloseHandle(HIDDevice);

	return DevInfo;
}

namespace
{
	bool FindDevData(const SK::String& devicePath, HIDType type, HDEVINFO& outDevInfo, SP_DEVINFO_DATA& outDevInfoData)
	{
		GUID ClassGuid;
		switch (type)
		{
		case HIDType::Mouse: ClassGuid = GUID_DEVCLASS_MOUSE; break;
		case HIDType::Keyboard: ClassGuid = GUID_DEVCLASS_KEYBOARD; break;
		case HIDType::Interface: ClassGuid = GUID_DEVCLASS_HIDCLASS; break;
		}

		outDevInfo = SetupDiGetClassDevs(&ClassGuid, nullptr, nullptr, DIGCF_PRESENT);
		if (outDevInfo != INVALID_HANDLE_VALUE)
		{
			outDevInfoData.cbSize = sizeof(outDevInfoData);
			for (int i = 0; SetupDiEnumDeviceInfo(outDevInfo, i, &outDevInfoData); ++i)
			{
				wchar_t buf[2048];
				DWORD size = 0;
				if (SetupDiGetDeviceInstanceId(outDevInfo, &outDevInfoData, buf, sizeof buf, &size))
				{
					if (SK::Encoding::UTF16::FromBytes(reinterpret_cast<uint8_t*>(buf)).EqualsCI(devicePath))
					{
						return true;
					}
				}
			}

			SetupDiDestroyDeviceInfoList(outDevInfo);
		}

		return false;
	}
}
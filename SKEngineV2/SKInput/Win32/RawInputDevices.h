#pragma once
//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//---- Notes ----//
//===============//

//--- Include ---//
//===============//
#include <SKSystem/Win32/Win32.h>
#include <SKInput/Device.h>
#include <unordered_map>

namespace SK
{
	namespace Input
	{
		//---- Types ----//
		//===============//

		class RawInputDevice : public Device
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			RawInputDevice(const String& name, const SK::Guid& guid, const Input::DeviceInfo& devInfo)
				: Device(name, guid, devInfo) {}

			virtual HANDLE RawInutDevice() = 0;
			virtual void   InputEvent(const RAWINPUT& eventData) = 0;
			virtual void   DeviceRemoved(HANDLE handle) = 0;

			String         GetKeyName(Key key) const override;

			int            GetVirtualKey(const RAWKEYBOARD& keyboard);

			bool           SetButtonStateDown(uint8_t& button);
			bool           SetButtonStateUp(uint8_t& button);
			bool           UpdateButtonState(uint8_t& button);
			bool           ClearButtonState(uint8_t& button);

			//-- Variables --//
			//===============//

		protected:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			static String GetDeviceName(const String& deviceKey, bool isMouse);
			static String GetDevicePath(HANDLE deviceHandle);
			static SK::Guid GetGuid(HANDLE deviceHandle);
			static Input::DeviceInfo GetDevInfo(HANDLE deviceHandle);

			//-- Variables --//
			//===============//
		};

		class KeyboardDevice : public RawInputDevice
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			KeyboardDevice(HANDLE deviceHandle);
			~KeyboardDevice();

			void        ClearInput() override;
			Input::Type Type() const override { return Type::Keyboard; }
			float       GetValue(Key key) const override;

			HANDLE      RawInutDevice() override { return m_Handle; }
			void        InputEvent(const RAWINPUT& eventData) override;
			void        DeviceRemoved(HANDLE handle) override;

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			void        Update() override;

			//-- Variables --//
			//===============//

			HANDLE  m_Handle;
			uint8_t m_Buttons[256];
		};

		class SystemKeyboard : public RawInputDevice
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			SystemKeyboard();
			~SystemKeyboard();

			void        ClearInput() override;
			Input::Type Type() const override { return Type::Keyboard; }
			float       GetValue(Key key) const override;

			HANDLE      RawInutDevice() override { return INVALID_HANDLE_VALUE; }
			void        InputEvent(const RAWINPUT& eventData) override;
			void        DeviceRemoved(HANDLE handle) override;
			
			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			struct Buttons
			{
				uint8_t Button[256];
			};

			//--- Methods ---//
			//===============//

			void        Update() override;

			//-- Variables --//
			//===============//
			
			std::unordered_map<HANDLE, Buttons*> m_Keyboards;
		};

		class MouseDevice : public RawInputDevice
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			MouseDevice(HANDLE deviceHandle);
			~MouseDevice();

			void        ClearInput() override;
			Input::Type Type() const override { return Type::Mouse; }
			float       GetValue(Key key) const override;

			HANDLE      RawInutDevice() override { return m_Handle; }
			void        InputEvent(const RAWINPUT& eventData) override;
			void        DeviceRemoved(HANDLE handle) override;

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			void        Update() override;

			//-- Variables --//
			//===============//

			HANDLE  m_Handle;
			uint8_t m_Buttons[5];
			short   m_WheelDelta;
			int     m_DeltaX;
			int     m_DeltaY;
			int     m_PosX;
			int     m_PosY;
		};

		class SystemMouse : public RawInputDevice
		{
		public:
			//---- Types ----//
			//===============//

			//--- Methods ---//
			//===============//

			SystemMouse();

			void        ClearInput() override;
			Input::Type Type() const override { return Type::Mouse; }
			float       GetValue(Key key) const override;

			HANDLE      RawInutDevice() override { return INVALID_HANDLE_VALUE; }
			void        InputEvent(const RAWINPUT& eventData) override;
			void        DeviceRemoved(HANDLE handle) override;

			//-- Variables --//
			//===============//

		private:
			//---- Types ----//
			//===============//

			struct MouseData
			{
				int8_t  Buttons[5];
				short   WheelDelta;
			};

			//--- Methods ---//
			//===============//

			void        Update() override;

			//-- Variables --//
			//===============//

			std::unordered_map<HANDLE, MouseData*> m_Mice;
			int                                    m_DeltaX;
			int                                    m_DeltaY;
			int                                    m_PosX;
			int                                    m_PosY;
		};

		//--- Methods ---//
		//===============//
	}
}
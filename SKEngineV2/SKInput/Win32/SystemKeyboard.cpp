//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKSystem/Debug.h>
#include <SKInput/Win32/RawInputDevices.h>

using namespace SK::Input;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

SystemKeyboard::SystemKeyboard()
: RawInputDevice("System Keyboard", SK::Guid("ED4BBC0A-6308-48EC-9CC1-A9BE7C13996F"), { 0, 0, 0 })
{

}

SystemKeyboard::~SystemKeyboard()
{
	for(auto& KVP : m_Keyboards)
	{
		delete KVP.second;
	}
}

void SystemKeyboard::ClearInput()
{
	m_Keyboards.clear();
}

void SystemKeyboard::InputEvent(const RAWINPUT& eventData)
{
	HANDLE MapKey = eventData.header.hDevice;
	if (m_Keyboards.find(MapKey) == m_Keyboards.end())
	{
		// New device
		Buttons* ButtonSet = new Buttons();
		memset(ButtonSet->Button, 0, sizeof(ButtonSet->Button));
		m_Keyboards.insert({ MapKey, ButtonSet });
	}
	
	int VK = GetVirtualKey(eventData.data.keyboard);
	if (eventData.data.keyboard.Flags & RI_KEY_BREAK)
	{
		m_Keyboards[MapKey]->Button[VK] &= ~0x01;
	}
	else
	{
		m_Keyboards[MapKey]->Button[VK] = 0x03;
	}
}


float SystemKeyboard::GetValue(Key key) const
{
	int Index = static_cast<int>(key);
	if (Index < 255)
	{
		for (auto& KVP : m_Keyboards)
		{
			if (KVP.second->Button[Index] != 0)
			{
				return 1.0f;
			}
		}
	}

	return 0.0f;
}

void SystemKeyboard::DeviceRemoved(HANDLE handle)
{
	auto KVP = m_Keyboards.find(handle);
	if (KVP != m_Keyboards.end())
	{
		memset(KVP->second->Button, 0, sizeof(KVP->second->Button));
	}
}

//-- Private Methods --//
//=====================//

void SystemKeyboard::Update()
{
	for (auto& KVP : m_Keyboards)
	{
		for (int i = 0; i < 256; ++i)
		{
			KVP.second->Button[i] &= ~0x02;
		}
	}
}
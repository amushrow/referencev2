//=============================================================//
//                                                             //
//                         SKEngineV2                          //
//   -------------------------------------------------------   //
//  Copyright � 2013 - 2014 Anthony Mushrow                    //
//  All rights reserved                                        //
//                                                             //
//  This code is licensed under The Code Project Open License  //
//  (CPOL) See http://www.codeproject.com/info/cpol10.aspx     //
//  for more information.                                      //
//                                                             //
//=============================================================//

//--- Include ---//
//===============//
#include <SKInput/DeviceManager.h>

using namespace SK::Input;

//-- Declarations --//
//==================//

//-- Public Variables --//
//======================//

//-- Private Variables --//
//=======================//

//-- Public Methods --//
//====================//

DeviceIterator::DeviceIterator(const std::vector<Device*>& devices, bool end)
: m_Devices(devices)
, m_It(end ? devices.end() : devices.begin())
{

}

DeviceIterator::DeviceIterator(const DeviceIterator& rhs)
: m_Devices(rhs.m_Devices)
, m_It(rhs.m_It)
{

}

bool DeviceIterator::operator == (const DeviceIterator& other)
{
	return m_It == other.m_It;
}

bool DeviceIterator::operator != (const DeviceIterator& other)
{
	return m_It != other.m_It;
}

DeviceIterator DeviceIterator::operator++(int)
{
	DeviceIterator Tmp(*this);
	this->operator++();
	return Tmp;
}

DeviceIterator DeviceIterator::operator--(int)
{
	DeviceIterator Tmp(*this);
	this->operator--();
	return Tmp;
}

DeviceIterator& DeviceIterator::operator++()
{
	++m_It;
	return *this;
}

DeviceIterator& DeviceIterator::operator--()
{
	--m_It;
	return *this;
}

Device& DeviceIterator::operator*()
{
	return **m_It;
}

const Device& DeviceIterator::operator*() const
{
	return **m_It;
}

void DeviceManager::AddDevice(Device* newDevice)
{
	bool DeviceExists = false;
	for (auto ExistingDevice : m_Devices)
	{
		if (ExistingDevice->Guid() == newDevice->Guid() &&
			ExistingDevice->DeviceInfo() == newDevice->DeviceInfo())
		{
			DeviceExists = true;
			break;
		}
	}

	if (!DeviceExists)
	{
		newDevice->SetId(static_cast<unsigned int>(m_Devices.size()));
		m_Devices.push_back(newDevice);

		switch (newDevice->Type())
		{
		case Type::Keyboard: m_Keyboards.push_back(newDevice); break;
		case Type::Mouse:    m_Mice.push_back(newDevice); break;
		case Type::Joypad:   m_Joypads.push_back(newDevice); break;
		case Type::Wheel:    m_Wheels.push_back(newDevice); break;
		case Type::Joystick: m_Joysticks.push_back(newDevice); break;
		}

		// Register for events
		using std::placeholders::_1;
		using std::placeholders::_2;
		newDevice->ButtonUp.Register(m_EventKey, std::bind(&DeviceManager::ButtonUpHandler, this, _1, _2));
		newDevice->ButtonDown.Register(m_EventKey, std::bind(&DeviceManager::ButtonDownHandler, this, _1, _2));
		newDevice->AxisChanged.Register(m_EventKey, std::bind(&DeviceManager::AxisChangedHandler, this, _1, _2));
	}
	else
	{
		delete newDevice;
	}
}

//-- Private Methods --//
//=====================//

DeviceManager::DeviceManager()
: DeviceList(m_Devices)
, ButtonUp(m_ButtonUp)
, ButtonDown(m_ButtonDown)
, AxisChanged(m_AxisChanged)
, m_EventKey(GetEventKey())
{
	PlatformInit();
}

void DeviceManager::ButtonUpHandler(void* sender, KeyEventArgs& e)
{
	m_ButtonUp.OnEvent(sender, e);
}

void DeviceManager::ButtonDownHandler(void* sender, KeyEventArgs& e)
{
	m_ButtonDown.OnEvent(sender, e);
}

void DeviceManager::AxisChangedHandler(void* sender, KeyEventArgs& e)
{
	m_AxisChanged.OnEvent(sender, e);
}